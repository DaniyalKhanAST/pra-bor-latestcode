// remap jQuery to $
(function($){


/* trigger when page is ready */
$(document).ready(function() {	

	$('#dataSearchForm').on('submit', function(e) {
		
		var theSearch = $('#dataSearch').val();
		var theAction = $(this).attr('action');

		theAction += theSearch;

		window.location = theAction;

		e.preventDefault();

	});


	// "Filter" links
	$('#categoryLinks').on('change', function() {
		var theURL = $(this).val();
		window.location = theURL;
	});

});


/* optional triggers

$(window).load(function() {
	
});

$(window).resize(function() {
	
});

*/


})(window.jQuery);