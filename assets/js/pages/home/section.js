(function(a) {
    a(document).ready(function() {
        var g = a("#backToTop"),
            f = a(".drop-down"),
            e = a(window).width(),
            k = !1;
        a("section header, section .content-container").addClass("hidden");
        a(".toggle-menu, #overlay").on("click", function() {
            a("body").hasClass("showMenu") ? (a("body, html, #main-menu").removeClass("showMenu"), a(".toggle-menu").removeClass("active")) : (a("body, html, #main-menu").addClass("showMenu"), a(".toggle-menu").addClass("active"));
            a("#main-menu > ul > li").hasClass("open") && (a("li").removeClass("open"),
                a(".drop-down").slideUp(), a("nav#main-menu").removeClass("open-ie"))
        });
        a("#main-menu > ul > li:not(#nav-home) > a").on("click", function(b) {
            a("#socialmedialist").removeClass("open");

            if(typeof(srch) != "undefined" ) {
                0 == srch.kgb.active && srch.mod.reset();
            }
            a(this).parent().hasClass("open") ? (a(this).next(".drop-down").slideUp(240), a(this).parent().removeClass("open"), a("nav#main-menu").removeClass("open-ie")) : (a("#main-menu > ul li .drop-down").slideUp(240), 769 <= e && a("nav#main-menu").addClass("open-ie"), 768 < e && a("#main-menu > ul > li:not(#nav-home)").hasClass("open") ?
                a(this).next(".drop-down").delay(420).slideDown() : a(this).next(".drop-down").slideDown(), a("#main-menu > ul li").removeClass("open"), a(this).parent().addClass("open"));
            b.preventDefault()
        });
        a(window).on("resize load", function() {
            e = a(window).width();
            height = a(window).height();
            if (768 >= e) a("#searchField").on("focus", function(a) {
                k = !0
            });
            768 < e && (a("body, html").removeClass("showMenu"), a("#main-menu > ul li .drop-down").slideUp(), a("#main-menu > ul li").removeClass("open"), a("nav#main-menu").removeClass("open-ie"));
            a("#goToCity").on("click", function() {
                a(this).fadeOut("slow");
                a("html, body").animate({
                    scrollTop: a("#locals").offset().top
                }, 1200)
            });
            a(".section-nav a").on("click", function(d) {
                d.preventDefault();
                d = a(this).attr("href");
                a("html, body").animate({
                    scrollTop: a(d).offset().top
                }, 600);
                a(".section-nav a").removeClass("active");
                a(this).addClass("active")
            });
            var b, h = 0;
            a(window).scroll(function() {
                b && (clearTimeout(b), b = null);
                b = setTimeout(scrollHandler, 120)
            });
             scrollHandler = function() {
                 var d = a("body, html");
                      b = a(window).scrollTop();
                 a("#main-menu > ul > li:not(#nav-home)").hasClass("open") && 0 == k && (a("li").removeClass("open"), a(".drop-down").slideUp(), a("nav#main-menu").removeClass("open-ie"));
                 a("#socialmedialist").removeClass("open");
                 550< a(window).scrollTop() && (a("#settings, #socialmedialist").removeClass("open"), a("body > header nav").removeClass("settingsOpen"));
                 400 > b && a(".section-nav a").removeClass("active");
                 320 < b ? a("#main-header").addClass("toTop") : a("#main-header").removeClass("toTop");
                 400 < b ? g.addClass("show") : g.removeClass("show");
                 100 > b && a(".section-nav a.search").addClass("active");
                 a("section.scroll").each(function() {
                     var c = a(this).offset().top,
                         f = a(this).attr("id");
                     $viewHeight = a(this).height();
                     b + 128 > c && (a(this).addClass("scrolled"), a(this).children("header, .content-container").removeClass("hidden"));
                     var e;
                        e = 700 <= height ? 640 : 380;
                     b > h && b > c - e && b <= c ? (d.animate({
                         scrollTop: c
                     }, 500), a(".section-nav a").removeClass("active"), a(".section-nav a." + f).addClass("active")) : b < h && b < c + e && b >= c && (d.animate({
                         scrollTop: c
                     }, 500), a(".section-nav a").removeClass("active"),
                         a(".section-nav a." + f).addClass("active"))
                 });
                 h = b
             }
        });
        a("#backToTop").click(function(b) {
            a("body,html").animate({
                scrollTop: 0
            }, 1200);
            a(".section-nav a").removeClass("active")
        });
        a("#socialShare").click(function(b) {
            a("#socialmedialist").toggleClass("open");
            b.preventDefault()
        });
        var c = localStorage.getItem("settings");
        null == c && (localStorage.setItem("settings", "default"), c = "default");
        "highContrast" == c ? a("html").addClass("highContrast") : "textOnly" == c && a("html").addClass("textOnly");
        a("a#toggleSettings, a#toggleLocation").click(function(b) {
            var c =
                    a("body > header nav"),
                d = a("#settings");
            "Location" != a(this).attr("title") || d.hasClass("open") || d.hasClass("settings") ? "Location" == a(this).attr("title") && d.hasClass("open") && d.hasClass("settings") ? d.removeClass("settings").addClass("location") : "Location" == a(this).attr("title") && d.hasClass("open") && d.hasClass("location") ? (console.log("settings is open with the class of location"), d.removeClass("open location"), c.removeClass("settingsOpen")) : "Settings" != a(this).attr("title") || d.hasClass("open") || d.hasClass("location") ?
                "Settings" == a(this).attr("title") && d.hasClass("open") && d.hasClass("location") ? d.removeClass("location").addClass("settings") : "Settings" == a(this).attr("title") && d.hasClass("open") && d.hasClass("settings") && (d.removeClass("open settings"), c.removeClass("settingsOpen")) : (d.addClass("open settings"), c.addClass("settingsOpen")) : (d.addClass("location open"), c.addClass("settingsOpen"));
            b.preventDefault()
        });
        a(".settings a").on("click", function(b) {
            var c = a(this).attr("class");
            a("html").removeClass("textOnly highContrast");
            a("html").addClass(c);
            localStorage.setItem("settings", c);
            a("#settings a").removeClass("active");
            a(this).addClass("active");
            b.preventDefault()
        });
        c = localStorage.getItem("settings");
        if ("undefined" == c || "default" == c) a("#fontSizer .fontsize").removeClass("active"), a("#fontSizer .fontsize.default").addClass("active"), localStorage.setItem("settings", "default"), c = "default";
        "smallFont" == c ? (a("#fontSizer .fontsize").removeClass("active"), a("#fontSizer .fontsize.small").addClass("active"), a("html").addClass("smallFont")) :
        "largeFont" == c && (a("#fontSizer .fontsize").removeClass("active"), a("#fontSizer .fontsize.large").addClass("active"), a("html").addClass("largeFont"));
        a("#fontSizer .fontsize").on("click", function(b) {
            var c = a(this).data("size");
            a("#fontSizer .fontsize").removeClass("active");
            a(this).addClass("active");
            a("html").removeClass("largeFont smallFont");
            a("html").addClass(c);
            localStorage.setItem("settings", c);
            b.preventDefault()
        });
        a(document).on("click", function(b) {
            !a(b.target).closest("#main-header").length &&
            f.parent("li").hasClass("open") && (f.parent("li").removeClass("open"), f.slideUp(), a("nav#main-menu").removeClass("open-ie"))
        });
        a(".transparency-donut path").on("click", function(b) {
            var c = a(this).attr("class");
            b = a(this).attr("fill");
            c = a("li." + c);
            a(".transItem").removeClass("active").css("background", "");
            c.addClass("active").css("background", b)
        })
    })
})(window.jQuery);

function getURLpost(a) {
    for (var g = window.location.search.substring(1).split("&"), f = 0; f < g.length; f++) {
        var e = g[f].split("=");
        if (e[0] == a) return e[1]
    }
}
$(function() {
    var a = $("html title").text(),
        g = encodeURIComponent(a);
    $("#socialmedialist a").each(function() {
        var a = $(this).attr("href").replace("PAGE_TITLE", g, "g");
        $(this).attr("href", a)
    })
});



