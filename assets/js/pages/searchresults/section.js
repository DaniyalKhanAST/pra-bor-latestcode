// remap jQuery to $
(function($){

function getURLkeyword(term) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');

	for (var i = 0; i < sURLVariables.length; i++) {
		var kword = sURLVariables[i].split('=');
		if (kword[0] == term) return kword[1];
	}
}

/* trigger when page is ready */
$(document).ready(function (){

	


	// function icanhazmap(type) {
	// 	$('.locationAwareMap').data('markertype', type);
	// 	callLocationAwareMarkers();
	// 	$('.locationAwareMap').slideDown();
	// }

	//Get Type from URL
	var onlineservicesresults = $('.mdiSearchResults');
	var searchKeyword = getURLkeyword('q');
	//console.log(searchKeyword);
	var searchKeyFix = searchKeyword.replace(/\+/g,' ');
	//console.log(searchKeyFix + ' searchkeyfix');
	
	

	if (searchKeyword == 'hear+from+Governor+Herbert') {
		$('.specialResults').removeClass('hidden');
		$('ul.herbert').removeClass('hidden');
		searchKeyword = 'herbert';
		searchKeyFix = 'herbert';
		//window.location = 'http://www.utah.gov/governor/sots/';
	}
	if (searchKeyword == 'experience+new+tech+from+Utah') {
		window.location = '/digital/';
	}

	if (searchKeyword == 'know+about+new+or+changing+laws') {
		$('.specialResults').removeClass('hidden');
		$('ul.le').removeClass('hidden');
		searchKeyword = 'legislation';	
		searchKeyFix = 'legislation';
	}
	// 
	// 
	// if(searchKeyword == 'library' || searchKeyword == 'libraries' || searchKeyword == 'book') icanhazmap('3');
	// if(searchKeyword == 'school' || searchKeyword == 'schools' || searchKeyword == 'education') icanhazmap('2');
	// if(searchKeyword == 'jobs' || searchKeyword == 'job' || searchKeyword == 'career' || searchKeyword == 'employment') icanhazmap('10');
	// if(searchKeyword == 'parks' || searchKeyword == 'park') icanhazmap('4');
	// if(searchKeyword == 'court' || searchKeyword == 'courts' || searchKeyword == 'courthouse') icanhazmap('11');
	// if(searchKeyword == 'cemetery' || searchKeyword == 'cemeteries') icanhazmap('8');
	// if(searchKeyword == 'golf' || searchKeyword == 'club') icanhazmap('9');
	// if(searchKeyword == 'dmv' || searchKeyword == 'drivers' || searchKeyword == 'driverlicense' || searchKeyword == 'driverslicense') icanhazmap('12');
	// if(searchKeyword == 'beer' || searchKeyword == 'alcohol' || searchKeyword == 'liquor' || searchKeyword == 'booze') icanhazmap('13');
	

	//GET SERVICES RESULTS
	onlineservicesresults.data('keyword', searchKeyFix);
	onlineservicesresults.data('type', '1,2,3,5,63,6,48,65');

	// RUN MDI
	console.log(searchKeyword);
	mdi.initialize();
	onlineservicesresults.slideDown();


	//CSE
	(function() {
		//var cx = '011595275022494868614:jtle0ifunuw'; //TEST
		var cx = '005946968176299016736:b3muwlbpdj8'; //CURRENT ID
		var gcse = document.createElement('script');
		gcse.type = 'text/javascript';
		gcse.async = true;
		gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
		    '//www.google.com/cse/cse.js?cx=' + cx;
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(gcse, s);
	})();



});


/* optional triggers

$(window).load(function() {
	
});

$(window).resize(function() {
	
});

*/


})(window.jQuery);