<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Access to Credit &amp; Vertical Market Linkages <span style="float:right">ایکسیس ٹو کریڈٹ اینڈ ورٹیکل مارکیٹ لنکجز</span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <p>Despite chiefly contributing in Agri GDP, livestock farmers could barely use less than 7% of the Agri credit annually, showing the strategic loss this sector is bearing in the form of wastage of potential as 89% livestock farmers are landless and at subsistence level through ages. Moreover, price capping on raw milk and fresh meat, both traditional items of daily consumption in the Sub-Continent, has devastated theprimary production systems, which are no more willing to provide “subsidized” milk and meat out of their own pockets to the consumers, who are bearing the ultimate brunt of this injustice as “genuine” livestock products are fast becoming a thing of past due to above said factors coupled with absence of vertical marker linkages, a gap filled by the middle men.</p>
            <p><span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
                    مویشی پال کسان زراعت کی جی-ڈی-پی میں اپنا بنیادی کردار ادا کرنے کے باوجود بھی شعبہ زراعت کے سالانہ بجٹ سے ٧% سےبھی کم مستفید ہو پاتےہیں جسکی وجہ سے اس شعبے میں بہت نقصان اٹھانا پڑ رہا ہے اسکی بنیادی وجہ ٨٩% مویشی پال حضرات بے زمین ہیں یا بہت کم زمین کےمالک ہیں برصغیرمیںروزانہاستعمالہونےوالی
اشیاء جیساکہ خام دودھاورتازہگوشتکیبنیادیقیمتنہہونےکیوجہسےاسنے پرائمری پروڈکشن سسٹم کو تباہ کردیاہے پروڈوسرز کو صارفینکےلیے دودھ اورگوشتکیفراہمیکویقینیبنانانہممکنہوگیاہے.اس کی وجہ سے مویشیوں سے حاصل ہونے والی مصنوعات کا حصول ماضی کی بات لگتا ہے. مندرجہ بالا عوامل کے ساتھ ساتھ مارکیٹ کے راتوں میں کمی اورمڈل مین کے ملوثہ ہونے کی وجہ سے رہی سہی کسربھی پوری ہوگی ہے


                </span></p>

            <img src="<?php echo base_url() ?>assets/images/home/access_to_credit.jpg">
            <p style="font-style: italic;margin-top: 0px;">Developed by the Author on the basis of Economic Survey of Pakistan, 2014-15 & State Bank of Pakistan Report 2010</p>

        </div><!-- /.container -->
    </section>


</main>