<style>
    .mIndex a img{
        display: block;
    }
</style>
<main id="main" style="margin-top: 130px;">


    <header>

        <section>
            <!--[if gte IE 9]><!-->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px"
                 enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="7.842" r="2.813"/>
                        <rect x="9.248" y="5.667" fill="#676868" width="13.599" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="16" r="2.813"/>
                        <rect x="9.248" y="13.773" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="24.158" r="2.813"/>
                        <rect x="9.248" y="21.984" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                </g>
            </svg>

            <!--<![endif]-->

            <h1>Livestock and Access to Markets Project (LAMP)</h1>

            <p class="agency-search">
                <input type="text" class="search-agencies" title="Search" placeholder="Search..."/>
                <button type="button" class="clear-search">Clear Search</button>
            </p>

        </section>

    </header>






    <section class="" style="margin-bottom: 1px;">

        <h2 class="bukatutup1">   <a href="#" class="clickme">Production Support <span style="float: right;"></span></a></h2>

        <div class="container " id="target1" style="display: none">
            <div class="mIndex " data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <p> This component aims to increase livestock production and productivity in target areas by,
                    <ol class="container " type="i">
                        <li> Strengthening the capacity of targeted communities - including vulnerable women - private sector

                            players, and of the Livestock and Dairy Development Department, and </li>

                        <li> Supporting the construction of small-scale, community-managed infrastructure schemes aimed at

                            enhancing livestock productivity. </li>

                    </ol>
                <br>
                The component would facilitate the engagement of the private sector for the provision of technical assistance

                and capacity building activities, and the provision of a number of other services. This component includes

                following four sub-components:
                <br>
                <span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
                    اس کمپوننٹ کا مقصد محصوص علاقوں میں لائیوسٹاک کی پیداوار اور پیداواری صلاحیت بڑھانا ہے۔
                    <br>
                <div style="margin-right:20px;">
                    ۔ مخصوص کمپنیز کی صلاحیتوں کو مضبوط کرنا جن میں مجبور خواتین، نجی ادارے اور محکمہ لائیوسٹاک ڈیری ڈویلیپمنٹ شامل ہے۔
                    <br>
                    ۔ چھوٹے پیمانے پر کمیونٹی کے منظم ڈھانچے کے منصوبوں کی حمایت کرنا جن کا مقصد جانوروں کی پیداری صلاحیت بڑھاناہے۔
                </div>
                </span>
                </p>
                <div class="clearfix"></div>
                <br>
    <p >

                <div class="clearfix"></div>
                <span style="float:right;text-align:right;font-size:18px;">
                    یہ کمپوننٹ تکنیتی معاونت اور کیپیسیٹی بلڈنگ سرگرمیوں اور بہت سی مزید خدمات کے لیے جنی اداروں کی شمولیت کو ممکن بناے گا۔
اس کمپوننٹ کے ذیلی چار کمپوننٹ ہیں۔

                </span>
                    <ul class="container MDI-Results default">
                        <li ><a href="<?php echo base_url().'dashboard/community_capacity_building/';?>">Community Capacity Building (Community Organizations) <span style="float:right;">۔ کمیونٹی کی صلاحیتوں کی تعمیر</span></a></li>
                        <li ><a href="<?php echo base_url().'dashboard/community_infrastructure/';?>"> Community Infrastructure <span style="float:right;">۔ کمیونٹی انفراسٹرکچر/بنیادی ڈھانچہ</span></a></li>
                        <li ><a href="<?php echo base_url().'dashboard/capacity_building_of_private_sector/';?>">Capacity Building of Private Sector<span style="float:right;">۔ جنی اداروں کی صلاحیتوں کی تعمیر</span></a> </li>
                        <li  ><a href="<?php echo base_url().'dashboard/capacity_building_of_livestock_department';?>"> Capacity Building of L&amp;DD (Livestock and Dairy Development Department) <span style="float:right;">۔ محکمہ لائیوسٹاک اور ڈیری ڈویلپمنٹ کی تعمیر صلاحیتوں کی تعمیر</span></a></li>
                    </ul>
                </p>
                <div class="container">
                    <h3>Article Gallery</h3>

                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00168.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00168.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00182.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00182.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00187.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00187.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00198.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00198.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00204.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00204.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSCN3801.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSCN3801.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_103342910.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_103342910.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_103704090.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_103704090.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_103743622.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_103743622.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0001.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0001.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0029.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0029.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0030.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0030.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0031.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0031.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0033.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0033.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0215.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0215.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0222.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0222.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0229.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0229.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0231.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0231.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0234.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0234.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170129-WA0009.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170129-WA0009.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0023.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0023.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0033.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0033.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/SANY0029.jpg';?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/SANY0029.jpg';?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">Marketing Support <span style="float: right;"></span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex " data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <div class="container" style="text-align: justify;font-size: 20px">

                    <p>

                        <br>
                        This component aims to enhance access to markets for livestock farmers for sale of their produce (live animals and milk etc.). The component would facilitate the farmers in establishment of linkages with the private sector for the provision of inputs, capacity building activities and other services.
                        <br>
                        This component includes three sub-components:

                        <span style="float:right;text-align:right;font-size:18px;">

                            اس کمپوننٹ کا مقصد مویشی پال حضرات کی مارکیٹ تک رسائی کو مزید بڑھانا ہے تاکہ وہ اپنی مصنوعات (زندہ جانور، دودھ وغیرہ) بیچ سکیں۔ یہ کمپوننٹ مویشی پال حضرات کی جنی اداروں سے روابط استوار کرنے میں معاون ثابت ہو گا تاکہ وہ مویشی پال حضرات کی صلاحیتوں کی تعمیر اور دوسری خدمات کی فراہمی کو یقنینی بناسکیں۔
                        </span>
                        <span style="float:right;text-align:right;font-size:18px;">
                            اس کمپوننٹ کے مندرجہ ذیل تین ذیلی جزو ہیں۔
                        </span>
                        <div class="clearfix"></div>
                    <ul class="container">
                        <li><a href="<?php echo base_url().'dashboard/strengthening_farmer_organizations';?>">Strengthening Farmer Organizations <span style="float:right;">۔ مویشی پال حضرات کی تنظیموں کو مضبوطی دینا۔</span></a>  </li>
                        <li><a href="<?php echo base_url().'dashboard/market_infrastructure';?>"> Market Infrastructure <span style="float:right;">۔ مارکیٹ انفراسٹرکچر</span></a> </li>
                        <li><a href="<?php echo base_url().'dashboard/market_information_and_linkages';?>"> Market Information and Linkages. <span style="float:right;">۔ کارکیٹ کی معلومات اور روابط</span></a>  </li>
                        <li><a href="<?php echo base_url().'images/lamp/Banking_IFAD.docx';?>" target="_blank"> DIGITAL CREDIT LINE FOR RURAL POOR THROUGH VERTICAL MARKET LINKAGES <span style="float:right;">۔ دہی غریب کیلے ڈیجیٹل کریڈٹ لائن ورٹیکل مارکیٹ روابط تشکیل دینا۔ </span></a>  </li>

                    </ul>

                    <br>

                    </p>
                    <div class="container">
                        <h3>Article Gallery</h3>

                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC_0010m.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC_0010m.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC_0012m.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC_0012m.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSCN3690m.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSCN3690m.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_115843863m.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_115843863m.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_120004876m.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_120004876m.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_120136529m.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_120136529m.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>

                </div>
            </div><!-- /.container -->




                </div><!-- /.container -->






            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a href="#" class="clickme">Project Management<span style="float: right;"></span></a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex " data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <div class="container" style="text-align: justify;font-size: 20px">

                    <p> Under this Component a Project Coordination Unit (PCU) will be established and housed at Lahore. This component aims at supporting the project coordination and technical responsibilities of the Project Coordination Unit. In addition, a Special Financing Facility to support any activity that requires additional support and/or expanding any promising/well performing activity will be established under this component.
                    <div class="clearfix"></div>
                    <span style="float:right;text-align:right;font-size:18px;">
                        اس کمپوننٹ کے تحت پرجیکٹ کوآرڈینیشن یونٹ لاہور میں تشکیل دیا جائے گا۔ اس کمپوننٹ کا مقصد پرجیکٹ کوآرڈینیشن یونٹ تکنیکی ذمہ داریوں اور پروجیکٹ کوآرڈینیشن میں مدد فراہم کرنا ہے۔ اس کو ساتھ ساتھ اگر کسی بھی سرگرمی میں اضافہ مدد اور یا امیدافزاں وسعت یا اچھی کارکرگی کا مظاہر کرنا ہو تو اس کیلئے کخصوص قنائسنگ فیسیلٹی۔ اس کمپوننٹ کے تحت تشکیل دی جائے گی۔
                    </span>
                    <div class="clearfix"></div>


                    <ul class="container">
                        <li><a href="<?php echo base_url().'dashboard/component_objectives_and_indicators';?>">Component Objectives and Indicators<span style="float:right;">۔ کمپوننٹ کے مقاصد اور انڈیکیڑ</span></a> </li>
                        <li><a href="<?php echo base_url().'dashboard/component_activities_and_output';?>"> Component Activities and Output<span style="float:right;">۔ کمپوننٹ کی سرگرمیاں اور ممکنہ نتائج</span></a></li>
                    </ul>

                    <br>

                    </p>

                    <div class="container">
                        <h3>Article Gallery</h3>

                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0030p.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0030p.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0034p.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0034p.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0039p.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0039p.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="row">
                                <div class='list-group gallery'>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0045p.jpg';?>">
                                            <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0045p.jpg';?>" />
                                            <div class='text-right'>
                                                <small class='text-muted'></small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>





                </div><!-- /.container -->






            </div>
        </div><!-- /.container -->
    </section>



</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
    });
</script>