<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Market Infrastructure

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Market Infrastructure: </h3>
            <p> Parallel to development of a proper Cattle Markets Regulatory Framework, project will assist the Tehsil

                Municipal Administration (TMA or its successor) with technical and financial resources to upgrade the facilities of

                12 local livestock markets on the basis of a blue-print agreed between L&amp;DDD and Local Government
                <br>
                Department. Before funding can be provided, the PCU will ensure that the TMA

                <ol class="container" type="a">
                    <li> owns the land on which the market is to be established </li>
                    <li> Offers the land as its share of investment plus 50% share of civil works cost </li>
                    <li> operate and maintain the markets as per agreed SOP and </li>
                    <li> engage a Third Party to assess the performance of the market with respect to smallholder farmer‘s access and animal welfare. </li>
                </ol>
                The eligible infrastructure will include cattle/small ruminant sheds, watering facilities,

                loading/unloading ramps, washrooms, small office for management and veterinarian, weigh bridge/scales etc. It is expected that such improvements will result in the

                extension of the timing of market transaction from few hours in the morning to the full day; in better conditions

                for market users; improved welfare of livestock; increased opportunities for smallholder producers to access the

                market, etc.
                <br>
                With a view to encourage smallholders to undertake collective marketing of small ruminants for better

                bargaining power and enhanced incomes a production package will be provided to the groups which could

                include vaccinations, mineral supplements, weights and weighing scales, ramps, etc.
                <br>
                The Market infrastructure will also include 100 village based milk collection centres which will include 75 chillers

                of 500 liters and 25 of 1000 liters capacity and locally adapted technology such as the Qingqi milk chiller/cooling

                tanks which can transport milk in a cost effective manner, milk testing equipment and solar energy options for

                the cooling equipment. The provision of the chillers has been shown to have an immediate impact in terms of

                reduction in the spoilage of milk, increase in the price and improvement in the quality. The small ruminant

                marketing groups will be provided some basic equipment which could include weighing scales, small equipment,

                etc. The exact nature of the support to be provided will be discussed with the participating households. The size

                of the milk chiller will be as per the requirement of the beneficiaries based on the financial business model,

                keeping in view the number of animal in milk and the quantity of marketable milk available with individual

                household and the cluster.
                <br>


            </p>





        </div><!-- /.container -->
    </section>


</main>