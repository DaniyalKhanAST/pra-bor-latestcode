<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Tenders
            <div class="" style="float: right;">
                <form id="live-search" action="" class="styled" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" style="color: black" placeholder="Search for..." id="filter">

                        <span id="filter-count"></span>
                    </div>
                </form>
            </div>
        </h2>

        <div class="container">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="device" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach($tenders as $row){ ?>
                    <li itemscope="" itemtype="" class="item Blog local"><a itemprop="url"
                                                                            href="http://slcoarchives.wordpress.com/">
                            <h4 itemprop="name" class="title"> <?php echo $row->directorate_name ?></h4><span itemprop="description"
                                                                                                  class="description"><?php echo $row->name ?></span></a>
                    </li>
<?php } ?>
                </ul>
            </div>
        </div>
    </section>
</main>