<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Community Capacity Building
            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Community Capacity Building: </h3>
            <p> A Social Mobilization Partner (SMP) and implementing partner for Farmer Field School (FFS) will be

                engaged on competitive basis to implement this sub-component and its related community based activities

                under other sub-components. This sub-component will include community organization/mobilization, awareness

                sessions for the community on basic animal husbandry practices, organize Livestock Farmer Field Schools (LFFS)

                in dairy, small ruminant and poultry production, organize farmer field days, exchange visits and fairs and

                competitions. The broad based Village Organizations which generally comprise of a large number of households

                at the village level will be used as a forum to introduce the project, ensure village willingness to participate,

                identify participants in the farmer field schools, men and women for training as Community Livestock Extension

                Workers (CLEWs),identification of women recipients for the asset transfer, marketing groups for dairy and small

                ruminants, identify community infrastructure needs and contribution, ensure village participation in sessions

                organized to raise awareness about basic animal husbandry practices, vaccination campaigns, disseminate

                information about farmer fields days, livestock fairs and assist in facilitating the CLEWS and private sector and

                provide support for the project. The concept of socio economic uplift in the specific conditions of the target area

                will be implemented through social mobilization entities in the perspective of socio culture fabric. The social

                mobilization organizations will be selected on the basis of their experience and expertise of working in the area

                similar to the proposed target area of the project.
                <br>


            <dl class="container">
                <dt>Asset Building</dt>
                <dd>This sub-component will provide asset building support to vulnerable women who are
                    primarily responsible for the care of small ruminants and rural poultry. The project will distribute goat and poultry
                    packages to vulnerable women to help enhancing their food security, provide them a source of income and
                    enable them to enhance their decision-making power. The selected women will be given 2 goats and a buck
                    (6250) for a designated number or poultry packages for 6250 families, supported with proper training. Fifteen
                    thousand women will be assisted under this sub-component. The activity will be linked with the initiatives of the
                    L&amp;DD Department in the asset building and service delivery in vogue.</dd>
            </dl>

            <br>


            </p>





        </div><!-- /.container -->
    </section>


</main>