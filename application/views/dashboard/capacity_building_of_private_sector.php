<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Capacity Building of Private Sector

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Capacity Building of Private Sector: </h3>

            <p> The project will build the capacity of the private sector to provide artificial Insemination, animal health care,

                vaccination and other livestock services. The services of a Technical Service Provider like the University of

                Veterinary and Animal Sciences (UVAS), Faisalabad Agriculture University, Bahaud-din Zakaria University, etc. will

                be contracted by the project for assessment of service provider’s capacity, certification, refresher training and

                any additional technical training required. Those who qualify and have the level of knowledge and skill required

                will be registered with the DLO and the community will also be advised to use only those who have the requisite
                expertise. Through support from the project, those who meet the basic educational requirements will also be

                provided the opportunity to complete certificate and Livestock Assistant Diploma level courses. The project will

                also provide an opportunity to final year DVM students or new graduates to be placed in the project districts as

                interns. The project will also organize field days, livestock fairs and competition shows and exchange visits where

                smallholders and private sector buyers and sellers could interact and establish direct links and learn from each

                other.
                <br>
                In view of the limited capacity of rural women to access veterinary and extension services, the project will train

                850Community Livestock Extension Workers (CLEWs) of which 70% will be women (the target is one female

                extension worker per village) to ensure social inclusion in accessing good quality service provision in the project

                area. Some CLEWs (30%) will be male in order to enhance access to AI services, facilitate access to markets and

                contacts with suppliers in urban centres. A Technical Service Provider will be contracted by the Project to design

                and implement a training program for CLEWs. They will be provided a starter kit with basic tools and medicines

                for de-worming, vaccination and medicines which they will replenish from sales. The CLEWs will be required to

                conduct house to house visits, facilitate the access to services to DVMs and VAs services, maintain vaccination

                records, conduct follow up visits and provide animal health and production extension advice to both women and

                men farmers. The CLEWs will be linked with private sector service providers and quality suppliers as well as the

                DLO office for referral and back-up support and further supplies. The CLEWs will be paid a small stipend during

                the project period for their support in coordinating and monitoring project activities. They will be expected to

                recover the cost of medicines and vaccines and charge a small fee for the services they supply to the households

                from the outset thereby generating an income for them.
                <br>
                The project will also include funds to enable the technical training institution to undertake practical field trials on

                farmer fields and demonstrations of animal forage resilient to climate change and silage/hay making with

                consultation and by involving Punjab Agriculture Department. The Technical institution will also assess the

                potential for improving animal feeding through use of locally available feed resources. The Project will also

                finance the development and production of vaccines for Peste des Petits Ruminants (PPR) and Contagious Caprine

                Pleuroneumonia (CCP) for small ruminants and making these vaccines available for use in the project area. The

                selection of the private sector institutions for capacity building will be linked with the work in the area similar to

                target area and on the basis of socially acceptable approach based on need assessment.

                <br>

            </p>





        </div><!-- /.container -->
    </section>


</main>