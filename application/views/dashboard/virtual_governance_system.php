<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            9211 Virtual Governance System <span style="float: right;margin-right: 5px;direction:rtl">9211 ورچوئل گورنس کا نظام</span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <p>9211 Virtual Governance System was created through a unique ICT based dynamic communication

                platform that uses the simple GSM network and chiefly Unstructured Supplementary Service Data (USSD)

                to transfer huge amount of data sets right from the field through simple feature cellular phones already in

                use of 7000 plus field staff of Livestock Department Punjab, without using any apps, computers, androids,

                smart devices or internet for data entry, resulting in a low-cost but high-end database.</p>
            <img src="<?php echo base_url() ?>assets/images/home/vg1.jpg">
               <p>
                Virtual Governance operates on the concept of government being more virtual in nature than physical

                in existence and just like self-serving technologies e.g. ATM machines, public services can be made

                available 24/7 instead of 9-5. With the help of this system the Punjab Government generated good deal

                of empirical evidence that helped formulate the first ever Livestock &amp; Dairy Development Policy of the

                Punjab in January, 2016.23.
                <br>
                Each service that is delivered by the Department is not only mapped on the 9211 System but a system

                generated Urdu SMS also land on the mobile of the farmer for confirmation of delivered service (for

                instant social audit), backed by 24/ 7 Call Centre accessible through a toll free number. The System makes

                7000 plus field staff of Livestock Department Punjab the largest data entry machine in the world under

                open skies. The data entry cost is 340 times less as compared to a conventional computer based system

                with no possibility of hacking as DB is accessed through VPN tunnels only. In the system all of 25,892

                villages of the Punjab are codified. The system has created a live database of 6 million livestock farmers

                and their 65 million plus livestock. All services delivered by the Department like 48 filed services, 50 OPD

                services; 296 different types of tests, Breeding services like Artificial Insemination etc. are fully digitized

                with 360 degree performance view along specie, service delivery, farmer, village, target area, disease,

                intervention, employees’ performance and public access through multiple channels like web-interface or

                through business data sets by using simple SMS or through retrieval of information by using USSD by the

                field staff, making it the most versatile and dynamic solution ever customized for livestock sectors around

                the world. 9211 is a short code, which is mapped on all the five telecom companies’ networks, working in

                Pakistan and practically accessible from across the world by using any sort of mobile telephone.
                <br>
                Now Government of the Punjab is initiating branchless financial digital access to livestock farmers by

                using 9211 System so that financial inclusion could be facilitated as 73% potential banking market in

                Pakistan does not have access to banking products. It will have direct bearing on gender development as

                well as livestock is intrinsically a part of rural women’s lives that spend 59% of their daily time in livestock

                related activities.
            </p>
            <p>The system helped Provincial Disaster Management Authority (PDMA) to transmit flood related alerts

                to thousands of rural families along the flood belts of different rivers. Recently 4.8 million targeted robo

                calls are another addition to the already handsome basket of multiple communication channels, including

                website of the Department and social media.</p>
            <img src="<?php echo base_url() ?>assets/images/home/vggraph.jpg">
            <p>
                The same has not only helped effect the requisite paradigm shift but has completely transformed the role of

                Department into that of “Livestock Asset Manager” of the poor farmers by bringing them the benefits long

                overdue. A newly created highly mobile platform of services’ delivery in the form of 5000 plus customized

                medical kit mounted motor cycles and 251 mobile veterinary hospitals linked through 9211 Virtual

                Governance System is delivering 364 services at the doorstep of livestock farmers with additional benefit of

                creating a profile of the farmer that could then be used to determine credit worthiness of the landless

                livestock farmer.
                <br>
                It has put the control of service delivery initiation into hands of the public by leaving the ICT based autonomic

                system mapping the quality and quantity of services delivered with inbuilt escalation of alerts, if there occurs

                any compromise along these parameters. It has created much needed breathing space for the HR of the

                Department to focus on the diversification of their engagement with the public, tremendously enlarging the

                opportunity to earn public trust for furthering the sustainable performance optimization. The Punjab

                Government has succeeded to complete multiple rounds of free mass vaccination of animals in the Punjab

                with preparation of disease profiling up to village level besides erection of disease surveillance system with

                the help of 9211 System
            </p>
            <img src="<?php echo base_url() ?>assets/images/home/vg3.jpg">
            <p>
                <span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
۹۲۱۱ورچوئل گورننس سسٹم اپنے طور کا ایک منفرد آئی سی ٹی سسٹم ہے جو کہ سادہ موبائل نیٹ ورک(جی ایس ایم) اور یوایس ایس ڈی ٹیکنالوجی کو استعمال کرتے ہوئے بڑی مقدار میں ڈیٹا یا انفارمیشن کو فیلڈ سے محکمہ پنجاب کے 7000سے زائد ملازمین کے سادہ موبائل سے اٹھاتا ہے۔ یہ سسٹم موبائل ایپلیکیشن، کمپیوٹر، انڈروئیڈ، سمارٹ ڈیوائیسز اور انٹرنیٹ کا استعمال کیے بغیر ڈیٹا انٹر کرنے کی صہولت فراہم کرتا ہے نتیجتاً یہ ایک کم قیمت اور بہترین کوالٹی کا ڈیٹا فراہم کرتا ہے۔
                    <br>
ورچوئل گورننس کا تصور ایسا ہے کہ جس میں گورنمنٹ زیادہ مجازی طور پر موجود ہو ناکہ جسمانی طور پر اور جس طرح خود کار ٹیکنالوگی جس میں اے ٹی ایم مشینیں شامل ہیں ہم عوام کی خرمت کو 24/7 دستیاب ہے۔ اس سسٹم کی مدد سے پنجاب گورنمنٹ نے بہت اچھی بنیادوں پر ثبوت اکٹھے کیے ہیں جس کی مدد سے پہلی بار لائیوسٹاک اینڈ ڈیری ڈیویلپمنٹ پالیسی پنجاب 2016 تشکیل دی گئی ہے۔
                    <br>
اس سسٹم کے اندرپنجاب کے تمام 25892 دیہاتوں کو کوڈ لگے ہوئے ہیں اس سسٹم نے 7.4 ملین سے زائد کسانوں اور ان کے 65 ملین سے زائد جانوروں کا لائیوسٹاک ڈیٹا بیس بنایا ہے۔ محکمہ کی جانب سے مہیا کی جانے والی تمام سروسز جیسا کہ 48 فیلڈ سروسز، 50 او پی ڈی سروسز، 296 مختلف اقسام کے ٹیسٹ، مصنوعی تخم ریزی مکمل طور پر ڈیجیٹل ہیں جس کو ہم جانور کی نسل سروس کی فراہمی، کسان دیہات مخصوص علاقہ سے ملازمین کی کارکردگی کے لحاظ سے مکمل طور پر دیکھ سکتے ہیں۔ یہ معلومات مسیج کے ذریعے یا یو ایس ایس ڈی کے ذریعے فیلڈ سٹاف بھیجتا ہے۔ جو کہ اس نظام کو لائیوسٹاک سے متعلق پوری دنیا میں منفرد بناتا ہے۔ 9211 ایک مختصر کوڈ ہے جو کہ پاکستان کے اندر کام کرنے والی پانچ ٹیلیکام کمپنیوں کے ذریعے کام کرسکتا ہے اور عملی طور پی پوری دنیا کے اندر اس کو چلایا جا ستکا ہے۔ اب پنجاب حکومت 9211 سسٹم کو استعمال کرکے لائیوسٹاک کسانوں کے لئے معاسی ڈیجیٹل رسائی کا آغاز کررہی ہے کیونکہ پاکستان کی 73 فیصد بینکنگ مارکیٹوں کی بینکنگ مصنوعات تک رسائی نہیں ہے۔ اس عمل کا جینز ڈیویلپمنٹ اور لائیوسٹاک کی ترقی پر اثر ہو گا کیونکہ دیہاتی خواتین کے روزانیہ کے وقت کا 59 فیصد حصہ لائیوسٹاک سے متعلق کاموں میں سرف ہوتا ہے۔ اس نظام کو استعمال کر کے پراونشل ڈیزاسٹر اتھارٹی نے دریائوں کے کناروں پر رہنے والے لوگوں کو سیلاب سے قبل اووقت اطلاع دے دی تھی۔ حالیہ دور میں محکمہ کے مواصلاتی ذرائع جس میں ڈیپارٹمنٹ کی ویب سائٹ اور سوشل میڈیا میں 48 لاکھ ٹارگٹڈ روبو کالز کا اضافہ ہو ہے۔ اس نظٓام نے نہ صرف پیراڈائم شفٹ میں کردار ادا کیا ہے بلکہ غریب کسانوں کو رہنے والے فائدے دے کر محکمہ کے لائیوسٹاک اسٹیٹ منیجر کے کردار کو ظٓا ہر کیا ہے۔
                    <br>
ایک نیا سروس ڈیویلیوری کا متحرک نظٓم بنایا گیا ہے جس میں پانچ ہزار سے زائد میڈیکل کٹ مائینٹڈ موٹر سائیکل اور 251 سے زائد موبائل ویٹرنیری اسپتال شامل ہیں۔ اور یہ نظام 9211 روچوئل کورننس سسٹم کے ذریعہ جڑا ہوا ہے کسانوں کو انکے گھر کی دہلیز پر 364 سروسز مہیا کرتا ہے۔ اسطرح لائیوسٹاک کسانوں کا پروفائل بن جاتا ہے جو کہ بے زمین لائیوسٹاک کسانوں کا اثاثہ اس سسٹم نے سروس کی فراہمی کےآغاز کا اختیار لوگوں کے ہاتھوں میں دے دیا ہے۔ آئی سی ڈی بیسڈ سسٹم مہیا کی گئی سروس کی کوالٹی اور مقدار کو دیکھتا ہے۔ اس سسٹم نے شعبہ لائیوسٹاک کے انسانی ذرائع کو ایک پرسکوں جگہ فراہم کی ہے۔ تاکہ وہ مکمل توجہ کے ساتھ لوگوں سے میل جول پر کام کرسکیں۔ اس سے بھرپور کارکردگی کی پائیداری کے آگے بڑھاتے ہوئے لوگوں کا اعتبار حاصل کرنے کے انتہائی بڑے مواقع پیدا ہوئے ہیں۔ اس سسٹم کی مدد سے پنجاب حکومت نے پورے پنجاب میں بڑے پیمانے پر جانوروں کی ویکسینیشن کی ہے۔ جس میں بیماری کی تعارفی وضاحت کے ساتھ ساتھ بیماری کے سروے کا نظام بھی کھڑا کروا دیا گیا ہے۔
                    <br>


                </span>


            </p>
        </div><!-- /.container -->
    </section>


</main>