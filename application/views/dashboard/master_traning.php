<style>
    input[type="file"] {
    display: none;

}
.btn-self-enrollment{
    background:#05445E;
    color:white;
    border:1px solid black;
    width:6rem;
    height:3rem;
    margin-left: -15px;

}
/* The Modal (background) */
.modal {
  display: none;  /*Hidden by default */
  /* display:block; */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content-aun {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 40%; /* Could be more or less, depending on screen size */
  height:40%;
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}
#display-image{
  width: 40px;
  height: 225px;
  border: 1px solid black;
  background-position: center;
  background-size: cover;
}
.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
</style>
<main id="main" style="margin-top: 130px;">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <section >
        <h2 style="font-weight:bolder" >Master Traning Registration</h2>
        <div class="container">
            <ul style='margin:auto' class="col-6 searchlist">
            <h2 style="text-align:center;" class="emp-form-heading">Registration Form</h2>
            <?php if($this->session->flashdata('success')):?>
          <div class="alert alert-success"><?=$this->session->flashdata('success')?></div>
          <?php endif;?>
            <?php echo !empty($this->session->flashdata('fail'))? $this->session->flashdata('fail'):'';?>
            <form name="employee_enrollment" id="employee_enrollment" method="POST"
                      action="<?=base_url()?>dashboard/emp_authentication" onsubmit="required()">
                <div class="step step-1 active" >
                
                    <div class="form-group row">
                    
                        <label for="staticEmail" class="col-sm-3 col-form-label">CNIC</label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control" name="cnic" data-inputmask="'mask': '99999-9999999-9'" placeholder="Enter CNIC" >
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['cnic'])? $this->session->flashdata('error')['cnic']:'';?></span>
                        </div>
                        

                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Name</label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control"  name="name" placeholder="Enter Name"   >
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['name'])? $this->session->flashdata('error')['name']:'';?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Mobile</label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control" name="mobile" data-inputmask="'mask': '0399-9999999'"   placeholder="Enter Mobile" >
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['mobile'])? $this->session->flashdata('error')['mobile']:'';?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-6">
                        <input type="email" class="form-control" name="email" placeholder="Enter Email" >
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['email'])? $this->session->flashdata('error')['email']:'';?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Qualification</label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control" name="email" placeholder="Enter Qualification" >
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['Qualification'])? $this->session->flashdata('error')['email']:'';?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Experience</label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control" name="email" placeholder="Enter Experience" >
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['Experience'])? $this->session->flashdata('error')['email']:'';?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-sm-3">
                    <label style="font-size: smaller; padding: 10px; background: #05445E; display: table;color: #fff;"> Profile Picture
    <input type="file" size="90" name="profile_img" onchange="readURL(this);">

</label> 
</div>

                        <div class="col-sm-6">
                        <img  src="#" id="preview" class="img-thumbnail">
                        </div>
                        </div>
                   
                        <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Last Designation</label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control" name="email" placeholder="Enter Last Designation" >
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['Last Designation'])? $this->session->flashdata('error')['email']:'';?></span>
                        </div>
                    </div>
                    <input type='hidden' name="emp_auth" value='1'>
                    <div class="col-6">
                <button  type="submit"  id="myBtn"  class=" btn-self-enrollment">Submit</button>

            </div>
</div>
            </form>
            
          
            </ul>
            
            <div id="myModal" class="modal">

                <!-- Modal content -->
                <div class="modal-content-aun">
                    
                  <span onclick="closeWin()" style="margin-top: -20px;" class="close">&times;</span>
                  <div>
                      <h2 style="margin-top: -20px; margin-left:-20px;margin-right:-20px;text-align:center" class="emp-form-heading">Verification Code</h2>

                  </div>
                  <?php if(!empty($this->session->flashdata('message'))): ?> 
                    <p> <?= $this->session->flashdata('message')?></p>
                    <p><a href=<?=base_url('dashboard/add_employee_profile')?>>click here</a></p>
                    <?php else: ?>
                  <p>A verification code (VFC) has been sent to your mobile phone number; please enter code <strong>(<?php echo (!empty($this->session->flashdata('pin_code')))? $this->session->flashdata('pin_code'):''?>)</strong> below to complete the Enrollment Process:</p>
                  <form name="verification_code" method="post" action="<?=base_url()?>dashboard/emp_verification">
                  <div class="form-group row">
                  
                      <label for="staticEmail" class="col-sm-4 col-form-label">Enter VFC</label>
                      <div class="col-sm-6">
                      <input type="password" name="pin_code" autocomplete="current-password" required="" id="id_password">
                      <input type="hidden" name="cnic" value="<?php echo !empty($this->session->flashdata('cnic'))? $this->session->flashdata('cnic'):''?>">
                      <i class="far fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer;"></i>
                      </div>
                                        

                  </div>
                  <p style=' font-weight: bolder;color:#05445E'>Resend VFC</p>
                  <button style="float:right"  class="btn-self-enrollment">Submit</button>
                </div>
                </form>
                <?php endif;?>

          </div>
        </div>

       
    </section>
 
    <script>
      const image_input = document.querySelector("#image-input");

image_input.addEventListener("change", function() {
  const reader = new FileReader();
  reader.addEventListener("load", () => {
    const uploaded_image = reader.result;
    document.querySelector("#display-image").style.backgroundImage = `url(${uploaded_image})`;
  });
  reader.readAsDataURL(this.files[0]);
});



function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result)
                        .width(400)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


      $(":input").inputmask();

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
let myWindow;

function openWin() {
  myWindow = window.open("", "", "width=200,height=100");
}

function closeWin() {
  myWindow.close();
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
<?php if($this->session->flashdata('verify')=='true'){?>
// btn.onclick = function() {
//   modal.style.display = "block";
// }
modal.style.display = "block";
<?php }?>
function required()
{
var empt = document.form1.text1.value;

if (empt === "")
{
alert("Please Fill the Field ");
return false;
}
else 
{
modal.style.display = "block";
return true; 
}
}
// When the user clicks the button, open the modal

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#id_password');
 
  togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});
$(document).ready(function() {
    // $(document).on('submit', '#employee_enrollment', function() {
    //   // do your things
    //   window.location  = "<?= base_url() ?>dashboard/emp_authentication";
    //   return false;
    //  });
});
</script>

</main>