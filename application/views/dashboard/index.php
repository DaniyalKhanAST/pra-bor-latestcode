<style>
    #slider a {
        display: none;
    }
    .content{
  height: 1000px;
	font-family: 'Questrial';
	line-height: 1.5;
	letter-spacing: 3px;
	font-size: 20px;
	text-decoration: underline;
	text-align: center;
	margin-top: 30px;
}
.go-top {
  position:fixed;
  bottom: 20%;
  right: 3%;
  padding:20px;
  display:none;
  cursor: pointer;
	-webkit-font-smoothing: antialiased;
}
.go-top:after {
  font-family: FontAwesome;
  content: "\f106";
	background-color: #05445E;;
	padding: 10px 15px;
  color: #ffffff;
  position: absolute; 
  
  font-size: 28px;
}

.go-top-text {
	position: absolute;
	width: 60px;
	text-align: center;
	font-family: 'Questrial';
	line-height: 1.5;
	letter-spacing: 3px;
	font-size: 12px;
	margin: 20px 0 0 -4px;
}

.go-top:hover {
	transition: all .4s linear;
	transform: scale(1.1);
}
    #policy-link {
        height: 20px;
        width: 80px;
        position: fixed;
        left: -40px;
        top: 58%;
        z-index: 1000;
        transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
        padding: 20px 25px;
        display: block;
    }


    #policy-link a {
        display: block;
        background: orange;
        height: 50px;
        width: 125px;
        color: #000;
        font-family: Arial, sans-serif;
        font-size: 15px;
        font-weight: bold;
        text-decoration: none;
        padding: 4px 3px 10px;
        text-align: center;

    }


    #policy-link a:hover {
        background: #CCC;
    }

    .heading {
        background: #fff44f;
        padding: 5px;
        font-weight: bold;
        text-align: center;
    }

    .heading h4 {
        font-weight: bold;
        display: inline;
    }

    @media (max-width: 480px) {
        #policy-link {
            display: none;
        }
    }
    .hwrap {

    overflow: hidden;
    background: #eee;
    position: fixed;
    padding: -1em -2em;
    /* margin: -15em 31em 3em; */
    width: 17em;
    /* top: 224px; */
    padding: -1em 0em;
    /* margin: -4em 31em 11em; */
    top: 296px;
    padding: 0em 0.3em;
    margin: -4em 30em 3em;
    left: 7px;
    background:#00a651;;
    color: #fff;
    border: 2px solid white;
    z-index: 1;
    height:auto;
    font-size: 16px
    
    
}
 

.ribbon {
 font-size: 18px !important;
 width: 12%;
 z-index: 1;
 float:right;      
 top:171px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em;
 margin: -1em auto 3em; 
 border: 2px solid white;
}
.ribbon3 {
 font-size: 18px !important;
 width: 10%;
 z-index: 1;
 float:right;      
 top:296px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em; 
 margin: -8em 18em 3em;
 border: 2px solid white;
}
.ribbon4 {
 font-size: 18px !important;
 width:18%;
 z-index: 1;
 float:right;      
 top:296px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em; 
 margin: -8em 27em 3em;
 border: 2px solid white;
}
.ribbon1 {
 font-size: 18px !important;
 width: 10%;
 z-index: 1;
 float:right;      
 top:296px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em; 
 margin: -8em 9em 3em; 
 border: 2px solid white;
}

@media (max-width:480px){
    .ribbon {
 font-size: 16px !important;
 width: 30%;
 z-index: 1;
 float:right;      
 top:200px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em; 
 margin: -8em auto 3em; 
}
.ribbon1 {
 font-size: 16px !important;


 width: 30%;
 z-index: 1;
 float:right;      
 top:296px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em; 
 margin: -14em 8em 3em; 
}
.ribbon3 {
 font-size: 16px !important;
 width: 30%;
 z-index: 1;
 float:right;      
 top:296px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em; 
 margin: -14em 16em 3em; 
}
.ribbon4 {
 font-size: 16px !important;
 width: 30%;
 z-index: 1;
 float:right;      
 top:296px; 
 position: fixed;
 background: black;
 color: #fff;
 text-align: center;
 padding: 1em 0em; 
 margin: -8.5em auto 3em; 
}
.hwrap {
    overflow: hidden;
    background: #eee;
    position: fixed;
    padding: -1em -2em;
    /* margin: -15em 31em 3em; */
    width: 12.5em;
    /* top: 224px; */
    padding: -1em 0em;
    /* margin: -4em 31em 11em; */
    top: 296px;
    padding: 0em 0em;
    margin: -8em 7em 3em;
    left: 47px;
    background: #00a651;
    color: #fff;
    border: 2px solid white;
    z-index: 1;
    height: auto;
    font-size: 16px;
}
}

.carousel-item{
    width = 100vh;
    height = 100vh; 
    position = center;
    repeat = no-repeat;
}
.carousel-inner{
    width = 100vh;
    height = 100vh;
}



    
</style>

<?php
$array = [];
if(isset($slider_images)){
    foreach($slider_images as $slider)
    {
        $array[$slider->slider_number][] = $slider;

    }
    }
    ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<main id="main" class="main-home" style="background-color: #214b51;">

 <section id="topsection" style="height:0%; margin-top:-10;display: block;
line-height:0;
height: 0;
overflow: hidden;display: block !important;
line-height: 0 !important;
height: 0 !important;
overflow: hidden !important;
zoom: 1 !important;
padding: 0 !important;
margin-bottom: 0;
border: none;" tabindex="1"> </section> 
     <div id="search-banner" class="fade scroll" style="opacity:1">
        
        <form action="<?= base_url() ?>dashboard/search_results" class="formScript">
            <p>
                <label for="search-input">Search: <span>   تلاش ۔۔۔</span>
                </label>
               
                       <input  type="text" value="" name="q" id="search-input" autocomplete="off" class="typer"
                       data-keywords='[<?php $cnt = 1; foreach ($main_silder_images as $silder_image) {
                           echo '"'.$silder_image->title.'"';
                           if($cnt<sizeof($main_silder_images)){
                               echo ',';
                           }
                           $cnt++;
                       } ?>
                       ]'>
                       
                <button type="submit">
                    <svg version="1.1" x="0px" y="0px" viewBox="0 0 24 24" width="24px" height="24px"
                         enable-background="new 0 0 200 200" preserveAspectRatio="none">
                        <title>Search</title>
                        <path d="M23.5,21.3l-7-7c2.5-3.5,2.2-8.5-1-11.7C12-0.9,6.1-0.9,2.6,2.7c-3.5,3.5-3.5,9.3,0,12.8c3.2,3.2,8.1,3.5,11.7,1l7,7
		c0.6,0.6,1.6,0.6,2.2,0l0,0C24.2,22.9,24.2,22,23.5,21.3z M4.7,13.4c-2.4-2.4-2.4-6.2,0-8.5c2.4-2.4,6.2-2.4,8.5,0
	c2.4,2.4,2.4,6.2,0,8.5C11,15.7,7.1,15.7,4.7,13.4z"/>
                    </svg>

                </button>
                
                <button type="button" class="clear-search">Clear Search</button>
            </p>
        </form>
       
        <div id="searchResults" class="clearfix" style="width: 70%">

            <ul id="searchTabs" style="width: 100%">
                <li id="servicesTab">
                    <a href="#servicesList" data-tab="servicesList">NEWS</a>
                </li>
                <li id="formsTab">
                    <a href="#formsResults" data-tab="formsResults">Documents</a>
                </li>
                <li id="webTab">
                    <a href="#googleResults" data-tab="googleResults">All of PHFMC.gov</a>
                </li>
                <li>
                    <a href="#relatedResults" data-tab="relatedResults">Related</a>
                </li>
            </ul>

            <div id="results">

                <div id="servicesList" data-keyword="" data-type="1,2,3,5,6,12,48" data-limit="5" style="width: 100%">
                    Loading...
                </div>

                <div id="formsResults" data-keyword="" style="width: 100%">
                    Loading...
                </div>

                <div id="googleResults" data-keyword="" style="width: 100%">
                    Loading...
                </div>
            </div>
        </div>

        <a href="#locals" id="goToCity"><span></span></a>
    </div>

    </div>
     <div>
            
        </div>
    </div>
    
    <!-- <?php if(!empty($array)): foreach($array as $key=>$subArray):
        ?>
    <section class="scroll" id="slider-<?=$key?>">

        <div class="content-container-secondry-slider ">
        
        <div id="carouselIndicators-<?=$key?>" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php for($i=1;$i<=count($subArray);$i++):?>
    <li data-target="#carouselIndicators-<?=$key?>" data-slide-to="<?=$i?>" class=""></li>
    <?php endfor;?>
  </ol>
  <div class="carousel-inner">
  <?php $isFirst=true; foreach($subArray as $index=>$obj):
    ?>
    <div class="carousel-item <?=($isFirst)? 'active':'' ?>">
      <img class="d-block w-100" src="<?=str_replace('../',base_url(),$obj->banner_url)?>" alt="slide">
    </div>

    <?php 
$isFirst = false;
endforeach;?>
  </div>

</div>

           
        </div>

    </section>
    
    <?php endforeach; endif;?> -->
 
    <section class="scroll" id="locals" tabindex="2">

        <header>

            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
            </svg>

            <h2>پنجاب ریونیو اکیڈمی</h2>


        </header>

        <div class="feature mIndex" data-format="background" data-locate="device" data-type="99" data-limit="1">

            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
            </svg>

            <div class="feature-content">
                <a class="call-to-action" target="_blank" href="<?= base_url() ?>dashboard/pr">
                  
                <p class="call-out" >The Punjab Revenue Academy<span style="float:right">  پنجاب ریونیو اکیڈمی</span></p>
                </a>
            </div>
        </div>
<!-- 
        <div class="content-container">
            <div class="row" style="width:100%">
                <div class="col-sm-4">
                    <div style="width:100%"><a href="<?=base_url()?>assets/images/home/background_flyer_1.jpg"><img class="" src="<?= base_url()?>/assets/images/home/slide_1_flyer_1.jpg" style="width:100%;height: auto;"></a></div>
                </div>
                <div class="col-sm-4">
                        <div style="width:100%"><a href="<?=base_url()?>assets/images/home/background_flyer_2.jpg"><img class="img-fluid" src="<?= base_url()?>/assets/images/home/slide_1_flyer_2.jpg" style="width:100%;height: auto;"></a></div>
                </div>
                <div class="col-sm-4">
                          <div style="width:100%"><a href="<?=base_url()?>assets/images/home/background_flyer_3.jpg"><img class="img-fluid"  src="<?= base_url()?>/assets/images/home/slide_1_flyer_3.jpg" style="width:100%;height: auto"></a></div>
                </div>
            </div>
        </div> -->
        <div class="content-container">

<article class="content-item">
    <h3 style=""><a href="<?php echo base_url() ?>dashboard/pr" target="_blank"> Punjab Revenue Academy <span style="float: right">پنجاب ریونیو اکیڈمی </span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/flyer001.jpg);" href="<?php echo base_url() ?>dashboard/pr" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?=base_url()?>dashboard/bor" target="_blank">Board Of Revenue<span style="float: right">بورڈ آف ریونیو
</span> </a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/flyer02.jpg);" href="<?=base_url()?>dashboard/bor" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?=base_url()?>dashboard/fp" target="_blank"> Future Plans <span style="float: right">مستقبل </span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/background_flyer_3_663x464.jpg);" href="<?=base_url()?>dashboard/fp" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
                <h3 style=""><a href="<?= base_url() ?>aboutus/laws" target="_blank"> Laws/ Policy <span style="float: right;
    margin-right: 20px;" >قوانین/ پالیسی</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/legislation.png);" href="<?= base_url() ?>aboutus/laws" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>


</div>
    </section>
    <section class="scroll" id="gov-news" tabindex="3">

        <header>
  
  
             <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "style="fill: #0f353b !important;"/>
            </svg>

            <h2>پنجاب ریونیو اکیڈمی</h2>

        </header>


        <div class="feature"  >
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "style="fill: #0f353b !important;"/>
            </svg> 
            <div class="feature-content" >
                <a class="call-to-action" href="<?= base_url() ?>page/slider_detail" target="_blank">
                 
                <p class="call-out"  >New Training Paradigm<span style=" float:right">  نیا ٹریننگ پیراڈايم
                </span>
                </p>
                </a>
            </div>
        </div>
        <div class="content-container">

<article class="content-item">
    <h3 style=""><a href="<?php echo base_url() ?>page/pages/9211_virtual_governance" target="_blank"> V-Governance <span style="float: right">ورچوئل گورننس</span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/education.jpg);" href="<?php echo base_url() ?>page/pages/9211_virtual_governance" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/important_concepts_of_land_revenue_administration_mlrar.pdf" target="_blank"> Important Concepts of Land Revenue Administration  </a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="image-rendering: pixelated;background-image: url(<?= base_url() ?>assets/images/home/icl.png);" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/important_concepts_of_land_revenue_administration_mlrar.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/agriculture_marketing_and_supply_chain_management_of_basic_commodities_ll6a5.pdf" target="_blank"> Agriculture Marketing <span style="float: right"> </span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="image-rendering: -webkit-optimize-contrast;background-image: url(<?= base_url() ?>assets/images/home/ams.png);" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/agriculture_marketing_and_supply_chain_management_of_basic_commodities_ll6a5.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?= base_url() ?>aboutus/laws" target="_blank"> Laws/ Policy <span style="float: right;
    margin-right: 20px;
">قوانین/ پالیسی</span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/legislation.png);" href="<?= base_url() ?>aboutus/laws" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<!-- <article class="content-item testDiv" style="overflow: hidden; width: auto;">
    <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9212 <span style="float: right">ٹال فری نمبر</span></a></h3>
    <ul>
        <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                <h4 style="font-size:1rem">Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/contact_directory">
                <h4 style="font-size:1rem">Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                <h4 style="font-size:1rem">Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/e_filling">
            <h4 style="font-size:1rem">E-filing <span style="float: right;">ای فائلنگ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/events">
                <h4 style="font-size:1rem">Gallery <span style="float: right;">گیلری </span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>page/pages/media_banners">
                <h4 style="font-size:1rem">Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
            </a></li>
    </ul>
</article> -->
</div>

        <!-- <div class="content-container">

            <article class="content-item news testDiv">
                <h3><a href="<?=base_url()?>page/listing_page/service_delivery_platform"> Service Delivery Platform<span style="float: right;">سروس ڈ لیوری  پلیٹ فارم</span></a></h3>

                <ul>
                    <li>
                        <a href="<?= base_url() ?>page/pages/livestock_policy_punjab"><h4>1st Livestock &amp; Dairy Development Policy <span style="float: right;margin-right: 5px;">ڈیری ڈیویلپمنٹ پالیسی</span></h4></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>page/pages/paradigm_shift"><h4>Paradigm Shift <span style="float: right;margin-right: 5px;">پیرا میٹر تبدیلی</span></h4></a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/breeding_framework"><h4>Breeding Framework <span style="float: right;margin-right: 5px;">بریڈنگ فریم ورک</span></h4></a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/mobile_hospitals_dispensaries_labs_ad_bikes"><h4>Mobile Hospitals, Dispensaries, Labs &amp; Bikes<span style="float: right;margin-right: 5px;">موبائل ڈسپنسریزاور موٹر سائیکل</span></h4></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>dashboard/poverty_alleviation"><h4>Poverty Alleviation <span style="float: right;margin-right: 5px;">غربت کا خاتمہ</span></h4></a>
                    </li>


                    <li>
                        <a href="<?=base_url()?>page/listing_page/r_d_biologics"><h4>R &amp; D And Biologics<span style="float: right;margin-right: 5px;">آراینڈ ڈی اینڈ بائیولوجکس</span></h4></a>
                    </li>

                </ul>
            </article>

            <article class="content-item news">
                <h3><a href="<?=base_url()?>page/listing_page/mass_vaccination_campaign"> Mass Vaccination Campaign <span style="float: right">ماس ویکسینیشن </span></a></h3>

                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/MVDcombine.jpg);" href="<?=base_url()?>page/listing_page/mass_vaccination_campaign">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item news">
                <h3><a href="<?=base_url()?>page/listing_page/disease_diagnosis_and_surveillance"> Disease Diagnosis & Surveillance<span style="float: right;margin-right: 5px;">ڈیزیز سرویلنس</span></a></h3>

                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/lab.jpg);" href="<?=base_url()?>page/listing_page/disease_diagnosis_and_surveillance">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="tweets">
                <h3><a href="<?php echo base_url() ?>page/pages/9211_virtual_governance"> 9211 Virtual Governance System<span style="float: right;margin-right: 5px;">ورچوئل گورنس</span></a></h3>

                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/chart.jpg);" href="<?php echo base_url() ?>page/pages/9211_virtual_governance">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

        </div> -->

    </section>

    <section class="scroll" id="open1" tabindex="4">
   
        <header style="background:#2F3A8F !important">
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none" style="fill:#212a6b;">
                <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
            </svg>

            <h2>پنجاب ریونیو اکیڈمی</h2>

        </header>
        
        <div class="feature" style="height:100%">
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none" >
                <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
            </svg>
            
            <div class="feature-content" style="bottom:5rem !important">
                <a class="call-to-action" href="<?= base_url() ?>dashboard/training_program" target="_blank">
               
                <p class="call-out" >Training Programme <spna style="float:right"> تربيتي پروگرام</span>
                    </p>
                </a>
            </div>
            <div  style="margin-top:25rem !important;width:95%; cursor:pointer; position: absolute;">
                <a class="call-to-action" href="https://online.pra-borpunjab.gov.pk/Account/Signup" target="_blank">
                    <p  style="color:white; margin-left:70px;font-size:3rem;text-decoration: underline;">Register Here</p>
                </a>
    </div>
        </div>
        
      
        
        <!-- <div class="content-container">
<div class="grid_3" style="padding: 0;">
            <article class="content-item testDiv">
                <h3><a href="<?=base_url()?>page/listing_page/my_own_livestock_business"> My Own Livestock Business <span style="float: right;margin-right: 5px;">میرا اپنا لائیوسٹاک بزنس</span></a></h3>
                <ul>
                    <li>
                        <a href="<?= base_url() ?>infodesk/booklets"><h4>Business Startup<span style="float: right;margin-right: 5px;">کاروبار ابتدائیہ</span></h4></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>page/listing_page/feasibility_studies"><h4>Feasibility Studies<span style="float: right;margin-right: 5px;">امکانات کا مطالعہ</span></h4> </a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>page/listing_page/government_incentives"><h4>Government Incentives<span style="float: right;margin-right: 5px;">حکومت ترغیبات</span></h4> </a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>page/listing_page/extension_services"><h4>Extension Services<span style="float: right;margin-right: 5px;">توسیعی سروس</span></h4> </a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>page/listing_page/district_profiles"><h4>District Profiles<span style="float: right;margin-right: 5px;">ضلع پروفائل</span></h4> </a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>page/listing_page/livestock_farms_profiles"><h4>Livestock Farms Profiles<span style="float: right;margin-right: 5px;">لائیوسٹاک فارمز پروفائل</span></h4> </a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>page/listing_page/mass_vaccination_campaign"><h4>Mass Vaccination Campaign<span style="float: right;margin-right: 5px;">ماس ویکسینیشن مہم</span></h4> </a>
                    </li>

                </ul>
            </article>
</div>
<div class="grid_3" style="padding: 0;">
            <article class="content-item transparency testDiv">
                <h3><a href="<?=base_url()?>page/listing_page/data_catalogue">

                        Data Catalogue<span style="float: right;margin-right: 5px;">ڈیٹا کا اعداد و شمار</span></a ></h3>
                <h4>Pakistan- 25th Largest Economy in the World (PPP Basis)</h4><br>

                <div class="clearfix"></div>
                <h4 style="margin-top: 30px;">One of the Next Eleven<br> Emerging Economies</h4>
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" viewBox="134 -134 300 300" width="300px" height="300px"
                     enable-background="new 134 -134 300 300" xml:space="preserve" class="transparency-donut">
	<path class="publicEd" fill="#5D92B6" d="M311-45c33.8,14.8,48.7,54,34,87.8c-3,6.8-6.8,12.6-11.5,17.8l62.1,55.2c10.3-11.6,19.3-24.7,25.9-39.7
		c33-75.9-1.4-164.2-77.3-197.4L311-45z"/>

                    <path class="socialServices" fill="#81B053" d="M289.6,82.5c-36.7,2.8-68.6-23.7-72-60.4c-1.2-13.3,1.6-25.8,7.2-36.8l-73.7-38.2
		C138.3-28.2,131.9,0,134.6,29.7c7.5,82.5,80.2,143.2,162.6,135.6L289.6,82.5z"/>

                    <path class="capital" fill="#416e92"
                          d="M224.8-14.7c1.3-2.5,2.8-5,4.4-7.3l0,0l-68.6-46.8c-3.5,5.1-6.6,10.4-9.5,15.9L224.8-14.7z"/>

                    <path class="higherEd" fill="#a92807"
                          d="M297.2,165.4c36.6-3.3,72.1-19.8,98.4-49.6l-62.1-55.2c-11.8,13.3-27.6,20.4-43.9,21.9L297.2,165.4z"/>

                    <path class="law" fill="#8b0f0a"
                          d="M344.2-121.3c-14.1-6.1-29.3-10.4-45.4-11.9l-8,82.8c7.2,0.7,13.9,2.6,20.2,5.4L344.2-121.3z"/>

                    <path class="gov" fill="#2AAAE2"
                          d="M298.8-133.2c-21.1-2.1-43.4,0-64.9,7.7l28,78.5c9.6-3.4,19.4-4.3,28.9-3.4L298.8-133.2z"/>

                    <path class="transportation" fill="#F15C27"
                          d="M233.9-125.6c-20.1,7.2-39,18.9-55.2,35.1l58.6,58.9C244.5-38.8,253-43.8,262-47L233.9-125.6z"/>

                    <path class="debt" fill="#892785"
                          d="M178.6-90.6c-6.5,6.6-12.6,13.8-18,21.8L229.2-22c2.4-3.5,5.2-6.7,8.1-9.6L178.6-90.6z"/>

</svg>

                <ul>
                    <li class="transItem socialServices">
                        <i class="transIcon"></i>
                        <span class="percentage">62%</span>
                        <span class="category">Milk Production</span>
                    </li>
                    <li class="transItem publicEd active">
                        <i class="transIcon"></i>
                        <span class="percentage">55%</span>
                        <span class="category">Eggs Production</span>
                    </li>
                    <li class="transItem higherEd">
                        <i class="transIcon"></i>
                        <span class="percentage">65%</span>
                        <span class="category">Pultry Meat</span>
                    </li>
                    <li class="transItem gov">
                        <i class="transIcon"></i>
                        <span class="percentage">59%</span>
                        <span class="category">Hides</span>
                    </li>
                    <li class="transItem transportation">
                        <i class="transIcon"></i>
                        <span class="percentage">43%</span>
                        <span class="category">Beef</span>
                    </li>
                    <li class="transItem law">
                        <i class="transIcon"></i>
                        <span class="percentage">33%</span>
                        <span class="category">Mutton</span>
                    </li>
                    <li class="transItem debt">
                        <i class="transIcon"></i>
                        <span class="percentage">32%</span>
                        <span class="category">Skin</span>
                    </li>
                    <li class="transItem capital">
                        <i class="transIcon"></i>
                        <span class="percentage">24%</span>
                        <span class="category">Wool</span>
                    </li>
                </ul>

                <p class="description" style="font-weight: 800">
                    <a>Share of Livestock Products of Punjab in Pakistan</a>
                </p>
            </article>
</div>

            <article class="content-item">
                <h3><a href="<?= base_url() ?>dashboard/events"> Current News & Alerts<span style="float: right;margin-right: 5px;">موجودہ خبریں</span></a></h3>

                <div id="slider">
                <?php foreach ($flood_advertisement as $row) { ?>
                        <a class="fancybox" rel="ligthbox-news" href="<?= $row->image_url ?>">
                            <img style="height: 210px" src="<?= $row->image_url ?>"/>
                        </a>
                    <?php } ?>
                </div>

            </article>

            <article class="tweets">
                <h3><a href="<?= base_url() ?>infodesk/important_links"> Important Links<span style="float: right;margin-right: 5px;">اہم لنکس</span></a></h3>
                <ul>
                    <li>
                        <a href="<?= base_url() ?>infodesk/important_links"><h4>Academics <span style="float: right">تعلیمی ادارے</span></h4> <p></p></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>infodesk/important_links"><h4>Industrial <span style="float: right">صنعتی </span></h4> <p></p></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>infodesk/important_links"><h4>National <span style="float: right">قومی </span></h4> <p></p></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>infodesk/important_links"><h4>International <span style="float: right">بین الاقوامی </span></h4> <p></p></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>infodesk/important_links"><h4>Miscellaneous &amp; Articles <span style="float: right"> دیگر / ارٹیکلز</span></h4> <p></p></a>
                    </li>
                </ul>
            </article>

        </div> -->

    </section>
    <section class="scroll" id="citizen" tabindex="5">

        <header>
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
            </svg>
            <h2>پنجاب ریونیو اکیڈمی</h2>
        </header>

        <div class="feature"  >
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
            </svg>
            <div class="feature-content" >
                <a class="call-to-action" href="<?= base_url() ?>assets/files/Syllabus.pdf" target="_blank">
                
                    <p class="call-out" style="">Patwari's Syllabus<span style="float: right"> پٹواری سلیبس </span></p>
                </a>
            </div>
        </div>
        <div class="content-container">

<article class="content-item">
    <h3 style=""><a href="<?php echo base_url() ?>assets/files/SyllabusTheory.pdf" target="_blank"> Theory <span style="float: right">تھیوری</span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/theory_800x450_2400x1800.png);" href="<?php echo base_url() ?>assets/files/SyllabusTheory.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?=base_url()?>assets/files/syllabusPractical.pdf" target="_blank"> Pratical <span style="float: right"> پریکٹیکل</span> </a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/pratical_2400x1800.png);" href="<?=base_url()?>assets/files/syllabusPractical.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?=base_url()?>page/pages/social_audit" target="_blank"> Social Audit <span style="float: right">سماجی احتساب</span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/ult3.jpeg);" href="<?=base_url()?>page/pages/social_audit" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?= base_url() ?>aboutus/laws" target="_blank"> Laws/ Policy <span style="float: right;
    margin-right: 20px;
">قوانین/ پالیسی</span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/legislation.png);" href="<?= base_url() ?>aboutus/laws" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<!-- <article class="content-item testDiv" style="overflow: hidden; width: auto;">
    <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9212 <span style="float: right">ٹال فری نمبر</span></a></h3>
    <ul>
        <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                <h4 style="font-size:1rem">Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/contact_directory">
                <h4 style="font-size:1rem">Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                <h4 style="font-size:1rem">Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/e_filling">
            <h4 style="font-size:1rem">E-filing <span style="float: right;">ای فائلنگ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/events">
                <h4 style="font-size:1rem">Gallery <span style="float: right;">گیلری </span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>page/pages/media_banners">
                <h4 style="font-size:1rem">Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
            </a></li>
    </ul>
</article> -->
</div>

        <!-- <div class="content-container">

            <article class="content-item testDiv">
                <h3><a href="<?=base_url()?>page/listing_page/grannys_advice_for_parents">Granny's Advice for Parents <span style="float: right;">نانی جان کے والدین کو مشورے</span></a></h3>
                <ul>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/kitchen_poultry"><h4>Kitchen Poultry<span style="float: right;">مفت کے انڈے</span></h4></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>infodesk/country_medicine"><h4>Country Medicine <span style="float: right;">ٹوٹکے</span></h4></a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/consumer_rights"><h4>Consumer Rights<span style="float: right;">ہمارے حقوق</span></h4></a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/our_responsibilities"><h4>Our Responsibilities <span style="float: right;">ہمارے فرائض</span></h4></a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/animal_rights"><h4>Animal Rights <span style="float: right;">جانوروں کے حقوق</span></h4></a>
                    </li>
                </ul>
            </article>

            <article class="content-item">
                <h3><a href="<?=base_url()?>page/listing_page/informative_literature">Informative Literature<span style="float:right;">معلومات</span></a></h3>
                <ul>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/pet_clinic"><h4>Pet Clinic <span style="float: right;">پٹ کلینک</span></h4></a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>page/listing_page/nutritional_awareness"><h4>Nutritional Awareness<span style="float:right;">بچوں کی نشو نما میں خالص دودھ/انڈوں کی اہمیت</span></h4></a>
                    </li>
                </ul>
                <p><a><img src="<?php base_url() ?>assets/images/home/kids7.jpg" style="height: 110px;" alt="Informative Literature"></a></p>

            </article>

            <article class="collaborate">
                <h3><a href="<?=base_url()?>page/listing_page/pet_clinic">My Pet<span style="float:right;">میرا سا تھی</span></a></h3>
                <ul>
                    <li>
                        <a href="<?=base_url()?>dashboard/multimedia_management"><h4>Photos/ Videos/ Stories/ Games<span style="float: right">تصاویر/وڈیوز/کہانیاں/گیمز</span></h4></a>
                    </li>
                    <li>
                    <a href="<?=base_url()?>page/listing_page/hero_of_month"><h4>Hero of the Month<span style="float: right">اس مہینے کا ہیرو</span></h4></a>
                    </li>
                </ul>
                <p><a><img src="<?php base_url() ?>assets/images/home/kids10.jpg" style="height: 110px;" alt="Gallery"></a></p>
            </article>

        </div> -->

    </section>
    <section class="scroll" id="digital" tabindex="6">

        <header>
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
            </svg>
            <h2 style="">پنجاب ریونیو اکیڈمی</h2>

        </header>
        <div class="feature" >
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
            </svg>
            <div class="feature-content" >
                <a class="call-to-action" href="<?= base_url() ?>assets/files/Exams.pdf" target="_blank">
                
                <p class="call-out" style="">Exams<span style="float: right"> امتحانات
                    </span></p>
                </a>
            </div>
        </div>

        <div class="content-container" style="background:#443255">

            <article class="content-item">
                <h3 style=""><a href="<?php echo base_url() ?>assets/files/ExamTheory.pdf" target="_blank"> Theory <span style="float: right">تھیوری</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/examtheory_800x450_2400x1800.png);" href="<?php echo base_url() ?>assets/files/ExamTheory.pdf" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="<?=base_url()?>assets/files/ExamPractical.pdf" target="_blank"> Pratical <span style="float: right"> پریکٹیکل</span> </a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/exampratical_1_800x450_2400x1800.png);" href="<?=base_url()?>assets/files/ExamPractical.pdf" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="<?=base_url()?>page/pages/examination_procedure" target="_blank"> Examination Procedure<span style="float: right"> امتحانی طریقہ کار</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>images/home/slide5.jpeg);" href="<?=base_url()?>page/pages/examination_procedure" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="<?= base_url() ?>aboutus/laws" target="_blank"> Laws/ Policy <span style="float: right;
    margin-right: 20px;
">قوانین/ پالیسی</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/legislation.png);" href="<?= base_url() ?>aboutus/laws" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <!-- <article class="content-item testDiv">
                <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9211 <span style="float: right">ٹال فری نمبر</span></a></h3>
                <ul>
                    <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                            <h4>Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>dashboard/contact_directory">
                            <h4>Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
                        </a></li>
                    <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                            <h4>Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
                        </a></li>
                    <li><a href="<?=base_url()?>page/listing_page/e_filling">
                        <h4>E-filing <span style="float: right;">ای فائلنگ</span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>dashboard/events">
                            <h4>Gallery <span style="float: right;">گیلری </span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>page/pages/media_banners">
                            <h4>Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
                        </a></li>
                </ul>
            </article> -->
        </div> 
    </section>

    <section class="scroll" id="rti" tabindex="7">

        <header style="background: #733C3C">
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none" style="fill: #481e1e;">
                <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
            </svg>
            <h2 style="">حق آگاہی و عملی شمولیت</h2>

        </header>
        <div class="feature">
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>      
            </svg>
            <div class="feature-content">
                <a class="call-to-action" href="<?= base_url() ?>page/listing_page/right_to_information" target="_blank">
                
                <p class="call-out" >RTI & Beyond<span style="float:right"> حق آگاہی و عملی شمولیت</span>
                    </p>
                </a>
            </div>
        </div>

        <div class="content-container" style="background: #733C3C">

            <article class="content-item">
                <h3 style=""><a href="https://es.punjab.gov.pk/eStampCitizenPortal/ChallanFormView/HomePage" target="_blank"> E-Stamping <span style="float: right"> ای سٹیمپنگ</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/es.jpeg);" href="https://es.punjab.gov.pk/eStampCitizenPortal/ChallanFormView/HomePage" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="https://bor.punjab.gov.pk/" target="_blank"> Board of Revenue <span style="float: right"> بورڈ آف ریونیو</span> </a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/borimg.jpeg);" href="https://bor.punjab.gov.pk/" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="https://www.punjab-zameen.gov.pk/" target="_blank"> Punjab Land Records Authority <span style="float: right"> پنجاب لینڈ ریکارڈ اتھارٹی</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/landpic.jpeg);" href="https://www.punjab-zameen.gov.pk/" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="https://pdma.gop.pk/" target="_blank"><i Style="    position: relative;
    top: -4px;
    font-style: normal;
    font-weight: bold;"> Provincial Disaster<br> Management Authority </i> <span style="float: right; margin-top:-18px;
    margin-right: 20px;
">پروونشل ڈیزاسٹ<br> مینجمنٹ اتھارٹی</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/ffpic.jpeg);" href="https://pdma.gop.pk/" target="_blank">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <!-- <article class="content-item testDiv" style="overflow: hidden; width: auto;">
                <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9212 <span style="float: right">ٹال فری نمبر</span></a></h3>
                <ul>
                    <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                            <h4 style="font-size:1rem">Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>dashboard/contact_directory">
                            <h4 style="font-size:1rem">Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
                        </a></li>
                    <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                            <h4 style="font-size:1rem">Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
                        </a></li>
                    <li><a href="<?=base_url()?>page/listing_page/e_filling">
                        <h4 style="font-size:1rem">E-Registration <span style="float: right;"> ای رجسٹریشن</span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>dashboard/events">
                            <h4 style="font-size:1rem">Gallery <span style="float: right;">گیلری </span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>page/pages/media_banners">
                            <h4 style="font-size:1rem">Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
                        </a></li>
                </ul>
            </article> -->
        </div>
    </section>


    <section class="scroll" id="details" tabindex="8">

        <header style="background:#333C83 !important">
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none" style="fill: #21285c;">
                <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
            </svg>
            <h2 style="">پنجاب ریونیو اکیڈمی</h2>

        </header>
        <div class="feature" style="margin-top: -40px">
            <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
                <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
            </svg>
            <div class="feature-content" >
                <a class="call-to-action" href="<?= base_url() ?>assets/files/Land_Revenue_Laws-I_PPT.pdf" target="_blank">
                
                <p class="call-out" ><span style="float:right;">Details</span></p>
                </a>
            </div>
        </div>
        <div  style="margin-top: -5rem !important; !important;width:95%; cursor:pointer; position: absolute;margin-left:645px">
                <a class="call-to-action" href="https://online.pra-borpunjab.gov.pk/Account/Signup" target="_blank">
                    <p  style="visibility:hidden;color:white; margin-left:70px;font-size:2rem;text-decoration: underline;">REGISTER</p>
                </a>
    </div>
        <div class="content-container" style="background:#333C83">

<article class="content-item">
    <h3 style=""><a href="<?php echo base_url() ?>assets/files/PaperFPOE.pdf" target="_blank"> Paper - I for FPOE <span style="float: right"></span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/p1.jpeg);" href="<?php echo base_url() ?>assets/files/PaperFPOE.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?=base_url()?>assets/files/PaperNonFPOE.pdf" target="_blank">Paper - I for non FPOE   <span style="float: right"></span> </a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/p2n.jpeg);" href="<?=base_url()?>assets/files/PaperNonFPOE.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?=base_url()?>assets/files/Paper2.pdf" target="_blank"> Paper - II for FPOE <span style="float: right"></span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/lrlp211.jpeg);" href="<?=base_url()?>assets/files/Paper2.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?= base_url() ?>assets/files/LandRevenueAct.pdf" target="_blank"> Land Revenue Act <span style="float: right;
    margin-right: 20px;
">لینڈ ریونیو ایکٹ</span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/ik.jpeg);" href="<?= base_url() ?>assets/files/LandRevenueAct.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<!-- <article class="content-item testDiv">
    <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9211 <span style="float: right">ٹال فری نمبر</span></a></h3>
    <ul>
        <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                <h4>Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/contact_directory">
                <h4>Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                <h4>Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/e_filling">
            <h4>E-filing <span style="float: right;">ای فائلنگ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/events">
                <h4>Gallery <span style="float: right;">گیلری </span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>page/pages/media_banners">
                <h4>Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
            </a></li>
    </ul>
</article> -->
</div> 
        <!-- <div class="content-container">

            <article class="content-item">
                <h3 style=""><a href="<?php echo base_url() ?>page/pages/9211_virtual_governance"> V-Governance <span style="float: right">ورچوئل گورننس</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/education.jpg);" href="<?php echo base_url() ?>page/pages/9211_virtual_governance">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="<?=base_url()?>page/listing_page/digital_access"> Digital Access <span style="float: right">ڈیجیٹل رسائی</span> </a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/BackDropwithBaba.jpg);" href="<?=base_url()?>page/listing_page/digital_access">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="<?=base_url()?>page/listing_page/social_audit"> Social Audit <span style="float: right">سماجی احتساب</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/SocialAudit.jpg);" href="<?=base_url()?>page/listing_page/social_audit">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item">
                <h3 style=""><a href="<?= base_url() ?>aboutus/laws"> Laws/ Policy <span style="float: right">قوانین/ پالیسی</span></a></h3>
                <div class="news-item" data-type="featured" data-whatsnew="commerce">
                    <a style="background-image: url(<?= base_url() ?>assets/images/home/legislation.png);" href="<?= base_url() ?>aboutus/laws">
                        <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                    </a>
                </div>
            </article>

            <article class="content-item testDiv">
                <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9211 <span style="float: right">ٹال فری نمبر</span></a></h3>
                <ul>
                    <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                            <h4>Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>dashboard/contact_directory">
                            <h4>Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
                        </a></li>
                    <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                            <h4>Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
                        </a></li>
                    <li><a href="<?=base_url()?>page/listing_page/e_filling">
                        <h4>E-filing <span style="float: right;">ای فائلنگ</span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>dashboard/events">
                            <h4>Gallery <span style="float: right;">گیلری </span></h4>
                        </a></li>
                    <li><a href="<?= base_url() ?>page/pages/media_banners">
                            <h4>Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
                        </a></li>
                </ul>
            </article>
        </div> -->
    </section>
    

<!-- <section class="scroll" id="open">

    <header>
        <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
            <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
        </svg>

        <h2>محکمہ لائیو سٹاک پنجاب</h2>

    </header>

    <div class="feature">
        <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
            <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
        </svg>
        <div class="feature-content">
            <a class="call-to-action" href="<?= base_url() ?>page/listing_page/my_own_livestock_business">
                <p class="call-out" style="">Business Corner <span style="float: right">کاروبار و روزگار</span></p>
            </a>
        </div>
    </div>

    <div class="content-container">
<div class="grid_3" style="padding: 0;">
        <article class="content-item testDiv">
            <h3><a href="<?=base_url()?>page/listing_page/my_own_livestock_business"> My Own Livestock Business <span style="float: right;margin-right: 5px;">میرا اپنا لائیوسٹاک بزنس</span></a></h3>
            <ul>
                <li>
                    <a href="<?= base_url() ?>infodesk/booklets"><h4>Business Startup<span style="float: right;margin-right: 5px;">کاروبار ابتدائیہ</span></h4></a>
                </li>
                <li>
                    <a href="<?= base_url() ?>page/listing_page/feasibility_studies"><h4>Feasibility Studies<span style="float: right;margin-right: 5px;">امکانات کا مطالعہ</span></h4> </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>page/listing_page/government_incentives"><h4>Government Incentives<span style="float: right;margin-right: 5px;">حکومت ترغیبات</span></h4> </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>page/listing_page/extension_services"><h4>Extension Services<span style="float: right;margin-right: 5px;">توسیعی سروس</span></h4> </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>page/listing_page/district_profiles"><h4>District Profiles<span style="float: right;margin-right: 5px;">ضلع پروفائل</span></h4> </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>page/listing_page/livestock_farms_profiles"><h4>Livestock Farms Profiles<span style="float: right;margin-right: 5px;">لائیوسٹاک فارمز پروفائل</span></h4> </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>page/listing_page/mass_vaccination_campaign"><h4>Mass Vaccination Campaign<span style="float: right;margin-right: 5px;">ماس ویکسینیشن مہم</span></h4> </a>
                </li>

            </ul>
        </article>
</div>
<div class="grid_3" style="padding: 0;">
        <article class="content-item transparency testDiv">
            <h3><a href="<?=base_url()?>page/listing_page/data_catalogue">

                    Data Catalogue<span style="float: right;margin-right: 5px;">ڈیٹا کا اعداد و شمار</span></a ></h3>
            <h4>Pakistan- 25th Largest Economy in the World (PPP Basis)</h4><br>

            <div class="clearfix"></div>
            <h4 style="margin-top: 30px;">One of the Next Eleven<br> Emerging Economies</h4>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                 y="0px" viewBox="134 -134 300 300" width="300px" height="300px"
                 enable-background="new 134 -134 300 300" xml:space="preserve" class="transparency-donut">
<path class="publicEd" fill="#5D92B6" d="M311-45c33.8,14.8,48.7,54,34,87.8c-3,6.8-6.8,12.6-11.5,17.8l62.1,55.2c10.3-11.6,19.3-24.7,25.9-39.7
    c33-75.9-1.4-164.2-77.3-197.4L311-45z"/>

                <path class="socialServices" fill="#81B053" d="M289.6,82.5c-36.7,2.8-68.6-23.7-72-60.4c-1.2-13.3,1.6-25.8,7.2-36.8l-73.7-38.2
    C138.3-28.2,131.9,0,134.6,29.7c7.5,82.5,80.2,143.2,162.6,135.6L289.6,82.5z"/>

                <path class="capital" fill="#416e92"
                      d="M224.8-14.7c1.3-2.5,2.8-5,4.4-7.3l0,0l-68.6-46.8c-3.5,5.1-6.6,10.4-9.5,15.9L224.8-14.7z"/>

                <path class="higherEd" fill="#a92807"
                      d="M297.2,165.4c36.6-3.3,72.1-19.8,98.4-49.6l-62.1-55.2c-11.8,13.3-27.6,20.4-43.9,21.9L297.2,165.4z"/>

                <path class="law" fill="#8b0f0a"
                      d="M344.2-121.3c-14.1-6.1-29.3-10.4-45.4-11.9l-8,82.8c7.2,0.7,13.9,2.6,20.2,5.4L344.2-121.3z"/>

                <path class="gov" fill="#2AAAE2"
                      d="M298.8-133.2c-21.1-2.1-43.4,0-64.9,7.7l28,78.5c9.6-3.4,19.4-4.3,28.9-3.4L298.8-133.2z"/>

                <path class="transportation" fill="#F15C27"
                      d="M233.9-125.6c-20.1,7.2-39,18.9-55.2,35.1l58.6,58.9C244.5-38.8,253-43.8,262-47L233.9-125.6z"/>

                <path class="debt" fill="#892785"
                      d="M178.6-90.6c-6.5,6.6-12.6,13.8-18,21.8L229.2-22c2.4-3.5,5.2-6.7,8.1-9.6L178.6-90.6z"/>

</svg>

            <ul>
                <li class="transItem socialServices">
                    <i class="transIcon"></i>
                    <span class="percentage">62%</span>
                    <span class="category">Milk Production</span>
                </li>
                <li class="transItem publicEd active">
                    <i class="transIcon"></i>
                    <span class="percentage">55%</span>
                    <span class="category">Eggs Production</span>
                </li>
                <li class="transItem higherEd">
                    <i class="transIcon"></i>
                    <span class="percentage">65%</span>
                    <span class="category">Pultry Meat</span>
                </li>
                <li class="transItem gov">
                    <i class="transIcon"></i>
                    <span class="percentage">59%</span>
                    <span class="category">Hides</span>
                </li>
                <li class="transItem transportation">
                    <i class="transIcon"></i>
                    <span class="percentage">43%</span>
                    <span class="category">Beef</span>
                </li>
                <li class="transItem law">
                    <i class="transIcon"></i>
                    <span class="percentage">33%</span>
                    <span class="category">Mutton</span>
                </li>
                <li class="transItem debt">
                    <i class="transIcon"></i>
                    <span class="percentage">32%</span>
                    <span class="category">Skin</span>
                </li>
                <li class="transItem capital">
                    <i class="transIcon"></i>
                    <span class="percentage">24%</span>
                    <span class="category">Wool</span>
                </li>
            </ul>

            <p class="description" style="font-weight: 800">
                <a>Share of Livestock Products of Punjab in Pakistan</a>
            </p>
        </article>
</div>

        <article class="content-item">
            <h3><a href="<?= base_url() ?>dashboard/events"> Current News & Alerts<span style="float: right;margin-right: 5px;">موجودہ خبریں</span></a></h3>

            <div id="slider">
                <?php foreach ($flood_advertisement as $row) { ?>
                    <a class="fancybox" rel="ligthbox-news" href="<?= $row->image_url ?>">
                        <img style="height: 210px" src="<?= $row->image_url ?>"/>
                    </a>
                <?php } ?>
            </div>

        </article>

        <article class="tweets">
            <h3><a href="<?= base_url() ?>infodesk/important_links"> Important Links<span style="float: right;margin-right: 5px;">اہم لنکس</span></a></h3>
            <ul>
                <li>
                    <a href="<?= base_url() ?>infodesk/important_links"><h4>Academics <span style="float: right">تعلیمی ادارے</span></h4> <p></p></a>
                </li>
                <li>
                    <a href="<?= base_url() ?>infodesk/important_links"><h4>Industrial <span style="float: right">صنعتی </span></h4> <p></p></a>
                </li>
                <li>
                    <a href="<?= base_url() ?>infodesk/important_links"><h4>National <span style="float: right">قومی </span></h4> <p></p></a>
                </li>
                <li>
                    <a href="<?= base_url() ?>infodesk/important_links"><h4>International <span style="float: right">بین الاقوامی </span></h4> <p></p></a>
                </li>
                <li>
                    <a href="<?= base_url() ?>infodesk/important_links"><h4>Miscellaneous &amp; Articles <span style="float: right"> دیگر / ارٹیکلز</span></h4> <p></p></a>
                </li>
            </ul>
        </article>

    </div>

</section> -->
<!-- <section class="scroll" id="open">

<header>
    <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
        <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
    </svg>

    <h2 style="">پنجاب ریونیو اکیڈمی</h2>


</header>

<div class="feature">
    <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none" style="fill: #27415c;">
        <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
    </svg>
    <div class="feature-content">
        <a class="call-to-action" href="<?= base_url() ?>page/listing_page/my_own_livestock_business">
            <p class="call-out" style="">Business Corner <span style="float: right">کاروبار و روزگار</span></p>
        </a>
    </div>
</div>

<div class="content-container" style=" background:#001E6C;">
<div class="grid_3" style="padding: 0;">
    <article class="content-item testDiv">
        <h3 style="font-size:12px"><a href="<?=base_url()?>page/listing_page/my_own_livestock_business" target="_blank"> My Own Livestock Business <span style="float: right;margin-right: 5px;">میرا اپنا لائیوسٹاک بزنس</span></a></h3>
        <ul style="width: 100%">
            <li>
                <a href="<?= base_url() ?>infodesk/booklets" target="_blank"><h4>Business Startup<span style="float: right;margin-right: 5px;">کاروبار ابتدائیہ</span></h4></a>
            </li>
            <li>
                <a href="<?= base_url() ?>page/listing_page/feasibility_studies" target="_blank"><h4>Feasibility Studies<span style="float: right;margin-right: 5px;">امکانات کا مطالعہ</span></h4> </a>
            </li>
            <li>
                <a href="<?= base_url() ?>page/listing_page/government_incentives" target="_blank"><h4>Government Incentives<span style="float: right;margin-right: 5px;">حکومت ترغیبات</span></h4> </a>
            </li>
            <li>
                <a href="<?= base_url() ?>page/listing_page/extension_services" target="_blank"><h4>Extension Services<span style="float: right;margin-right: 5px;">توسیعی سروس</span></h4> </a>
            </li>
            <li>
                <a href="<?= base_url() ?>page/listing_page/district_profiles" target="_blank"><h4>District Profiles<span style="float: right;margin-right: 5px;">ضلع پروفائل</span></h4> </a>
            </li>
            <li>
                <a href="<?= base_url() ?>page/listing_page/livestock_farms_profiles" target="_blank"><h4>Livestock Farms Profiles<span style="float: right;margin-right: 5px;">لائیوسٹاک فارمز پروفائل</span></h4> </a>
            </li>
            <li>
                <a href="<?= base_url() ?>page/listing_page/mass_vaccination_campaign" target="_blank"><h4>Mass Vaccination Campaign<span style="float: right;margin-right: 5px;">ماس ویکسینیشن مہم</span></h4> </a>
            </li>

        </ul>
    </article>
</div>
<div class="grid_3" style="padding: 0;">
    <article class="content-item transparency testDiv">
        <h3><a href="<?=base_url()?>page/listing_page/data_catalogue" target="_blank">

                Data Catalogue<span style="float: right;margin-right: 5px;">ڈیٹا کا اعداد و شمار</span></a ></h3>
        <h4>Pakistan- 25th Largest Economy in the World (PPP Basis)</h4><br>

        <div class="clearfix"></div>
        <h4 style="margin-top: 50px;">One of the Next Eleven<br> Emerging Economies</h4>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
             y="0px" viewBox="134 -134 300 300" width="300px" height="300px"
             enable-background="new 134 -134 300 300" xml:space="preserve" class="transparency-donut">
<path class="publicEd" fill="#5D92B6" d="M311-45c33.8,14.8,48.7,54,34,87.8c-3,6.8-6.8,12.6-11.5,17.8l62.1,55.2c10.3-11.6,19.3-24.7,25.9-39.7
c33-75.9-1.4-164.2-77.3-197.4L311-45z"/>

            <path class="socialServices" fill="#81B053" d="M289.6,82.5c-36.7,2.8-68.6-23.7-72-60.4c-1.2-13.3,1.6-25.8,7.2-36.8l-73.7-38.2
C138.3-28.2,131.9,0,134.6,29.7c7.5,82.5,80.2,143.2,162.6,135.6L289.6,82.5z"/>

            <path class="capital" fill="#416e92"
                  d="M224.8-14.7c1.3-2.5,2.8-5,4.4-7.3l0,0l-68.6-46.8c-3.5,5.1-6.6,10.4-9.5,15.9L224.8-14.7z"/>

            <path class="higherEd" fill="#a92807"
                  d="M297.2,165.4c36.6-3.3,72.1-19.8,98.4-49.6l-62.1-55.2c-11.8,13.3-27.6,20.4-43.9,21.9L297.2,165.4z"/>

            <path class="law" fill="#8b0f0a"
                  d="M344.2-121.3c-14.1-6.1-29.3-10.4-45.4-11.9l-8,82.8c7.2,0.7,13.9,2.6,20.2,5.4L344.2-121.3z"/>

            <path class="gov" fill="#2AAAE2"
                  d="M298.8-133.2c-21.1-2.1-43.4,0-64.9,7.7l28,78.5c9.6-3.4,19.4-4.3,28.9-3.4L298.8-133.2z"/>

            <path class="transportation" fill="#F15C27"
                  d="M233.9-125.6c-20.1,7.2-39,18.9-55.2,35.1l58.6,58.9C244.5-38.8,253-43.8,262-47L233.9-125.6z"/>

            <path class="debt" fill="#892785"
                  d="M178.6-90.6c-6.5,6.6-12.6,13.8-18,21.8L229.2-22c2.4-3.5,5.2-6.7,8.1-9.6L178.6-90.6z"/>

</svg>

        <ul>
            <li class="transItem socialServices">
                <i class="transIcon"></i>
                <span class="percentage">62%</span>
                <span class="category">Milk Production</span>
            </li>
            <li class="transItem publicEd active">
                <i class="transIcon"></i>
                <span class="percentage">55%</span>
                <span class="category">Eggs Production</span>
            </li>
            <li class="transItem higherEd">
                <i class="transIcon"></i>
                <span class="percentage">65%</span>
                <span class="category">Pultry Meat</span>
            </li>
            <li class="transItem gov">
                <i class="transIcon"></i>
                <span class="percentage">59%</span>
                <span class="category">Hides</span>
            </li>
            <li class="transItem transportation">
                <i class="transIcon"></i>
                <span class="percentage">43%</span>
                <span class="category">Beef</span>
            </li>
            <li class="transItem law">
                <i class="transIcon"></i>
                <span class="percentage">33%</span>
                <span class="category">Mutton</span>
            </li>
            <li class="transItem debt">
                <i class="transIcon"></i>
                <span class="percentage">32%</span>
                <span class="category">Skin</span>
            </li>
            <li class="transItem capital">
                <i class="transIcon"></i>
                <span class="percentage">24%</span>
                <span class="category">Wool</span>
            </li>
        </ul>

        <p class="description" style="font-weight: 800">
            <a>Share of Livestock Products of Punjab in Pakistan</a>
        </p>
    </article>
</div>

    <article class="content-item">
        <h3><a href="<?= base_url() ?>dashboard/events" target="_blank"> Current News & Alerts<span style="float: right;margin-right: 5px;">موجودہ خبریں</span></a></h3>
         <div id="slider">
                <?php foreach ($flood_advertisement as $row) { ?>
                    <a class="fancybox" rel="ligthbox-news" href="<?= $row->image_url ?>">
                        <img style="height: 210px" src="<?= $row->image_url ?>"/>
                    </a>
                <?php } ?>
            </div>  
         <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/open-p.jpg);" href="<?=base_url()?>page/listing_page/social_audit" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>

    </article>

    <article class="tweets">
        <h3><a href="<?= base_url() ?>infodesk/important_links" target="_blank"> Important Links<span style="float: right;margin-right: 5px;
    margin-right: 20px;
">اہم لنکس</span></a></h3>
        <ul>
            <li>
                <a href="<?= base_url() ?>infodesk/important_links" target="_blank"><h4>Academics <span style="float: right">تعلیمی ادارے</span></h4> <p></p></a>
            </li>
            <li>
                <a href="<?= base_url() ?>infodesk/important_links" target="_blank"><h4>Industrial <span style="float: right">صنعتی </span></h4> <p></p></a>
            </li>
            <li>
                <a href="<?= base_url() ?>infodesk/important_links" target="_blank"><h4>National <span style="float: right">قومی </span></h4> <p></p></a>
            </li>
            <li>
                <a href="<?= base_url() ?>infodesk/important_links" target="_blank"><h4>International <span style="float: right">بین الاقوامی </span></h4> <p></p></a>
            </li>
            <li>
                <a href="<?= base_url() ?>infodesk/important_links" target="_blank"><h4>Miscellaneous &amp; Articles <span style="float: right"> دیگر / ارٹیکلز</span></h4> <p></p></a>
            </li>
        </ul>
    </article>

</div>

</section> -->
    <section class="scroll" id="r_n_d" tabindex="9">

<header style="background: #0093AB;">
    <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none" style="fill: #017486;">
        <polygon class="angle" points="100,96 65.1,0 0,0 0,96 "/>
    </svg>
    <h2 style="">پنجاب ریونیو اکیڈمی</h2>

</header>
<div class="feature" >
    <svg x="0px" y="0px" viewBox="0 0 100 96" preserveAspectRatio="none">
        <polygon class="angle" points="0,96 34.9,0 100,0 100,96 "/>
    </svg>
    <div class="feature-content" >
        <a class="call-to-action" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/new_training_paradigm_fli97.pdf" target="_blank">
        
        <p class="call-out" style="">R&D<span style="float:right;">  ریسرچ اینڈ ڈیویلوپمنٹ</span></p>
        </a>
    </div>
</div>
 
        <div class="content-container" style="background:#0093AB;">

<article class="content-item">
    <h3 style=""><a href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/new_training_paradigm_fli97.pdf" target="_blank"> New Training Paradigm <span style="float: right"></span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="image-rendering: -webkit-optimize-contrast;background-image: url(<?= base_url() ?>assets/images/home/ntp.png);" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/new_training_paradigm_fli97.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/property_registration_systems-_concepts_practices_1_jg13v.pdf" target="_blank"> Property Registration Systems Concepts <span style="float: right">  </span> </a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="background-image: url(<?= base_url() ?>assets/images/home/prs.png);" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/property_registration_systems-_concepts_practices_1_jg13v.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="<?=base_url()?>https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_urban_land_titling_registration_system_1_6n2y7.pdf" target="_blank"><i style="    position: relative;
    top: -3px;
    font-style: normal;
    font-weight: bold;">The Punjab Urban Land Titling Registration System</i><span style="float: right;margin-top:-18px">     </span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style=" image-rendering: -webkit-optimize-contrast;background-image: url(<?= base_url() ?>assets/images/home/ult2.jpeg);" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_urban_land_titling_registration_system_1_6n2y7.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<article class="content-item">
    <h3 style=""><a href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/existing_legal_framework_for_creating_punjab_urban_land_titling_registration_system_0h6ae.pdf" target="_blank"> Scope of Existing Legal Framework <span style="float: right;
    margin-right: 20px;
"></span></a></h3>
    <div class="news-item" data-type="featured" data-whatsnew="commerce">
        <a style="image-rendering: -webkit-optimize-contrast;background-image: url(<?= base_url() ?>assets/images/home/sel.png);" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/existing_legal_framework_for_creating_punjab_urban_land_titling_registration_system_0h6ae.pdf" target="_blank">
            <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
        </a>
    </div>
</article>

<!-- <article class="content-item testDiv">
    <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9211 <span style="float: right">ٹال فری نمبر</span></a></h3>
    <ul>
        <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                <h4>Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/contact_directory">
                <h4>Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                <h4>Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
            </a></li>
        <li><a href="<?=base_url()?>page/listing_page/e_filling">
            <h4>E-filing <span style="float: right;">ای فائلنگ</span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>dashboard/events">
                <h4>Gallery <span style="float: right;">گیلری </span></h4>
            </a></li>
        <li><a href="<?= base_url() ?>page/pages/media_banners">
                <h4>Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
            </a></li>
    </ul>
</article> -->
</div> 
<!-- <div class="content-container">

    <article class="content-item">
        <h3 style=""><a href="<?php echo base_url() ?>page/pages/9211_virtual_governance"> V-Governance <span style="float: right">ورچوئل گورننس</span></a></h3>
        <div class="news-item" data-type="featured" data-whatsnew="commerce">
            <a style="background-image: url(<?= base_url() ?>assets/images/home/education.jpg);" href="<?php echo base_url() ?>page/pages/9211_virtual_governance">
                <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
                     </a>
        </div>
    </article>

    <article class="content-item">
        <h3 style=""><a href="<?=base_url()?>page/listing_page/digital_access"> Digital Access <span style="float: right">ڈیجیٹل رسائی</span> </a></h3>
        <div class="news-item" data-type="featured" data-whatsnew="commerce">
            <a style="background-image: url(<?= base_url() ?>assets/images/home/BackDropwithBaba.jpg);" href="<?=base_url()?>page/listing_page/digital_access">
                <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
            </a>
        </div>
    </article>

    <article class="content-item">
        <h3 style=""><a href="<?=base_url()?>page/listing_page/social_audit"> Social Audit <span style="float: right">سماجی احتساب</span></a></h3>
        <div class="news-item" data-type="featured" data-whatsnew="commerce">
            <a style="background-image: url(<?= base_url() ?>assets/images/home/SocialAudit.jpg);" href="<?=base_url()?>page/listing_page/social_audit">
                <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
            </a>
        </div>
    </article>

    <article class="content-item">
        <h3 style=""><a href="<?= base_url() ?>aboutus/laws"> Laws/ Policy <span style="float: right">قوانین/ پالیسی</span></a></h3>
        <div class="news-item" data-type="featured" data-whatsnew="commerce">
            <a style="background-image: url(<?= base_url() ?>assets/images/home/legislation.png);" href="<?= base_url() ?>aboutus/laws">
                <h5 style="text-align: right;padding-right: 40px;">مزید</h5>
            </a>
        </div>
    </article>

    <article class="content-item testDiv">
        <h3 style=""><a href="<?= base_url() ?>page/listing_page/080009211/"> 08000-9211 <span style="float: right">ٹال فری نمبر</span></a></h3>
        <ul>
            <li><a href="<?= base_url() ?>page/listing_page/public_info_officers">
                    <h4>Public Info Officers <span style="float: right;">پبلک انفارمیشن افیسرذ</span></h4>
                </a></li>
            <li><a href="<?= base_url() ?>dashboard/contact_directory">
                    <h4>Contact Directory <span style="float: right;">رابطہ کی ڈائریکٹری</span></h4>
                </a></li>
            <li><a href="<?=base_url()?>page/listing_page/data_catalogue">
                    <h4>Data Catalogue <span style="float: right">اعداد و شمار </span></h4>
                </a></li>
            <li><a href="<?=base_url()?>page/listing_page/e_filling">
                <h4>E-filing <span style="float: right;">ای فائلنگ</span></h4>
                </a></li>
            <li><a href="<?= base_url() ?>dashboard/events">
                    <h4>Gallery <span style="float: right;">گیلری </span></h4>
                </a></li>
            <li><a href="<?= base_url() ?>page/pages/media_banners">
                    <h4>Media Banners<span style="float: right;">میڈیا بینرز</span></h4>
                </a></li>
        </ul>
    </article>
</div> -->
</section>

</main>
<div id="overlay"></div>

<ul class="section-nav">

    <li><a href="#search-banner" class="search active" title="Home">Search</a></li>
    <!-- <?php if(!empty($array)): foreach($array as $key=>$subArray):
        ?>
    <li><a href="#slider-<?=$key?>" class="locals" title="Slider"></a></li>
    <?php endforeach; endif;?> -->
    <li><a href="#locals" class="locals" title="The Punjab Revenue Academy">The Punjab Revenue Academy</a></li>
    <li><a href="#gov-news" class="gov-news" title="Key Initiatives">Key Initiatives</a></li>
    <li><a href="#open1  " class="open" title="Newly Recruited Patwaris' Training Programme">Newly Recruited Patwaris' Training Programme</a></li>
    <li><a href="#citizen" class="citizen" title="Syllabus">Syllabus</a></li>
    <li><a href="#digital" class="digital" title="Exams">Exams</a></li>
    <li><a href="#rti" class="rti" title="RTI & Beyond">RTI & Beyond</a></li>
    <li><a href="#details" class="details" title="Details">Details</a></li>
    <!-- <li><a href="#open" class="details" title="Details">Business Corner</a></li> -->
    <li><a href="#r_n_d" class="r_n_d" title="R&D">R&D</a></li>
</ul>

<div class="go-top">
	
</div>
<a href="#" id="backToTop" title="Back to Top">Back to Top</a>
<script>
    function initMap() {
        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 31.4857, lng: 74.3265},
            scrollwheel: false,
            zoom: 12
        });
    }
    $(function(){

//Scroll event
$(window).scroll(function(){
  var scrolled = $(window).scrollTop();
  if (scrolled > 200) $('.go-top').fadeIn('slow');
  if (scrolled < 200) $('.go-top').fadeOut('slow');
});

//Click event
$('.go-top').click(function () {
  $("html, body").animate({ scrollTop: "0" },  500);
});

});
document.addEventListener('keydown', e => {
  if (e.key === 'ArrowDown' || e.key === 'ArrowRight') {
    const section = document.querySelector('section:focus ~ section[tabindex]') || document.querySelector('section[tabindex]');
    section.focus();
    window.scrollBy(0,-200)
  } else if (e.key === 'ArrowUp' || e.key === 'ArrowLeft') {
    [...document.querySelectorAll('section[tabindex]')].forEach((section, index, arr) => {
      if (section.matches(':focus')) {
        (arr[index - 1] || arr.pop()).focus();
      }
    });
  }
  
});

</script>
<script src="<?= base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.marquee.min.js"></script>
<script src="<?= base_url() ?>assets/js/easing.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.pause.js"></script>       