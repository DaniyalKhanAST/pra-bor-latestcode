<style>
  .btn-self-enrollment{
    background:#05445E;
    color:white;
    border:1px solid black;
    width:6rem;
    height:3rem;
   

}
.emp-ver-main{
  width:40%;
  margin:auto;

}
</style>
<main id="main" style="margin-top: 130px;">
  <section class>
  <div class="container">
        <?php if($this->session->flashdata('message')):?>
        <div class="alert alert-dark">
              <?php echo $this->session->flashdata('message');?>
      </div><br>
      <?php endif;?>
    <div class="emp-ver-main">
    <h2 style="margin-top: -20px;text-align:center;font-weight: bolder;" class="">Verification Code</h2>
    <p  style="font-size:19px">A verification code (VFC) has been sent to your mobile phone number or your email address; please enter verification code (VFC) below to complete the Enrollment Process:</p>
    <?php echo !empty($this->session->flashdata('error'))? '<div style="color:red">'.$this->session->flashdata('error').'</div>' : ''?>
        <form name="verification_code" method="post" action="<?=base_url()?>dashboard/emp_verification">
        <div class="form-group row">
        
            <label for="staticEmail" class="col-sm-4 col-form-label">Enter VFC</label>
            <div class="col-sm-6">
            <input type="password" name="pin_code" autocomplete="current-password"  id="id_password">
            <input type="hidden" name="cnic" value="<?php echo !empty($this->session->userdata('cnic'))? $this->session->userdata('cnic'):''?>">
            <input type="hidden" name="code_verification" value="1">
            <i class="fa fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer;"></i>
            </div>
                              

        </div>
        <p style='font-weight: bolder;color:#05445E'>Resend VFC</p>
        <button style="float:left"  class="btn-self-enrollment">Submit</button>
      </div>
      </form>
  </div>
  </div>
</section>
<script>
  const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#id_password');
 
  togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});
</script>
</main>