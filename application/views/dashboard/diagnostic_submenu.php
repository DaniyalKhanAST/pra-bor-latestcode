<main id="main" style="margin-top: 130px;">
    
    <section style="margin-bottom: 3px;"><h2 style="font-family:Open Sans Condensed, Open Sans, Helmut, sans-serif;">Diagnostics<span style="float: right;">تشخیص</span></h2></section>
   

    
    <section style="margin-bottom: 2px;">
        <h2 class="bukatutup0"><a href="#" class="clickme">Lab Tests  <span style="float: right;">لیب ٹیسٹ</span></a></h2>
        <div class="container " id="target0" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($lab_tests->page_detail)){
                    echo $lab_tests->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom:1px;">
        <h2 class="bukatutup1">   <a href="#" class="clickme">Radiology  <span style="float: right;">ایکس رے وغیرہ</span></a></h2>
        <div class="container " id="target1" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($radiology->page_detail)){
                    echo $radiology->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    


</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
     $('.bukatutup0').click(function() {
        $('#target0').toggle('slow');
    });
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    
</script>