<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Production Support

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Production Support </h3>
            <p> This component aims to increase livestock production and productivity in target areas by,
                <ol class="container" type="i">
                    <li> Strengthening the capacity of targeted communities - including vulnerable women - private sector

                        players, and of the Livestock and Dairy Development Department, and </li>

                    <li> Supporting the construction of small-scale, community-managed infrastructure schemes aimed at

                        enhancing livestock productivity. </li>

                </ol>


                The component would facilitate the engagement of the private sector for the provision of technical assistance

                and capacity building activities, and the provision of a number of other services. This component includes

                following four sub-components:
                <ul class="container">
                    <li><a href="<?php echo base_url().'dashboard/community_capacity_building/';?>">Community Capacity Building (Community Organizations) </a></li>
                    <li><a href="<?php echo base_url().'dashboard/community_infrastructure/';?>"> Community Infrastructure </a></li>
                    <li><a href="<?php echo base_url().'dashboard/capacity_building_of_private_sector/';?>">Capacity Building of Private Sector</a> </li>
                    <li><a href="<?php echo base_url().'dashboard/capacity_building_of_livestock_department';?>"> Capacity Building of L&amp;DD (Livestock and Dairy Development Department) </a></li>
                </ul>


                <br>

            </p>
            <div class="container">
                <h3>Article Gallery</h3>

                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00168.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00168.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00182.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00182.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00187.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00187.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00198.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00198.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC00204.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC00204.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSCN3801.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSCN3801.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_103342910.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_103342910.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_103704090.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_103704090.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_103743622.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_103743622.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0001.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0001.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0029.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0029.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0030.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0030.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0031.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0031.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20161208-WA0033.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20161208-WA0033.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0215.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0215.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0222.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0222.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0229.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0229.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0231.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0231.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170128-WA0234.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170128-WA0234.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170129-WA0009.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170129-WA0009.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0023.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0023.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0033.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0033.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/SANY0029.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/SANY0029.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>






        </div><!-- /.container -->
    </section>


</main>