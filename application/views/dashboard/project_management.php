<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Project Management

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Project Management </h3>
            <p> Under this Component a Project Coordination Unit (PCU) will be established and housed at Lahore. This component aims at supporting the project coordination and technical responsibilities of the Project Coordination Unit. In addition, a Special Financing Facility to support any activity that requires additional support and/or expanding any promising/well performing activity will be established under this component.



            <ul class="container">
                <li><a href="<?php echo base_url().'dashboard/component_objectives_and_indicators';?>">Component Objectives and Indicators</a> </li>
                <li><a href="<?php echo base_url().'dashboard/component_activities_and_output';?>"> Component Activities and Output</a></li>
            </ul>

            <br>

            </p>

            <div class="container">
                <h3>Article Gallery</h3>

                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0030p.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0030p.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0034p.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0034p.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0039p.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0039p.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG-20170223-WA0045p.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG-20170223-WA0045p.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>




            </div>





        </div><!-- /.container -->
    </section>


</main>