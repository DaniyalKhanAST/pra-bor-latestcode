<main id="main" style="margin-top: 130px;">

    <section>
        <h2>
            Capacity Building of L&amp;DD (Livestock Department)

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Capacity Building of L&amp;DD (Livestock Department): </h3>
            <p>Policy and Regulatory Framework: This component will focus on two policy areas
                <ol class="container" type="i">
                    <li> regulation of livestock markets </li>
                    <li> certification and regulation of private animal health service providers </li>
                </ol>

                The Government has made a good start in developing a Livestock Strategy. However, there is a need to put in place a more strategic vision
                about the sector, the future role of Government extension services and the role of the private sector. The project
                will provide the opportunity to provide international expertise and venues to discuss some of these fundamental
                issues and translate them into operational guidelines for the department to test in the project districts. The
                impact of these two policy areas and their scaling-up potential could be significant.
                <br>
                This sub-component is also aimed at addressing some of the critical constraints faced by the livestock department
                such as strategic planning, limited mobility, operational funds, equipment and limited travel allowance. The field
                staff will also be provided any additional refresher training with support for mobility which is identified as critical.
                To facilitate this process, the PCU will hire 08 women Doctors of Veterinary Medicine (DVMs) and 12 female
                Veterinary Assistants to provide a system of regular training, serve as facilitator for LFFS, provide technical
                support to the CLEWS as well as undertake some of the mainstream tasks assigned by the department. The
                women DVMs and VAs will be attached to the NGO for the project duration which will provide them transport
                and accommodation facilities and facilitate their interaction in the field. The project will also provide support to
                the female VAs. These women staff members will be facilitated, and preferred to join the department on regular
                basis against future departmental vacancies.

                <br>

                The lack of adequate and good quality vaccines and de-worming medicines is one of the most critical constraints
                in ensuring protection of animals against disease. While the Government has a policy to supply these at
                subsidized rates, in practice it does not have the resources to procure the quality and quantity required. To
                ensure timely procurement and effective protection, IFAD and District Governments will contribute to a fund
                which will ensure a sustainable mechanism for supplying vaccines for the project districts. This incentive will be
                paid to the CLEWs, private para-vets or Government livestock staff who participate in the vaccination campaigns.
                The fund established for the supplies will be subject to annual audits as is the normal practice for funds instituted
                by the Government for similar purposes.

                <br>
            </p>

        </div><!-- /.container -->
    </section>


</main>