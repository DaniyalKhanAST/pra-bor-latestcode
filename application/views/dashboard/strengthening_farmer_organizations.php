<main id="main" style="margin-top: 130px;">

    <section>
        <h2>
            Strengthening Farmer Organizations

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Strengthening Farmer Organizations: </h3>
            <p> The NGO recruited to strengthen community organizations will also be charged with the task of identifying those

                village organizations in which the members are interested in organizing into dairy marketing groups or small

                ruminant marketing groups. It is envisaged that 500 dairy marketing groups and 750 small ruminant village

                groups clustered in 40 marketing groups will be formed during the project period. The NGO will be provided

                operating costs for organizing, mobilizing, training and monitoring these groups.


                <br>

                <p>About 250 men and 250 women members of the dairy and small ruminant groups interested in

                entrepreneurial trainings will be identified by social mobilization organization and trained and supported to

                undertake entrepreneurship in veterinary supplies, feed and nutritional inputs and sale of milk and milk products,

                etc.</p>
                <br>
                <p>
                    The members of DMGs/Associations, Small Ruminant Marketing Groups will be offered opportunities to

                    participate in technical sessions on improved animal husbandry practices including management practices,

                    breeding and feeding practices and marketing techniques.</p>
                <br>
                <p>The exchange visits of 3000 beneficiaries with 70% women to Farmer&#39;s Milk Cooperatives under Plan Milk

                    Value Chain Project Vehari, Livestock Farms, livestock related activities, Field days with 40,000 expected

                    participation including 50% women, livestock fairs and competition shows with 10,000 expected participation

                    including 50% women for giving prizes to best animals and progressive farmers will be arranged periodically to

                    promote learning and experience sharing for motivation of farmers.</p>

                <br>

            </p>

        </div><!-- /.container -->
    </section>


</main>