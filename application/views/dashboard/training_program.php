<style>
    table.dataTable tr.odd {
        background-color: #f0f0f0;
    }
    thead, th td{text-align: left;}
</style>
<main id="main" style="margin-top: 130px;">
    <section>
        <h2>Training Program</h2>
        <div class="container">
            <table class="table table-bordred table-striped" id="pp_dt">
                <thead>
                <tr>
                    <th>Year</th>
                    <th>Description</th>
                    <th>PDF</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($procurement_plans)) {
                    $i = 1;
                    foreach ($procurement_plans as $row) {
                        ?>
                        <tr class="">
                            <td ><?=$row->year?></td>
                            <td><?= $row->description ?></td>
                            <td><?php if(!empty($row->pdf_link)){?><a href="<?= $row->pdf_link	 ?>" target="_blank">View</a> <?php }?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</main>
<script>
    $( document ).ready(function() {
        $('#pp_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
    });
</script>