<main id="main" style="margin-top: 130px;">
    <header>

        <section>
            <!--[if gte IE 9]><!-->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px" enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="7.842" r="2.813" />
                        <rect x="9.248" y="5.667" fill="#676868" width="13.599" height="4.349" />
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="16" r="2.813" />
                        <rect x="9.248" y="13.773" fill="#676868" width="21.756" height="4.349" />
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="24.158" r="2.813" />
                        <rect x="9.248" y="21.984" fill="#676868" width="21.756" height="4.349" />
                    </g>
                </g>
            </svg>

            <!--<![endif]-->

            <h1>Livestock Cholistan</h1>

            <p class="agency-search">
                <input type="text" class="search-agencies" title="Search" placeholder="Search..."/>
                <button type="button" class="clear-search">Clear Search</button>
            </p>

        </section>
    </header>

        <section class="agency-section file-link-sec">
            <h2>A BIRD EYE VIEW OF CHOLISTAN
            </h2>
            <div class="container " id="target1" >
                <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                     data-getmore="20">
                    <ul class="MDI-Results default searchlist">
                        <?php if(!empty($livestock_cholistan)){ foreach($livestock_cholistan as $row){ ?>
                            <li itemscope="" itemtype="" class="item Data state file-link"><a itemprop="url" target="_blank" href="<?php echo $row->image_url ?>">
                                    <h4 itemprop="name" class="title"><?php echo $row->title ?></h4><span itemprop="description" class="description"><?php echo $row->description ?></span></a>
                            </li>

                        <?php }} ?>
                    </ul>
                </div>
            </div><!-- /.container -->

        </section>
        <!--<section class="agency-section file-link-sec">
            <h2>Test 2
            </h2>
            <div class="container " id="target1" >
                <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                     data-getmore="20">
                    <ul class="MDI-Results default searchlist">
                        <?php /*if(!empty($livestock_cholistan_2)){ foreach($livestock_cholistan_2 as $row){ */?>
                            <li itemscope="" itemtype="" class="item Data state file-link"><a itemprop="url" target="_blank" href="<?php /*echo $row->image_url */?>">
                                    <h4 itemprop="name" class="title"><?php /*echo $row->title */?></h4><span itemprop="description" class="description"><?php /*echo $row->description */?></span></a>
                            </li>

                        <?php /*}} */?>
                    </ul>
                </div>
            </div>

        </section>
        <section class="agency-section file-link-sec">
            <h2>Test 3
            </h2>
            <div class="container " id="target1" >
                <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                     data-getmore="20">
                    <ul class="MDI-Results default searchlist">
                        <?php /*if(!empty($livestock_cholistan_3)){ foreach($livestock_cholistan_3 as $row){ */?>
                            <li itemscope="" itemtype="" class="item Data state file-link"><a itemprop="url" target="_blank" href="<?php /*echo $row->image_url */?>">
                                    <h4 itemprop="name" class="title"><?php /*echo $row->title */?></h4><span itemprop="description" class="description"><?php /*echo $row->description */?></span></a>
                            </li>

                        <?php /*}} */?>
                    </ul>
                </div>
            </div>

        </section>
        <section class="agency-section file-link-sec">
            <h2>Test 4
            </h2>
            <div class="container " id="target1" >
                <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                     data-getmore="20">
                    <ul class="MDI-Results default searchlist">
                        <?php /*if(!empty($livestock_cholistan_4)){ foreach($livestock_cholistan_4 as $row){ */?>
                            <li itemscope="" itemtype="" class="item Data state file-link"><a itemprop="url" target="_blank" href="<?php /*echo $row->image_url */?>">
                                    <h4 itemprop="name" class="title"><?php /*echo $row->title */?></h4><span itemprop="description" class="description"><?php /*echo $row->description */?></span></a>
                            </li>

                        <?php /*}} */?>
                    </ul>
                </div>
            </div>

        </section>-->







</main>