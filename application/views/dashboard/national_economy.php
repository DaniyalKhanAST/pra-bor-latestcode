<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Contribution of Livestock Sector in National Economy
            <span style="float: right">قومی معیشت میں ھیلتھ سیکٹر کا کردار</span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <p>The economy of Pakistan is the 25thlargest in the world in terms of purchasing power parity (PPP), and 38th largest in terms of nominal gross domestic product. Pakistan has a population of over 191 million (the world's 6th largest). However, Pakistan's undocumented economy is estimated to be 36% of its overall economy. Some non-conventional estimates put this figure at much higher level, a fact, which is substantiated to some extent by the external shocks the economy has had succeeded to bear. Pakistan is one of the ‘Next Eleven’, the eleven countries that, along with the BRICS (five major emerging national economies: Brazil, Russia, India, China and South Africa) has a potential to become one of the world's large economies in the 21st century.
       <br>

            Pakistan's average economic growth rate in the first five decades (1947–1997) has been higher than the growth rate of the world economy during the same period.
        <br>
            The World Bank (WB) and International Finance Corporation's report on “Ease of Doing Business Index 2015” ranked Pakistan highest in South Asia and also higher than China and Russia.
        <br>
            Agriculture accounted for about 53% of Pakistan’s GDP in 1947. While per-capita agricultural output has grown since then, it has been outpaced by the growth of the non-agricultural sectors. The share of agriculture has dropped to 21% of Pakistan's economy by 2016. In recent years, the country has seen rapid growth in industrial and services sectors, with latter crossing half the total GDP output.
      <br>
            Livestock sub-sector contributes 11.6% in National GDP and 56.8% in overall Agri. Sector GDP of Pakistan;3 it employees 46% direct labour force that comes from 67% of the population. The share of livestock in developed countries’ Agri sectors ranges between 59%- 79%; for India, it stands around 25%; it was 28% for Pakistan in 90s.
        <br>
            Livestock sector is the biggest cost reducer of Agriculture sector through harvesting operations in Pakistan.
       <br>
            Livestock’s value exceeds the combined value of all the major and minor crops by about 6.1%. The share of livestock products in the generation of foreign exchange is about 13%. More significantly, livestock is an integral part (30-40%) of livelihood of about 30 to 35 million rural farmers. Presently, the reported gross value addition of livestock stands at PKR 1,172 billion, whereas livestock share in export is 8.5%
        <br>

            Dairy sector in Pakistan consists of three types of producers; 80% small farmers raising more than 50% of total milch animals (herd size less than 5 animals), 14% medium-sized farmers/ producers, raising 29% milch animals (herd size 5-10 animals), 3% large-scale producers sharing 21% of milch animals (herd size more than 10 animals). Animals are raised in agricultural lands and almost 1/5th of this land is used as pasture land to raise animals including milk-producing animals. Medium and large size producers are mostly located in peri-urban areas having better farm-to-market access.</p>
<h3>Livestock sector</h3>
            <p>The Livestock Sector has been performing well below its potential. Research has shown that overcoming the weaknesses of this sector can reduce income variability and provide better employment for women. The Government will create a policy environment that helps farmers by smartly deploying public investments in core public goods and inducing private capital in the sector.
            <br>


                To improve service delivery under this sector, the Government will seek to do the following:
                Increasing per animal productivity, via a number of measures including genetic improvement, cross breeding and compliance to international standards;

            </p>

            <ul class="container national_list" >
                <li >Increasing the coverage and results of extension services</li>
                <li>Improving animal husbandry</li>
                <li>Improving regulation in livestock sector and the functioning of livestock markets, and focus on transformation of livestock business from subsistence to commercial ventures</li>
                <li>Developing and implementing a Livestock Export Strategy</li>
                <li>Providing nutritious food and improving the quality of semen</li>
                <li>Bringing livestock farmers into formal sector through collective services</li>
                <li>Adoption of modern techniques such as e-monitoring while encouraging farmers to use modern technology</li>
                <li>Adoption of revolving fund measures to decrease reliance on the ‘gawala’ system</li>
            </ul>



            <span style="float:right;text-align:right;font-size:18px;">

            </span>

            <span style="float:right;text-align:right;font-size:18px;">
                    پاکستان کی معیشت قوت خرید کے لحاظ سے پچیسویں اور مجموعی ملکی پیداوار کے لحاظ سے38 ویں نمبر پر ہے۔ پاکستان کی آبادی انیس کروڑ سے زائد ہے۔ تاہم پاکستان کی غیر اندراج شدہ معیشت کل معیشت کا 36فیصدہے۔ کچھ غیر روایتی اندازے اس اعدادوشمار کو زیادہ بتاتے ہیں۔ جوکہ حقیقت ہے اس نقصان کی جو کی معیشت برداشت کر رہی ہے۔ پاکستان نیکسٹ الیون تنظیم کے گیارہ ممالک میں سے ایک ہے کہ برکس کی پانچ بڑی ابھرتی ہوئی معیشتوں جس میں برازیل، روس، انڈیا، چین اور جنوبی افریقہ کے ساتھ اکیسویں صدی میں دنیا کی بڑی معیشت بننے کی صلاحیت رکھتی ہے۔
                </span>
            <br><br>
            <span style="float:right;text-align:right;font-size:18px;">
        پہلی پانچ راستوں میں (1947-1997) پاکستان کی اوسطً اقتصادی ترقی کی شرح اسی مدت میں عالمی ترقی کی شرح کے مقابلے میں زیادہ ہے۔ عالمی بنک اور بین الاقوامی مالیاتی کارپوریشن کی رپورٹ کاروبار کرنے کی آسانی کا انڈیکس 2015 کے مطابق پاکستان، جنوبی ایشیا میں پہلے نمبر پر ہے جو چین اور روس کے مقابلے سے بھی اس کی ترقی کی شرح زیادہ ہے۔ ذراعت 1997 میں پاکستان کی جی ڈی پی کا 53 فیصد حصہ تھی۔ جبکہ فی کس زرعی پیداوار بڑھی ہے۔ لیکن غیر زرعی شعبوں کی ترقی سے اس کو پیچھے چھوڑ دیا ہے۔ زراعت کا حصہ 2016 تک پاکستانی معیشت کا اکیس فیصد رہ گیا ہے۔ حالیہ برسوں میں ملک نے صنعتی اور خماتی شعبوں میں تیزی سے ترقی کی ہے۔ جس کا حصہ ملک کی تقریباً جی ڈی پی سے زیادہ ہے۔ ذیلی شعبہ لائیوسٹاک قومی جی ڈی پی میں 11.6 فیصد اور زراعت میں 56.8 فیصد ہے۔

                </span>
            <br><br>
             <span style="float:right;text-align:right;font-size:18px;">
                    یہ شعبہ 46 فیصد مزدوری قوت دیتا ہے جو کہ کل آبادی کا 67 فیصد ہے۔ ترقی یافتہ ممالک میں لائیوسٹاک ایگریکلچر کا 59 سے 79 فیصد ہے۔ بھارت میں 25 فیصد ہے اور پاکستان میں 90 کی دہائی میں 28 فیصد ہے۔

                </span>
            <br><br>
             <span style="float:right;text-align:right;font-size:18px;">
                    لائیوسٹاک سیکٹر ایگریکلچر سیکٹر کا سب سے بڑا قیمت کم کرنے والا شعبہ ہے۔ لائیوسٹاک کی مالیت چھوٹی اور بڑی فصلوں سے 6.1 فیصد زیادہ ہے۔ لائیوسٹاک پراڈکٹس بیرونی تجارت کا 13 فیصد ہے۔ نمایاں طور پر لائیوسٹاک 30 سے 35 ملین کسانوں کے روزگار کا لازمی حصہ ہے۔ اس وقت مویشیوں کی مجموعی قیمت 1172 بلین روپے پاکستان میں ڈیر سیکٹر 3 اقسام پر مشتمل ہے۔ 80 فیصد وہ کسان ہیں جو 50 فیصد سے زائد دودھ دینے والے جانوروں کو پالتے ہیں اور ان کے پاس جانوروں کی تعداد 5 سے کم ہے۔ چودہ فیصد وہ فارمرز ہیں جو29 فیصد دودھ دینے والے جانوروں کو پالتے ہیں اور جانوروں کی تعداد 5 سے 10ہوتی ہے۔ 3 فیصد بڑے پیمانے کے کسان ہیں جو کہ دودھ دینے والے جانوروں کا 21 فیصد ہے ان کے پاس 10 سے زیادہ جانور ہوتے ہیں۔ جانور زرعی زمیں پر پالے جاتےہیں اور تقریباً پانچواں حصہ دودھ دینے والے جانوروں کو پالنےکیلئے چراہ گاہ کے طور پر استعمال ہوتا ہے۔ درمانے اور بڑے پیمانے کے کسان زیادہ تر شہروں کے آس پاس کے علاقوں میں رہتے ہیں۔ جہاں سے مارکیٹ تک رسائی آسان ہے۔ لائیوسٹاک کا شعبہ پوری صلاحیت سے ہونے کے باوجود صحیح کام کر رہا ہے۔ تحقیق سے ثابت ہوا ہے کہ اس شعبہ کی کمزوریوں کے ختم کرکے خواتین کے لئے روزگار کے مواقع پیدا کیے جا سکتے ہیں۔ حکومت اس طرح کی پالیسی وضع کرے گی جو کہ کسانوں کو عوامی سرمایہ کاری کو مرکزی عوامی اشیائ میں اور نجی سرمایہ کاری کے شعبہ میں فروغ دے۔
جانوروں کی فی کس پیداوار بڑھانے کے لئے گورنمنٹ خرمات کی فراہمی کو بڑھائے گی جس میں جنیاتی بہتری اور کراس بدیڈنگ شامل ہے۔

                </span>
            <br><br>
            <span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
                <br>
                <div style="margin-right:20px;">

                    ۔ توسیعی خدمات کی فراہمی اور نتائج کو بڑھانا
                    <br>
                    ۔ جانوروں کی دیکھ بھال میں بہتری
                    <br>
                    ۔ لائیوسٹاک کے متعلقہ قوانین اور مارکیٹس میں بہتری، لائیوسٹاک کو گزارہ کے کاروبار سے کمرشل کاروبار میں بدلنے پر توجہ
                    <br>
                    ۔ لائیوسٹاک کی برآمدات کی حکمت عملی بنانا اور اس کو لاگو کرنا
                    <br>
                    ۔ غزائیت سے بھرپور خوراک کی فراہمی اور سیمن کی کوالٹی کو بہتر کرنا
                    <br>
                    ۔ مجموعی خدمات کے ذریعے کسانوں کو رسمی شعبہ میں لانا
                    <br>
                    ۔ مانیٹرنگ جیسی جدید ٹیکنالوجییز کو اپنانا تاکہ کسانوں کا جدید ٹیکنالوجی کو استعمال کرنے کا حوصلہ بڑھایا جا سکے
                    <br>
                    ۔ گردشی فنڈ کے اقدامات کو اپنانا تاکہ گوالہ کے نظام کے انحصار کو کم کیا جاسکے
                    <br>
                </div>





            </span>

        </div><!-- /.container -->
    </section>


</main>