<main id="main" style="margin-top: 130px;">
    
    <section style="margin-bottom: 3px;"><h2 style="font-family:Open Sans Condensed, Open Sans, Helmut, sans-serif;">Treatments<span style="float: right;">علاج</span></h2></section>
   

    
    <section style="margin-bottom: 2px;">
        <h2 class="bukatutup0"><a href="#" class="clickme">Emergency  <span style="float: right;">ایمرجنسی</span></a></h2>
        <div class="container " id="target0" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($emergency->page_detail)){
                    echo $emergency->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom:1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">Outdoor  <span style="float: right;">اوٹدور</span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($outdoor->page_detail)){
                    echo $outdoor->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a href="#" class="clickme">Indoor <span style="float: right;">انڈور</span></a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <?php 
                if(isset($indoor->page_detail)){
                    echo $indoor->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup4">   <a href="#" class="clickme">24/ 7 Services  <span style="float: right;">چوبیس گھنتے سروسز</span></a></h2>
        <div class="container " id="target4" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <?php 
                if(isset($services_24_7->page_detail)){
                    echo $services_24_7->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup6">   <a href="#" class="clickme">Mobile Health Services <span style="float: right;">موبائل ہیلتھ سروسز </span></a></h2>
        <div class="container " id="target6" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <?php 
                if(isset($mobile_health_services->page_detail)){
                    echo $mobile_health_services->page_detail;
                }
                ?>
            </div>   
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup5">   <a href="#" class="clickme">Health Services at your Doorsteps   <span style="float: right;">صحت سہولت آپکی دہلیز پر </span></a></h2>
        <div class="container " id="target5" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <?php 
                if(isset($health_services_doorsteps->page_detail)){
                    echo $health_services_doorsteps->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>



</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
     $('.bukatutup0').click(function() {
        $('#target0').toggle('slow');
    });
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
        
    });
    $('.bukatutup5').click(function() {
        $('#target5').toggle('slow');
        
    });
    $('.bukatutup6').click(function() {
        $('#target6').toggle('slow');
        
    });
</script>