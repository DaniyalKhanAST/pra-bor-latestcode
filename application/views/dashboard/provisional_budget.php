<main id="main" style="margin-top: 130px;">
    <header>

        <section>
            <!--[if gte IE 9]><!-->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px" enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="7.842" r="2.813" />
                        <rect x="9.248" y="5.667" fill="#676868" width="13.599" height="4.349" />
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="16" r="2.813" />
                        <rect x="9.248" y="13.773" fill="#676868" width="21.756" height="4.349" />
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="24.158" r="2.813" />
                        <rect x="9.248" y="21.984" fill="#676868" width="21.756" height="4.349" />
                    </g>
                </g>
            </svg>

            <!--<![endif]-->

            <h1>Company Budget Allocation</h1>

            <p class="agency-search">
                <input type="text" class="search-agencies" title="Search" placeholder="Search..."/>
                <button type="button" class="clear-search">Clear Search</button>
            </p>

        </section>

    </header>

    <section class="agency-section">
        <h2>Company Budget</h2>

        <div class="container " >
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul >
                    <?php if(!empty($provisional_budget)){ foreach($provisional_budget as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description" style="color: #4D4D4D"><?php echo $row->description ?></span></a>
                        </li>
                    <?php }}?>
                </ul>
            </div>
        </div>
    </section>
</main>