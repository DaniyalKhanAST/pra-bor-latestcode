<style>
    table.dataTable tr.odd {
        background-color: #f0f0f0;
    }
    thead, th td{text-align: left;}
</style>
<main id="main" style="margin-top: 130px;">
    <section>
        <h2>Evaluation Reports</h2>
        <div class="container">
            <table class="table table-bordred table-striped" id="pp_dt">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>PDF File</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($reports)) {
                    $i = 1;
                    foreach ($reports as $row) {
                        ?>
                        <tr class="">
                            <td ><?=$row->category_name?></td>
                            <td ><?=$row->name?></td>
                            <td><?= $row->detail ?></td>
                            <td><?php if(!empty($row->pdf_link)){?><a href="<?= $row->pdf_link	 ?>" target="_blank">View</a> <?php }?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</main>
<script>
    $( document ).ready(function() {
        $('#pp_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
    });
</script>