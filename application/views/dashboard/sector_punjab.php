<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Livestock Sector Punjab
<span style="float:right;">لائیوسٹاک سیکٹر پنجاب</span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <p>In the Punjab Province, livestock sector is the largest women engagement in any economic activity as a rural woman spends 59% of her daily life in livestock related activities.
            

                <br>
                Punjab is the largest province, population wise (both human and animal), of Pakistan. By 2011, Punjab had a GDP of $104 billion, which is growing steadily. It is featured well within the list of country subdivisions with a GDP over $100 billion. It is the biggest contributor to the national GDP with 59% share in 2014. It is especially prominent in the Services & Agriculture sectors of the economy with a thriving private sector.
                <br>
                As per 2006 Census, the Punjab dominates Pakistan’s livestock sector.
                

            </p>
            <br/>
<img src="<?php echo base_url() ?>assets/images/home/sector_punjab1.jpg">
<p style="font-style: italic;margin-top: 0px;">A Ram with 240 Kg weight prepared for breeding purpose from the South of Punjab.</p>
        <p>Province has 49% of Pakistan’s cattle, 65% of the buffaloes, 24% of the sheep, and 37% of the goats.1 In value of product, it is producing 62% of milk, 43% of beef, 32% of mutton and 75 % of poultry of Pakistan. However, livestock productivity and standards of preventive health are far below world benchmarks. The reasons are inextricably not linked with the genetic potential of the breeds but with wider market distortions.
        <span style="float:right;text-align:right;font-size:18px;">

<br><br>
<span style="text-align: right;float: right;font-size: 18px;">
                صوبہ پنجاب میں لائیوسٹاک سیکٹر خواتین کی سب سے بڑی اقتصادی شراکت ہے۔ کسی بھی معاشی سرگرمی جیسے کہ ہم دیکھتے ہیں کہ دیہی خواتین اپنی روزمرہ زندگی کا 59 فیصد جانوروں سے متعلق سرگرمیوں میں گزارتی ہیں۔
            </span>
<br>
<span style="float:right;text-align:right;font-size:18px;" >
                    آبادی کے لحاظ سے (انسانی اور جانوروں) پنجاب پاکستان کا سب سے بڑا صوبہ ہے۔ 2011 کے مطابق پنجاب کی جی ڈی پی 104 بلین ڈالر ہے جو کہ مستقل بڑھ رہی ہے اور لائیوسٹاک محکمہ اپنی 100 بلین ڈالر جی ڈی پی کی وجہ سے اس میں سرفہرست رہا ہے۔ 2014 کی ملکی جی ڈی پی کے حصے سے یہ سب سے بڑا حصہ دار ہے۔ ایگریکلچر سیکٹر کی معیشت میں محکمہ لائیوسٹاک خاص طور پر نمایا رہا ہے فروض پاتے ہوئے جنی سیکٹر کے ساتھ 2006 کے شماریات کے مطابق پاکستان میں سب سے زیادہ لائیوسٹاک صوبہ پنجاب میں ہے۔
                </span>
<br><br>
            اس صوبہ میں پاکستان کی 49 فیصد گائے، 65 فیصد بھینس، 24 فیصد بھیڑیں اور 37 فیصد بکریاں پائی جاتی ہیں۔ ان سے حاصل ہونے والے فوائد کو دیکھا جاےئے تو یہ جانور پاکستان کا 62 فیصد دودھ، 43 فیصد بیف اور 32 فیصد مٹن اور 75 فیصد چکن پیداکر رہے ہیں۔ تاہم جانوروں کی پیداواری صلاحیتوں میں اور بیماریوں سے بچائو کے اسٹینڈرڈ مں یہ دینا معیارات میں بہت پیچھے ہیں۔ اس کی یہ وجوہات بہت پیچیدہ ہیں۔ جو کہ یہاں کے جانوروں کی نسلوں کی جنیاتی صلاحیتوں سے منسلک نہیں ہے بلکہ وسیع ترمارکیٹ کے مطابق بگاڑ سے منسلک ہے۔
        </span>

        </p>
            <br>
            <img src="<?php echo base_url() ?>assets/images/home/sector_punjab2.jpg">
            <p style="font-style: italic;margin-top: 0px;">Nili Ravi Buffalo milk competition 47.565 Kg in 36 hours with 9% fat content.</p>
        </div><!-- /.container -->
    </section>


</main>