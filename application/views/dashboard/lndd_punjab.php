<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Livestock & Dairy Development Department Punjab
            <span style="float:right;">محکمہ لائیوسٹاک و ڈیری ڈیولپمنٹ پنجاب</span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <p>The Punjab Government in the Livestock and Dairy Development Department moved swiftly to undertake, perhaps, the biggest transformation in organizational posture in its recent history: transforming the role of Provincial Livestock & Dairy Development Department from “Disease Attendant” to “Livestock Asset Manager” by inducing a complete paradigm shift initially from curative to preventive to set a stage for making the whole sector competitive, for ensuring food security; improving live of stakeholders and for ultimately generating exportable surpluses for domestic and international markets.

                <br>
                The semi-arid and arid climatic conditions of the country coupled wisth shrinking water resources due to varied reasons have severely undermined the future prospects of crops, if practiced as per the prevalent model.

                <br><br>
                Statistics from the Punjab Agriculture Census, 2010 are quite significant from the perspective of devising a strategy for the agriculture development as land holding size in the Punjab renders it relatively unfeasible economically unless deployed for horticulture or livestock rearing. Moreover, it reveals huge numbers (68.6%) of absentee land holders out of 90% less than 12.5 acres’ holders, which on one hand shows demographic transition (migration) from rural to urban side and on the other hand competing cost for agriculture labour market with urban markets with the resultant relative shrinking supply of labour in the rural areas, thus putting extra burden on the already fragile Agri ecosystem, historically witnessing price shocks.
                <br><br>
                <span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
                پنجاب گورمنٹ نے محکمہ لائیوسٹاک اینڈ ڈیری ڈویلپمنٹ ڈیپارٹمنٹ کو بہت تیزی سے منتقل کیا ہے جو کہ شاید اس کی حالیہ تاریخ میں تنظیمی طرز عمل میں سب سے بڑی تبدیلی ہے . صوبائی لائیوسٹاک و ڈیری ڈویلوپمنٹ محکمہ کے کردار کو "ڈیزیز اٹینڈنٹ" سے"لائیوسٹاک ایسیٹ مینیجر" میں منتقل کیا گیا ہے. محکمہ کو مسا بقتی اسٹیج پر لانے کے لیے سب سے پہلے تمام پیراڈائم کی منتقلی ، علاج سے بہتر پرہیز کو ممکن بنایا گیا ہے- اس کے ساتھ ساتھ خوراک کی حفاظت کو یقینی بنایا گیا ہے اور سٹیک ہولڈرز (فارمرز) کی ذندگیوں کو بہتر بنانا شامل ہے جس سے ہمارا مقصد ملکی اور بین الاقوامی مارکیٹوں کے لیے برآمدات حاصل کرنا ہے- ملک کے نیم بنجر اور بنجر موسمی حالات مزید براں مختلف وجوہات کی بنا پر کم ہوتے ہوے آبی وسائل نے فصلوں کے امکانات کو برے طریقے سے مجروح کیا ہے اگر انہیں موجودہ مقبول طریقہ کر پر کاشت کیا جاے-
            </span>
                <br>
                <span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
پنجاب ایگریکلچر اعداد و شمار کے مطابق ٢٠١٠ زراعت کی ترقی کے لیے حکمت عملی وضع کرنے کے نقطہنظر سے بہت اہم ہے جیسے کے ہم دیکھتے ہیں کے پنجاب میں زمین جوت سائز معاشی طور نسبتاً نا قابل استعمال ہے جب تک کے اس زمینی حصّے کو باغبانی یا مویشی پالنے کے استعمال میں نا لیا جائے مزید براں یہ بیان کرتا ہے کے ٩٠%
زراعت پیشہ افرادجن کے پاس ١٢.٥ ایکڑ سےکم زمین ہے ان میں سے کثیر تعداد (٦٨.٨%) کے پاس اپنی زمین نہیں ہے جو ایک طرف گاؤں سے شہر آبادیاتی منتقلی کو ظاہر کرتا ہے اور دوسری طرف ایگریکلچر لیبر اور شہری مارکیٹ میں مقابلاتی لاگتکی فضاپیدا ہو جاتی ہےجس کے نتیجے میں دیہی علاقوں میں مزدور کم
ملتے ہیں لیہذا پہلے سے ہی کمزور ایگری ماحولیاتی نظام پر بوج بھڑ جاتا ہے جس سے تاریخی طور پر قیمتوں میں جھٹکے مشاہدہکئے گئے ہیں.

            </span>
            </p>
        </div><!-- /.container -->
    </section>


</main>