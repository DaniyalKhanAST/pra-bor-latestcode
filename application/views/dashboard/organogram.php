<main id="main" style="margin-top: 130px;">

    <section style="margin-bottom: 1px;">

        <h2 class="bukatutup1">   <a style="cursor: pointer" class="clickme">Board Of Directors</a></h2>

        <div class="container " id="target1" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <img style="height: 600px;width: 1200px;" src="<?php echo $organogram1->image_url  ?>">
            </div>
        </div><!-- /.container -->
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup2">   <a style="cursor: pointer"  class="clickme">PHFMC - Head Office</a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <img style="height: 600px;width: 1200px;" src="<?php echo $organogram2->image_url  ?>">
            </div>
        </div><!-- /.container -->
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a style="cursor: pointer" class="clickme">PHFMC - Core Team</a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <img style="height: 600px;width: 1200px;" src="<?php echo $organogram3->image_url  ?>">
            </div>
        </div><!-- /.container -->
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup4">   <a style="cursor: pointer" class="clickme">PHFMC - Hospitals</a></h2>
        <div class="container " id="target4" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <img style="height: 600px;width: 1200px;" src="<?php echo $organogram3->image_url  ?>">
            </div>
        </div><!-- /.container -->
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup5">   <a style="cursor: pointer" class="clickme">PHFMC - Rural Health Centers</a></h2>
        <div class="container " id="target5" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <img style="height: 600px;width: 1200px;" src="<?php echo $organogram3->image_url  ?>">
            </div>
        </div><!-- /.container -->
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup6">   <a style="cursor: pointer" class="clickme">PHFMC - Basic Health Units</a></h2>
        <div class="container " id="target6" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <img style="height: 600px;width: 1200px;" src="<?php echo $organogram3->image_url  ?>">
            </div>
        </div><!-- /.container -->
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup7">   <a style="cursor: pointer" class="clickme">PHFMC - Mobile Health Units</a></h2>
        <div class="container " id="target7" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <img style="height: 600px;width: 1200px;" src="<?php echo $organogram3->image_url  ?>">
            </div>
        </div><!-- /.container -->
    </section>


</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
    });
    $('.bukatutup5').click(function() {
        $('#target5').toggle('slow');
    });
    $('.bukatutup6').click(function() {
        $('#target6').toggle('slow');
    });
    $('.bukatutup7').click(function() {
        $('#target7').toggle('slow');
    });
</script>