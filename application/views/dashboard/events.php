<main id="main" style="margin-top: 130px;">

<!-- 
    <div style="padding: 30px;text-align: right">
        <form id="live-search" action="" class="styled" method="post">
            <div class="input-group">

                <input type="text" class="form-control" style="color: black;    width: 270px;height: 35px;" placeholder="Search for..." id="filter">

                <span id="filter-count"></span>
            </div>
        </form>
    </div> -->

    <section class="tertlinks alt">
        <h2>Events</h2>
        <div class="container">
            <ul class="grid_10 searchlist">
                <?php foreach($events as $row){ ?>
                <li style="padding-right: 0;">
                    <a href="<?= base_url(); ?>index.php/dashboard/event_detail?event_id=<?= $row->id; ?>">
                        <img alt="" src="<?php echo $row->image_url ?>" style="width: 200px;height: 150px">
                        <h3 style="font-size: 24px"><?php echo $row->title ?></h3>
                        <p style="margin-top: 10px;font-size: 20px"><?php echo $row->description ?></p>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div><!-- /.container -->
    </section>


</main>
