<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<main id="main" style="margin-top: 130px;">
    <section >
        <!-- <h2>Administration Department</h2> -->
        <h2>Head Office</h2>
        <table>
                <tr style="background-color:#00a651;color:#fff">
                    <th>Name</th>
                    <th>Designation</th>
                    <!-- <th>District</th> -->
                    <th>Mobile No</th>
                    <th>Phone No</th>
                    <th>Email</th>
                </tr>
            <?php if(!empty($head_office)):foreach($head_office as $row): ?> 
                <tr>
                    <td><?=$row->name_of_office_facility;?></td>
                    <td><?=$row->designation;?></td>
                    <!-- <td><?=$row->district;?></td> -->
                    <td><?=$row->mobile_no;?></td>
                    <td><?=$row->phone_no;?></td>
                    <td><?=$row->email;?></td>
                </tr>
            <?php endforeach; endif;?>
  
  
</table>

    </section>

</main>