
<style>
    table.dataTable tr.odd {
        background-color: #f0f0f0;
    }
    thead, th td{text-align: left;}
</style>
<main id="main" style="margin-top: 130px;">
    <section style="margin-bottom: 1px;">
        <h2>Manuals <span style="float: right;">مینولز</span></h2>
        <div class="container ">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                 <section>
        <div class="container">
            <table class="table table-bordred table-striped" id="manual_dt">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Manual Name</th>
                    <th>Manual Document</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($manuals)) {
                    $i = 1;
                    foreach ($manuals as $row) {
                        ?>
                        <tr class="">
                            <td ><?=$i?></td>
                            <td ><?=$row->name?></td>
                            <td><?php if(!empty($row->pdf_file_link)){?><a href="<?= $row->pdf_file_link	 ?>" target="_blank">View</a> <?php }?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
            </div>
        </div><!-- /.container -->
    </section>
            </div>   
        </div>
    </section>



</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
    $( document ).ready(function() {
        $('#manual_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
    });
</script>