<main id="main" style="margin-top: 130px;">
    <div style="padding: 30px;text-align: right">
        <form id="live-search" action="" class="styled" method="post">
            <div class="input-group">

                <input type="text" class="form-control" style="color: black;    width: 270px;height: 35px;" placeholder="Search for..." id="filter">

                <span id="filter-count"></span>
            </div>
        </form>
    </div>
    <section style="margin-bottom: 1px;">

        <h2 class="bukatutup1">   <a href="#" class="clickme">Poverty Alleviation</a></h2>

        <div class="container " id="target1" >
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach($poverty as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description"><?php echo $row->description ?></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>

</main>
