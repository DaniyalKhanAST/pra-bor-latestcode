<main id="main" style="margin-top: 130px;">
    
    <section style="margin-bottom: 3px;"><h2 style="font-family:Open Sans Condensed, Open Sans, Helmut, sans-serif;">Experience Certificates<span style="float: right;">ایکسپیرینس سرٹیفکیٹس</span></h2></section>
    <section style="margin-bottom: 2px;">
        <h2 class="bukatutup0"><a href="#" class="clickme">Good  <span style="float: right;"> اچھا </span></a></h2>
        <div class="container " id="target0" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($good->page_detail)){
                    echo $good->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom:1px;">
        <h2 class="bukatutup1">   <a href="#" class="clickme">Average  <span style="float: right;"> اوسط </span></a></h2>
        <div class="container " id="target1" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($average->page_detail)){
                    echo $average->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
</section>
<section style="margin-bottom:1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">Poor  <span style="float: right;"> برا </span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($poor->page_detail)){
                    echo $poor->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
</section>


</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
     $('.bukatutup0').click(function() {
        $('#target0').toggle('slow');
    });
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    
</script>