<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Livestock and Access to Markets Project (LAMP)

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Introduction <span style="float:right;">
                    لیمپ انٹرو
                </span></h3>
            <p> The Livestock and Access to Markets Project (LAMP) was approved in December 2013, and the project became effective on 12 February 2015. The total project cost is estimated at USD 40.83 million over a six year implementation period with IFAD contribution of USD 35 million (86 per cent of total cost), the Government of Punjab USD 3.40 million (8.3 per cent), the Tehsil Municipal Administration (TMA) USD 0.46 million (1.1 per cent) and beneficiaries USD 1.87 million (4.6 per cent).
                <br>
                The overall goal of the project is to contribute to rural growth and poverty reduction in four districts of Punjab: Bhakkar, Khushab, Layyah and Mianwali. The development objective is to enhance the livelihoods of 112,500 poor households in four project districts.

                <br>
                The project has three components:
                <ul class="container">
                    <li ><a href="<?php echo base_url().'dashboard/production_support';?>">Production Support </a> </li>
                    <li><a href="<?php echo base_url().'dashboard/marketing_support';?>">Marketing Support</a>  </li>
                    <li><a href="<?php echo base_url().'dashboard/project_management';?>">Project Management</a>  </li>
                </ul>

                <br>
                <dl>
                    <dt>Project Period:</dt>
                    <dd>06 years (2013-14 to 2018-19)</dd>
                    <dt>Project objectives:</dt>
                    <dd>
                        The overall goal of the project is to contribute in rural growth rate and poverty reduction in rural Punjab by;-

                        <ul class="container">
                            <li> 112,500 households with increased livelihoods. </li>
                            <li> 50% increase in the net income of targeted households. </li>
                            <li> 17% increase in livestock productivity </li>
                            <li> 50% and 33% decrease in mortality rates of large ruminants (cows/buffalos) and small ruminants respectively by the end of the project. </li>
                        </ul>

                        The development objective is to augment the livelihoods of 112,500 poor households (equivalent to approximately 765,000 individuals) in the districts of Mianwali, Khushab, Bhakkar and Layyah of rain fed areas of Punjab.
                    </dd>
                </dl>

            <div class="clearfix"></div>
            <span style="float:right;text-align:right;font-size:18px;">
                <h3>لیمپ انٹرو</h3>

لائیوسٹاک اور مارکیٹوں پروجیکٹ کی رسائی(لیمپ)دسمبر 2013 میں منظور کیا گیا تھا اور اس منصوبے پر 12 فروری 2015 کو موثر ہوئی جو کل منصوبے کی لاگت چھ سال سے زائد امریکی ڈالر 40.8 لاگت لگایا گیا ہے۔35 یو ایس ڈی ملین کے آئی ایف اے ڈی کی شراکت کے نفازذ کی مدت (کل لاگت کا 86 فیصد) حکومت پنجاب 3.40 یوایس ڈی ملین(8.3) تحصیل میونسپل ایڈمنسٹریشن (ٹی ایم اے) امریکی ڈالر 6.46ملین (1.1فیصد) اور فائدہ اٹھانے والوں 1.87 یو ایس ڈی ملین (4.6فیصد) ہے۔ منصوبےکامقصدپنجابکےچار دیہیاضلاعمیںترقیاورغربتمیںکمیمیںشراکتکرنا ہے: بھکر، خوشاب، لیہ اور میانوالی۔ اس ترقی کا مقصد چار اضلاع میں 112.500غریبگھرانوں کا معیار زندگی کو بہتر بنانا ہے۔
                    <br>
پراجیکٹ کے تین حصے ہیں۔
                <br><br>
                <div style="margin-right:20px;">

                    ۔ پراڈکشن سپورٹ
                    <br>
                    ۔ مارکیٹنگ سپورٹ
                    <br>
                    ۔ پراجیکٹ منیجمنٹ
                    <br>
                </div>
                <br><br>

<h3>لیمپ پراجیکٹ کی مدت</h3>

                چھ سال

                <br><br>
                <h3>منصوبے کے مقاصد</h3>

                منصوبے کا بنیادی مقصد دیہی ترقی کی شرح میں شراکت اوردیہی پنجاب میں غربت کا خاتمہ ہے
                <br><br>
                <div style="margin-right:20px;">

                    ۔ 112500گھروں کے معیار زندگی میں بہتری
                    <br>
                    ۔ تارگٹڈ گھرانوں کی آمدن میں 50 فیصد اضافہ
                    <br>
                    ۔ مویشیوںپیداوریمیں 17 فیصداضافہ
                    <br>
                    ۔ اس منصوبے کے اختتام تک چھوٹے اور بڑے جانوروں کی شرح اموات میں بالترتیب 33 سے 50 فیصد تک کمی

                </div>
                <br> <br>
ترقی کا مقصد پنجاب کے اضلاع میانوالی، خوشاب، بھکر اور لیہ کے بارانی علاقوں میں 112500(تقریبا 765000افراد) غریب گھرانوں کے معیار زندگی کو بہتر بنانا ہے۔
                </span>
            </p>






        </div><!-- /.container -->
    </section>


</main>