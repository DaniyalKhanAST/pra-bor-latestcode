<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Market Information and Linkages

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Market Information and Linkages: </h3>
            <p>The project will assist the smallholders establish link with private sector entrepreneurs for sale of produce.

                Market information will be provided through innovative delivery channels for the small holder farmer and some

                of the pilot initiatives of the Livestock Department such as their experiment with mobile technology for provision

                of extension messages and information on markets will be scaled-up. Telenor/Tameer is in the process of

                developing an eco-system through which they can share both information services and financial services with

                their clients. The Financial Service Provider will be procured through a competitive process or procured through

                the single source modality if it emerges that no other service provider can offer these services. These services will

                be offered to farmers wishing to subscribe to the services using mobile technology.
                <br>
                The project will not provide a line of credit but will facilitate the engagement of agencies to provide a range of

                financial services to the target group such as savings, transfer payments and remittance services through the

                rapidly growing network of branchless banking outlets of Easy paisa, UBL Omni, etc. which can provide an

                opportunity for offering a range of savings and loan products to the smallholder especially women‘s groups.

                Telenor/Tameer also intends to expand the range of its services to include an agriculture produce and input

                transaction service, agriculture insurance and microfinance loans using the Easy paisa Agriculture Commodity

                Trading portal. The project will pilot and facilitate the linkages of these innovative initiatives in the project area

                and procure the services of private sector agencies for this purpose on a competitive basis. The system will be

                linked with the integrated software of the L&amp;DD Department as per the compatibility. A budget for defraying the

                initial establishment costs of the participating agency and a preliminary TORS have been included in the design

                document.
                <br>
            </p>
        </div><!-- /.container -->
    </section>


</main>