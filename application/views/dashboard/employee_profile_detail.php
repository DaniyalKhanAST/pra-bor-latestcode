<style>
    input[type="file"] {
    display: none;

}
.btn-self-enrollment{
    background:#05445E;
    color:white;
    border:1px solid black;
    width:6rem;
    height:3rem;

}
.main-div-flex{
    display:flex;
}
@media (max-width: 480px) {
    .main-div-flex{
       display:flow-root !important
    }
}
</style>
<main id="main" style="margin-top: 130px;">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <section >
        <h2>Employee Profile</h2>
        <div class="container">
            <ul class="grid_10 searchlist">
                <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success"><?=$this->session->flashdata('success')?></div>
                <?php endif;?>
            <form>
            <fieldset disabled="disabled">
                <div class="step step-1 active" >
                <h2 class="emp-form-heading">A. PERSONAL DATA</h2>
                <div class="main-div-flex">
                <div class="col-8">
                    <div class="form-group row">
                    
                        <label for="staticEmail" class="col-sm-5 col-form-label">CNIC</label>
                        <div class="col-sm-5">
                        <input type="text" class="form-control" data-inputmask="'mask': '99999-9999999-9'" name="cnic" placeholder="Enter Cnic" value="<?=isset($employee_detail->cnic)? $employee_detail->cnic:''?>">
                        </div>
                        

                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Name</label>
                        <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" placeholder="Enter Name" required value="<?=isset($employee_detail->name)? $employee_detail->name:''?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Father's Name</label>
                        <div class="col-sm-5">
                        <input type="text" class="form-control" name="father_name" placeholder="Enter Father's Name" required value="<?=isset($employee_detail->father_name)? $employee_detail->father_name:''?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Gender</label>
                        <div class="col-sm-5">
                        <select id="select-gender" class="form-control" name="gender" required>
                            <option><?=isset($employee_detail->gender)? $employee_detail->gender:'choose'?></option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Date of Birth</label>
                        <div class="col-sm-5">
                        <input type="date" class="form-control" name="date_of_birth" placeholder="" required value="<?=isset($employee_detail->date_of_birth)? $employee_detail->date_of_birth:''?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputDomicile" class="col-sm-5 col-form-label">Domicile</label>
                        <div class="col-sm-5">
                        <select id="select-Domicile" name="domicile" class="form-control"  required>
                            <option selected><?=isset($employee_detail->domicile)? $employee_detail->domicile:'Choose'?></option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Marital Status</label>
                        <div class="col-sm-5">
                        <select  name="marital_status" class="form-control" >
                                <option selected><?=isset($employee_detail->marital_status)? $employee_detail->marital_status:'Choose'?></option>
                            </select>
                        
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Nationality</label>
                        <div class="col-sm-5">
                        <select id="select-Nationality" name="nationality" class="form-control" >
                            <option><?=isset($employee_detail->nationality)? $employee_detail->nationality:'Choose'?></option>
                           
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Religion</label>
                        <div class="col-sm-5">
                        <input type="text" class="form-control" name="religion" placeholder="Enter Religion" value="<?=isset($employee_detail->religion)? $employee_detail->religion:''?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-5 col-form-label">Blood Group</label>
                        <div class="col-sm-5">
                        <input type="text" class="form-control" id="inputPassword" name="blood_group" placeholder="Enter Blood Group" value="<?=isset($employee_detail->blood_group)? $employee_detail->blood_group:''?>">
                        </div>
                    </div>
                   </div>
                   <div class="">
                   <div class="ml-2 col-sm-6">
                            <?php $image_url = isset($employee_detail->profile_img)? base_url().$employee_detail->profile_img:'';?>
                            <img style="float:right" src="<?=$image_url?>" id="preview" class="img-thumbnail" alt="Profile Picture">
                        </div>
                        </div>
                   </div>
                    </div>
                  
                    <div class="step step-2 " >
                    <div class="tab">
  <h2 class="emp-form-heading">B. CONTACT INFORMATION</h2>
  <div class="col-8">
    <div class="form-group row">
        
                            <label for="inputPassword" class="col-sm-5 col-form-label">Mobile No.</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="mobile_no" data-inputmask="'mask': '0399-9999999'" placeholder="Enter Mobile No." value="<?=isset($employee_detail->mobile)? $employee_detail->mobile:''?>">
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Whats App No.</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="whatsapp_no" data-inputmask="'mask': '0399-9999999'" placeholder="Enter Whats App No." value="<?=isset($employee_detail->whatsapp_no)? $employee_detail->whatsapp_no:''?>">
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Residential Phone No (If any)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="residential_phone_no" placeholder="Enter Phone No." value="<?=isset($employee_detail->residential_phone_no)? $employee_detail->residential_phone_no:''?>">
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">E-Mail Address</label>
                            <div class="col-sm-5">
                            <input type="email" class="form-control" name="email" placeholder="Enter E-Mail Address." value="<?=isset($employee_detail->email)? $employee_detail->email:''?>">
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Permanent Address</label>
                            <div class="col-sm-5">
                            <textarea name="permanent_address" class="form-control"><?=isset($employee_detail->permanent_address)? $employee_detail->permanent_address:''?></textarea>
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Present Address</label>
                            <div class="col-sm-5">
                            <textarea name="present_address" class="form-control"><?=isset($employee_detail->present_address)? $employee_detail->present_address:''?></textarea>
                            </div>
    </div>
  </div>
  </div>
  <div class="tab">
  <h2 class="emp-form-heading">C. QUALIFICATION</h2>
  <div class="col-8">
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Highest Qualification</label>
                            <div class="col-sm-5">
                            <select  name="highest_qualification" class="form-control" >
                                <option selected><?=isset($employee_detail->highest_qualification)? $employee_detail->highest_qualification:''?></option>
                            </select>
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Year of Passing</label>
                            <div class="col-sm-5">
                            <input type="text" name="year_of_passing" placeholder="Enter Year Of Passing" class="form-control" value="<?=isset($employee_detail->year_of_passing)? $employee_detail->year_of_passing:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Grade/ Division</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="grade" placeholder="Enter Grade/ Division." value="<?=isset($employee_detail->grade)? $employee_detail->grade:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Institute</label>
                            <div class="col-sm-5">
                            <textarea name="institute" class="form-control"><?=isset($employee_detail->institute)? $employee_detail->institute:''?></textarea>
                            </div>
</div>
  </div>
  </div>
  <div class="tab">
  <h2 class="emp-form-heading">D. JOB RELATED DATA</h2>
  <div class="col-8">
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Date of Entry in Service</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="date_of_entry_in_service" placeholder="Enter Date of Entry in Service." value="<?=isset($employee_detail->date_of_entry_in_service)? $employee_detail->date_of_entry_in_service:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Designation at Service Entry</label>
                            <div class="col-sm-5">
                            <select  name="designation_at_service_entry" class="form-control" >
                                <option selected><?=isset($employee_detail->designation_at_service_entry)? $employee_detail->designation_at_service_entry:''?></option>
                            </select>
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Pay Scales (BPS)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="pay_scales" placeholder="Enter Pay Scales (BPS)." value="<?=isset($employee_detail->pay_scales)? $employee_detail->pay_scales:''?>"> 
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of office Joined</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="name_of_office_joined" placeholder="Enter Pay Scales (BPS)." value="<?=isset($employee_detail->name_of_office_joined)? $employee_detail->name_of_office_joined:''?>" />
                            <!-- <select  name="name_of_office_joined" class="form-control" >
                                <option selected><?=isset($employee_detail->name_of_office_joined)? $employee_detail->name_of_office_joined:''?></option>
                            </select> -->
                            </div>
</div>
<div class="form-group row">
<label for="inputPassword" class="col-sm-5 col-form-label">Place of first Posting</label>
                            <div class="col-sm-7">
                            <span>District</span>
                            <?php $array = isset($employee_detail->place_of_first_posting)? $employee_detail->place_of_first_posting:'';
                                $array = explode(',',$array);
                            ?>
                            <select class="col-6" style="margin-left:24px" name="place_of_first_posting_district"  id="place_of_first_posting_district">
                                <option><?=!empty($array)? $array[0]:'Choose'?></option>
                            </select>
                            <br><br>
                            <span>Tehsil</span>
                            <select class="col-6" style="margin-left:36px" name="place_of_first_posting_tehsil" id="place_of_first_posting_tehsil">
                                 <option><?=!empty($array)? $array[1]:'Choose'?></option>
                            </select>
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Current Designation</label>
                            <div class="col-sm-5">
                            <select  name="current_designation" class="form-control" >
                                <option selected><?=isset($employee_detail->current_designation)? $employee_detail->current_designation:''?></option>
                            </select>
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Current Pay Scale (BPS)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="current_pay_scale" placeholder="Enter Current Pay Scale (BPS)." value="<?=isset($employee_detail->current_pay_scale)? $employee_detail->current_pay_scale:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Current Office</label>
                            <div class="col-sm-5">
                                <input name="name_of_current_office" class="form-control" value="<?=isset($employee_detail->name_of_current_office)? $employee_detail->name_of_current_office:''?>"/>
                            <!-- <select  name="name_of_current_office" class="form-control" >
                                <option selected><?=isset($employee_detail->name_of_current_office)? $employee_detail->name_of_current_office:''?></option>
                            </select> -->
                            </div>
</div>
<div class="form-group row">
<label for="inputPassword" class="col-sm-5 col-form-label">Place of Current Posting</label>
                            <br>
                            <div class="col-sm-7" class="form-control">
                            <span>District</span>
                            <?php $array = isset($employee_detail->place_of_current_posting)? $employee_detail->place_of_current_posting:'';
                                $array = explode(',',$array);
                            ?>
                            <select style="margin-left:24px" class="col-6" name="place_of_current_posting_district"  id="place_of_current_posting_district">
                                 <option><?=!empty($array)? $array[0]:'Choose'?></option>
                            </select>
                            <br><br>
                            <span>Tehsil</span>
                            <select class="col-6" style="margin-left:36px"  name="place_of_current_posting_tehsil" id="place_of_current_posting_tehsil">
                                <option><?=!empty($array)? $array[1]:'Choose'?></option>
                            </select>
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Date of Last Promotion (if any)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="date_of_last_promotion" placeholder="Enter Date of Last Promotion (if any)." value="<?=isset($employee_detail->date_of_last_promotion)? $employee_detail->date_of_last_promotion:''?>">
                            </div>
</div>
  </div>
  </div>
  <div class="tab">
  <h2 class="emp-form-heading">E. EMERGENCY CONTACT</h2>
  <div class="col-8">
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="emergency_name" placeholder="Enter Name." value="<?=isset($employee_detail->emergency_contact_name)? $employee_detail->emergency_contact_name:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Relationship</label>
                            <div class="col-sm-5">
                            <select  name="relationship" class="form-control" >
                                <option selected><?=isset($employee_detail->relationship)? $employee_detail->relationship:''?></option>
                                </select>
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Mobile Number</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="emergency_mobile_no" placeholder="Enter Mobile Number." value="<?=isset($employee_detail->emergency_mobile_no)? $employee_detail->emergency_mobile_no:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Postal Address</label>
                            <div class="col-sm-5">
                            <textarea name="postal_address" class="form-control"><?=isset($employee_detail->postal_address)? $employee_detail->postal_address:''?></textarea>
                            </div>
</div>
 
</div>
</div>
  <div class="tab">
  <h2 class="emp-form-heading">F. TRAININGS PROGRAMS ALREADY ATTENDED</h2>
  <div class="col-8">
  <div class="form-group row">

                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (1)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_1" placeholder="Name of Training Program." value="<?=isset($employee_detail->training_program_1)? $employee_detail->training_program_1:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (2).</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_2" placeholder="Name of Training Program." value="<?=isset($employee_detail->training_program_2)? $employee_detail->training_program_2:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (3)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_3" placeholder="Name of Training Program." value="<?=isset($employee_detail->training_program_3)? $employee_detail->training_program_3:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (4)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_4" placeholder="Name of Training Program." value="<?=isset($employee_detail->training_program_4)? $employee_detail->training_program_4:''?>">
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (5)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_5" placeholder="Name of Training Program." value="<?=isset($employee_detail->training_program_5)? $employee_detail->training_program_5:''?>">
                            </div>
</div>    
</div>
<div class="tab">
  <h2 class="emp-form-heading">G. CURRENT TRAINING PROGRAM JOINED</h2>
  <div class="col-8">
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Program</label>
                            <div class="col-sm-5">
                            <select id="select-gender" class="form-control" name="name_of_program">
                            <option><?=isset($employee_detail->name_of_program)? $employee_detail->name_of_program:'Choose'?></option>
                        </select>
                            </div>
</div>
</div>
</div>
                    </div>
                    <input type="hidden" name="add_employee_detail" value="1">
                   <!-- <button type="submit" class="btn-self-enrollment">Submit</button> -->
                            </fieldset>
            </form>
            </ul>
        </div>


    </section>
 <script>
       $(":input").inputmask();

 </script>

</main>