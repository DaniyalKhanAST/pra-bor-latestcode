

<main id="main" style="margin-top: 130px;">

<style>

    @keyframes fadein { from { opacity: 0; } to { opacity: 1; } }
    @-moz-keyframes fadein { from { opacity: 0; } to { opacity: 1; } }
    @-webkit-keyframes fadein { from { opacity: 0; } to { opacity: 1; } }
    @-o-keyframes fadein { from { opacity: 0; } to { opacity: 1; } }
    
    html {
        scroll-behavior: smooth;
    }

    *, *::before, *::after {
        box-sizing: border-box;
    }

    a {
        text-decoration: none;
    }

    body {
        font-family: 'Poppins';
        margin:0;
    }
    
    .mr-t-50 {
        margin-top:50px;
    }

    .close {
        position: sticky;
        right: 10px;
        top:10px;
    }

    .close a {
        border:solid 1px #d92727;
        color: #d92727;
        font-weight: 600;
        display: inline-block;
        padding:10px 20px;
    }

    .close a:hover {
        border:solid 1px #d92727;
        background-color: #d92727;
        color: #fff;
    }

    ul {
        list-style: none;
        padding:0;
    }
    
    .grid {
        margin-top:20px;
        display: grid;
        grid-gap: 10px;
        grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
    }

    .grid_item {
        text-align: center;
        
    }

    .grid_item {
        width:13rem;
        height:7rem;
        margin:0 auto;
        background-color: #05445D;
        color: #fff;
        padding:15px;
        align-items: center;
        display: grid;
        /* border:solid 2px transparent; */
        border-radius: 5px;

        position: relative;

        animation: fadein 0.5s;
        -moz-animation: fadein 0.5s;
        -webkit-animation: fadein 0.5s;
        -o-animation: fadein 0.5s;

        
        -webkit-box-shadow: 0px 6px 28px 0px #00000015;
        -moz-box-shadow: 0px 6px 28px 0px #00000015;
        box-shadow: 0px 5px 15px 0px #00000015;
    }
    
    .grid_item a:hover {
        /* border:solid 2px #d92727; */
        transition: all .2s ease-in-out;
    }

    .grid_item a:hover span {
        opacity: 1;
    }

    .content {
        max-width: 70%;
        margin: 0 auto;
    }

    .ic_grid {
        display: grid;
        grid-template-columns: 1fr;
        justify-content: center;
        align-items: center;
    }
    
    .cat_dec {
        font-weight: 600;
        font-size:14px;
        color:#fff;
        height: 50px;
        
        align-items: center;
        justify-content: center;
        display: flex;
    }
    

    .cat_img {
        height: 100px;
        display: inline-flex;
        justify-content: center;
        align-items: center;
    }

    .cat_img img {
        height: 80px;
    }
    
    .input-container {
        display: flex;
        width: 60%;
        margin-bottom: 15px;
        border:solid 2px #eee;
        border-radius: 8px;
        margin-top:20px;
        
        animation: fadein 0.5s;
        -moz-animation: fadein 0.5s;
        -webkit-animation: fadein 0.5s;
        -o-animation: fadein 0.5s;

        -webkit-box-shadow: 0px 6px 28px 0px #00000015;
        -moz-box-shadow: 0px 6px 28px 0px #00000015;
        box-shadow: 0px 5px 15px 0px #00000015;
    }

    .input-field {
        width: 100%;
        padding: 20px;
        outline: none;
        background: #fff;
        border: none;
        color: #282828;
        height: 55px;
        border-radius: 5px;
        font-weight: 400;
        font-size:14px;
        font-family: 'Poppins', sans-serif;
    }

    .content_top {
        display: flex;
        justify-content: center;
    }
    
    .cat_dec span {
        color:#fff;
        font-weight: 500;
        background-color: #00000070;
        padding:3px 5px;
        border-radius: 3px;
        margin-left:10px;
        
        position: absolute;
        top: 10px;
        right: 10px;

        opacity: 0.2;
    }


    @media only screen and ( max-width:1200px ) {
        .content {
            max-width: 90%;
            margin: 0 auto;
        }
        .input-container {
            display: flex;
            width: 90%;
        }

    }

    @media only screen and ( max-width:992px ) {
        .content {
            max-width: 90%;
            margin: 0 auto;
        }
        .input-container {
            display: flex;
            width: 90%;
        }
    }

    @media only screen and ( max-width:768px ) {

        .content {
            max-width: 90%;
            margin: 0 auto;
        }
        .input-container {
            display: flex;
            width: 90%;
        }
    }

    @media only screen and ( max-width:576px ) {
        
        .content {
            max-width: 90%;
        }
            
        .grid {
            grid-template-columns: 1fr;
        }

        .ic_grid {
            grid-template-columns: 1fr 4.2fr;
        }
        
        .cat_dec {
            margin:0;
            text-align: left;
            font-size:14px;
            height: auto;
            display: inline-block;
        }

        .grid_item a, .ic_grid, .cat_img {
            height: 80px;
        }

        .grid_item a {
            padding:0;
        }

        .grid_item img {
            height: 45px;
        }
        
        .input-container {
            width: 100%;
        }
        
        .cat_dec span {
            top: 26px;
            right: 20px;

            opacity: 0.8;
        }
    }
    .previous {
  background-color: #05445D;
  color: #fff;
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
  margin-top:30px;
  margin-left:20px
}
.imagegalview{
    height:200px;
    width:200px;
}
.cat_decimg{
    font-weight: 600;
    font-size: 14px;
    color: #fff;
    height: 220px;
    align-items: center;
    justify-content: center;
    display: flex;
}
.caption_img{
    text-align:center;
}
.previous:hover {
  background-color: #ddd;
  color: black;
}

</style>
<button class="previous"id="btn-pre" >&laquo; Previous</button>
<ul id="cat_list" class="grid">
</ul>
<script>
 
$(function () {

var categories = [];
$.getJSON("/_/category.json", function (data) {
    categories = data.cat_tr;
    abc();
});

function abc(){
    $.each(categories, function (i, f) {
        var cat_index = -1;
        if (typeof f.cat_name_alt !== 'undefined' && f.cat_name_alt.length > 0) {
            cat_index = i;
            
        }
        var tblRow = '<li class="grid_item" data-index="' + cat_index + '">' + '<div class="cat_dec">' + f.cat_name  + '</div>' + '</div>' + '</li>'


        $(tblRow).appendTo("#cat_list")
            
    });
    level='districts';
}
function refreshPage(){
    location.reload();
} 
document.getElementById ("btn-pre").addEventListener ("click",refreshPage, abc, false);

var level='districts';
var districtId;
var thesilid; 

$(document).on('click', '.grid_item', function (e) {
    //if ($(this).data('index') != -1) {
        e.preventDefault();
   
        var sub_categories;
        $('#cat_list').html('');
        if(level=='districts'){
            sub_categories = categories[parseInt($(this).data('index'))].cat_name_alt;
            districtId=parseInt($(this).data('index'));
            level='tehsil';
        }
        else if(level=='tehsil'){
            sub_categories = categories[districtId].cat_name_alt[parseInt($(this).data('index'))].event;
            level = 'event';
            thesilid=parseInt($(this).data('index'));
         
        }
        else if (level=='images'){
            sub_categories = categories[districtId].cat_name_alt[thesilid].event[parseInt($(this).data('index'))].images;
            level="images"; 

        }
        else {
            sub_categories = categories[districtId].cat_name_alt[thesilid].event[parseInt($(this).data('index'))].cap;
            level="images"; 
        }
        
        
        $.each(sub_categories, function (i, f) {
            var cat_index = -1;
            if (typeof f.cat_name_alt !== 'undefined' && f.cat_name_alt.length > 0) {
                cat_index = i;
            }
            var tblRow;
            if(level=='tehsil'){
                tblRow= '<li class="grid_item" data-index="' + i + '">' + '<div class="ic_grid">'  + '<div class="cat_dec">' + f.cat_name  + '</div>' + '</div>' + '</li>'
        }
        else if (level=='event'){
            tblRow= '<li class="grid_item" data-index="' + i + '">' + '<div class="ic_grid">'  + '<div class="cat_dec">' + f.name  + '</div>' + '</div>' + '</li>'
        }
        else if (level=='images'){
            tblRow= '<li class="" data-index="' + i + '">' + '<div class="">'  + '<div class="cat_decimg">' +'<img class="imagegalview" src="' + f+ '">' + '</div>' + '</div>' +'<p class="caption_img">'+f+'</p>'+ '</li>'

        }

        
            $(tblRow).appendTo("#cat_list")
            
         
        });
    //}
});
})

</script>

</main>