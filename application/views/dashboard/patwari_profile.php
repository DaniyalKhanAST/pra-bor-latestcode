<style>
    
.btn-self-enrollment{
    background:#05445E;
    color:white;
    border:1px solid black;
    width:6rem;
    height:3rem;

}
.main-div-flex{
    display:flex;
}
@media (max-width: 480px) {
    .main-div-flex{
       display:flow-root !important
    }
}

</style>
<main id="main" style="margin-top: 130px;">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <section >
        <h2>Patwari  Profile</h2>
        <div class="container">
            <ul class="col-11 searchlist" style="margin:auto">
            <form method="post" name="add_employee_detail" action="<?=base_url()?>/dashboard/add_employee_profile" enctype="multipart/form-data">
                <div class="step step-1 active" >
                <h2 class="emp-form-heading" style="text-align:center"> Add Basta Submitted (PDF)</h2>
                <div class="" style="width:70%;margin:auto" >
                <div class=""> 
                     <div class="form-group row">
      
      <label for="Field_Map" class="col-sm-6 col-form-label">Field Map</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Field_Map" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <!-- <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['cnic'])? $this->session->flashdata('error')['cnic']:'';?></span> -->
      </div>
   

  </div>
  <div class="form-group row">
      <label for="Field_Book" class="col-sm-6 col-form-label">Field Book</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Field_Book" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <!-- <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['name'])? $this->session->flashdata('error')['name']:'';?></span> -->
      </div>
  </div>
  <div class="form-group row">
      <label for="Khatoni" class="col-sm-6 col-form-label">Khatoni</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Khatoni" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['father_name'])? $this->session->flashdata('error')['father_name']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Khatoni_Pemaish" class="col-sm-6 col-form-label">Khatoni Pemaish</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Khatoni_Pemaish" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['gender'])? $this->session->flashdata('error')['gender']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Record_of_Right" class="col-sm-6 col-form-label">Record of Right (  مثل معیادی)</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Record_of_Right" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['date_of_birth'])? $this->session->flashdata('error')['date_of_birth']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Record_of_Right" class="col-sm-6 col-form-label">Record of Right (مثل ہیت)</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Record_of_Right" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      
      </div>
  </div>
  <div class="form-group row">
      <label for="Mutation_Register" class="col-sm-6 col-form-label">Mutation Register(رجسٹرانتقاالت)</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Mutation_Register" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
          <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['marital_status'])? $this->session->flashdata('error')['marital_status']:'';?></span>
      </div> 
  </div>
  <div class="form-group row">
      <label for="Crop_Inspection_Register" class="col-sm-6 col-form-label">Crop Inspection Register(گرداوری)</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Crop_Inspection_Register" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Roznamcha_Waqiati" class="col-sm-6 col-form-label">Roznamcha Waqiati(روزنامچہ واقعاتی)</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Roznamcha_Waqiati" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Roznamcha_Partal" class="col-sm-6 col-form-label">Roznamcha Partal(روزنامچہ پڑتال)</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Roznamcha_Partal" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Register_Kargozari" class="col-sm-6 col-form-label">Register Kargozari</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Register_Kargozari" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Register_Taghurat_Kasht" class="col-sm-6 col-form-label">Register Taghurat Kasht</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Register_Taghurat_Kasht" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Register_Fard_Badr" class="col-sm-6 col-form-label">Register Fard Badr
</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Register_Fard_Badr" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Animal_Husbandry" class="col-sm-6 col-form-label">Animal Husbandry(الل کتاب دیہی)</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Animal_Husbandry" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="Register_Jinswar" class="col-sm-6 col-form-label">Register Jinswar</label>
      <div class="col-sm-6">
      <input style="padding: 3px;" type="file" class="form-control"  name="Register_Jinswar" placeholder="Field_Map" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div>
</div>
  </div>
 
                
 

                   
            </form>
            <div class="col-9" style="margin:auto">
                    <input type="hidden" name="add_employee_detail" value="1">
                   <button type="submit" class="btn-self-enrollment">Submit</button>
                   </div>
            </ul>
           
        </div>


    </section>
    <script type="text/javascript" src="https://www.appelsiini.net/download/jquery.chained.mini.js"></script>
  <script>
//       $alloption=$("#select-program").html();
// $("#current_designation").change(function(){

//      $("#select-program").html($alloption);

//     var val=$("#current_designation").find(":selected").val();

//     $("#select-program option[value!="+val+"]").remove();


// });
//       $(function(){
//  $("#select-program").chained("#current_designation");
// });
  </script>
 <script>

     $(function(){
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;  
      
 
    $('#txtDate1').attr('max', maxDate);
    $('#txtDate2').attr('max', maxDate);
});
var maxBirthdayDate = new Date();
maxBirthdayDate.setFullYear( maxBirthdayDate.getFullYear() - 18 );
maxBirthdayDate.setMonth(11,31);
$( function() {
  $( "#txtDate" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    maxDate: maxBirthdayDate,
    yearRange: '1900:'+maxBirthdayDate.getFullYear(),
  });
}) 
      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result)
                        .width(400)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
       $(":input").inputmask();
        var base_url = "<?php echo base_url()?>";
       $(document).on("change", 'select#place_of_current_posting_district', function(e) {
        let district_name = $(this).val();

        $.ajax({
            type: "POST",
            data: {district_name: district_name},
            url: base_url+'dashboard/get_tehsil',
            dataType: 'json',
            success: function(json) {
                var $el = $("select#place_of_current_posting_tehsil");
                $el.empty(); // remove old options
                $el.append("<option disabled>choose</option>");
                $.each(json, function(k, v) {
                    let select = (k=='0')? 'selected':'';
                    $el.append("<option "+select+" value='" + v.tehsil + "'>" + v.tehsil + "</option>");
                });
            }
        });

    });

    $(document).on("change", 'select#place_of_first_posting_district', function(e) {
        let district_name = $(this).val();

        $.ajax({
            type: "POST",
            data: {district_name: district_name},
            url: base_url+'dashboard/get_tehsil',
            dataType: 'json',
            success: function(json) {
                var $el = $("select#place_of_first_posting_tehsil");
                $el.empty(); // remove old options
                $el.append("<option disabled>choose</option>");
                $.each(json, function(k, v) {
                    let select = (k=='0')? 'selected':'';
                    $el.append("<option "+select+" value='" + v.tehsil + "'>" + v.tehsil + "</option>");
                });
            }
        });

    });

    $(document).on("change", 'select#current_designation', function(e) {
        
        let designation = $(this).val();
        $.ajax({
            type: "POST",
            data: {designation: designation},
            url: base_url+'dashboard/get_programs',
            dataType: 'json',
            success: function(json) {
                console.log(json);
                let $el = $("select#select-program");
                $el.empty();
                $el.append("<option disabled>choose</option>");
                $.each(json, function(k, v) {
                    $el.append("<option  value='" + v.name + "'>" + v.name + "</option>");
                });
            }
        });
        
});
   
 </script>

</main>