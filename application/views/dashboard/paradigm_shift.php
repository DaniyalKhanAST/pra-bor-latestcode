<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Paradigm Shift <span style="float:right;">پیرا میٹر تبدیلی</span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <p>A complete paradigm shift was thus required to make the whole sector competitive and feasible.
                What was the way out? Retire the existing static infrastructure- one generation’s work; rebuild the requisite mobile infrastructure- another generation’s work; optimize the correct positioning of the sector governance- yet another generation’s work.
                <br>

            </p>
            <br>
            <img src="<?php echo base_url() ?>assets/images/home/paradigmshift.jpg">
            <p style="font-style: italic;margin-top: 0px;">Concept diagram developed by the Author</p>
<p>Public Policy formulation is inherently a process whereby “Expression of Popular Will” is synthesized through basic political organizations and institutionally shaped up through public representative “institutions”. However, in any other scenario, extremely important challenge is to generate enough empirical evidence along dimension of availability, practice and collection, to frame appropriate sets of policies and interventions.
    <br>

    <br>
    <span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
پہلے سے موجود جامد بنیادی ڈھانچہ کو ختم کردیا جائے جو کہ ایک نسل کام ہے اور مطلوبہ موبائل بنیادی ڈھانچہ کی تعمیر نو جوکہ ایک اور نسل کا کام گورننس کے شعبے کی درست پوزیشنگ کو بہترین بنانا اس کام میں بھی مزید ایک نسل جتنا عرصہ درگا ہے۔ پبلک پالیسی کی تشکیل ایک موروثی عمل ہے جبکہ کثرت اظہار رائے بنیادی سیاسی تنظیموں کے ذریعے تشکیل دیا جاتا ہے اور محکمانہ شکل عوامی نمائندگی اداروں کے ذریعے دی جاتی ہے۔ تاہم دوسرے منظرنامے میں پالیسیوں اور مداخلتوں کی مناسب تشکیل کے لئے جو انتہائی اہم چیلنج درپیش ہے وہ یہ ہے کہ عملی ثبوت کا مجموعہ پیداکرنا ان کی دستیابی کے ساتھ ساتھ اس پر عمل کرنا ہے۔
    </span>
</p>
        </div><!-- /.container -->
    </section>


</main>