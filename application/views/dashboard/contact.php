<main id="main" style="margin-top: 130px;">


    <section class="tertlinks">
        <h2><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px" enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#FFFFFF" cx="3" cy="7.3" r="3"></circle>
                        <rect x="8.8" y="5" fill="#FFFFFF" width="14.5" height="4.6"></rect>
                    </g>
                    <g>
                        <circle fill="#FFFFFF" cx="3" cy="16" r="3"></circle>
                        <rect x="8.8" y="13.6" fill="#FFFFFF" width="23.2" height="4.6"></rect>
                    </g>
                    <g>
                        <circle fill="#FFFFFF" cx="3" cy="24.7" r="3"></circle>
                        <rect x="8.8" y="22.4" fill="#FFFFFF" width="23.2" height="4.6"></rect>
                    </g>
                </g>
            </svg>
            Connect Menu</h2>

        <div class="container">
            <ul class="grid_list_4">
                <li>
                    <a href="/connect/facebook.html">
                        <i class="icon icons-connect-facebook"></i>
                        <h3>Facebook</h3>
                        <p>Find agency Facebook pages by category and see their latest status updates.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/twitter.html">
                        <i class="icon icons-connect-twitter"></i>
                        <h3>Twitter</h3>
                        <p>Find and follow agency Twitter accounts and see their latest tweets.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/youtube.html">
                        <i class="icon icons-connect-youtube"></i>
                        <h3>YouTube</h3>
                        <p>Find and subscribe to agency YouTube accounts.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/googleplus.html">
                        <i class="icon icons-connect-googleplus"></i>
                        <h3>Google+</h3>
                        <p>Utah.gov on Google+.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/feeds.html">
                        <i class="icon icons-connect-feeds"></i>
                        <h3>RSS Feeds</h3>
                        <p>Find and subscribe to agency RSS Feeds.</p>
                    </a>
                </li>
                <li>
                    <a href="/multimedia/index.html">
                        <i class="icon icons-connect-multimedia"></i>
                        <h3>Multimedia</h3>
                        <p>Keep up to date with audio, video and media resources from state agencies.</p>
                    </a>
                </li>

                <li>
                    <a href="/connect/mobile.html">
                        <i class="icon icons-connect-mobile"></i>
                        <h3>Mobile Applications</h3>
                        <p>Find Utah government applications for your mobile phone.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/photos.html">
                        <i class="icon icons-connect-photos"></i>
                        <h3>Photo Networking</h3>
                        <p>View photos of Utah.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/widgets.html">
                        <i class="icon icons-connect-widgets"></i>
                        <h3>Widgets</h3>
                        <p>Get state government delivered directly to your desktop.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/presentations.html">
                        <i class="icon icons-connect-presentations"></i>
                        <h3>Presentations</h3>
                        <p>View a wide variety of presentations from across the state.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/podcasts.html">
                        <i class="icon icons-connect-podcasts"></i>
                        <h3>Podcasts</h3>
                        <p>Listen to gripping state government podcasts.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/publications.html">
                        <i class="icon icons-connect-publications"></i>
                        <h3>Publications</h3>
                        <p>Browse and array of Utah publications.</p>
                    </a>
                </li>
                <li>
                    <a href="/developer/index.html">
                        <i class="icon icons-connect-developer"></i>
                        <h3>Developer</h3>
                        <p>Resources for Utah.gov development</p>
                    </a>
                </li>

                <li>
                    <a href="/connect/blogs.html">
                        <i class="icon icons-connect-blogs"></i>
                        <h3>Utah Blogs</h3>
                        <p>Discover agency, legislative, and education blogs from across Utah state government.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/pinterest.html">
                        <i class="icon icons-connect-pinterest"></i>
                        <h3>Pinterest</h3>
                        <p>Pinterest is a way for Utahns to share, or "pin", images, videos, and other objects to a pinboard.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/linkedin.html">
                        <i class="icon icons-connect-linkedin"></i>
                        <h3>LinkedIn</h3>
                        <p>Find Utah agencies on LinkedIn.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/flipboard.html">
                        <i class="icon icons-connect-flipboard"></i>
                        <h3>Flipboard</h3>
                        <p>Tips on viewing the hundreds of Utah.gov connection strams with Flipboard.</p>
                    </a>
                </li>
                <li>
                    <a href="/collaborate/index.html">
                        <i class="icon icons-connect-collaborate"></i>
                        <h3>Collaborate</h3>
                        <p>Collaborate to make Utah.gov even better.</p>
                    </a>
                </li>
                <li>
                    <a href="/connect/qr.html">
                        <i class="icon icons-connect-qrcodes"></i>
                        <h3>QR Codes</h3>
                        <p>An explanation of QR (Quick Response) codes and how Utah.gov uses them.</p>
                    </a>
                </li>
            </ul>
        </div><!-- /.container -->
    </section>


</main>