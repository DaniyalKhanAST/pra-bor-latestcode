<main id="main" style="margin-top: 130px;">
    <header>

        <section>
            <!--[if gte IE 9]><!-->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px"
                 enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="7.842" r="2.813"/>
                        <rect x="9.248" y="5.667" fill="#676868" width="13.599" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="16" r="2.813"/>
                        <rect x="9.248" y="13.773" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="24.158" r="2.813"/>
                        <rect x="9.248" y="21.984" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                </g>
            </svg>

            <!--<![endif]-->

            <h1>Contact Us</h1>

            <p class="agency-search">
                <input type="text" class="search-agencies" title="Search" placeholder="Search..."/>
                <button type="button" class="clear-search">Clear Search</button>
            </p>

        </section>

    </header>
    <section class="agency-section">
        <!-- <h2>Administration Department</h2> -->
        <h2>PHFMC Head Office</h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php 
                    //echo "<pre>",print_r($phfmc_head_office);die; 
                    if (isset($phfmc_head_office) && !empty($phfmc_head_office)) {
                        foreach ($phfmc_head_office as $row) { ?>
                            <li itemscope="" itemtype="" class="item  local"><a target="_blank" itemprop="url">
                                    <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name ?></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <table style="width:100%">
                                    <tr ">
                                        <th><b>Designation: </b><?php if (!empty($row->designation)) {
                                                echo $row->designation;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Email Address: </b><?php if (!empty($row->email_address)) {
                                                echo $row->email_address;
                                            } else {
                                                echo "N/A";
                                            } ?> </th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Contact Number: </b> <?php if (!empty($row->contact_number)) {
                                                echo $row->contact_number;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Department: </b><?php if (!empty($row->fax)) {
                                                echo $row->department;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                </table>

                            </span></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        <!-- <img src="<?=base_url()?>assets/images/phfmc_head_office.jpeg"> -->

        </div>
    </section>
    <section class="agency-section">
        <!-- <h2>Director General Extension wing</h2> -->
        <h2>PHFMC Board</h2>
        
        <div class="container " id="target2" style="display: none">
        <!-- <img src="<?=base_url()?>assets/images/phfmc_head_office.jpeg"> -->
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php if (isset($phfmc_board) && !empty($phfmc_board)) {
                        foreach ($phfmc_board as $row) { ?>
                            <li itemscope="" itemtype="" class="item  local"><a target="_blank" itemprop="url">
                                    <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name ?></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <table style="width:100%">
                                    <tr >
                                        <th><b>Designation: </b><?php if (!empty($row->designation)) {
                                                echo $row->designation;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Email Address: </b><?php if (!empty($row->email_address)) {
                                                echo $row->email_address;
                                            } else {
                                                echo "N/A";
                                            } ?> </th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Contact Number: </b> <?php if (!empty($row->contact_number)) {
                                                echo $row->contact_number;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Fax: </b><?php if (!empty($row->fax)) {
                                                echo $row->fax;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                </table>

                            </span></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </div>
    </section>
    <section class="agency-section">
        <!-- <h2> Director General Research Wing</h2> -->
        <h2>Hospitals</h2>
        <div class="container " id="target2" style="display: none">
        <!-- <img src="<?=base_url()?>assets/images/phfmc_head_office.jpeg"> -->

            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php if (isset($phfmc_hospitals) && !empty($phfmc_hospitals)) {
                        foreach ($phfmc_hospitals as $row) { ?>
                            <li itemscope="" itemtype="" class="item  local"><a target="_blank" itemprop="url">
                                    <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name ?></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <table style="width:100%">
                                    <tr >
                                        <th><b>Designation: </b><?php if (!empty($row->designation)) {
                                                echo $row->designation;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Email Address: </b><?php if (!empty($row->email_address)) {
                                                echo $row->email_address;
                                            } else {
                                                echo "N/A";
                                            } ?> </th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Contact Number: </b> <?php if (!empty($row->contact_number)) {
                                                echo $row->contact_number;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Fax: </b><?php if (!empty($row->fax)) {
                                                echo $row->fax;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                </table>

                            </span></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </div>
    </section>

    <section class="agency-section">
        <!-- <h2> Additional Director Livestock</h2> -->
        <h2>Rural Health Centers</h2>
        
        <div class="container " id="target2" style="display: none">
        <!-- <img src="<?=base_url()?>assets/images/phfmc_head_office.jpeg"> -->
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php if (isset($phfmc_rural_health_centers) && !empty($phfmc_rural_health_centers)) {
                        foreach ($phfmc_rural_health_centers as $row) { ?>
                            <li itemscope="" itemtype="" class="item  local"><a target="_blank" itemprop="url">
                                    <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name ?></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <table style="width:100%">
                                    <tr >
                                        <th><b>Designation: </b><?php if (!empty($row->designation)) {
                                                echo $row->designation;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Email Address: </b><?php if (!empty($row->email_address)) {
                                                echo $row->email_address;
                                            } else {
                                                echo "N/A";
                                            } ?> </th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Contact Number: </b> <?php if (!empty($row->contact_number)) {
                                                echo $row->contact_number;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Fax: </b><?php if (!empty($row->fax)) {
                                                echo $row->fax;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                </table>

                            </span></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </div>
    </section>
    
    <section class="agency-section">
        <!-- <h2> Divisional Directors</h2> -->
        <h2>Basic Health Centers</h2>
        
        <div class="container " id="target2" style="display: none">
        <!-- <img src="<?=base_url()?>assets/images/phfmc_head_office.jpeg"> -->
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php if (isset($phfmc_basic_health_centers) && !empty($phfmc_basic_health_centers)) {
                        foreach ($phfmc_basic_health_centers as $row) { ?>
                            <li itemscope="" itemtype="" class="item  local"><a target="_blank" itemprop="url">
                                    <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name ?></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <table style="width:100%">
                                    <tr>
                                        <th><b>Designation: </b><?php if (!empty($row->designation)) {
                                                echo $row->designation;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Email Address: </b><?php if (!empty($row->email_address)) {
                                                echo $row->email_address;
                                            } else {
                                                echo "N/A";
                                            } ?> </th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Contact Number: </b> <?php if (!empty($row->contact_number)) {
                                                echo $row->contact_number;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Fax: </b><?php if (!empty($row->fax)) {
                                                echo $row->fax;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                </table>

                            </span></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </div>
    </section>

    

    <section class="agency-section">
        <!-- <h2> Deputy Director Officer</h2> -->
        <h2>Mobile Health Centers</h2>
        
        <div class="container ">
        <!-- <img src="<?=base_url()?>assets/images/phfmc_head_office.jpeg"> -->
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php if (isset($phfmc_mobile_health_centers) && !empty($phfmc_mobile_health_centers)) {
                        foreach ($phfmc_mobile_health_centers as $row) { ?>
                            <li itemscope="" itemtype="" class="item  local"><a target="_blank" itemprop="url">
                                    <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name ?></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <table style="width:100%">
                                    <tr >
                                        <th><b>Designation: </b><?php if (!empty($row->designation)) {
                                                echo $row->designation;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Email Address: </b><?php if (!empty($row->email_address)) {
                                                echo $row->email_address;
                                            } else {
                                                echo "N/A";
                                            } ?> </th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Contact Number: </b> <?php if (!empty($row->contact_number)) {
                                                echo $row->contact_number;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Fax: </b><?php if (!empty($row->fax)) {
                                                echo $row->fax;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                </table>

                            </span></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </div>
    </section>

    <!-- <section class="agency-section">
        <h2> Government Farms</h2>

        <div class="container ">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php if (isset($govf) && !empty($govf)) {
                        foreach ($govf as $row) { ?>
                            <li itemscope="" itemtype="" class="item  local"><a target="_blank" itemprop="url">
                                    <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name ?></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <table style="width:100%">
                                    <tr>
                                        <th><b>Designation: </b><?php if (!empty($row->designation)) {
                                                echo $row->designation;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Email Address: </b><?php if (!empty($row->email_address)) {
                                                echo $row->email_address;
                                            } else {
                                                echo "N/A";
                                            } ?> </th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Contact Number: </b> <?php if (!empty($row->contact_number)) {
                                                echo $row->contact_number;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                    <tr style="margin-top: 15px">
                                        <th><b>Fax: </b><?php if (!empty($row->fax)) {
                                                echo $row->fax;
                                            } else {
                                                echo "N/A";
                                            } ?></th>
                                    </tr>
                                </table>

                            </span></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </div>
    </section> -->
</main>