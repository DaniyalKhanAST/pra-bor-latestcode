<style>
    input[type="file"] {
    display: none;

}
.btn-self-enrollment{
    background:#05445E;
    color:white;
    border:1px solid black;
    width:6rem;
    height:3rem;

}
.main-div-flex{
    display:flex;
}
@media (max-width: 480px) {
    .main-div-flex{
       display:flow-root !important
    }
}

</style>
<main id="main" style="margin-top: 130px;">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <section >
        <h2>Employee Profile</h2>
        <div class="container">
            <ul class="col-11 searchlist" style="margin:auto">
            <form method="post" name="add_employee_detail" action="<?=base_url()?>/dashboard/add_employee_profile" enctype="multipart/form-data">
                <div class="step step-1 active" >
                <h2 class="emp-form-heading">A. PERSONAL DATA</h2>
                <div class="main-div-flex" >
                <div class="col-8"> 
                     <div class="form-group row">
      
      <label for="staticEmail" class="col-sm-5 col-form-label">CNIC</label>
      <div class="col-sm-5">
      <input type="text" class="form-control" readonly data-inputmask="'mask': '99999-9999999-9'" name="cnic" placeholder="Enter CNIC" value="<?php echo !empty($this->session->userdata('user_cnic'))? $this->session->userdata('user_cnic'):''?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['cnic'])? $this->session->flashdata('error')['cnic']:'';?></span>
      </div>
   

  </div>
  <div class="form-group row">
      <label for="inputPassword" class="col-sm-5 col-form-label">Name</label>
      <div class="col-sm-5">
      <input type="text" class="form-control" readonly name="name" placeholder="Enter Name" value="<?php echo !empty($this->session->userdata('user_name'))? $this->session->userdata('user_name'):''?>" value="<?=($this->input->post('name'))? $this->input->post('name'):'' ?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['name'])? $this->session->flashdata('error')['name']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="inputPassword" class="col-sm-5 col-form-label">Father's Name</label>
      <div class="col-sm-5">
      <input type="text" class="form-control" name="father_name" placeholder="Enter Father's Name" value="<?=($this->input->post('father_name'))? $this->input->post('father_name'):'' ?>" >
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['father_name'])? $this->session->flashdata('error')['father_name']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="inputPassword" class="col-sm-5 col-form-label">Gender</label>
      <div class="col-sm-5">
      <select id="select-gender" class="form-control" name="gender" >
          <option disabled>Choose...</option>
          <option value="male">Male</option>
          <option value="female">Female</option>
          <option value="other">Other</option>
      </select>
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['gender'])? $this->session->flashdata('error')['gender']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="inputPassword" class="col-sm-5 col-form-label">Date of Birth</label>
      <div class="col-sm-5">
      <input type="date" id="txtDate" class="form-control" name="date_of_birth" placeholder="" min="1980-01-01" max="2004-01-01" value="<?=($this->input->post('date_of_birth'))? $this->input->post('date_of_birth'):'' ?>">
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['date_of_birth'])? $this->session->flashdata('error')['date_of_birth']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="inputDomicile" class="col-sm-5 col-form-label">Domicile</label>
      <div class="col-sm-5">
      <select id="select-Domicile" name="domicile" class="form-control">
          <option disabled>Choose...</option>
          <?php foreach($districts as $city):?>
          <option value="<?=$city->district?>"><?=$city->district?></option>
          <?php endforeach; ?>
      </select>
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['domicile'])? $this->session->flashdata('error')['domicile']:'';?></span>
      </div>
  </div>
  <div class="form-group row">
      <label for="inputPassword" class="col-sm-5 col-form-label">Marital Status</label>
      <div class="col-sm-5">
          <select  name="marital_status" class="form-control" >
              <option disabled>Choose...</option>
              <?php foreach($marital_status as $item):?>
              <option value="<?=$item->name?>"><?=$item->name?></option>
              <?php endforeach; ?>
          </select>
          <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['marital_status'])? $this->session->flashdata('error')['marital_status']:'';?></span>
      </div> 
  </div>
  <div class="form-group row">
      <label for="inputPassword" class="col-sm-5 col-form-label">Nationality</label>
      <div class="col-sm-5">
      <select id="select-Nationality" name="nationality" class="form-control" >
      <option selected="selected" value="Pakistan">Pakistan</option>
          <?php foreach($countries as $country):?>
          <option value="<?=$country->nicename?>"><?=$country->nicename?></option>
          <?php endforeach; ?>
      </select>
      <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['nationality'])? $this->session->flashdata('error')['nationality']:'';?></span>
      </div>
  </div></div>
 <div class="col">
<label style=" float:right; padding: 10px; background: #05445E; display: table;color: #fff;"> Upload Picture
    <input type="file" size="90" name="profile_img" onchange="readURL(this);">

</label> 
<div class="col-7">
    <img style="float:right" src="#" id="preview" class="img-thumbnail">

</div>
<span style="color:red;"><?php echo !empty($this->session->flashdata('error')['profile_img'])? $this->session->flashdata('error')['profile_img']:'';?></span>
                </div>
                </div>
          </div>
                <div class="step step-2 " >
<div class="tab">
  <h2 class="emp-form-heading">B. CONTACT INFORMATION</h2>
  <div class="col-8"> 
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Mobile No.</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" readonly name="mobile_no" data-inputmask="'mask': '0399-9999999'" placeholder="Enter Mobile No." value="<?php echo !empty($this->session->userdata('user_mobile'))? $this->session->userdata('user_mobile'):''?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['mobile_no'])? $this->session->flashdata('error')['mobile_no']:'';?></span>
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Whats App No.</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="whatsapp_no" data-inputmask="'mask': '0399-9999999'" placeholder="Enter Whats App No." value="<?=($this->input->post('whatsapp_no'))? $this->input->post('whatsapp_no'):'' ?>">                            
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['whatsapp_no'])? $this->session->flashdata('error')['whatsapp_no']:'';?></span>

                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Residential Phone No (If any)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" data-inputmask="'mask': '099-9999999'"  name="residential_phone_no" placeholder="Enter Phone No." value="<?=($this->input->post('residential_phone_no'))? $this->input->post('residential_phone_no'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['residential_phone_no'])? $this->session->flashdata('error')['residential_phone_no']:'';?></span>
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">E-Mail Address</label>
                            <div class="col-sm-5">
                            <input type="email" class="form-control" readonly name="email" placeholder="Enter E-Mail Address." value="<?php echo !empty($this->session->userdata('user_email'))? $this->session->userdata('user_email'):''?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['email'])? $this->session->flashdata('error')['email']:'';?></span>
                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Permanent Address</label>
                            <div class="col-sm-5">
                            <textarea name="permanent_address" class="form-control"><?=($this->input->post('permanent_address'))? $this->input->post('permanent_address'):'' ?></textarea>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['permanent_address'])? $this->session->flashdata('error')['permanent_address']:'';?></span>

                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Present Address</label>
                            <div class="col-sm-5">
                            <textarea name="present_address" class="form-control"><?=($this->input->post('present_address'))? $this->input->post('present_address'):'' ?></textarea>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['present_address'])? $this->session->flashdata('error')['present_address']:'';?></span>

                            </div>
    </div>
  </div>
  </div>
  <div class="tab">
  <h2 class="emp-form-heading">C. QUALIFICATION</h2>
  <div class="col-8"> 
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Highest Qualification</label>
                            <div class="col-sm-5">
                                <select  name="highest_qualification" class="form-control" >
                                <option disabled>Choose...</option>
                                <?php foreach($qualifications as $item):?>
                                <option value="<?=$item->name?>"><?=$item->name?></option>
                                <?php endforeach; ?>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['highest_qualification'])? $this->session->flashdata('error')['highest_qualification']:'';?></span>

                            </div>
    </div>
    <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Year of Passing</label>
                            <div class="col-sm-5">
                            <input type="text" name="year_of_passing" placeholder="Enter Year Of Passing" class="form-control" value="<?=($this->input->post('year_of_passing'))? $this->input->post('year_of_passing'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['year_of_passing'])? $this->session->flashdata('error')['year_of_passing']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Grade/ Division</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="grade" placeholder="Enter Grade/ Division." value="<?=($this->input->post('grade'))? $this->input->post('grade'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['grade'])? $this->session->flashdata('error')['grade']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Institute</label>
                            <div class="col-sm-5">
                            <textarea name="institute" class="form-control"><?=($this->input->post('institute'))? $this->input->post('institute'):'' ?></textarea>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['institute'])? $this->session->flashdata('error')['institute']:'';?></span>

                            </div>
</div>
  </div>
  </div>
  <div class="tab">
  <h2 class="emp-form-heading">D. JOB RELATED DATA</h2>
  <div class="col-8"> 
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Date of Entry in Service</label>
                            <div class="col-sm-5">
                            <input  type="date" class="form-control" id="txtDate1" name="date_of_entry_in_service" placeholder="Enter Date of Entry in Service." value="<?=($this->input->post('date_of_entry_in_service'))? $this->input->post('date_of_entry_in_service'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['date_of_entry_in_service'])? $this->session->flashdata('error')['date_of_entry_in_service']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Designation at Service Entry</label>
                            <div class="col-sm-5">
                            <select  name="designation_at_service_entry" class="form-control" >
                                <option disabled>Choose...</option>
                                <?php foreach($designations as $item):?>
                                <option value="<?=$item->name?>"><?=$item->name?></option>
                                <?php endforeach; ?>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['designation_at_service_entry'])? $this->session->flashdata('error')['designation_at_service_entry']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Pay Scales (BPS)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control"  name="pay_scales" placeholder="Enter Pay Scales (BPS)" value="<?=($this->input->post('pay_scales'))? $this->input->post('pay_scales'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['pay_scales'])? $this->session->flashdata('error')['pay_scales']:'';?></span>
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of office Joined</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="name_of_office_joined" value="<?=($this->input->post('name_of_office_joined'))? $this->input->post('name_of_office_joined'):'' ?>">
                            <!-- <select  name="name_of_office_joined" class="form-control" >
                                <option disabled>Choose...</option>
                                <?php foreach($offices as $item):?>
                                <option value="<?=$item->name?>"><?=$item->name?></option>
                                <?php endforeach; ?>
                            </select> -->
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['name_of_office_joined'])? $this->session->flashdata('error')['name_of_office_joined']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Place of first Posting</label>
                            <div class="col-sm-7">
                            <span>District</span>
                            <select style="margin-left:12px" class="col-6" name="place_of_first_posting_district"  id="place_of_first_posting_district">
                                <option disabled>Choose...</option>
                                <?php foreach($districts as $item):?>
                                <option value="<?=$item->district?>"><?=$item->district?></option>
                                <?php endforeach; ?>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['place_of_first_posting_district'])? $this->session->flashdata('error')['place_of_first_posting_district']:'';?></span>

                            <br><br>
                            <span>Tehsil</span>
                            <select class="col-6" style="margin-left:24px" name="place_of_first_posting_tehsil" id="place_of_first_posting_tehsil">
                                <option disabled>Choose</option>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['place_of_first_posting_tehsil'])? $this->session->flashdata('error')['place_of_first_posting_tehsil']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Current Designation</label>
                            <div class="col-sm-5">
                            <select  name="current_designation" class="form-control"  id="current_designation">
                                <option disabled>Choose...</option>
                                <?php foreach($designations as $item):?>
                                <option value="<?=$item->name?>"><?=$item->name?></option>
                                <?php endforeach; ?>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['current_designation'])? $this->session->flashdata('error')['current_designation']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Current Pay Scale (BPS)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control"  name="current_pay_scale" placeholder="Enter Current Pay Scales (BPS)" value="<?=($this->input->post('current_pay_scale'))? $this->input->post('current_pay_scale'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['current_pay_scale'])? $this->session->flashdata('error')['current_pay_scale']:'';?></span>
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Current Office</label>
                            <div class="col-sm-5">
                                <input type="text" name="name_of_current_office" class="form-control" value="<?=$item->name?>">
                            <!-- <select  name="name_of_current_office" class="form-control" >
                                <option disabled>Choose...</option>
                                <?php foreach($offices as $item):?>
                                <option value="<?=$item->name?>"><?=$item->name?></option>
                                <?php endforeach; ?>
                            </select> -->
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['name_of_current_office'])? $this->session->flashdata('error')['name_of_current_office']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Place of Current Posting</label>
                            <br>
                            <div class="col-sm-7" class="form-control">
                            <span>District</span>
                            <select style="margin-left:12px"  class="col-6" name="place_of_current_posting_district"  id="place_of_current_posting_district">
                                <option disabled>Choose...</option>
                                <?php foreach($districts as $item):?>
                                <option value="<?=$item->district?>"><?=$item->district?></option>
                                <?php endforeach; ?>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['place_of_current_posting_district'])? $this->session->flashdata('error')['place_of_current_posting_district']:'';?></span>

                            <br><br>
                            <span>Tehsil</span>
                            <select class="col-6" style="margin-left:24px"  name="place_of_current_posting_tehsil" id="place_of_current_posting_tehsil">
                                <option disabled>Choose</option>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['place_of_current_posting_tehsil'])? $this->session->flashdata('error')['place_of_current_posting_tehsil']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Date of Last Promotion (if any)</label>
                            <div class="col-sm-5">
                            <input type="date" class="form-control" id="txtDate2" name="date_of_last_promotion" placeholder="Enter Date of Last Promotion (if any)." value="<?=($this->input->post('date_of_last_promotion'))? $this->input->post('date_of_last_promotion'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['date_of_last_promotion'])? $this->session->flashdata('error')['date_of_last_promotion']:'';?></span>

                            </div>
</div>
  </div>
  </div>
  <div class="tab">
  <h2 class="emp-form-heading">E. EMERGENCY CONTACT</h2>
  <div class="col-8">
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="emergency_name" placeholder="Enter Name." value="<?=($this->input->post('emergency_name'))? $this->input->post('emergency_name'):'' ?>" >
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['emergency_name'])? $this->session->flashdata('error')['emergency_name']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Relationship</label>
                            <div class="col-sm-5">
                                <select  name="relationship" class="form-control" >
                                <option disabled>Choose...</option>
                                <?php foreach($relations as $relation):?>
                                <option value="<?=$relation->name?>"><?=$relation->name?></option>
                                <?php endforeach; ?>
                            </select>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['relationship'])? $this->session->flashdata('error')['relationship']:'';?></span>

                            <!-- <input type="text" class="form-control" name="relationship" placeholder="Enter Relationship." > -->
                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Mobile Number</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="emergency_mobile_no" placeholder="Enter Mobile Number." value="<?=($this->input->post('emergency_mobile_no'))? $this->input->post('emergency_mobile_no'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['emergency_mobile_no'])? $this->session->flashdata('error')['emergency_mobile_no']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Postal Address</label>
                            <div class="col-sm-5">
                            <textarea name="postal_address" class="form-control"><?=($this->input->post('postal_address'))? $this->input->post('postal_address'):'' ?></textarea>
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['postal_address'])? $this->session->flashdata('error')['postal_address']:'';?></span>
                            </div>
</div>
 
</div>
</div>
  <div class="tab">
  <h2 class="emp-form-heading">F. TRAININGS PROGRAMS ALREADY ATTENDED</h2>
  <div class="col-8">
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (1)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_1" placeholder="Name of Training Program." value="<?=($this->input->post('training_program_1'))? $this->input->post('training_program_1'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['training_program_1'])? $this->session->flashdata('error')['training_program_1']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (2).</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_2" placeholder="Name of Training Program." value="<?=($this->input->post('training_program_2'))? $this->input->post('training_program_2'):'' ?>" >
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['training_program_2'])? $this->session->flashdata('error')['training_program_2']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (3)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_3" placeholder="Name of Training Program." value="<?=($this->input->post('training_program_3'))? $this->input->post('training_program_3'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['training_program_3'])? $this->session->flashdata('error')['training_program_3']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (4)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_4" placeholder="Name of Training Program." value="<?=($this->input->post('training_program_4'))? $this->input->post('training_program_4'):'' ?>" >
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['training_program_4'])? $this->session->flashdata('error')['training_program_4']:'';?></span>

                            </div>
</div>
<div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Training Program (5)</label>
                            <div class="col-sm-5">
                            <input type="text" class="form-control" name="training_program_5" placeholder="Name of Training Program." value="<?=($this->input->post('training_program_5'))? $this->input->post('training_program_5'):'' ?>">
                            <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['training_program_5'])? $this->session->flashdata('error')['training_program_5']:'';?></span>

                            </div>
</div> 
</div>    
<div class="tab">
  <h2 class="emp-form-heading">G. CURRENT TRAINING PROGRAM JOINED</h2>
  <div class="col-8">
  <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Name of Program</label>
                            <div class="col-sm-5">
                            <select id="select-program" class="form-control" name="name_of_program">
                                <option disabled>Choose...</option>
                                <?php foreach($programs as $item):?>
                                    <option value="<?=$item->name?>" class="<?=$item->designations?>"><?=$item->name?></option>
                                <?php break; endforeach; ?>
                            </select>
                        <span style="color:red;"><?php echo !empty($this->session->flashdata('error')['name_of_program'])? $this->session->flashdata('error')['name_of_program']:'';?></span>

                            </div>
</div>
</div>
                    </div>
                    </div>
                    <div class="col-8">
                    <input type="hidden" name="add_employee_detail" value="1">
                   <button type="submit" class="btn-self-enrollment">Submit</button>
                   </div>
            </form>
            </ul>
        </div>


    </section>
    <script type="text/javascript" src="https://www.appelsiini.net/download/jquery.chained.mini.js"></script>
  <script>
//       $alloption=$("#select-program").html();
// $("#current_designation").change(function(){

//      $("#select-program").html($alloption);

//     var val=$("#current_designation").find(":selected").val();

//     $("#select-program option[value!="+val+"]").remove();


// });
//       $(function(){
//  $("#select-program").chained("#current_designation");
// });
  </script>
 <script>

     $(function(){
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;  
      
 
    $('#txtDate1').attr('max', maxDate);
    $('#txtDate2').attr('max', maxDate);
});
var maxBirthdayDate = new Date();
maxBirthdayDate.setFullYear( maxBirthdayDate.getFullYear() - 18 );
maxBirthdayDate.setMonth(11,31);
$( function() {
  $( "#txtDate" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    maxDate: maxBirthdayDate,
    yearRange: '1900:'+maxBirthdayDate.getFullYear(),
  });
}) 
      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result)
                        .width(400)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
       $(":input").inputmask();
        var base_url = "<?php echo base_url()?>";
       $(document).on("change", 'select#place_of_current_posting_district', function(e) {
        let district_name = $(this).val();

        $.ajax({
            type: "POST",
            data: {district_name: district_name},
            url: base_url+'dashboard/get_tehsil',
            dataType: 'json',
            success: function(json) {
                var $el = $("select#place_of_current_posting_tehsil");
                $el.empty(); // remove old options
                $el.append("<option disabled>choose</option>");
                $.each(json, function(k, v) {
                    let select = (k=='0')? 'selected':'';
                    $el.append("<option "+select+" value='" + v.tehsil + "'>" + v.tehsil + "</option>");
                });
            }
        });

    });

    $(document).on("change", 'select#place_of_first_posting_district', function(e) {
        let district_name = $(this).val();

        $.ajax({
            type: "POST",
            data: {district_name: district_name},
            url: base_url+'dashboard/get_tehsil',
            dataType: 'json',
            success: function(json) {
                var $el = $("select#place_of_first_posting_tehsil");
                $el.empty(); // remove old options
                $el.append("<option disabled>choose</option>");
                $.each(json, function(k, v) {
                    let select = (k=='0')? 'selected':'';
                    $el.append("<option "+select+" value='" + v.tehsil + "'>" + v.tehsil + "</option>");
                });
            }
        });

    });

    $(document).on("change", 'select#current_designation', function(e) {
        
        let designation = $(this).val();
        $.ajax({
            type: "POST",
            data: {designation: designation},
            url: base_url+'dashboard/get_programs',
            dataType: 'json',
            success: function(json) {
                console.log(json);
                let $el = $("select#select-program");
                $el.empty();
                $el.append("<option disabled>choose</option>");
                $.each(json, function(k, v) {
                    $el.append("<option  value='" + v.name + "'>" + v.name + "</option>");
                });
            }
        });
        
});
   
 </script>

</main>