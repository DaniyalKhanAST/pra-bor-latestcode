<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Component Objectives and Indicators

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Component Objectives and Indicators: </h3>
            <p>The component objective is to establish a comprehensive project

                management structures to take care of project‘s planning, coordination, implementation, M&amp;E and financial

                management requirements in an effective and efficient manner. Key performance indicators are: establishment

                of all specified institutions in timely manner; recruitment/deployment of qualified/competent staff at provincial,

                district and subsidiary level in a timely manner; preparation and approval of annual and quarterly plans as per

                specified timelines; establishment of a smooth financial management system including special project accounts in

                designated banks, timely drawl of IFAD and counterpart funds, effective accounting and reporting system;

                engagement of a competent internal auditor; completion of annual audits in specified timelines; and quarterly

                and annual reporting on comprehensive formats within stipulated times.

                <br>

            </p>

        </div><!-- /.container -->
    </section>


</main>