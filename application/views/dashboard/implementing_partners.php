<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Implementing Partners

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3> Implementing Partners: </h3>
            <p>

                <br>
            <dl class="container">
                <dt>Social Mobilization and other Service Providers (SMP):</dt>
                <dd>

                    A social mobilization and other service providers will be contracted on competitive basis to assist in the project‘s

                    social mobilization needs and implementation of community organization level activities. TOR and criteria for the

                    selection of service providers are given in PIM The service provider will notify a district office in each of the four

                    districts headed by a District Program Manager (DPM) who will be the focal person for interface with the DMU.

                    The DPM will also be a member of the District Coordination Committee. The CEO of the service provider will be

                    member of the PSC. Funding for the social mobilization activities will be released by PCU in terms of the

                    agreement signed with the service provider and annual targets. The service provider will be required to establish

                    accounting and reporting systems as specified in the service provision agreement. Funding for the annual

                    approved planned activities will be provided in quarterly installments and next quarter installment will be

                    released on submission of previous quarter financial and physical progress reports to the PCU.</dd>
                <dt>Veterinary Universities and Faculties (TSP):</dt>
                <dd>
                    Veterinary Universities and Faculties in Punjab will be the key partners for all fodder, feed and management

                    related pilots and research as well for the training and capacity building activities financed by the project. The

                    Universities will be engaged on competitive basis against clearly defined tasks/TOR and output indicators. Local

                    Government &amp; Community Development Department (LG&amp;CD): LG&amp;CD will be the partner department for

                    reform of cattle market regulations and upgrading of 12 cattle markets in the project area. Secretary LG&amp;CD will

                    be a member of the project steering committee. LG&amp;CD will designate as a focal person to work with the PCU,

                    L&amp;DD and consultants on reform of market operations and regulations. Implementation framework for the

                    contracting and construction work for the cattle markets will also be agreed between LG&amp;CD and LS&amp;DD/PCU.</dd>
                <dt>Community Organizations (COs):</dt>
                <dd>

                    All village/farmer level project interventions will be implemented through active involvement of community

                    organizations established in each target village. CO formation, mobilization and organizational structure/process

                    will follow the already well-established process developed by RSPs/NGOs. These COs will primarily be women-

                    centered. A CO level need assessment for livestock development will inform the plan of interventions in each CO,

                    articulating the mutual responsibilities between the CO and project. COs will be required to contribute in cash or

                    kind 20% of all community based infrastructure and 50% of all household based investments. All decisions about

                    project interventions in any village will be made in CO meetings, preferably with consensus and, if need be,

                    majority vote.
                </dd>
            </dl>
<dl class="container">
            <span style="float:right;direction:rtl;font-size:18px; text-align:justify;">
            <h3>
                نافزی شراکت دار:
            </h3>

سوشل موبلائزیشن اور دوسرے سروس پرووائیڈر:
                <br>
ایک سوشل موبلائزیسن اور دوسرے پرووائیڈرز کے ساتھ مقابلہ کی بنیاد پر معادہ کیا جائے گا تاکہ پراجیکٹ کی سوشل موبلائیزیشن کو اسسٹ کیا جاسکے اور کمیونٹی آرگنائیزیشن لیول سرگرمیوں کو نافز کیا جاسکے۔ اس کی ٹی او آراور معیار پروجیکٹ کی پی آئی ایم میں دیا گیا ہے۔ سروس پرووائیڈرہر ضلع کے اندر ایک ڈسٹرکٹ آفس قائم کرے گا جس کا سربراہ ڈسٹرکٹ پروگرام منیجر ہو گا جو کہ ڈی ایم یو کیلئے فوکل پرسن ہو گا۔ ڈی پی ایم، ڈسٹرکٹ کوآرڈینیشن کمیٹی کا ممبر بھی ہو گا۔ سروس پرووئیڈر کا سی ای او، پی ایس سی کا ممبر ہو گا۔ سوشل موبلائزیشن کی سرگومیوں کیلئے فنڈنگ پی سی او کی جانب سے سروس پرووئیڈر اور سالانہ ٹارگٹ کے معاہدے کے مطابق جاری کی جائے گی۔ معاہدے کے مطابق سروس پرووئیڈر اکائونٹنگ اور رپورٹنگ سسٹم بنائے گا۔ سالانہ سرگرمیوں کیلئے فنڈنگ چار حصوں میںجاری کی جائے گی۔ اگلی قسط بچھلی قسط کی فنانشل اور فزیکل پراگرس رپورٹ پی سی یو کو جمع کروانے کے بعد جاری کی جائے گی۔
                <br><br>
                <h3>
                    ویٹرنری یونیورسٹیرز اور فکلٹیز، ٹی ایس پی
                </h3>

پنجاب کے اندر ویٹرنری یونیورسٹیرز اور فکلٹیز چارہ، خوراک اور منیجمنٹ سے متعلقہ ریسرچ اور پراجیکٹس کے اہم شراکت دار ہوں گے۔ علاوہ ازیں پراجیکٹ کے معاشی سپورٹ کے ساتھ ٹریننگ اور کپیسٹی پلڈنگ کے شراکت دار ہون گے۔
                <br>
یونیورسٹیز مقابلہ کی بیناد پر واضح طور پر بتائے گی ٹی او آرز میں شامل ہوں گی۔
                <br>
لوکل گورنمنٹ اور کمیونٹی ڈیویلپمنٹ کیٹل مارکیٹ کی اصطلاحات اور ان کی اپ گریڈیشن کے لئے شراکت دار ہوں گے۔
                <br>
                سیکرٹری لوکل گورنمنٹ پروجیکٹ سیڑنگ کمیٹی کا ممبر ہو گا۔ لوکل گورنمنٹ پی سی او اورمحکمہ لائیوسٹاک کے ساتھ پراجیکٹ ماکیٹڈ ریگولیشنز کیلئے بطور فوکل پرسن کام کرگی۔
                <br>
                کیٹل مارکیٹ کی تعمیر کیلئے لوکل گورنمنٹ، لائیوسٹاک ڈیپارٹمنٹ اور پی سی او کا معاہدہ ہو گا۔
                <br>
                <br>
                <h3>
                    کمیونتی آرگنائزیشن:
                </h3>

                پراجیکٹ کی تمام دیہی، کسانوں کی سطح کی انٹرنیز کمیونٹی آرگنائزیشن کی متحرک مداخلت کے ذریعے لاگو کی جائے گی۔ جو کہ ہر دیہات میں قائم کی گئی ہیں۔ کمیونٹی آرگنائزیشن کو بنانے کیلئے پہلے سے بہتر طریقہ کار کو اپنایا جائے گا۔ یہ کمیونٹی آرگنائزیشن بنیادی طور پر خواتین پر منحصر ہوں گی۔ ہر کمیونٹی آرگنائزیشن کی ایک بیسمنٹ بنائی جائے گی جو کہ بتائے گی کہ اس آرگنائزیشن میں کیا گیا انٹروینشن مطلوب ہیں اور یہ پراجیکٹ اور آرگنائزیشن کے درمیان ذمہ داریاں منصفانہ طور پر تقسیم بھی ہوں گی۔ ان آرگنائزیشن میں بیس فیصد رقم کمیونٹی بیسڈ کے ڈھانچے پر لگائی جائے گی اور پچاس فیصد رقم گھریلو کاموں پر خرچ ہو گی۔ اور تمام پراجیکٹس کی انٹروینشن کے فیصلے سی اوز کی میٹنگ میں کثرت رائے سے ہوں گے۔
                <br>
                اس کمپوننٹ کے تحت پرجیکٹ کوآرڈینیشن یونٹ لاہور میں تشکیل دیا جائے گا۔ اس کمپوننٹ کا مقصد پرجیکٹ کوآرڈینیشن یونٹ تکنیکی ذمہ داریوں اور پروجیکٹ کوآرڈینیشن میں مدد فراہم کرنا ہے۔ اس کو ساتھ ساتھ اگر کسی بھی سرگرمی میں اضافہ مدد اور یا امیدافزاں وسعت یا اچھی کارکرگی کا مظاہر کرنا ہو تو اس کیلئے کخصوص قنائسنگ فیسیلٹی۔ اس کمپوننٹ کے تحت تشکیل دی جائے گی۔
                <br>
                <div style="margin-right:20px;">
                    ۔ کمپوننٹ کے مقاصد اور انڈیکیڑ
                    <br>
                    ۔ کمپوننٹ کی سرگرمیاں اور ممکنہ نتائج
                </div>
                <br>

                <br>
            </span>

            </p>

</dl>



        </div><!-- /.container -->
    </section>


</main>