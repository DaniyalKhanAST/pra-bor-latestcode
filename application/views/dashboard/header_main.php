<?php header('Access-Control-Allow-Origin: *'); ?>
<?php $CI =& get_instance();
$CI->load->model('download_career_jobs_model');
$result = $CI->download_career_jobs_model->get_all();
$jobs = "#";
$locum = "#";
$volunteering = "#";
$call_letters = "#";
$filling_objection = "#";
$leave = "#";
$transfer_request = "#";
$experience_certificate = '#';

if(!empty($result))
{
    foreach($result as $row){
        if(trim($row->name) == 'Application Form for Jobs'){
            $jobs = $row->file_link;
        }else if(trim($row->name) == 'Application Form for Locum'){
            $locum = $row->file_link;
        }else if(trim($row->name) == 'Application for Volunteering'){
            $volunteering = $row->file_link;
        }else if(trim($row->name) == 'Interview Call Letters'){
            $call_letters = $row->file_link;
        }else if(trim($row->name) == 'Application Form for filing of objections on merit list'){
            $filling_objection = $row->file_link;
        }else if(trim($row->name) == 'Application Form for Leave'){
            $leave = $row->file_link;
        }else if(trim($row->name) == 'Application Form for Transfer Requests'){
            $transfer_request = $row->file_link;
        }else if(trim($row->name) == 'Application Form for Experience Certificate'){
            $experience_certificate = $row->file_link;
        }
    }
}


?>

<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<header id="main-header">

    <div class="wrapper">
        <a href="#" id="logo">

        </a>
        <button class="toggle-menu cmn-toggle-switch__htx">
            <span>toggle menu</span>
        </button>
    </div>

    <nav id="main-menu">

        <div id="settings">
            <p class="settings">
                <a href="#" class="defaultStyles">Default Settings</a>
                <a href="#" class="highContrast">High Contrast</a>
                <a href="#" class="textOnly">Text Only</a>
            </p>

            <div id="fontSizer">
                <span>Font Size:</span>
                <a href="#" title="Font size smaller" class="fontsize small" data-size="smallFont">A</a>
                <a href="#" title="Font size default" class="fontsize default">A</a>
                <a href="#" title="Font size larger" class="fontsize large" data-size="largeFont">A</a>
            </div>
            <p id="locationCheckin">
                <button type="button" class="check-in">Detect your location</button>
            </p>

            <p id="locationUpdate">
                <label for="check-in-mdata">or Set your location:</label>
                <input list="utcities" type="text" id="check-in-mdata" class="check-in-mdata" placeholder="City or zip"/>
                <button type="button" id="check-in-m" class="check-in-m">Set</button>
            </p>
            <datalist id="utcities" class="mIndex" data-format="optionlist" data-type="95" data-limit="300"></datalist>

        </div>

        <div id="dateline">
            <a href="#" id="toggleLocation" title="Location">
                <?php echo file_get_contents(base_url('assets/images/icons/location.svg')); ?>
                <span>My Location: </span><strong>Lahore</strong>
            </a>

            <a href="#" id="toggleSettings" title="Settings">
                <span>Settings</span>
                <?php echo file_get_contents(base_url('assets/images/icons/settings.svg')); ?>
            </a>

            <a id="supportLink" href="<?=base_url()?>temp.php" title="Support" target="_blank">
                <span>info@pra-borpunjab.gov.pk</span>
                <?php echo file_get_contents(base_url('assets/images/icons/support.svg')); ?>
            </a>
            
        </div>
        <ul>
            <li id="nav-home">
                <a href="<?= base_url(); ?>" title="PHFMC.gov">
                    <!-- <div>
                    style="height:80px;width:100px" -->
                    <!-- <img src="<?=base_url()?>/assets/images/home/prabor_logo.jpeg" style="height:30px;width:100px">  -->
                    
                    <svg class="utahgov-logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="850" height="200">
                    <text class="heading-text" x="2" y="40%" fill="#05445E"  font-size="30">Punjab Revenue Academy</text> 
                    <text class="heading-text1" x="30%" y="70%" fill="#05445E"  font-size="20">08000-9212</text>
                    </svg>
                </a>
            </li>
            <li id="nav-search">
                <a href="#" title="Search PHFMC.gov">
                    <?php echo file_get_contents(base_url('assets/images/icons/search.svg')); ?>
                    <span>Search</span></a>
                <div class="drop-down">
                    <form action="<?= base_url() ?>dashboard/search_results" id="searchbox_005946968176299016736:b3muwlbpdj8" class="formScript">
                        <label for="searchField">Search:</label>
                        <input name="q" type="text" class="textField" id="searchField" title="Search Field" maxlength="40" placeholder="Search PHFMC.gov" autocomplete="off">
                        <button type="submit" title="Search">Search</button>
                    </form>
                </div>
            </li>
            <li id="nav-connect">
                <div>
                <a id="link-connect" href="<?= base_url(); ?>" title="Home">
                    <span style="color:black">Home</span>
                 
<svg  version="1.1" x="0px" y="0px" viewBox="0 0 24 24" width="24px" height="24px" enable-background="new 0 0 200 200" preserveAspectRatio="none"><g><path d="M11.3 6.3l-7.2 6.3v8.2c0 0.7 0.5 1.2 1.1 1.2h4.3v-8h5.1v8h4.3c0.6 0 1.1-0.5 1.1-1.2v-8.2l-7.2-6.3C12.3 5.9 11.7 5.9 11.3 6.3z"/><path d="M23.9 11.8l-4-3.4V2.7c0-0.4-0.3-0.7-0.7-0.7h-1.7c-0.4 0-0.7 0.3-0.7 0.7v3l-3.7-3.2c-0.7-0.6-1.7-0.6-2.4 0L0.1 11.8C-0.1 12 0 12.5 0.4 12.5h1.9l8.8-7.7c0.5-0.4 1.2-0.4 1.7 0l8.8 7.7h1.9C24 12.5 24.1 12 23.9 11.8z"/></g></svg>
                    <span style="color:black">ھوم</span>
                </a>
                </div>
            </li>
            <li id="nav-government">
                <a id="link-government" href="#" title="About Us">
                    <span style="color:black">About Us</span>
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 502.643 502.643" style="enable-background:new 0 0 502.643 502.643;" xml:space="preserve">
<g>
	<g>
		<path d="M251.256,237.591c37.166,0,67.042-30.048,67.042-66.977c0.043-37.037-29.876-66.999-67.042-66.999
			c-36.908,0-66.869,29.962-66.869,66.999C184.387,207.587,214.349,237.591,251.256,237.591z"/>
		<path d="M305.032,248.506H197.653c-19.198,0-34.923,17.602-34.923,39.194v107.854c0,1.186,0.604,2.243,0.669,3.473h175.823
			c0.129-1.229,0.626-2.286,0.626-3.473V287.7C339.912,266.108,324.187,248.506,305.032,248.506z"/>
		<path d="M431.588,269.559c29.832,0,53.754-24.008,53.754-53.668s-23.922-53.711-53.754-53.711
			c-29.617,0-53.582,24.051-53.582,53.711C377.942,245.53,401.972,269.559,431.588,269.559z"/>
		<path d="M474.708,278.317h-86.046c-15.445,0-28.064,14.107-28.064,31.472v86.413c0,0.928,0.453,1.812,0.518,2.826h141.03
			c0.065-1.014,0.496-1.898,0.496-2.826v-86.413C502.707,292.424,490.11,278.317,474.708,278.317z"/>
		<path d="M71.011,269.559c29.789,0,53.733-24.008,53.733-53.668S100.8,162.18,71.011,162.18c-29.638,0-53.603,24.051-53.603,53.711
			S41.373,269.559,71.011,269.559L71.011,269.559z"/>
		<path d="M114.109,278.317H27.977C12.576,278.317,0,292.424,0,309.789v86.413c0,0.928,0.453,1.812,0.539,2.826h141.03
			c0.065-1.014,0.475-1.898,0.475-2.826v-86.413C142.087,292.424,129.489,278.317,114.109,278.317z"/>	</g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                    <span style="color:black">ہمارے بارے</span></a>

                <div class="drop-down">
                    <div class="container">
                        <h3><a  href="#">About Us</a></h3>
                        <ul>
                            <li class="nav-more"><a href="#">About Us</a></li>
                            <li><a href="<?= base_url() ?>page/pages/quick_overview">Overview
                                    <div style="float: right">جائزہ</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>page/pages/leadership">Leadership
                                    <div style="float: right">قیادت</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/organogram">Organogram
                                    <div style="float: right">آرگینوگرام</div>
                                </a></li>
                                <li><a href="<?=base_url()?>page/pages/our_team" title="Core Team">Our Team
                                    <div style="float: right">ہماری ٹیم</div>
                                </a></li>
                                <li><a href="<?=base_url()?>page/pages/future_plans">Future Plans
                                    <div style="float: right">مستقبل</div>
                                </a></li>
                               
                                
                        </ul>
                        
                    </div>
                </div>
            </li>
            
            <li id="nav-employment" style="flex: 1.2;">
                <a id="link-employment" href="#" title="Course Registration">
                    <span style="color:black">Course Registration</span>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="256px" height="256px" viewBox="0 0 256 256" enable-background="new 0 0 256 256" xml:space="preserve">
<path d="M128,100.602V17.996H96.5V2h-63v15.996H2V254h47.25v-47.25h31.5V254h30.516H128h126V100.602H128z M96.5,100.468H74.844
	v23.594H55.033v-23.594H33.5V80.627h21.533V57.094h19.811v23.533H96.5V100.468z M147.688,206.75H128v-19.688h19.688V206.75z
	 M147.688,167.375H128v-19.688h19.688V167.375z M187.063,206.75h-19.688v-19.688h19.688V206.75z M187.063,167.375h-19.688v-19.688
	h19.688V167.375z M226.438,206.75H206.75v-19.688h19.688V206.75z M226.602,167.375H206.75v-19.852h19.852V167.375z"/>
</svg>
                    <span style="color:black">کورس رجسٹریشن</span>
                </a>
                <div class="drop-down">
                    <div class="container">
                        <h3><a href="#">Course Registration</a></h3>
                        <ul>
                            <li class="nav-more"><a class="Projects-subheading" href="#">Course Registration</a></li>
                            <li><a  href="<?=base_url()?>dashboard/emp_authentication">Registration Form
                                <div style="float: right"> رجسٹریشن فارم</div>
                            </a></li> 
                            <!-- <li><a  href="<?= base_url() ?>page/pages/registration_form">Registration Form
                                <div style="float: right"> رجسٹریشن فارم</div>
                            </a></li> -->
                            <li><a href="<?= base_url() ?>page/pages/code_of_conduct">Code of Conduct
                                <div style="float: right">ضابطہ اخلاق</div>
                            </a></li>
                            <li><a href="<?= base_url() ?>page/footer_page/helpline">Helpline
                                    <div style="float: right;"> 08000-9212</div>
                            </a></li>
                            <!-- <li><a href="<?= base_url() ?>dashboard/master_traning">Master Trainer Registration
                                    <div style="float: right;color:red"></div>
                            </a></li> -->
                        </ul>
                        
                    </div>
                </div>
            </li>
            <li id="nav-residents" style="flex:1;overflow:auto">
                <a id="link-education" href="#" title="We Offer">
                    <span style="color:black">We Offer </span>
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="422.097px" height="422.097px" viewBox="0 0 422.097 422.097" style="enable-background:new 0 0 422.097 422.097;"
	 xml:space="preserve">
<g>
	<g>
		<path d="M260.512,178.992c32.487-21.778,72.919-48.881,72.919-103.243C333.431,33.982,299.452,0,257.684,0
			c-17.171,0-33.385,5.631-46.636,16.038C197.8,5.632,181.584,0,164.414,0c-41.768,0-75.749,33.981-75.749,75.749
			c0,54.544,40.135,81.503,72.386,103.164c18.123,12.175,35.244,23.673,39.502,36.633c1.492,4.541,5.729,7.597,10.491,7.597
			c0.075,0,0.151,0,0.223-0.001c4.853-0.099,9.075-3.351,10.404-8.018C225.229,202.642,242.366,191.154,260.512,178.992z
			 M165.894,33.2c-21.977,6.309-36.74,25.061-36.74,46.66c0,7.418,1.726,14.542,5.136,21.172c0.873,1.703,0.489,3.782-0.938,5.058
			c-0.793,0.71-1.796,1.071-2.805,1.071c-0.807,0-1.617-0.231-2.324-0.701c-12.78-8.477-20.409-22.361-20.409-37.137
			c0-24.973,21.417-45.289,47.741-45.289c3.363,0,6.732,0.336,10.015,1.001c1.897,0.384,3.29,2.018,3.367,3.954
			C169.013,30.926,167.758,32.665,165.894,33.2z"/>
		<path d="M178.537,358.348c-1.589-3.889-3.367-7.736-5.286-11.441c-12.563-24.272-29.815-40.471-46.504-56.137
			c-4.703-4.412-10.692-11.316-17.034-18.625c-9.258-10.666-18.832-21.695-25.265-25.902c-5.829-3.81-10.592-3.916-14.16-0.312
			c-5.202,5.257-3.667,15.048,4.978,31.75c5.777,11.164,14.154,24.312,23.024,38.231c1.425,2.236,2.872,4.511,4.329,6.806
			c-15.709-17.183-27.246-32.808-34.344-46.521c-8.152-15.748-9.811-27.916-4.667-34.26c3.632-4.482,10.545-5.596,19.465-3.136
			l1.322,0.363l-0.319-1.335c-0.165-0.692-16.648-69.361-20.13-82.358c-0.766-2.867-1.833-5.63-3.17-8.212
			c-4.801-9.276-12.388-14.907-18.042-13.39c-2.729,0.734-7.223,3.835-6.631,16.398l8.583,108.402
			c0.44,3.54,0.749,7.247,1.077,11.176c1.001,12.01,2.038,24.43,6.953,33.924c2.044,3.95,4.413,7.52,7.24,10.906
			c5.091,6.102,19.25,20.835,34.242,36.432c14.593,15.183,29.684,30.883,35.195,37.438c4.323,5.145,8.064,10.812,11.44,17.336
			c2.719,5.252,5,10.672,7.038,16.215h52.745l-0.792-2.692C193.489,397.871,186.524,377.898,178.537,358.348z"/>
		<path d="M379.364,133.87c-5.654-1.517-13.241,4.114-18.042,13.39c-1.338,2.583-2.405,5.346-3.172,8.212
			c-3.48,12.997-19.965,81.667-20.13,82.358l-0.318,1.335l1.321-0.363c8.92-2.46,15.833-1.347,19.466,3.136
			c5.143,6.344,3.484,18.512-4.667,34.26c-7.1,13.713-18.636,29.338-34.345,46.521c1.458-2.294,2.904-4.569,4.329-6.806
			c8.87-13.92,17.247-27.067,23.024-38.231c8.645-16.7,10.18-26.493,4.978-31.75c-3.567-3.604-8.332-3.498-14.16,0.312
			c-6.433,4.207-16.007,15.236-25.265,25.902c-6.342,7.309-12.332,14.213-17.034,18.625c-16.688,15.666-33.941,31.863-46.504,56.137
			c-1.919,3.705-3.697,7.554-5.286,11.441c-7.987,19.55-14.952,39.522-21.288,61.057l-0.792,2.692h52.745
			c2.038-5.543,4.319-10.963,7.038-16.215c3.376-6.522,7.117-12.191,11.44-17.336c5.511-6.556,20.603-22.256,35.194-37.438
			c14.992-15.597,29.151-30.332,34.242-36.432c2.827-3.387,5.196-6.956,7.24-10.907c4.915-9.493,5.952-21.913,6.953-33.923
			c0.328-3.93,0.637-7.636,1.077-11.176l8.583-108.402C386.588,137.705,382.093,134.604,379.364,133.87z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                    <span style="color:black">ہماری خدمات</span></a>

                <div style="height:22rem" class="drop-down" >
                    <div class="container" >
                        <h3><a href="#">We Offer </a></h3>
                        <ul>
                            <!-- <li class="nav-more"><a class="Campaigns-subheading" href="#">Community Support Prorgramme, News, Media &amp; Resources</a></li> -->
                            <li class="nav-more"><a class="Campaigns-subheading" href="#">We Offer</a></li>
                            <li><a href="<?= base_url() ?>page/pages/post_induction_trainings">Post Induction Trainings
                                    <div style="float: right;">پوسٹ انڈکشن ٹریننگ</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/demand_based_trainings">Demand Based Trainings

                                    <div style="float: right;">ڈیمانڈ بیسڈ ٹریننگ</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/short_courses">Short Courses 
                                    <div style="float: right;">مختصر کورسز</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/e_learning">E-Learning
                                    <div style="float: right;">ای لرننگ</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/hands_on_training">Practical/ Hands-on Training  
                                    <div style="float: right;">پریکٹیکل/ ہینڈ آن ٹریننگ</div>  
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/field_trainings">Field Trainings 
                                    <div style="float: right;">فیلڈ ٹریننگ</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/course_designing">Course Designing  
                                    <div style="float: right;"> کورس ڈیزائننگ</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/content_development">Content Development 
                                    <div style="float: right;">مواد کی ترقی</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/toolkits">Toolkits
                                    <div style="float: right;">ٹول کٹس </div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/examinations">Examinations 
                                    <div style="float: right;">امتحانات </div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/workshops">Workshops 
                                    <div style="float: right;">ورکشاپس </div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/symposia">Symposia 
                                    <div style="float: right;">سمپوزیا </div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/domain_support">Domain Support 
                                    <div style="float: right;">ڈومین سپورٹ </div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/r_n_d">R&D 
                                    <div style="float: right;">آر اینڈ ڈی </div>
                                </a></li>
                            </ul>
                        
                    </div>
                </div>
            </li>
           
            <li id="nav-services" style="flex: 1.2; overflow:auto">
                <a id="link-services" href="#" title="Our Campuses">
                    <span style="color:black">Our Campuses</span>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 28 28" width="28px" height="28px" enable-background="new 0 0 28 28" preserveAspectRatio="none"><g><g><path d="M13.5 10.5l3 3c0.2 0 0.5-0.1 0.7-0.1 0.4 0 0.9 0.1 1.3 0.2l0 0c1.7 0.4 3.6-0.1 5-1.4 1.5-1.5 1.9-3.7 1.2-5.6l-2.7 2.7c-0.9 0.9-2.3 0.9-3.2 0 -0.9-0.9-0.9-2.3 0-3.2l2.7-2.7c-1.9-0.7-4.1-0.3-5.6 1.3 -1.4 1.4-1.8 3.3-1.4 5L13.5 10.5z"/><path d="M12.9 18.4c-0.1-0.5-0.1-0.9 0-1.4l-3-3 -6.1 6.1c-1.1 1.1-1.1 2.9 0 4 1.1 1.1 2.9 1.1 4 0L13 19C13 18.8 12.9 18.6 12.9 18.4zM6.8 23.1c-0.5 0.5-1.4 0.5-1.9 0 -0.5-0.5-0.5-1.4 0-1.9 0.5-0.5 1.4-0.5 1.9 0C7.3 21.7 7.3 22.5 6.8 23.1z"/></g><g><path d="M16 15.2L9.1 8.3 8.7 6.5c0-0.2-0.2-0.3-0.3-0.4L5.2 4.2C5 4.1 4.6 4.1 4.4 4.3L3.8 4.9C3.6 5.2 3.5 5.5 3.7 5.8l1.9 3.1C5.7 9.1 5.8 9.2 6 9.2l1.8 0.4 6.9 6.9c-0.5 1-0.3 2.2 0.6 3.1l4.5 4.5c1.1 1.1 2.8 1.1 3.9 0 1.1-1.1 1.1-2.8 0-3.9l-4.5-4.5C18.3 14.9 17 14.8 16 15.2z"/></g></g></svg>
                    <span style="color:black">ہمارے کیمپس</span></a>

                <div class="drop-down" style="height: 21rem;">
                    <div class="container">
                        <h3><a href="#">Our Campuses</a></h3>
                        <ul>
                            <li class="nav-more"><a class="Info-Desk-subheading" href="<?= base_url() ?>infodesk">Our Campuses</a></li>
                            <li><a href="<?= base_url() ?>page/pages/main_campus">Main Campus 
                                    <div style="float: right">مین کیمپس</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>page/pages/virtual_campus">Virtual Campus
                                    <div style="float: right">ورچوئل کیمپس</div>
                                </a></li>
                            <li><a href="<?=base_url()?>page/pages/satellite_campuses">Satellite Campuses
                                    <div style="float:right;">سیٹلائٹ کیمپس</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>page/pages/libraries">Libraries 
                                    <div style="float: right">لائبریریاں</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>page/pages/hostel_facilities">Hostel Facilities 
                                    <div style="float: right">ہاسٹل کی سہولیات</div>
                             </a></li>
                        </ul>
                       
                    </div>
                </div>
            </li>
            <li id="nav-services" style="flex: 1.2; overflow:auto">
                <a id="link-services" href="#" title="Info Desk">
                    <span style="color:black">Info Desk</span>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 28 28" width="28px" height="28px" enable-background="new 0 0 28 28" preserveAspectRatio="none"><g><g><path d="M13.5 10.5l3 3c0.2 0 0.5-0.1 0.7-0.1 0.4 0 0.9 0.1 1.3 0.2l0 0c1.7 0.4 3.6-0.1 5-1.4 1.5-1.5 1.9-3.7 1.2-5.6l-2.7 2.7c-0.9 0.9-2.3 0.9-3.2 0 -0.9-0.9-0.9-2.3 0-3.2l2.7-2.7c-1.9-0.7-4.1-0.3-5.6 1.3 -1.4 1.4-1.8 3.3-1.4 5L13.5 10.5z"/><path d="M12.9 18.4c-0.1-0.5-0.1-0.9 0-1.4l-3-3 -6.1 6.1c-1.1 1.1-1.1 2.9 0 4 1.1 1.1 2.9 1.1 4 0L13 19C13 18.8 12.9 18.6 12.9 18.4zM6.8 23.1c-0.5 0.5-1.4 0.5-1.9 0 -0.5-0.5-0.5-1.4 0-1.9 0.5-0.5 1.4-0.5 1.9 0C7.3 21.7 7.3 22.5 6.8 23.1z"/></g><g><path d="M16 15.2L9.1 8.3 8.7 6.5c0-0.2-0.2-0.3-0.3-0.4L5.2 4.2C5 4.1 4.6 4.1 4.4 4.3L3.8 4.9C3.6 5.2 3.5 5.5 3.7 5.8l1.9 3.1C5.7 9.1 5.8 9.2 6 9.2l1.8 0.4 6.9 6.9c-0.5 1-0.3 2.2 0.6 3.1l4.5 4.5c1.1 1.1 2.8 1.1 3.9 0 1.1-1.1 1.1-2.8 0-3.9l-4.5-4.5C18.3 14.9 17 14.8 16 15.2z"/></g></g></svg>
                    <span style="color:black">معلومات</span></a>

                <div class="drop-down" style="height: 21rem;">
                    <div class="container">
                        <h3><a href="#">Info Desk</a></h3>
                        <ul>
                            <li class="nav-more"><a class="Info-Desk-subheading" href="<?= base_url() ?>infodesk">Info Desk</a></li>
                            <li><a href="<?= base_url() ?>infodesk/press_release">News/Events 
                                    <div style="float: right">نیوز/ تقریبات </div>
                                </a></li>
                            <li><a href="<?= base_url() ?>page/pages/policies">Policies
                                    <div style="float: right">پالیسیاں</div>
                                </a></li>
                            <li><a href="<?=base_url()?>page/pages/laws">Laws
                                    <div style="float:right;">قوانین</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>page/pages/rules">Rules 
                                    <div style="float: right">قواعد</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>dashboard/manuals">Manuals 
                                    <div style="float: right">مینولز</div>
                             </a></li>
                            <li><a href="<?= base_url() ?>page/pages/notifications">Notifications 
                                    <div style="float: right">اطلاعات</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>Infodesk/publications">Other Publications
                                    <div style="float: right">دیگر مطبوعات</div>
                                </a></li>
                            <li><a href="<?= base_url() ?>dashboard/events">Gallery
                                    <div style="float: right">گیلری</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/useful_links">Useful Links 
                                    <div style="float: right"> لنکس</div>
                                </a></li>
                                <li><a href="<?= base_url() ?>page/pages/faqs">FAQs 
                                    <div style="float: right">عمومی سوالات</div>
                                </a></li>
                                <li><a href="#">Contact Us 
                                    <div style="float: right">ہم سے رابطہ کریں</div>
                                </a></li>

                        </ul>
                        
                    </div>
                </div>
            </li>
            <li id="nav-login">
                <a id="link-connect" href="#" title="Login">
                    <span style="color:black">Login</span>
                 
<svg  version="1.1" x="0px" y="0px" viewBox="0 0 24 24" width="24px" height="24px" enable-background="new 0 0 200 200" preserveAspectRatio="none"><g><path d="M11.3 6.3l-7.2 6.3v8.2c0 0.7 0.5 1.2 1.1 1.2h4.3v-8h5.1v8h4.3c0.6 0 1.1-0.5 1.1-1.2v-8.2l-7.2-6.3C12.3 5.9 11.7 5.9 11.3 6.3z"/><path d="M23.9 11.8l-4-3.4V2.7c0-0.4-0.3-0.7-0.7-0.7h-1.7c-0.4 0-0.7 0.3-0.7 0.7v3l-3.7-3.2c-0.7-0.6-1.7-0.6-2.4 0L0.1 11.8C-0.1 12 0 12.5 0.4 12.5h1.9l8.8-7.7c0.5-0.4 1.2-0.4 1.7 0l8.8 7.7h1.9C24 12.5 24.1 12 23.9 11.8z"/></g></svg>
                    <span style="color:black">لاگ ان</span>
                </a>

                <div class="drop-down" style="height: 21rem;">
                    <div class="container">
                        <h3><a href="#">Login</a></h3>
                        <ul>
                        <?php if($this->session->userdata('user_login')):?>
                            <li>
                            <a id="link-connect" href="<?=base_url()?>login/user_logout" title="Logout">
                                    <span style="color:black">Logout</span>
                                    &nbsp;
                
                                    <span style="color:black">لاگ آوٹ</span>
                                </a>
                        </li>
                            <?php else:?>
                            <li class="nav-more"><a class="Info-Desk-subheading" href="<?= base_url() ?>login">Login</a></li>
                            <li><a href="<?=base_url()?>dashboard/emp_authentication">Self Enrollment 
                                    <div style="float: right">خود اندراج </div>
                                </a></li>
                            <li><a href="<?= base_url() ?>login">login
                                    <div style="float: right">لاگ ان</div>
                                </a></li>
                                <?php endif;?>
                        </ul>
                        
                    </div>
                </div>

            </li>
        </ul>
    </nav>
</header>