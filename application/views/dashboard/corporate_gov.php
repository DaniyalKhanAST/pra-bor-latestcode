
<style>
    table.dataTable tr.odd {
        background-color: #f0f0f0;
    }
    thead, th td{text-align: left;}
</style>
<main id="main" style="margin-top: 130px;">


    <!-- <header>

      

    </header> -->






    
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup0">   <a href="#" class="clickme">Board of Directors  <span style="float: right;"> بورڈ  آف  ڈائریکٹرز </span></a></h2>
        <div class="container " id="target0" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($board_of_directors->page_detail)){
                    echo $board_of_directors->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">BoD Committees  <span style="float: right;">بورڈ کمیٹیاں </span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <!-- <ul class="MDI-Results default"></ul> -->
                <?php 
                if(isset($bod_committees->page_detail)){
                    echo $bod_committees->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a href="#" class="clickme">Corporate Social Responsibility <span style="float: right;">سماجی ذمہ داری </span></a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <?php 
                if(isset($corporate_social_responsibility->page_detail)){
                    echo $corporate_social_responsibility->page_detail;
                }
                ?>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup4">   <a href="#" class="clickme">Manuals <span style="float: right;">مینولز</span></a></h2>
        <div class="container " id="target4" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                 <section>
        <div class="container">
            <table class="table table-bordred table-striped" id="manual_dt">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Manual Name</th>
                    <th>Manual Document</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($manuals)) {
                    $i = 1;
                    foreach ($manuals as $row) {
                        ?>
                        <tr class="">
                            <td ><?=$i?></td>
                            <td ><?=$row->name?></td>
                            <td><?php if(!empty($row->pdf_file_link)){?><a href="<?= $row->pdf_file_link	 ?>" target="_blank">View</a> <?php }?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup6">   <a href="#" class="clickme">Company Policies<span style="float: right;">کمپنی پالیسیز</span></a></h2>
        <div class="container " id="target6" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                 <section>
        <div class="container">
            <table class="table table-bordred table-striped" id="cp_dt">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Policy Name</th>
                    <th>Policy Document</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($company_policies)) {
                    $i = 1;
                    foreach ($company_policies as $row) {
                        ?>
                        <tr class="">
                            <td ><?=$i?></td>
                            <td ><?=$row->name?></td>
                            <td><?php if(!empty($row->pdf_file_link)){?><a href="<?= $row->pdf_file_link	 ?>" target="_blank">View</a> <?php }?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
            </div>   
        </div>
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup5">   <a href="#" class="clickme">Audit Reports  <span style="float: right;">آڈٹ رپورٹس</span></a></h2>
        <div class="container " id="target5" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                 <ul class="MDI-Results default searchlist">
                    <?php if(!empty($audit_reports)) {
                        foreach ($audit_reports as $row) { ?>
                            <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->pdf_link ?>">
                                    <h4 itemprop="name" class="title"><?php echo $row->title ?></h4><span itemprop="description" class="description"><?php echo $row->year ?></span></a>
                            </li>

                        <?php }
                        }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>



</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
     $('.bukatutup0').click(function() {
        $('#target0').toggle('slow');
    });
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
        
    });
    $('.bukatutup5').click(function() {
        $('#target5').toggle('slow');
        
    });
    $('.bukatutup6').click(function() {
        $('#target6').toggle('slow');
        
    });

    $( document ).ready(function() {
        $('#cp_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
        $('#manual_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
    });
</script>