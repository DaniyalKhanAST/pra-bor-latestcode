<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Marketing Support

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Marketing Support </h3>
            <p>

                <br>
                This component aims to enhance access to markets for livestock farmers for sale of their produce (live animals and milk etc.). The component would facilitate the farmers in establishment of linkages with the private sector for the provision of inputs, capacity building activities and other services.
                <br>
                This component includes three sub-components:
            <ul class="container">
                <li><a href="<?php echo base_url().'dashboard/strengthening_farmer_organizations';?>">Strengthening Farmer Organizations</a>  </li>
                <li><a href="<?php echo base_url().'dashboard/market_infrastructure';?>"> Market Infrastructure </a> </li>
                <li><a href="<?php echo base_url().'dashboard/market_information_and_linkages';?>"> Market Information and Linkages. </a>  </li>
                <li><a href="<?php echo base_url().'images/lamp/Banking_IFAD.docx';?>" target="_blank"> DIGITAL CREDIT LINE FOR RURAL POOR THROUGH VERTICAL MARKET LINKAGES </a>  </li>

            </ul>

            <br>

            </p>
            <div class="container">
                <h3>Article Gallery</h3>

                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC_0010m.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC_0010m.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSC_0012m.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSC_0012m.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/DSCN3690m.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/DSCN3690m.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_115843863m.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_115843863m.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_120004876m.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_120004876m.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="row">
                        <div class='list-group gallery'>
                            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url().'images/lamp/IMG_20170110_120136529m.jpg';?>">
                                    <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo base_url().'images/lamp/IMG_20170110_120136529m.jpg';?>" />
                                    <div class='text-right'>
                                        <small class='text-muted'></small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div><!-- /.container -->
    </section>


</main>