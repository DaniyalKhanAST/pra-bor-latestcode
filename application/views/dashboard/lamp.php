<main id="main" style="margin-top: 130px;">


    <header>

        <section>
            <!--[if gte IE 9]><!-->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px"
                 enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="7.842" r="2.813"/>
                        <rect x="9.248" y="5.667" fill="#676868" width="13.599" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="16" r="2.813"/>
                        <rect x="9.248" y="13.773" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="24.158" r="2.813"/>
                        <rect x="9.248" y="21.984" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                </g>
            </svg>

            <!--<![endif]-->

            <h1>Livestock and Access to Markets Project (LAMP)</h1>

            <p class="agency-search">
                <input type="text" class="search-agencies" title="Search" placeholder="Search..."/>
                <button type="button" class="clear-search">Clear Search</button>
            </p>

        </section>

    </header>






    <section style="margin-bottom: 1px;">

        <h2 class="bukatutup1">   <a href="<?php echo base_url().'dashboard/lamp_introduction';?>" class="clickme">Introduction <span style="float: right;"></span></a></h2>

        <div class="container " id="target1" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">

            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">Components <span style="float: right;"></span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">

                    <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo base_url().'dashboard/production_support'; ?>">
                            <h4 itemprop="name" class="title"> Production Support </h4><span itemprop="description" class="description"></span></a>
                    </li>
                    <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo base_url().'dashboard/production_support'; ?>">
                            <h4 itemprop="name" class="title">Marketing Support</h4><span itemprop="description" class="description"></span></a>
                    </li>
                    <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo base_url().'dashboard/project_management'; ?>">
                            <h4 itemprop="name" class="title">Project Management</h4><span itemprop="description" class="description"></span></a>
                    </li>


                </ul>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a href="<?php echo base_url().'dashboard/implementing_partners';?>" class="clickme">Implementing Partners<span style="float: right;"></span></a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">

            </div>
        </div><!-- /.container -->
    </section>



</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
    });
</script>