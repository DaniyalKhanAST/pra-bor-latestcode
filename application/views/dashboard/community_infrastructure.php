<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Community Infrastructure

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Community Infrastructure </h3>
            <p> The community infrastructure sub-component will include schemes which have a direct impact on enhancing

                livestock productivity, can be implemented and managed by the community, have well defined operations and

                maintenance arrangements which are within community capacity. The schemes will be selected on a

                participatory demand driven basis by the community groups. The community will be expected to contribute 20%

                of the share for schemes of collective community use and 50% for those type of schemes which benefits

                individual households. The social setup of the area for community based interventions on cooperative basis in the

                heterogeneous society will be addressed through active participation of the community organizations and Local

                Support Organizations.
                The types of infrastructure schemes that are envisaged could include



            <!--<ol class="container">
                <li><a href="#"> Water solutions for livestock </a> </li>
                <li><a href="#"> Energy solutions for livestock </a> </li>
                <li><a href="#"> Housing solutions for livestock </a> </li>

            </ol>-->

            <dl class="cintainer">
                <dt> Water solutions for livestock </dt>
                <dd> Water solutions for livestock (water supply, water ponds) The project will construct about 600 schemes (i.e.

                    150 water supply schemes, 200 water conveyance efficiency schemes, 215 water ponds, and 35 water troughs at

                    household levels) will be financed with the objective of improving water quality and availability for livestock, and

                    increasing water availability for fodder production.</dd>
                <dt> Energy solutions for livestock </dt>
                <dd> Energy solutions for livestock (bio-gas digesters, solar pumps) The Project will implement about 525

                    innovative schemes under energy solutions for livestock. These will include 300 traditional biogas digesters which

                    are relatively cheaper and 25 imported flexi biogas digesters as a pilot for demonstrating efficient and increased

                    output (biogas) against lesser input (cow dung and labor) . In addition, about 200 solar powered water pumps will

                    be installed for demonstration and pilot testing particularly in off grid areas. </dd>
                <dt> Housing solutions for livestock </dt>
                <dd>Housing solutions for livestock (80 water troughs and sheds etc.)and(d) Any other infrastructure identified by

                    the beneficiaries that can potentially contribute to livestock development and marketing. The final selection will

                    be made on the basis of a set of criteria ratified by Steering Committee. The community will be expected to

                    contribute 20% of the share in schemes which are designed for collective community use and management and

                    50% for those types of schemes which benefit individual households.</dd>
            </dl>


            <br>
            The project will implement 30 low-cost/indigenous materials based livestock sheds and other low cost

            solutions which could easily be replicated by other farmers.



            </p>





        </div><!-- /.container -->
    </section>


</main>