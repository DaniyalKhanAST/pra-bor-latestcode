<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Component Activities and Output

            <span style="float:right;"></span>
        </h2>
        <div class="container" style="text-align: justify;font-size: 20px">
            <h3>Component Activities and Output: </h3>
            <p> Main component activities and outputs are: establishment of specified

                institutions at provincial, district and subsidiary levels. These institutions and their responsibilities are:
                <br>
            <dl>
                <dt>Federal Level:</dt>
                <dd>
                    Economic Affairs Division (EAD) is the designated executing agency at Federal level for all foreign development

                    assistance. IFAD loan would be negotiated by EAD together with the Ministry of Finance and Government of the

                    Punjab representative and the loan would be signed by EAD. Joint Secretary WB/IFAD would be the designated

                    focal person for all project related loan signing and approval processes, coordination and periodic progress

                    reviews at the federal level.</dd>
                <dt>Provincial Level:</dt>
                <dd>
                    Livestock and Dairy Development Department (L&amp;DDD) will be the main implementing agency for LAMP. Project

                    implementation will follow a hybrid model where by different project activities will be implemented through a

                    mix of public and private sector agencies coordinated by a Programme Coordination Unit. The overall oversight

                    for the programme and coordination between various agencies will be provided by a Project Steering Committee

                    (PSC) duly notified by the provincial government. PSC would be headed by Chairman P&amp;D Board with Project

                    Coordinator, LAMP as its Secretary. Chief Agriculture, P&amp;D would be provide the secretariat for the PSC and

                    provide support during meetings and follow-up action.
                    <dl class="container">
                        <dt>A Project Executive Committee</dt>
                        <dd> headed by Secretary L&amp;DDD, with Project Coordinator and one representative

                            each from P&amp;D Board (not below the rank of a Section Chief), Finance Department and Local Government

                            Department will be notified. The Committee will consider and decide on any emergent issue needing immediate

                            decision that cannot wait the convening of PSC. All matters decided by the PEC will be placed before the next

                            immediate meeting of PSC for endorsement/approval. The committee will also ensure synergy and

                            complementarily between LAMP activities and those of L&amp;DDD‘s other adjunct companies and projects like

                            PLDDB and PAMCO.</dd>

                    </dl>



                </dd>
                <dt>District Level</dt>
                <dd>
                    <dl class="container">
                        <dt>Gender Mainstreaming</dt>
                        <dd>Gender will be mainstreamed into entire project approach and implementation right from recruitment of staff

                            for the PCU to the selection of beneficiaries for the project interventions. In PCU recruitments, 30% quota will be

                            fixed for women staff in addition to any other women that qualify for recruitment on open merit. A Director

                            gender in PCU will be responsible for ensuring that all project interventions are gender sensitive. In certain

                            aspects, project activities like Asset transfer will be solely targeted at women. Where social conditions do not

                            permit, separate women based COs will be established. In CLEWs, 50% of the seats will be reserved for women.
                            <ul class="container">
                                <li><a href="<?php echo base_url().'images/lamp/gender_action.docx';?>" target="_blank">Gender Action Plan</a></li>
                            </ul>



                        </dd>
                        <dt>Monitoring and Evaluation System for LAMP</dt>
                        <dd>

                            The M&amp;E system will be a key management tool for the project and will serve the primary purpose of providing

                            data and feedback to improve the effectiveness, efficiency, sustainability, relevance and impact of project

                            activities. The overall responsibility for the M&amp;E activities will lie with PCU/DCUs who will be responsible for

                            collecting and analyzing the data gathered from all units and implementing partners/service providers on the

                            basis of agreed reporting format and timing. All indicators will be disaggregated according to gender and socio-

                            economic status to the extent possible so as to enable a proper assessment as to whether the project is reaching

                            its intended target beneficiaries, poor households, smallholders, women and vulnerable groups.
                            <ul class="container">
                                <li><a href="<?php echo base_url().'images/lamp/RIMS.docx';?>" target="_blank">RIMS & M&E Systems Proposal for LAMP Final</a></li>
                            </ul>

                        </dd>

                        <dt>District Coordination Committee (DCC):</dt>
                        <dd> PSC/provincial government will notify a District Coordination

                            Committee, headed by the District Coordination Officer, in each of the four target districts. The DCC will be

                            primarily a district level coordination and trouble-shooting forum and will have no executive authority as far as

                            project management, finances and activities are concerned. DCC meetings will be convened on need-basis as and

                            when required by the District Project Coordinator/DLO. Other members of the DCC will be EDO Agriculture, EDO

                            Local Government, DLO (as Secretary), concerned TMO in case of issues related to livestock markets, District

                            Treasury Officer, Assistant Commissioners of the target Tehsils and district head of the social mobilization service

                            provider. </dd>
                        <dt>District Coordination Units (DCUs): </dt>
                        <dd>A District Coordination Unit (DCU) will be established within each target

                            district in the office of District Livestock Officer (DLO) and the DLO will also be ex-officio District Coordinator for

                            implementation of project activities. In view of multifarious other engagements of DLO, a full time District

                            Finance Manager along with 02 DVMs, 03 VAs and a driver will be recruited in each Project District, to assist the

                            DLO in project activities. DCU will be the main implementation arm for all project funded activities at the village

                            level in close collaboration with the social mobilization organization. All DCU positions will be manned by the

                            regular staff of DOL. However, prior to start of the project, a screening of incumbent staff will be done and

                            suitable staff for each position will be selected by a committee headed by Secretary L&amp;DDD, notified by PSC, on

                            competitive basis. All project designated staff in DCU will be eligible for a project allowance as specified in the

                            project budgets. The DCU composition and TOR are reflected in PIM. </dd>
                    </dl>

                </dd>
                <dt>Sub-District Level</dt>
                <dd>
                    The existing and program specified incremental staff, on the analogy of Livestock Support Services Project (LSSP),

                    in identified target 29 Union Councils and Veterinary Hospitals/Dispensaries would be responsible for the

                    implementation of all L&amp;DDD related activities in close coordination with social mobilization agency through

                    community organizations. District Livestock Officer/District Project Coordinator would be responsible for their

                    deployment and performance. The Veterinary Officer in the target Union Council would be overall responsible for

                    coordination of all planned activities in the target villages within the Union Council and for effective interface

                    with the social mobilization agency in need identification, village planning, para-vet selection and delivery of

                    project inputs. APVO in each cluster of UCs within a Tehsil will be designated by DMU as Coordinator for all

                    program inputs in the Tehsil.</dd>
            </dl>







                <br>

            </p>





        </div><!-- /.container -->
    </section>


</main>