<style>
    .mdiSearchResults li.item {
        background-color: #ffffff;
        background-position: 99% 97%;
        background-size: 5% auto;
        border-radius: 2px;
        box-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
        display: block;
        height: 10.5em;
        padding: 0;
        position: relative;
        transition: all 0.4s ease 0s;
        margin-bottom: 10px;
    }
    .mdiSearchResults li.item .title{
        background-color: #4d4d4d;
        border-bottom: 1px solid rgba(255, 255, 255, 0.125);
        color: white;
        font-weight: 700;
        line-height: 1;
        margin: 0;
        padding: 0.5rem 1rem;
        text-shadow: 0 1px 2px rgba(0, 0, 0, 0.5);
        transition: all 0.4s ease 0s;
    }
    .mdiSearchResults li.item .description {
        margin: 0 1rem 0.5rem;
        padding-top: 0.5rem;
    }
</style>
<main id="main" style="margin-top: 130px;">
    <header>

        <section>
            <!--[if gte IE 9]><!-->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px"
                 enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="7.842" r="2.813"/>
                        <rect x="9.248" y="5.667" fill="#676868" width="13.599" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="16" r="2.813"/>
                        <rect x="9.248" y="13.773" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="24.158" r="2.813"/>
                        <rect x="9.248" y="21.984" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                </g>
            </svg>

            <!--<![endif]-->

            <h1>Company Budget</h1>

            <p class="agency-search">
                <input type="text" class="search-agencies" title="Search" placeholder="Search..."/>
                <button type="button" class="clear-search">Clear Search</button>
            </p>

        </section>

    </header>

    <section class="agency-section">
        <h2>Company Budget
        </h2>

        <div class="container ">
            <div class="mIndex mdiSearchResults grid_list_4">
                <div class="mIndex mdiSearchResults">
                    <ul class="MDI-Results onlineservices">
                        <?php if (!empty($provincial_budget)) {
                            foreach ($provincial_budget as $row) { ?>
                                <li itemscope="" itemtype="" class="item">
                                    <a itemprop="url" href="<?php echo $row->link_url ?>">
                                        <h4 itemprop="name" class="title"><?php echo $row->name ?></h4>
                                        <p itemprop="description" class="description" style="color: #4D4D4D">This Utah Amber Alert Web Page is just one source and tool for an Amber Alert Activation. This Web Site will be utilized to post updates or cancellations of the</p>
                                    </a>
                                </li>
                            <?php }
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>