<style>
    #button {
  display: inline-block;
  background-color: #05445E;
  width: 50px;
  height: 50px;
  text-align: center;
  border-radius: 4px;
  position: fixed;
  bottom: 30px;
  right: 30px;
  transition: background-color .3s, 
    opacity .5s, visibility .5s;
  opacity: 0;
  visibility: hidden;
  z-index: 1000;
}
#button::after {
  content: "\f077";
  font-family: FontAwesome;
  font-weight: normal;
  font-style: normal;
  font-size: 2em;
  line-height: 50px;
  color: #fff;
}
#button:hover {
  cursor: pointer;
  background-color: #05445E;
}
#button:active {
  background-color: #555;
}
#button.show {
  opacity: 1;
  visibility: visible;
}

/* Styles for the content section */

.content {
  width: 77%;
  margin: 50px auto;
  font-family: 'Merriweather', serif;
  font-size: 17px;
  color: #6c767a;
  line-height: 1.9;
}
@media (min-width: 500px) {
  .content {
    width: 43%;
  }
  #button {
    margin: 30px;
  }
}
.content h1 {
  margin-bottom: -10px;
  color: #03a9f4;
  line-height: 1.5;
}
.content h3 {
  font-style: italic;
  color: #96a2a7;
}
    .gallery
    {
        display: inline-block;
        margin-top: 20px;
    }
    .img-responsive{
        width:250px !important;
        height:200px !important;
                
    }
    @media (max-width:400px){
    .img-responsive{
        margin-left:30px 
    }}
    
</style>
<main id="main" style="margin-top: 130px;">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Merriweather:400,900,900i" rel="stylesheet">

<a id="button"></a>
    <section class="tertlinks alt">
        <h2>Gallery</h2>
        <!-- <div class="container">
            <ul class="grid_10 searchlist">

                    <li style="padding-right: 0;">
                        <a href="#">
                            <img alt="" src="<?php echo $event->image_url ?>" style="width: 200px;height: 150px">
                            <h3 style="font-size: 24px"><?php echo $event->title ?></h3>
                            <p style="margin-top: 10px;font-size: 20px"><?php echo $event->description ?></p>
                            <div class="project-details">
                                <?php if($event->directorate_name):?>
                                    <p><span>Directorate: </span><?php echo $event->directorate_name;?></p>
                                <?php endif;?>
                                 <?php if($event->created_date):?>
                                    <p><span>Date:</span> <?php echo $event->created_date;?></p>
                                <?php endif;?>
                                <?php if($event->district_name):?>
                                    <p><span>District:</span> <?php echo $event->district_name;?></p>
                                <?php endif;?>
                            </div>
                        </a>
                    </li>

            </ul>
        </div> -->




        <div class="container">
            <h3>Event Gallery</h3>
            <?php if (!empty($event_gallery)) {

                foreach ($event_gallery as $row) {
                    ?>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $row->image_url;?>">
                                        <img  class="img-responsive" alt="" src="<?php echo $row->image_url;?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>

                                        </div> <!-- text-right / end -->
                                        
                                    </a>
                                    <p><?php echo $row->image_desc;?></p>
                                </div> <!-- col-6 / end -->
                            </div> <!-- list-group / end -->
                        </div> <!-- row / end -->
                    </div> <!-- container / end -->
                    <?php
                }
            } ?>
        </div>

    </section>
    <div class="go-top">
	
    </div>

</main>

<script>
var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});


</script>