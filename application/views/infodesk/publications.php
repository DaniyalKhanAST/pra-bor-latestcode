<main id="main" style="margin-top: 130px;">
    <div style="padding: 30px;text-align: right">
        <form id="live-search" action="" class="styled" method="post">
            <div class="input-group">

                <input type="text" class="form-control" style="color: black;    width: 270px;height: 35px;" placeholder="Search for..." id="filter">

                <span id="filter-count"></span>
            </div>
        </form>
    </div>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup1">   <a href="#" class="clickme">Publications<span style="float: right">مطبوعات </span></a></h2>
        <div class="container " id="target1" >
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php foreach($publications as $row){ ?>
                        <li itemscope="" itemtype="" class="item  local"><a>
                                <h4 itemprop="name" class="title" style="font-size: 16px;"></h4><span  itemprop="" class="description">

                                <span ><?php echo $row->title ?></span>

                                  <table style="width:100%">
                                      <tr>
                                          <th><b>Author</b></th>
                                          <th><b>Year</b></th>
                                      </tr>
                                      <tr>
                                          <td><?php if(!empty($row->author_publisher)) {echo $row->author_publisher;} else{ echo "N/A";} ?></td>
                                          <td><?php if(!empty($row->year)) {echo $row->year;} else{ echo "N/A";} ?></td>
                                      </tr>
                                  </table>
                            </span></a>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>
</main>
