<style>
    .gallery
    {
        display: inline-block;
        margin-top: 20px;
    }
    #main section.tertlinks li {
        height: auto;
        padding: 0 1rem 0 0;
        margin: 0 0 .5rem;
    }
    #main section.tertlinks a li {
        list-style: inside;
    }
</style>
<main id="main" style="margin-top: 130px;">
    <section class="tertlinks alt">
        <h2>Medicine Detail</h2>
        <div class="container">
            <ul class="grid_10 searchlist">

                <li style="padding-right: 0;height: 100%">
                    <a <?= isset($medicine->file_link) && !empty($medicine->file_link) ? "href='$medicine->file_link' target='_blank' title='click to open file' ": "" ?>>
                        <h3 style="font-size: 24px"><?php echo $medicine->title ?></h3>
                        <p style="margin-top: 10px;font-size: 20px"><?php echo $medicine->description ?></p>
                    </a>
                </li>

            </ul>
        </div>
        <div class="container">
            <h3>Medicine Gallery</h3>
            <?php if (!empty($medicine_images)) {

                foreach ($medicine_images as $row) {
                    ?>
                    <div class="grid_3">
                        <div class="row">
                            <div class='list-group gallery'>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $row->image_url;?>">
                                        <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo $row->image_url;?>" />
                                        <div class='text-right'>
                                            <small class='text-muted'></small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } ?>
        </div>
    </section>
</main>