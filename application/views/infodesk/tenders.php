<style>
    table.dataTable tr.odd {
        background-color: #f0f0f0;
    }
    thead, th td{text-align: left;}
</style>
<main id="main" style="margin-top: 130px;">
    <section>
        <h2>Tenders</h2>
        <div class="container">
            <table class="table table-bordred table-striped" id="tenders_dt">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Department</th>
                    <th>Name</th>
                    <th>Closing Date</th>
                    <th>Status</th>
                    <th>Advertisment</th>
                    <th>BID Document</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($auction)) {
                    $i = 1;
                    foreach ($auction as $row) {
                        ?>
                        <tr class="">
                            <td ><?=$row->type_name?></td>
                            <td><?= $row->directorate_name ?></td>
                            <td ><?= $row->name ?></td>
                            <td><?= $row->closing_date ?></td>
                            <td><?= $row->auc_status  ?></td>
                            <td><?php if(!empty($row->pdf_link)){?><a href="<?= $row->pdf_link	 ?>" target="_blank">View</a> <?php }?></td>
                            <td><?php if(!empty($row->bid_doc)){?> <a href="<?= $row->bid_doc	 ?>" target="_blank">View</a> <?php }?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</main>
<script>
    $( document ).ready(function() {
        $('#tenders_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
    });
</script>