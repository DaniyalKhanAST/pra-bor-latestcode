<main id="main" style="margin-top: 130px;">
    <div style="padding: 30px;text-align: right">
        <form id="live-search" action="" class="styled" method="post">
            <div class="input-group">

                <input type="text" class="form-control" style="color: black;    width: 270px;height: 35px;" placeholder="Search for..." id="filter">

                <span id="filter-count"></span>
            </div>
        </form>
    </div>
    <section style="margin-bottom: 1px;">

        <h2 class="bukatutup1">   <a href="#" class="clickme">Notifications<span style="float: right">نوٹیفکیشن</span></a></h2>

        <div class="container " id="target1" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach($all_notification as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->status_link ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->instructions_texts ?></h4><span itemprop="description" class="description"><?php echo $row->notification_no ?><br><?php echo $row->date ?></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div>
        </div>
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">Transfer Posting <span style="float: right">منتقلی / پوسٹنگ</span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php foreach($transfer_posting as $row){ ?>
                    <li itemscope="" itemtype="" class="item  local"><a target="_blank" href="<?php echo $row->status_link ?>"  itemprop="url"
                                                                            >
                            <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name_of_officer ?></h4><span  itemprop="" class="description">
                                <span style="float: right"><?php echo $row->date ?></span>
                                <table style="width:100%">
                                    <tr>
                                        <th><b>Designation From</b></th>
                                        <th><b>Designation To</b></th>
                                    </tr>
                                    <tr>
                                        <td><?php if(!empty($row->designation_from)) {echo $row->designation_from;} else{ echo "N/A";} ?></td>
                                        <td><?php if(!empty($row->designation_to)) {echo $row->designation_to;} else{ echo "N/A";} ?></td>
                                    </tr>
                                </table>
                                  <table style="width:100%">
                                      <tr>
                                          <th><b>Transfer From</b></th>
                                          <th><b>Transfer To</b></th>
                                      </tr>
                                      <tr>
                                          <td><?php if(!empty($row->transfer_from)) {echo $row->transfer_from;} else{ echo "N/A";} ?></td>
                                          <td><?php if(!empty($row->transfer_to)) {echo $row->transfer_to;} else{ echo "N/A";} ?></td>
                                      </tr>
                                  </table>
                            </span></a>
                    </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a href="#" class="clickme">Suspension/Termination Orders<span style="float: right">معطلی / برطرفی کے آڈرز </span></a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php foreach($all_suspension as $row){ ?>
                        <li itemscope="" itemtype="" class="item  local"><a target="_blank" href="<?php echo $row->status_link ?>"  itemprop="url"
                                                                           >
                                <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name_of_officer ?></h4><span  itemprop="" class="description">
                                <span style="float: right"><?php echo $row->date ?></span>
                                <table style="width:100%">
                                    <tr>

                                        <th><b>Designation</b></th>
                                        <th><b>Scale</b></th>
                                    </tr>
                                    <tr>

                                        <td><?php if(!empty($row->designation_to)) {echo $row->designation_to;} else{ echo "N/A";} ?></td>
                                        <td><?php if(!empty($row->scale)) {echo $row->scale;} else{ echo "N/A";} ?></td>
                                    </tr>
                                </table>
                                  <table style="width:100%">
                                      <tr>

                                          <th><b>Place Of Posting</b></th>
                                      </tr>
                                      <tr>

                                          <td><?php if(!empty($row->transfer_to)) {echo $row->transfer_to;} else{ echo "N/A";} ?></td>
                                      </tr>
                                  </table>
                            </span></a>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup4">   <a href="#" class="clickme">Promotion Orders<span style="float: right">پروموشن آڈرز</span></a></h2>
        <div class="container " id="target4" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php foreach($promotions as $row){ ?>
                        <li itemscope="" itemtype="" class="item  local"><a target="_blank" href="<?php echo $row->status_link ?>"  itemprop="url"
                                                                           >
                                <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->notification_no ?></h4><span  itemprop="" class="description">
                                <?php echo $row->instructions_texts ?>

                            </span></a>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup5">   <a href="#" class="clickme">Appointment Orders<span style="float: right">تقرری کے احکامات</span></a></h2>
        <div class="container " id="target5" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php foreach($all_appointments as $row){ ?>
                        <li itemscope="" itemtype="" class="item  local"><a target="_blank" href="<?php echo $row->status_link ?>"  itemprop="url">
                                <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name_of_officer ?></h4><span  itemprop="" class="description">
                                <span style="float: right"><?php echo $row->date ?></span>
                                <table style="width:100%">
                                    <tr>

                                        <th><b>Designation</b></th>
                                    </tr>
                                    <tr>

                                        <td><?php if(!empty($row->designation_to)) {echo $row->designation_to;} else{ echo "N/A";} ?></td>
                                    </tr>
                                </table>
                                  <table style="width:100%">
                                      <tr>
                                          <th><b>Directorate</b></th>
                                      </tr>
                                      <tr>
                                          <td><?php if(!empty($row->name)) {echo $row->name;} else{ echo "N/A";} ?></td>
                                      </tr>
                                  </table>
                            </span></a>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </section>

    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup6">   <a href="#" class="clickme">Experience Certificate<span style="float: right">تجربہ سرٹیفیکیٹ</span></a></h2>
        <div class="container " id="target6" style="display: none">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul class="MDI-Results searchlist">
                    <?php foreach($all_appointments as $row){ ?>
                        <li itemscope="" itemtype="" class="item  local"><a target="_blank" href="<?php echo $row->status_link ?>"  itemprop="url">
                                <h4 itemprop="name" class="title" style="font-size: 16px;"><?php echo $row->name_of_officer ?></h4><span  itemprop="" class="description">
                                <span style="float: right"><?php echo $row->date ?></span>
                                <table style="width:100%">
                                    <tr>

                                        <th><b>Designation</b></th>
                                    </tr>
                                    <tr>

                                        <td><?php if(!empty($row->designation_to)) {echo $row->designation_to;} else{ echo "N/A";} ?></td>
                                    </tr>
                                </table>
                                  <table style="width:100%">
                                      <tr>
                                          <th><b>Directorate</b></th>
                                      </tr>
                                      <tr>
                                          <td><?php if(!empty($row->name)) {echo $row->name;} else{ echo "N/A";} ?></td>
                                      </tr>
                                  </table>
                            </span></a>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </section>

</main>

<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
    });
    $('.bukatutup5').click(function () {
        $('#target5').toggle('slow');
    });
    $('.bukatutup6').click(function () {
        $('#target6').toggle('slow');
    });
</script>