<style>
    table.dataTable tr.odd {
        background-color: #f0f0f0;
    }
    thead, th ,td{text-align: left;}
</style>
<main id="main" style="margin-top: 130px;">
    <section>
        <h2>Job Postings</h2>
        <div class="container">
            <table class="table table-bordred table-striped" id="jobs_dt">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Description</th>
                    <th>Advertisement Date</th>
                    <th>Closing Date</th>
                    <th>Status</th>
                    <th>Attachment</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($jobs)) {
                    foreach ($jobs as $row) {
                        ?>
                        <tr style="text-align: left">
                            <td ><?= $row->id ?></td>
                            <td><?= $row->title ?></td>
                            <td ><?= $row->adv_date ?></td>
                            <td><?= $row->close_date ?></td>
                            <td><?= $row->job_status  ?></td>
                            <td><?php if(!empty($row->attachment_link)){?> <a href="<?= $row->attachment_link?>" target="_blank">View</a> <?php }?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</main>
<script>
    $( document ).ready(function() {
        $('#jobs_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
    });
</script>