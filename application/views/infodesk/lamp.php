<main id="main" style="margin-top: 130px;">


    <section>
        <h2>Livestock and Access To Markets Project(LAMP)</h2>

        <div class="container">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?= base_url() ?>LiveStockAdmin/uploads/lamp/lamp_pc1.docx">
                                <h4 itemprop="name" class="title">Lamp Project</h4><span itemprop="description" class="description">Lamp Project</span></a>
                        </li>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?= base_url() ?>LiveStockAdmin/uploads/lamp/financial_agreement_lamp_1275.pdf">
                                <h4 itemprop="name" class="title">Financial Agreement</h4><span itemprop="description" class="description">Lamp Financial Agreement</span></a>
                        </li>

                </ul>
            </div>
        </div><!-- /.container -->
    </section>

</main>