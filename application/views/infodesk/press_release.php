

<main id="main" style="margin-top: 130px;">

    <div style="padding: 30px;text-align: right">
        <form id="live-search" action="" class="styled" method="post">
            <div class="input-group">

                <input type="text" class="form-control" style="color: black;    width: 270px;height: 35px;" placeholder="Search for..." id="filter">

                <span id="filter-count"></span>
            </div>
        </form>
    </div>
    <section>
        <h2>News/Events  </h2>



        <div class="container">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">

                <ul class="MDI-Results default searchlist">
                    <?php foreach($press_releases as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->image_link ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description"><?php echo $row->description ?></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>

</main>