<style>
    table.dataTable tr.odd {
        background-color: #f0f0f0;
    }
    thead, th td{text-align: left;}
</style>
<main id="main" style="margin-top: 130px;">
    <section>
        <h2>Preliminary List of Interested Candidates</h2>
        <div class="container">
            <table class="table table-bordred table-striped" id="ml_dt">
                <thead>
                <tr>
                    <th>District</th>
                    <th>Description</th>
                    <th>PDF Files</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($preliminary_list_candidates)) {
                    $i = 1;
                    foreach ($preliminary_list_candidates as $row) {
                        ?>
                        <tr class="">
                            <td ><?=$row->district?></td>
                            <td><?= $row->description ?></td>
                            <td><?php if(!empty($row->pdf_link)){?><a href="<?= $row->pdf_link	 ?>" target="_blank">View</a> <?php }?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</main>
<script>
    $( document ).ready(function() {
        $('#ml_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
    });
</script>