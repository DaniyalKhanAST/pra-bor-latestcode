<main id="main" style="margin-top: 130px;">
    <div style="padding: 30px;text-align: right">
        <form id="live-search" action="" class="styled" method="post">
            <div class="input-group">

                <input type="text" class="form-control" style="color: black;    width: 270px;height: 35px;" placeholder="Search for..." id="filter">

                <span id="filter-count"></span>
            </div>
        </form>
    </div>

    <section class="tertlinks alt">
        <h2>Country Medicines</h2>

        <div class="container">
            <ul class="grid_10 searchlist">
                <?php if (isset($medicines) && !empty($medicines)) {
                    foreach ($medicines as $row) { ?>
                        <li style="padding-right: 0;">
                            <a href="<?= isset($row->image_url) && !empty($row->image_url) ? base_url('index.php/infodesk/country_medicine_detail/' . $row->id) : $row->file_link ?>" target="_blank">
                                <img alt="" src="<?= isset($row->image_url) && !empty($row->image_url) ? $row->image_url : base_url('assets/images/icons/file_doc.svg') ?>" style="width: 200px;height: 150px">

                                <h3 style="font-size: 24px"><?php echo $row->title ?></h3>

                                <p style="margin-top: 10px;font-size: 20px"><?php if (strlen(strip_tags($row->description)) < 300) {
                                        echo $row->description;
                                    } else {
                                        echo substr(strip_tags($row->description), 0, 300) . '...';
                                    } ?></p>
                            </a>
                        </li>
                    <?php }
                } ?>
            </ul>
        </div>
    </section>
</main>