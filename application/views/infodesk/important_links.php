<main id="main" style="margin-top: 130px;">

    <section style="margin-bottom: 1px;">

        <h2 class="bukatutup1">   <a href="#" class="clickme">Academics<span style="float: right">تعلیمی ادارے</span></a></h2>

        <div class="container " id="target1" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
<?php if(!empty($academics)) {
foreach($academics as $row){ ?>
                        <li itemscope="" itemtype="" class="item state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description"></span></a>
                        </li>
               <?php }}?>


                </ul>
            </div>
        </div>
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">Industrial <span style="float: right">صنعتی </span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                    <?php if(!empty($industrial)) {
                    foreach($industrial as $row){ ?>
                        <li itemscope="" itemtype="" class="item state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description"></span></a>
                        </li>
                    <?php }}?>

                </ul>
            </div>
        </div>
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a href="#" class="clickme">National<span style="float: right">قومی</span></a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                    <?php if(!empty($national)) {
                    foreach($national as $row){ ?>
                        <li itemscope="" itemtype="" class="item state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description"></span></a>
                        </li>
                    <?php }}?>

                </ul>
            </div>
        </div>
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup4">   <a href="#" class="clickme">International <span style="float: right">بین الاقوامی
 </span></a></h2>
        <div class="container " id="target4" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                    <?php if(!empty($international)) {
                        foreach($international as $row){ ?>
                        <li itemscope="" itemtype="" class="item state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description"></span></a>
                        </li>
                    <?php }}?>
                </ul>
            </div>
        </div>
    </section>

<section style="margin-bottom: 1px;">
       <h2 class="bukatutup5">   <a href="#" class="clickme">Miscellaneous & Articles <span style="float: right">دیگر /آرٹیکلز
</span></a></h2>
       <div class="container " id="target5" style="display: none">
           <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                data-getmore="20">
               <ul class="MDI-Results default">
                   <?php if(!empty($miscellaneous_articles)) {
                       foreach($miscellaneous_articles as $row){ ?>
                       <li itemscope="" itemtype="" class="item state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                               <h4 itemprop="name" class="title"><?php echo $row->name ?></h4><span itemprop="description" class="description"></span></a>
                       </li>
                   <?php }}?>
               </ul>
           </div>
       </div>
   </section>


</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
    });
$('.bukatutup5').click(function() {
       $('#target5').toggle('slow');
   });
</script>