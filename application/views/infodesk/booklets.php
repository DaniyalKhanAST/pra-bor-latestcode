<main id="main" style="margin-top: 130px;">

    <section style="margin-bottom: 1px;">

        <h2 class="bukatutup1">   <a href="#" class="clickme">Booklets<span style="float: right">کتابچے</span></a></h2>

        <div class="container " id="target1" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                    <?php foreach($booklets as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description"></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup2">   <a href="#" class="clickme">Supplements <span style="float: right">اضافی مواد</span></a></h2>
        <div class="container " id="target2" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                    <?php foreach($supplements as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description"></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup3">   <a href="#" class="clickme">Posters<span style="float: right">پوسٹرز</span></a></h2>
        <div class="container " id="target3" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                    <?php foreach($posters as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description"></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>
    <section style="margin-bottom: 1px;">
        <h2 class="bukatutup4">   <a href="#" class="clickme">Feasibility <span style="float: right">فزیبلٹی رپورٹ</span></a></h2>
        <div class="container " id="target4" style="display: none">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default">
                    <?php foreach($booklets as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->link_url ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description"></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>


</main>
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script>
    $('.bukatutup1').click(function() {
        $('#target1').toggle('slow');
    });
    $('.bukatutup2').click(function() {
        $('#target2').toggle('slow');
    });
    $('.bukatutup3').click(function() {
        $('#target3').toggle('slow');
    });
    $('.bukatutup4').click(function() {
        $('#target4').toggle('slow');
    });
</script>