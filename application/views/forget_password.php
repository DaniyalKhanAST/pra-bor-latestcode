<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<!DOCTYPE html>
<html>
<style>
  body,
		html {
			margin: 0;
			padding: 0;
			height: 100%;
			background: #05445E !important;
		}
    .user_card {
			height: 500px;
			width: 450px;
			margin-top: auto;
			margin-bottom: auto;
			background: white;
			position: relative;
			display: flex;
			justify-content: center;
			flex-direction: column;
			padding: 10px;
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			border-radius: 5px;

		}
		.brand_logo_container {
			position: absolute;
			height: 170px;
			width: 170px;
			top: -75px;
			border-radius: 50%;
			background: #05445E;
			padding: 10px;
			text-align: center;
		}
		.brand_logo {
			height: 150px;
			width: 150px;
			border-radius: 50%;
			border: 2px solid white;
		}
		.form_container {
			margin-top: 100px;
		}
		.login_btn {
			width: 100%;
			background: #05445E !important;
			color: white !important;
		}
		.login_btn:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.login_container {
			padding: 0 2rem;
		}
		.input-group-text {
			background: #05445E !important;
			color: white !important;
			border: 0 !important;
			border-radius: 0.25rem 0 0 0.25rem !important;
		}
		.input_user,
		.input_pass:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.custom-checkbox .custom-control-input:checked~.custom-control-label::before {
			background-color: #05445E   !important;
		}
		.hide{
			visibility: hidden;
		}
		.panel-title{
			
			text-align: center 
		}
</style>
  <head>
    <meta charset="utf-8">
    <title>Forget Password</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
	
  </head>
  <body>
	<div class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="user_card">
				<div class="d-flex justify-content-center">
					<div class="brand_logo_container">
						<img src="<?= base_url() ?>assets/images/icons/Board-of-Revenue-Punjab-logo.png"  class="brand_logo" alt="Logo">
					</div>
				</div>
				<div class="d-flex justify-content-center form_container">
					<form role="form" method="post" action="<?php echo base_url('login/forget_password'); ?>">
          <!-- <?php
              $success_msg= $this->session->flashdata('message');
                  if($success_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $success_msg; ?>
                    </div>
                  <?php
                  }
                  ?> -->
				  <?php
              $error_msg= $this->session->flashdata('not_found');
                  if($error_msg){
                    ?>
                    <div class="alert alert-danger">
                      <?php echo $error_msg; ?>
                    </div>
                  <?php
                  }
                  ?>
          <h3 class="panel-title"> Forget your password? </h3>
		  <p >Confirm your CNIC and Mobile No.<br> We will send pincode on your email.</p>
						  
						<div class="input-group mb-3 ">
              
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
							<input type="text" class="form-control input_user" data-inputmask="'mask': '99999-9999999-9'" placeholder="Enter CNIC" id="cnic" name="cnic" type="text">
						</div>
						<span style="color:red"><?php echo !empty($this->session->flashdata('error')['cnic'])? $this->session->flashdata('error')['cnic']:''?></span>
						<div class="input-group mb-2 password" >
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<input type="text" class="form-control" name="mobile" data-inputmask="'mask': '0399-9999999'"   placeholder="Enter Mobile" >
						</div>
						<span style="color:red"><?php echo !empty($this->session->flashdata('error')['mobile'])? $this->session->flashdata('error')['mobile']:''?></span>
							<div class="d-flex justify-content-center mt-3 login_container">
								<input type="hidden" name="forget_password" value="1">
								<div class="d-flex justify-content-center mt-3 login_container">
				 	<button  type="submit" class="btn login_btn">Reset Password</button>
				   </div>
				   </div>
					</form>
				</div>
				
				<!-- <?php if(isset($pin_code)):?>
				<div>
					<h3>Your Pincode is: <?=$pin_code;?>.  Please login using this code.</h3>
					<a href="<?=base_url()?>/login">Login</a>
				</div>
				<?php endif;?> -->
			</div>
			</div>
		</div>
	</div>
<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script>
       $(":input").inputmask();
	   </script>
  </body>
</html>