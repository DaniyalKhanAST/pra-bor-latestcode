<script>
    var base_url = "<?=base_url();?>";
</script>
<script src="<?= base_url() ?>assets/js/services.min.js"></script>
<?php if ($folder_name == 'dashboard' && $page_name == 'index') { ?>
    <script src="<?= base_url() ?>assets/js/search.min.js"></script>
<?php } ?>
<script src="<?= base_url() ?>assets/js/pages/home/section.js"></script>
<script src="<?= base_url() ?>assets/js/pages/government/section.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?= base_url() ?>assets/js/jquery.dataTables.min.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-105651154-1', 'auto');
  ga('send', 'pageview');

</script>

<script type="text/javascript">
    $(function () {
        $('.testDiv').slimscroll({
            disableFadeOut: true,
            height: ''
        });


    });
</script>
<script>
    $(document).ready(function () {

        $("#slider a").first().show();

        var index = 0;
        var count = 5;

        function bannerRotator() {
            $('#slider a').delay(4300).eq(index).fadeOut(function () {
                if (index === count) {
                    index = -1;
                }

                $('#slider a').eq(index + 1).fadeIn(function () {
                    index++;
                    bannerRotator();
                });
            });
        }

        bannerRotator();

    });
</script>
<script>
    $(document).ready(function () {
        //FANCYBOX
        //https://github.com/fancyapps/fancyBox
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
    });
    $(document).ready(function () {
        $("#filter").keyup(function () {
            var filter = $(this).val(), count = 0;
            $(".searchlist li").each(function () {
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();
                } else {
                    $(this).show();
                    count++;
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('#example').dataTable({"sPaginationType": "full_numbers"});
    });
</script>
<script>
    $( document ).ready(function() {
        $('.agency-section h2').eq(0).trigger('click');
    });
</script>