<?php $CI =& get_instance();
$CI->load->model('footer_model');
$result = $CI->footer_model->get_all();
$twitter_detail = '';
$twitter_tooltip = '';
$facebook_detail = '';
$facebook_tooltip = '';
$instagram_detail = '';
$instagram_tooltip = '';
$blogs_detail = '';
$blogs_tooltip = '';
$youtube_detail = '';
$youtube_tooltip = '';
$helpline_detail = '';
$helpline_tooltip = '';
$copyright = '';
foreach($result as $row)
{
    if($row->page_slug=='twitter')
    {
        $twitter_detail = ($row->page_url)? $row->page_url:base_url().'page/footer_page/twitter';
        $twitter_tooltip = ($row->page_tooltip)? $row->page_tooltip: 'Twitter';
    }elseif($row->page_slug=='facebook'){
        $facebook_detail = ($row->page_url)? $row->page_url:base_url().'page/footer_page/facebook';
        $facebook_tooltip = ($row->page_tooltip)? $row->page_tooltip: 'Facebook';
    }else if($row->page_slug=='instagram'){
        $instagram_detail = ($row->page_url)? $row->page_url:base_url().'page/footer_page/instagram';
        $instagram_tooltip = ($row->page_tooltip)? $row->page_tooltip: 'Instagram';
    }else if($row->page_slug=='blogs'){
        $blogs_detail = ($row->page_url)? $row->page_url:base_url().'page/footer_page/blogs';
        $blogs_tooltip = ($row->page_tooltip)? $row->page_tooltip: 'Blogs';
    }else if($row->page_slug=='youtube'){
        $youtube_detail = ($row->page_url)? $row->page_url:base_url().'page/footer_page/youtube';
        $youtube_tooltip = ($row->page_tooltip)? $row->page_tooltip: 'YouTube';
    }else if($row->page_slug=='helpline'){
        $helpline_detail = ($row->page_url)? $row->page_url:base_url().'page/footer_page/helpline';
        $helpline_tooltip = ($row->page_tooltip)? $row->page_tooltip: 'Helpline';
    }else if($row->page_slug=='copy_right'){
        $copyright = ($row->page_url)? $row->page_url:$row->page_detail;
    }
}
?>

<footer>

    <ul id="footerSocial">
        <!-- <li class="grid_1-5" id="iconConnectTwitter"><a class="footer-text" target="_blank" href="<?=$twitter_detail;?>" title="<?=$twitter_tooltip?>">Twitter</a></li>
        <li class="grid_1-5" id="iconFacebook"><a class="footer-text" target="_blank" href="<?=$facebook_detail;?>" title="<?=$facebook_tooltip;?>">Facebook</a></li>
        <li class="grid_1-5" id="iconConnectMobile"><a class="footer-text" target="_blank" href="<?=$instagram_detail;?>" title="<?=$instagram_tooltip;?>">Instagram</a></li>
        <li class="grid_1-5" id="iconConnectBlogs"><a class="footer-text" href="<?= $blogs_detail?>" target="_blank" title="<?= $blogs_tooltip?>">Blogs</a></li>
        <li class="grid_1-5" id="iconYouTube"><a class="footer-text" target="_blank" href="<?= $youtube_detail?>" title="<?= $youtube_tooltip?>">YouTube</a></li> -->
        <li class="grid_1-5" id="iconConnectTwitter"><a class="footer-text" target="_blank" href="https://twitter.com/PRABOR3" title="<?=$twitter_tooltip?>">Twitter</a></li>
        <li class="grid_1-5" id="iconFacebook"><a class="footer-text" target="_blank" href="https://www.facebook.com/Pra-Bor-Punjab-110888748258205" title="<?=$facebook_tooltip;?>">Facebook</a></li>
        <li class="grid_1-5" id="iconConnectMobile"><a class="footer-text" target="_blank" href="https://www.instagram.com/punjab.revenue.academy/" title="<?=$instagram_tooltip;?>">Instagram</a></li>
        <li class="grid_1-5" id="iconConnectBlogs"><a class="footer-text" href="<?= $blogs_detail?>" target="_blank" title="<?= $blogs_tooltip?>">Blogs</a></li>
        <li class="grid_1-5" id="iconYouTube"><a class="footer-text" target="_blank" href="https://www.youtube.com/channel/UCEzqvvq7DAUzqBJFPxsw8Fw" title="<?= $youtube_tooltip?>">YouTube</a></li>
    </ul>
 
    <div class="wrapper">


        <div class="flex-item">
            <h4 id="footerHelp" class="footer-title "><a class="footer-text" href="<?= $helpline_detail?>?>" title="<?= $helpline_tooltip?>">Helpline</a></h4>
            <ul>
                <li><a class="footer-text" href="<?=base_url().'page/listing_page/08000-9212/'?>">08000-9212</a></li>

                <li>info@pra-borpunjab.gov.pk</li>

            </ul>
        </div>

        <div class="flex-item">
           <!-- <h4 id="footerExecutive" class="footer-title"><a href="#" title="Executive" style="color:white">Executive</a></h4>-->
            <ul style="text-align: center;">
                <br>
                <span>
                <a class="footer-text" style="margin-right: 10px;" href="<?=base_url()?>page/pages/chief_minister" title="Chief Minister">Chief Minister</a>
                <!-- <a class="footer-text" style="margin-right: 10px;" href="<?=base_url()?>page/pages/minister" title="Minister">Minister</a> -->
                <a class="footer-text" style="margin-right: 10px;" href="<?=base_url()?>page/pages/smbr" title="SMBR">SMBR</a>
                <a class="footer-text" href="<?= base_url() ?>page/pages/collaborations" title="Our Partners">Our Partners</a>
            </span>
               <!-- <li><a class="footer-text" href="<?=base_url()?>page/pages/collaborations" title="Collaborations">Collaborations</a></li>-->
            </ul>
        </div>

        <!-- <div class="flex-item">
            <h4 id="footerLegislative" class="footer-title"><a href="" title="Legislative">Legislative</a></h4>
            <ul>
                <li><a href="<?=base_url().'aboutus/laws'?>" title="Laws/Policy">Laws/Rules </a></li>
                <li><a href="<?=base_url()?>page/pages/policies" title="Policies">Policies</a></li>
                <li><a href="<?=base_url()?>page/pages/public_notifications" title="Public Notifications">Public Notifications</a></li>
            </ul>
        </div> -->

        <div class="flex-item">

        </div>

    </div>

    <ul id="portalFooter">
        <li><a class="footer-text" href="<?=base_url().'dashboard/contact_directory'?>" title="Contact Us">Contact Us</a></li>
        <li><a class="footer-text" href="<?=base_url()?>dashboard/jobs" title="Jobs">Jobs</a></li>
        <li><a class="footer-text" href="<?=base_url()?>dashboard/tender" title="Tender">Tender</a></li>
        <li><a class="footer-text" href="<?=base_url()?>infodesk/download_careers/" title="Downloads">Downloads</a></li>
        <li><a class="footer-text" href="<?= base_url() ?>page/pages/site_map" title="Site Map">Site Map</a></li>
        <li><a class="footer-text" href="<?= base_url() ?>page/pages/faqs" title="FAQs">FAQs</a></li>
    </ul>
    
    <div class="footer-counter"><p class="copyright"><?=$copyright?></p></div>
    <!-- <i class="footer-counter-text" style="font-style: normal; float:right;font-size:24px;margin-top: -115px;background-color:black;color:white;border-radius:5px;padding: 0px 20px;text-align:center "><svg xmlns="http://www.w3.org/2000/svg" width="70" height="40" viewBox="0 0 18 18" fill="white"><path d="M17.997 18h-11.995l-.002-.623c0-1.259.1-1.986 1.588-2.33 1.684-.389 3.344-.736 2.545-2.209-2.366-4.363-.674-6.838 1.866-6.838 2.491 0 4.226 2.383 1.866 6.839-.775 1.464.826 1.812 2.545 2.209 1.49.344 1.589 1.072 1.589 2.333l-.002.619zm4.811-2.214c-1.29-.298-2.49-.559-1.909-1.657 1.769-3.342.469-5.129-1.4-5.129-1.265 0-2.248.817-2.248 2.324 0 3.903 2.268 1.77 2.246 6.676h4.501l.002-.463c0-.946-.074-1.493-1.192-1.751zm-22.806 2.214h4.501c-.021-4.906 2.246-2.772 2.246-6.676 0-1.507-.983-2.324-2.248-2.324-1.869 0-3.169 1.787-1.399 5.129.581 1.099-.619 1.359-1.909 1.657-1.119.258-1.193.805-1.193 1.751l.002.463z"/></svg><br>Visitors<br><?= ($counts)? $counts : 0?></i> -->
</footer>