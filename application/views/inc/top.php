<meta charset="utf-8">

<!-- Always force latest IE rendering engine -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?= isset($page_title) && !empty($page_title) ? ucwords($page_title): 'PHFMC Punjab'?></title>

<!--[if lte IE 9]>
<script type="text/javascript" src="<?= base_url()?>assets/js/lib/html5.js"></script>
<![endif]-->

<link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/css/home/style.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/css/footer.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/city/section.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/government/section.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.fancybox.min.css" media="screen">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

<link rel="apple-touch-icon" href="apple-touch-icon.png">

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery.dataTables_themeroller.css">

<meta name="keywords"
      content="phfmcpunajb.gov.pk, PHFMC Punjab, PHFMC, Punjab, Country Medicine, PHFMC Jobs, Government of Punjab, PHFMC News, PHFMC Articles">

<meta property="og:image" content="../www.utah.gov/mstile-310x310.png"/>

<!--[if IE 9]>
<link type="text/css" rel="stylesheet" href="<?= base_url()?>assets/css/ie9.css"/>
<![endif]-->

<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>