<main id="main" style="margin-top: 130px;">
    <header>

        <section>
            <!--[if gte IE 9]><!-->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" width="32px" height="32px"
                 enable-background="new 0 0 32 32" preserveAspectRatio="none">
                <g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="7.842" r="2.813"/>
                        <rect x="9.248" y="5.667" fill="#676868" width="13.599" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="16" r="2.813"/>
                        <rect x="9.248" y="13.773" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                    <g>
                        <circle fill="#676868" cx="3.809" cy="24.158" r="2.813"/>
                        <rect x="9.248" y="21.984" fill="#676868" width="21.756" height="4.349"/>
                    </g>
                </g>
            </svg>

            <!--<![endif]-->

            <h1>Annual Development Program</h1>

            <p class="agency-search">
                <input type="text" class="search-agencies" title="Search" placeholder="Search..."/>
                <button type="button" class="clear-search">Clear Search</button>
            </p>

        </section>

    </header>

    <section class="agency-section">
        <h2>New Projects<span style="float: right">نئے منصوبے</span>
        </h2>

        <div class="container ">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul>
                    <?php foreach ($new_project as $row) { ?>
                        <li itemscope="" itemtype="" class="item  local"><a itemprop="url">
                                <h4 itemprop="name" class="title" style="font-size: 16px;"></h4><span itemprop="" class="description" style="color: #4D4D4D">
                                    <span><?php echo $row->name_of_scheme ?></span>

                                    <table style="width:100%">
                                        <tr>
                                            <th><b>From Year</b></th>
                                            <th><b>To Year</b></th>
                                            <th><b>Cost In Million Rs</b></th>
                                        </tr>
                                        <tr>
                                            <td><?php if (!empty($row->from_year)) {
                                                    echo $row->from_year;
                                                } else {
                                                    echo "N/A";
                                                } ?></td>
                                            <td><?php if (!empty($row->to_year)) {
                                                    echo $row->to_year;
                                                } else {
                                                    echo "N/A";
                                                } ?></td>
                                            <td><?php if (!empty($row->cost_in_million)) {
                                                    echo $row->cost_in_million;
                                                } else {
                                                    echo "N/A";
                                                } ?></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <?php if (!empty($row->link_url)) {
                                                    $file_name = $row->link_url;
                                                    if($row->link_type==1) {
                                                        $tempfile = explode("/", $row->link_url);
                                                        $file_name_mix = end($tempfile);
                                                        $temp = explode("_", $file_name_mix);
                                                        $file = end($temp);
                                                        $ext = explode('.', end($temp));
                                                        $file_name = sizeof($temp) > 1 ? trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix)))) . '.' . end($ext) : $file_name_mix;
                                                    }
                                                    ?>
                                                    <br>
                                                    <a  target="_blank" href="<?= $row->link_url ?>" title="<?= strtoupper(urldecode($file_name)); ?>">
                                                        <span style="color:black;font-weight: bold"><i>
                                                                <?= $row->link_type == '1'? file_get_contents(base_url('assets/images/icons/file_icon.svg')) : file_get_contents(base_url('assets/images/icons/video_icon.svg')); ?>
                                                            </i>
                                                            <?php if($row->link_type==1){ echo  strlen($file_name) > 60 ? strtoupper(substr(urldecode(trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix))))), 0, 60) . '....' . end($ext)) : strtoupper(substr(urldecode($file_name), 0, 60));} else{echo 'CLICK SEE VIDEO';} ?>
                                                        </span>
                                                    </a>
                                                    <?php
                                                }  ?>
                                            </td>
                                        </tr>
                                    </table>

                            </span></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
    <section class="agency-section">
        <h2>Ongoing Projects<span style="float: right">جاری   منصوبے</span>
        </h2>

        <div class="container ">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul>
                    <?php foreach ($ongoing_project as $row) { ?>
                        <li itemscope="" itemtype="" class="item  local"><a itemprop="url">
                                <h4 itemprop="name" class="title" style="font-size: 16px;"></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <span><?php echo $row->name_of_scheme ?></span>

                                  <table style="width:100%">
                                      <tr>
                                          <th><b>From Year</b></th>
                                          <th><b>To Year</b></th>
                                          <th><b>Cost In Million Rs</b></th>
                                      </tr>
                                      <tr>
                                          <td><?php if (!empty($row->from_year)) {
                                                  echo $row->from_year;
                                              } else {
                                                  echo "N/A";
                                              } ?></td>
                                          <td><?php if (!empty($row->to_year)) {
                                                  echo $row->to_year;
                                              } else {
                                                  echo "N/A";
                                              } ?></td>
                                          <td><?php if (!empty($row->cost_in_million)) {
                                                  echo $row->cost_in_million;
                                              } else {
                                                  echo "N/A";
                                              } ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="3">
                                              <?php if (!empty($row->link_url)) {
                                                  $file_name = $row->link_url;
                                                  if($row->link_type==1) {
                                                      $tempfile = explode("/", $row->link_url);
                                                      $file_name_mix = end($tempfile);
                                                      $temp = explode("_", $file_name_mix);
                                                      $file = end($temp);
                                                      $ext = explode('.', end($temp));
                                                      $file_name = sizeof($temp) > 1 ? trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix)))) . '.' . end($ext) : $file_name_mix;
                                                  }
                                                  ?>
                                                  <br>
                                                  <a  target="_blank" href="<?= $row->link_url ?>" title="<?= strtoupper(urldecode($file_name)); ?>">
                                                        <span style="color:black;font-weight: bold"><i>
                                                                <?= $row->link_type == '1'? file_get_contents(base_url('assets/images/icons/file_icon.svg')) : file_get_contents(base_url('assets/images/icons/video_icon.svg')); ?>
                                                            </i>
                                                            <?php if($row->link_type==1){ echo  strlen($file_name) > 60 ? strtoupper(substr(urldecode(trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix))))), 0, 60) . '....' . end($ext)) : strtoupper(substr(urldecode($file_name), 0, 60));} else{echo 'CLICK SEE VIDEO';} ?>
                                                        </span>
                                                  </a>
                                                  <?php
                                              }  ?>
                                          </td>
                                      </tr>
                                  </table>
                            </span></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
    <section class="agency-section">
        <h2>Completed Projects <span style="float: right">مکمل منصوبے</span>
        </h2>

        <div class="container ">
            <div class="mIndex mdiSearchResults grid_list_4" data-locate="" data-type="5,7" data-limit="24"
                 data-radius="8" data-getmore="20">
                <ul>
                    <?php foreach ($completed_project as $row) { ?>
                        <li itemscope="" itemtype="" class="item  local"><a itemprop="url">
                                <h4 itemprop="name" class="title" style="font-size: 16px;"></h4><span itemprop="" class="description" style="color: #4D4D4D">

                                <span ><?php echo $row->name_of_scheme ?></span>

                                  <table style="width:100%">
                                      <tr >
                                          <th><b>From Year</b></th>
                                          <th><b>To Year</b></th>
                                          <th><b>Cost In Million Rs</b></th>
                                      </tr>
                                      <tr >
                                          <td><?php if (!empty($row->from_year)) {
                                                  echo $row->from_year;
                                              } else {
                                                  echo "N/A";
                                              } ?></td>
                                          <td><?php if (!empty($row->to_year)) {
                                                  echo $row->to_year;
                                              } else {
                                                  echo "N/A";
                                              } ?></td>
                                          <td><?php if (!empty($row->cost_in_million)) {
                                                  echo $row->cost_in_million;
                                              } else {
                                                  echo "N/A";
                                              } ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="3">
                                              <?php if (!empty($row->link_url)) {
                                                  $file_name = $row->link_url;
                                                  if($row->link_type==1) {
                                                      $tempfile = explode("/", $row->link_url);
                                                      $file_name_mix = end($tempfile);
                                                      $temp = explode("_", $file_name_mix);
                                                      $file = end($temp);
                                                      $ext = explode('.', end($temp));
                                                      $file_name = sizeof($temp) > 1 ? trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix)))) . '.' . end($ext) : $file_name_mix;
                                                  }
                                                  ?>
                                                  <br>
                                                  <a  target="_blank" href="<?= $row->link_url ?>" title="<?= strtoupper(urldecode($file_name)); ?>">
                                                        <span style="color:black;font-weight: bold"><i>
                                                                <?= $row->link_type == '1'? file_get_contents(base_url('assets/images/icons/file_icon.svg')) : file_get_contents(base_url('assets/images/icons/video_icon.svg')); ?>
                                                            </i>
                                                            <?php if($row->link_type==1){ echo  strlen($file_name) > 60 ? strtoupper(substr(urldecode(trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix))))), 0, 60) . '....' . end($ext)) : strtoupper(substr(urldecode($file_name), 0, 60));} else{echo 'CLICK SEE VIDEO';} ?>
                                                        </span>
                                                  </a>
                                                  <?php
                                              } ?>
                                          </td>
                                      </tr>
                                  </table>
                            </span></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
</main>