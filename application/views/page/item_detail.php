<style>
    .gallery {
        display: inline-block;
        margin-top: 20px;
    }

    #main section.tertlinks li {
        height: auto;
        padding: 0 1rem 0 0;
        margin: 0 0 .5rem;
    }

    #main section.tertlinks a li {
        list-style: inside;
    }

    a {
        outline: none;
    }

    main section ul li {
        background-image: none;
    }
</style>
<main id="main" style="margin-top: 130px;">
    <section class="alt">
        <h2><?= $meta_page->page_title ?> Detail</h2>

        <div class="container">
            <div class="grid_6">
                <ul class=" searchlist">

                    <li style="padding-right: 0;height: 100%; font-size:20px;text-align: justify;">
                        <h3 style="font-size: 24px"><?php echo $item->title ?></h3>

                        <?php echo $item->description; ?>
                    </li>

                </ul>
                <?php if (!empty($item_files)): ?>


                    <div class="container grid_12" id="target1" style="">
                        <h3>Related Files </h3>

                        <div class="mIndex grid_list_12">
                            <ul class="MDI-Results default">


                                <?php $count = 0; ?>
                                <?php foreach ($item_files as $row):
                                    $tempfile = explode("/", $row->file_link);
                                    $file_name_mix = end($tempfile);
                                    $temp = explode("_", $file_name_mix);
                                    $file = end($temp);
                                    $ext = explode('.', end($temp));
                                    $file_name = sizeof($temp) > 1 ? trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix)))) . '.' . end($ext) : $file_name_mix;
                                    ?>
                                    <li itemscope="" itemtype="" class="item Data state">
                                        <a itemprop="url" target="_blank" href="<?= $row->file_link ?>" title="<?= strtoupper(urldecode($file_name)); ?>">
                                            <h4 itemprop="name"
                                                class="title"><?= strlen($file_name) > 60 ? strtoupper(substr(urldecode(trim(str_replace('_', ' ', str_replace('___', ',', str_replace($file, '', $file_name_mix))))), 0, 60) . '....' . end($ext)) : strtoupper(substr(urldecode($file_name), 0, 60)); ?></h4>
                                        </a>
                                    </li>


                                    <div class="clearfix"></div>

                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
            <div class="grid_6">
                <div class="container">
                    <h3><?= $meta_page->page_title ?> Gallery</h3>
                    <?php if (!empty($item_images)) {

                        foreach ($item_images as $row) {
                            ?>
                            <div class="grid_6">
                                <div class="row">
                                    <div class='list-group gallery'>
                                        <div class='col-sm-6 col-xs-6 col-md-6 col-lg-6'>
                                            <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $row->image_url; ?>">
                                                <img style="width: 300px;height: 200px" class="img-responsive" alt="" src="<?php echo $row->image_url; ?>"/>

                                                <div class='text-right'>
                                                    <small class='text-muted'></small>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } ?>
                </div>
            </div>
    </section>
</main>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $(document).tooltip();
    });
</script>