<link rel="stylesheet" href="<?= base_url() ?>assets/css/froala_style.min.css">
<style>
    main section ul li {
        background: none;
        list-style: inside disc;
        margin: 0;
        padding: 0 2rem;
    }

    ol {
        margin: 0;
        padding: 0 2rem;
    }

    p {
        margin: .5rem 0 !important;
    }
    .toggle-element{
        margin-bottom: 1px;
        cursor: pointer;
    }
</style>
<main id="main" style="margin-top: 130px;">
    <section>
        <!-- <h2><?= $page_title ?></h2> -->

        <div class="container" style="font-size:20px;">
            <div class="grid_12 fr-view">
                <div class="row">
                    <div class="col">
                    <div style="width:100%"><img class="img-fluid"  src="<?= base_url()?>images/home/background_slide_2.jpg" style="width:100%;height: auto;"></div>

                    </div>
                </div>
            </div>
        </div>


    </section>
</main>

<script>

    $(document).ready(function () {
        $('#main img').each(function () {
            $(this).wrap('<a class="fancybox" href="' + $(this).attr('src') + '" rel="fancy-main"></a>');
        }).promise().done(function () {
            $('.fancybox').fancybox({});
        });
        $('.toggle-element').each(function () {
            $(this).nextUntil('.toggle-element').hide();
        });

        $('.toggle-element').click(function () {
            $(this).nextUntil('.toggle-element').toggle('slow');
        });

    });

</script>