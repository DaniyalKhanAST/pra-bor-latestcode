<link rel="stylesheet" href="<?= base_url() ?>assets/css/froala_style.min.css">
<style>
    main section ul li {
        background: none;
        list-style: inside disc;
        margin: 0;
        padding: 0 2rem;
    }

    ol {
        margin: 0;
        padding: 0 2rem;
    }

    p {
        margin: .5rem 0 !important;
    }
    .toggle-element{
        margin-bottom: 1px;
        cursor: pointer;
    }
</style>
<main id="main" style="margin-top: 130px;">
    <section>
        <h2><?= $page->page_title ?> <span style="float: right;direction: rtl"><?= $page->page_urdu_title ?></span></h2>

        <div class="container" style="font-size:20px;">
            <div class="grid_12 fr-view">

                <?php echo $page->page_detail; ?>


            </div>
        </div>


    </section>
</main>

<script>

    $(document).ready(function () {
        $('#main img').each(function () {
            $(this).wrap('<a class="fancybox" href="' + $(this).attr('src') + '" rel="fancy-main"></a>');
        }).promise().done(function () {
            $('.fancybox').fancybox({});
        });
        $('.toggle-element').each(function () {
            $(this).nextUntil('.toggle-element').hide();
        });

        $('.toggle-element').click(function () {
            $(this).nextUntil('.toggle-element').toggle('slow');
        });

    });

</script>