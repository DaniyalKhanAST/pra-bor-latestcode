﻿<main id="main" style="margin-top: 100px;">


    <!-- <div style="padding: 30px;text-align: right">
         <form id="live-search" action="" class="styled" method="post">
            <div class="input-group">

                <input type="text" class="form-control" style="color: black;    width: 270px;height: 35px;" placeholder="Search for..." id="filter">

                <span id="filter-count"></span>
            </div>
        </form> 
    </div> -->



    <section>
        <h2>Laws<span style="float: right"> قوانین</span></h2>

        <div class="container"> 
		<ul>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/abolition_of_jagirs_act_1952_qarcl.pdf" target="_blank">Abolition Of Jagirs Act, 1952.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/agricultural_income_tax_act_1997_r0ftt.pdf" target="_blank">Agricultural Income Tax Act 1997.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/agriculturists_loans_act_1958_iy8xj.pdf" target="_blank">Agriculturists Loans Act, 1958.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/alienation_of_land_act_1900_pylci.pdf" target="_blank">Alienation Of Land Act 1900.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/board_of_revenue_act_1957_yi7ck.pdf" target="_blank">Board Of Revenue Act 1957.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/border_area_regulation_1959_g70de.pdf" target="_blank">Border Area Regulation 1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/bwp_court_of_wards_act_1942_u3kjl.pdf" target="_blank">Bwp Court Of Wards Act 1942.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/conferment_of_proprietary_rights_on_non-proprietors_in_abadi_deh_act_1995_qnqe7.pdf" target="_blank">Conferment Of Proprietary Rights On Non-proprietors In Abadi Deh Act, 1995.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/copying_fees_act_1936_ut1p7.pdf" target="_blank">Copying Fees Act, 1936.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/court_of_wards_act_1903_1_rz1fj.pdf" target="_blank">Court Of Wards Act 1903 (1).pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/evacuee_property_and_displaced_personslaws_repeal_act_1975_tnvs6.pdf" target="_blank">Evacuee Property And Displaced Personslaws Repeal Act 1975.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/government_dues_recovery_ordinance_1962_i4lra.pdf" target="_blank">Government Dues Recovery Ordinance, 1962.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/government_lands_and_buildings_recovery_of_possession_ordinance_1966_erk3z.pdf" target="_blank">Government Lands And Buildings Recovery Of Possession Ordinance, 1966.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/hindu_womens_rights_to_agricultural_land_ordinance_1959_7kc1m.pdf" target="_blank">Hindu Women&#39;s Rights To Agricultural Land Ordinance, 1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/jinnah_abadi_act_1986_2qh1n.pdf" target="_blank">Jinnah Abadi Act 1986.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/land_dispositions_saving_of_shamilat_ordinance_1959_yfumw.pdf" target="_blank">Land Dispositions Saving Of Shamilat Ordinance 1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/land_reforms_act_1977_n47xv.pdf" target="_blank">Land Reforms Act, 1977.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/land_reforms_regulation_1972_ba3dw.pdf" target="_blank">Land Reforms Regulation, 1972.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/pre-emption_act_1991_oz4z4.pdf" target="_blank">Pre-emption Act 1991.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/prohibition_of_private_money_lending_act_2007_wrx1f.pdf" target="_blank">Prohibition Of Private Money Lending Act 2007.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/protection_and_restoration_of_tenancy_rights_act_1950_rtwi1.pdf" target="_blank">Protection And Restoration Of Tenancy Rights Act, 1950.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/punjab_land_revenue_act_1967_u5i7n.pdf" target="_blank">Punjab Land Revenue Act, 1967.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/recovery_of_cost_copies_of_essential_revenue_records_ordinance_1963_ahkkj.pdf" target="_blank">Recovery Of Cost Copies Of Essential Revenue Records Ordinance, 1963.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/relief_of_indebtedness_ordinance_1960_1e5za.pdf" target="_blank">Relief Of Indebtedness Ordinance, 1960.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/revenue_recovery_act_1890_5imi1.pdf" target="_blank">Revenue Recovery Act, 1890.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/survey_and_rectangulation_of_lands_ordinance_1959_h5r0f.pdf" target="_blank">Survey And Rectangulation Of Lands Ordinance, 1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/tenancy_act_1887_8fl32.pdf" target="_blank">Tenancy Act 1887.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/tenancy_v_and_e_ordinance_1969_1rzuy.pdf" target="_blank">Tenancy V And E Ordinance, 1969.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/thal_development_act_1949_zm3py.pdf" target="_blank">Thal Development Act 1949.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_cholistan_development_authority_act_1976_mk0x2.pdf" target="_blank">The Cholistan Development Authority Act 1976.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_court_fees_act_1870_v6dec.pdf" target="_blank">The Court Fees Act 1870.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_government_grant_act_1895_s6krp.pdf" target="_blank">The Government Grant Act 1895.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_consolidation_of_holdings_ordinance_1960_7jha3.pdf" target="_blank">The Punjab Consolidation Of Holdings Ordinance 1960.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_land_records_authority_act_2017_ucrjn.pdf" target="_blank">The Punjab Land Records Authority Act, 2017.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_national_calamities_1958_4l0js.pdf" target="_blank">The Punjab National Calamities 1958.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_privatization_board_act_2010_eqc8d.pdf" target="_blank">The Punjab Privatization Board Act 2010.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_registration_act_1908_x2nfn.pdf" target="_blank">The Registration Act 1908.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_stamp_act_1899_ktm0t.pdf" target="_blank">The Stamp Act 1899.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/transfer_property_act_pom3q.pdf" target="_blank">Transfer Property Act.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/treasure-trove_act_1878_wbn99.pdf" target="_blank">Treasure-trove Act, 1878.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/usurious_loans_ordinance_1959_qyxzn.pdf" target="_blank">Usurious Loans Ordinance, 1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/west_pakistan_border_area_regulation_1959_m02wn.pdf" target="_blank">West Pakistan Border Area Regulation, 1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/land_reforms_regulation_1972_jn3y4.pdf" target="_blank">Land Reforms Regulation, 1972.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/revenue_recovery_act_1890_jqfhz.pdf" target="_blank">Revenue Recovery Act, 1890.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/scheme_for_the_lease_of_evacuee_trust_agricultural_land_1975_9fahl.pdf" target="_blank">Scheme For The Lease Of Evacuee Trust Agricultural Land, 1975.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_agriculturists_loans_act_1958_pzd3d.pdf" target="_blank">The [Punjab] Agriculturists&rsquo; Loans Act, 1958.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_government_dues_recovery_ordinance_1962_akiqk.pdf" target="_blank">The [Punjab] Government Dues Recovery Ordinance, 1962.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_government_dues_recovery_ordinance_1962_cuzsm.pdf" target="_blank">The [Punjab] Government Dues Recovery Ordinance, 1962.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_evacuee_trust_properties_management_dispoisal_act_1975_iz64w.pdf" target="_blank">The Evacuee Trust Properties (Management &amp; Dispoisal) Act 1975.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_land_acquisition_mines_act_1885_yriyj.pdf" target="_blank">The Land Acquisition Mines Act 1885.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_land_improvement_loans_act_1883_v63mf.pdf" target="_blank">The Land Improvement Loans Act, 1883.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_land_reforms_regulation_1972_3prqc.pdf" target="_blank">The Land Reforms Regulation 1972.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_acquisition_of_land_housing_repeal_act_1985_gjhve.pdf" target="_blank">The Punjab Acquisition Of Land Housing Repeal Act 1985.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_agricultural_income_tax_act_1997_7hree.pdf" target="_blank">The Punjab Agricultural Income Tax Act, 1997.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_land_revenue_abolition_act_1998_gs0al.pdf" target="_blank">The Punjab Land Revenue (Abolition) Act 1998.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_lands_improvement_tax_act_1975_2hpmv.pdf" target="_blank">The Punjab Lands Improvement Tax Act, 1975.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_requisitioned_land_continuance_act_1958_hqt4e.pdf" target="_blank">The Punjab Requisitioned Land Continuance Act 1958.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_sindh_revenue_board_act_2010_kj16l.pdf" target="_blank">The Sindh Revenue Board Act 2010.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_west_pakistan_board_of_revenue_act_1957_ylouu.pdf" target="_blank">The West Pakistan Board Of Revenue Act 1957.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_west_pakistan_determination_of_land_revenue_and_water_rate_ordinance_1959_act_no_lv_of_1959_x3ouf.pdf" target="_blank">The West Pakistan Determination of Land Revenue and Water Rate Ordinance, 1959 (Act No. LV of 1959).pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_land_acquisition_act_1894_o9rhk.pdf" target="_blank">The Land Acquisition Act 1894.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_katchi_abadis_act_1992_gkt6l.pdf" target="_blank">The Punjab Katchi Abadis Act 1992.pdf</a></span></li>
</ul>



            <!-- <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach($law as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->file_link ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->title ?></h4><span itemprop="description" class="description"><?php echo $row->description ?></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div> -->
        </div><!-- /.container -->
    </section>
    <section>
        <h2>Rules<span style="float: right">قواعد</span></h2>

        <div class="container">
		<ul>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/central_board_of_revenue_rules_1967_baikf.pdf" target="_blank">Central Board of Revenue Rules, 1967.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/evacuee_trust_properties_appeal_and_revision_rules_1980_1y8f1.pdf" target="_blank">Evacuee Trust Properties (appeal and revision) rules 1980.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/punjab_land_reforms_procedure_for_ejectment_suits_rules_1977_zvwc6.pdf" target="_blank">Punjab Land Reforms (Procedure for Ejectment Suits) Rules 1977.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/statement_of_conditions_for_grant_of_resumed_land_under_paragraph_183_of_the_land_reforms_regulation_1972_b35op.pdf" target="_blank">Statement of Conditions for Grant of Resumed Land Under Paragraph 18(3) of The Land Reforms Regulation, 1972.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/statement_of_conditions_for_grant_of_resumed_land_under_paragraph_324_of_the_land_reforms_regulation_1972_4ltzc.pdf" target="_blank">Statement of Conditions for Grant of Resumed Land Under Paragraph 32(4) of The Land Reforms Regulation, 1972.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_land_reforms_act_1977_federal_act_no_of_1977_s42rc.pdf" target="_blank">The Land Reforms Act, 1977 (Federal Act No. of 1977).pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_land_reforms_regulation_1972_material_law_regulation_no_115_of_1972_xx406.pdf" target="_blank">The Land Reforms Regulation, 1972 (Material Law Regulation No. 115 of 1972).pdf</a></span></li>
	<li><span style="font-size: 14px;">T<a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_land_reform_procedure_for_grant_of_land_rules_1978_d3kck.pdf" target="_blank">he Punjab Land Reform Rrocedure for Grant of Land Rules 1978.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_land_reforms_procedure_for_grant_of_land_rules_1978_4qhm9.pdf" target="_blank">The Punjab Land Reforms (Procedure for Grant of Land) Rules, 1978.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_land_reforms_rules_1972_2za9l.pdf" target="_blank">The Punjab Land Reforms Rules, 1972.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_land_reforms_rules_1977_zzdvz.pdf" target="_blank">The Punjab Land Reforms Rules, 1977.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_west_pakistan_land_records_ministerial_service_x3dwv.pdf" target="_blank">THE West Pakistan Land Records Ministerial Service.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_punjab_land_revenue_rules_1968_0wan3.pdf" target="_blank">The Punjab Land Revenue Rules 1968.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_west_pakistan_land_records_ministerial_service_midzw.pdf" target="_blank">THE West Pakistan Land Records Ministerial Service.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_west_pakistan_land_reforms_regulation_material_law_regulation_no_64_of_1959_z6cac.pdf" target="_blank">The West Pakistan Land Reforms Regulation (Material Law Regulation No. 64 of 1959).pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/the_west_pakistan_registration_department_ministerial_establishment_recruitment_rules_1963_jfjqz.pdf" target="_blank">The West Pakistan Registration Department Ministerial Establishment Recruitment Rules, 1963.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/west_pakistan_board_of_revenue_conduct_of_appeals_and_revisions_rules_1959_dqp0n.pdf" target="_blank">West Pakistan Board of Revenue (Conduct_of_Appeals_and_Revisions) Rules_1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/west_pakistan_board_of_revenue_conduct_of_meetings_rules_1959_59wtv.pdf" target="_blank">West Pakistan Board of Revenue (Conduct of Meetings) Rules 1959.pdf</a></span></li>
	<li><span style="font-size: 14px;"><a class="fr-file" href="https://pra-borpunjab.gov.pk/PRABORAdmin/uploads/editor_files/west_pakistan_land_revenue_conferment_of_rights_of_ownership_rules_1969_yk9oc.pdf" target="_blank">West Pakistan Land Revenue (Conferment of Rights of Ownership) Rules 1969.pdf</a></span></li>
</ul>


            <!-- <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach($rules as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->file_link ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->title ?></h4><span itemprop="description" class="description"><?php echo $row->description ?></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div> -->
        </div><!-- /.container -->
    </section>
    <!-- <section>
        <h2>Act<span style="float: right">ایکٹ</span></h2>

        <div class="container">
        <ul>
       
</ul>


            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach($rules as $row){ ?>
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?php echo $row->file_link ?>">
                                <h4 itemprop="name" class="title"><?php echo $row->title ?></h4><span itemprop="description" class="description"><?php echo $row->description ?></span></a>
                        </li>

                    <?php }?>
                </ul>
            </div> -->
        </div><!-- /.container 
    </section> -->

</main>