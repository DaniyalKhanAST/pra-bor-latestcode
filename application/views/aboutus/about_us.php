<style>
    h4 {
        font-size: 125%;
    }

    ol li {
        font-size: 125%;
    }
</style>

<main id="main" style="margin-top: 130px;">


    <section>
        <h2>
            Overview <span style="float:right;"> مجموعی جائزہ </span>

        </h2>

        <div class="container">
            <div class="mIndex grid_list_8" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach($overview as $row){ ?>
                                <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description""></span>
                    <?php } ?>
                </ul>
                <br>
                <ul class="MDI-Results default searchlist">

                    <h4 itemprop="name" class="title">
                    </h4>
                    <span itemprop="description" class="description" style="float:right;direction:rtl;font-size:18px; text-align:justify;">
ایگریکلچر سیکٹر پاکستان کی جی-ڈی-پی کا ٢١% حصہ ہے جو کہ ٤٦% لیبر کو روزگار فراہم کرتا ہے جو کے پاکستانی آبادی کا ٦٧% ہے . لائیوسٹاک شعبے کا حصہ  ایگریکلچر سیکٹر میں ٥٦% ہے یہ دونوں شعبہ جات دیہی علاقوں میں ایک دوسرے کو سپلیمنٹ کرتے ہیں.
                        
                        لائیوسٹاک کی قیمت تمام چھوٹی اور بڑی فصلوں کی مشترکہ قدرسے تقریباً 6.1 فیصد ہے۔ غیرملکی زرمبادلہ کی نسل میں لائیوسٹاک کی مصنوعات کاحصہ تقریبا 13 فیصدہے۔ نمایاں طورپر، لائیوسٹاک تقریباً 30سے 35 ملین غریب کسانوں کی روزی روٹی کا (30-40)فیصد ہے۔ حالیہ رپورٹ کے مطابق لائیوسٹاک کا مجموعی ویلیو ایڈیشن 1172 بلین پاکستانی روپے ہے، جبکہ لائیوسٹاک کا برآمدات میں حصہ 8.5 فیصد ہے۔
                        <br>
                        <br>
                    </span>
                    </a>

                </ul>
            </div>
        </div><!-- /.container -->
    </section>
    <section>
        <h2>
            Functions <span style="float:right;">فنکشنز</span>

        </h2>

        <div class="container">
            <div class="mIndex grid_list_8" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php if (!empty($functions)) {
                        foreach ($functions as $row) { ?>
                            <li itemscope="" itemtype="" class="item Data state"><a itemprop="url">
                                    <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description"></span>
                            </li>

                        <?php }
                    } ?>
                </ul>
                <div class="grid_6">

                    <ol type="i">
                        <li>Management of Livestock, Dairy & Poultry Farms.</li>
                        <li> Animal Health.</li>
                        <li>Livestock Production Extension Services.</li>
                        <li>Preservation and Development of Livestock Genetic resources.</li>
                        <li>Research & Training for Livestock Production.</li>


                    </ol>
                </div>
                <div class="grid_6" style="direction:rtl;">
                    <ol type="i">
                        <li>-لائیوسٹاک، ڈیری اور پولٹری فارمز کے انتظامات</li>
                        <li>- جانوروں کی صحت</li>
                        <li>- مویشیوں کی پیداوار بڑھانے کے حوالے سے خدمات</li>
                        <li>- لائیوسٹاککے جنیاتی وسائل کا تحفظ و ترقی</li>
                        <li>- مویشیوں کی پیداوار بڑھانے کے لیے تحقیق و تربیت</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container -->
    </section>
    <section>
        <h2>
            Background<span style="float:right;">پس منظر</span>

        </h2>

        <div class="container">
            <div class="mIndex grid_list_8" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach ($background as $row) { ?>
                        <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description"></span>

                    <?php } ?>
                </ul>
                <br>
                <ul class="MDI-Results default searchlist">
                    <h4 itemprop="name" class="title"></h4><span itemprop="description" class="description" style="float:right;direction:rtl;font-size:18px; text-align:justify;">
لائیوسٹاک کا قومی معیشیت میں کردار بھڑھتا جا رہا ہے- اس کے علاوہ لائیوسٹاک کا شعبہ ہزاروں غریب بے زمین فارمرز کو روزگار فراہم کر رہا ہے جو کے گھریلو فارمنگ پر زندہ رهتے ہیں- مختصر لائیوسٹاک اور اس کی مصنوعات چھوٹے اور درمیانے درجے کے کسانوں کی معیشیت سے جڑی ہوئی ہے- لائیوسٹاک خوراک کا ایک بہت اہم ذریعہ ہے جو کے دیہی اور شہری صارفین صحت ، غذایت اور بہتری پر خاطر خواہ اثر انداز ہوتی ہے- اس سب سیکٹر کی ابھرتی ہوئی اھمیت کے باوجود اس کی کامیابیوں اور مشکلات پر توجہ نہیں دی گئی اور ساتھ ہی ساتھ مستقبل کے ترقیاتی منصبوں پر کوئی زور نہیں دیا گیا- پاکستان جیسے ملک میں جہاں لائیوسٹاک کے بارے میں اعداد و شمار آسانی سے دستیاب نہیں ہیں لائیوسٹاک اینڈ ڈیری ڈویلوپمنٹ پنجاب اپنے بنیادی ڈھانچے اور نیٹ ورک کے زریعے لائیوسٹاک سیکٹر میں مختلف خدمات انجام دے رہا ہے- اس رپورٹ میں کوشش کی گئی ہے کے لائیوسٹاک ڈیپارٹمنٹ کے مختلف ڈائریکٹریٹ کی پرفارمنس سال ٢٠١٢-٢٠١٣ کمپائل کر لی گئی ہے یہ ایک ایسی رپورٹہے جو کے منصوبہ سازوں، مشیروں اور لائیوسٹاک کے شعبے کے محققین کے لیے ایک مفید ریفرنس کے طور پر کام کر سکتی ہے- ان کوششوں کے علاوہ حکومتی سطح پر زیادہ زور مارکیٹ میں لائیوسٹاک کے شعبے کو فروغ دینے پر ہے جسکا حتمی مقصد غربت کا خاتمہ اور دیہی علاقوں میں رہنے والے افراد کی سماجی و اقتصادی ترقی ہے-
                    </span></a>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>
    <section>
        <h2>
            Policy <span style="float:right;">پالیسی</span>

        </h2>

        <div class="container">
            <div class="mIndex grid_list_8" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <ul class="MDI-Results default searchlist">
                    <?php foreach ($policy as $row) { ?>
                        <h4 itemprop="name" class="title"><?php echo $row->description ?></h4><span itemprop="description" class="description"></span>
                    <?php } ?>
                </ul>
                <br>
                <ul class="MDI-Results default searchlist">
                    <h4 itemprop="name" class="title"></h4><br><span itemprop="description" class="description" style="float:right;direction:rtl;font-size:18px; text-align:justify;">
ہر پالیسی اور پروڈکٹ کا ایک دورانیہ حیات ہوتا ہے ایسا ہی موجودہلائیوسٹاک اور ڈیریڈویلوپمنٹ پالیسی کے ساتھ بھی ہے- مختصر درمیانے اور طویل مدتی ایکشن ایریاز کو پالیسی کے ایہداف کے ساتھ پلاٹ کیاجاے تو اس کا مجموعہ ہمیں سمجھنے کی مدد دے گا وو پیمانہ جس سے ہم اس لائیوسٹاک پالیسی پر پیشرفت  کا جائزہ لے سکتے ہیں. مزید پڑھیے                        <div
                            style="margin-right:20px;">

                            <br>
                            ٩٢١١نیکسٹ جنریشن ایکسٹینشن سروس ماڈل
                            <br>
                            ٩٢١١ورچوئل گورننس کا نظام کیریزولوشن ایف-اے-او، پنجاب، سندھ ، خیبر پختونخوا ، بلوچستان، آزاد کشمیر، فاٹا اور گلگت بلتستان ٢٥-٢٦ جنوری ٢٠١٦
                            <br>
                            ٩٢١١سسٹم کےحوالے سے کرائی گئی ایف-اے-او کی ورک شاپ بارے پریس ریلیز
                            <br>
                            لائیوسٹاک کی پالیسی پر طبصرے
                            <br>
                            پنجاب جانور سلاٹر کنٹرول ترمیمی ایکٹ٢٠١٦
                            <br>
                            پنجاب میں گورننس کے معیار کا تعین
                            <br>


                        </div>


                    </span></a>
                </ul>
            </div>
        </div><!-- /.container -->
    </section>


</main>