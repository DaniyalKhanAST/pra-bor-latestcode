<main id="main" style="margin-top: 130px;">






    <section>
        <h2>Livestock Policy <span style="float:right;">لائیوسٹاک پالیسی</span></h2>

        <div class="container">
            <div class="mIndex grid_list_4" data-locate="device" data-type="15" data-limit="24" data-radius="8"
                 data-getmore="20">
                <h4>Every policy and product has a lifecycle. So is the present policy on livestock and dairy development. Plotting the policy objectives along with distinct action areas against a lifecycle in terms of short, medium and long term will help understand the measuring yardstick with the help of which progress on the policy could be evaluated.</h4>
                <p><span style="float:right;text-align:right;font-size:18px;">
                        ہر پالیسی اور پروڈکٹ کا ایک دورانیہ حیات ہوتا ہے ایسا ہی موجودہ لائیوسٹاک اور ڈیری ڈویلوپمنٹ پالیسی کے ساتھ بھی ہے- مختصر، درمیانے اور طویل مدتی ایکشن ایریاز کو پالیسی کے اہداف کے ساتھ پلاٹ کیاجاے تو اس کا مجموعہ ہمیں سمجھنے میں مدد دے گا وہ پیمانہ جس سے ہم اس لائیوسٹاک پالیسی پر پیشرفت کا جائزہ لے سکتے ہیں
                    </span></p>
                <ul class="MDI-Results default">
                        <li itemscope="" itemtype="" class="item Data state"><a itemprop="url" target="_blank" href="<?= base_url()?>assets/policy/Punjab Livestock Policy _ 16.6.2015.pdf">
                                <h3 itemprop="name" class="title">Livestock Policy</h3><span itemprop="description" class="description"></span></a>
                        </li>

                </ul>
            </div>
        </div><!-- /.container -->
    </section>


</main>