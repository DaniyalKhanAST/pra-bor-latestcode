<!DOCTYPE html>

<html lang="en">
<?php
session_start();
        $CI =& get_instance();
        $CI->load->model('Count_visitor_model');
        $counts = $CI->Count_visitor_model->get_counts();


?>
<head>
    <?php include 'inc/top.php'; ?>
    <link rel="shortcut icon" href="<?= base_url();?>/assets/images/icons/favicon.png">
</head>

<body class="index home home" data-page="home" data-section="home">

<p id="skiptocontent"><a href="#main">Skip to content</a></p>

<?php include 'inc/' . $header_name . '.php'; ?>
    <?php include $folder_name . '/' . $page_name . '.php'; ?>
<?php include 'inc/footer.php'; ?>


<?php include 'inc/bottom.php'; ?>


</body>

</html>