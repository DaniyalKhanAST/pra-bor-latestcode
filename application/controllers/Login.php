<?php

class Login extends CI_Controller {

public function __construct(){

        parent::__construct();
  			$this->load->helper('url');
  	 		$this->load->model('Employee_model');
         $this->load->library(array('email'));
        // $this->load->library('session');

}

public function index()
{
$this->load->view("login");
}

public function forget_password()
{
  if($this->input->post('forget_password')){
    $this->load->library('form_validation');
    $this->form_validation->set_rules('cnic','CNIC','required');
    $this->form_validation->set_rules('mobile','Mobile','required');
    if ($this->form_validation->run()== FALSE) {
        $errors = $this->form_validation->error_array();
        $this->session->set_flashdata('error',$errors);
        redirect($_SERVER['HTTP_REFERER']);
    }else {
      $data = $this->input->post();
      $this->load->model('Employee_model');
      $result = $this->Employee_model->forget_password($data);
      if($result)
      {
        $data['pin_code'] = $result->pin_code;
        $mail = email_send($result->email,$data['pin_code'],"PRABOR forget password");
        if(!$mail->Send()) {
          $this->session->set_flashdata('not_found','Email not send.Please try again.');
          $this->load->view('forget_password');
          
        } else {
          $this->session->set_flashdata('message','Email send successfully.Check your email and login into your account.');
          $this->load->view('login');
        }
      }else{
        $this->session->set_flashdata('not_found','No Record found.');
        redirect($_SERVER['HTTP_REFERER']);
      }
        
    }

  }else{
    $this->load->view('forget_password');

  }
}


public function check_cnic(){

  $cnic = $this->input->post();
  if($this->Employee_model->check_cnic($cnic))
  {
   echo '<span style="color:red;">User Cnic is verified.</span>';
  }
  else{
    echo '<span style="color:red;">User Cnic is not verified.</span>';;

  }
}

public function login_user(){ 

  $this->load->library('form_validation');
  $this->form_validation->set_rules('cnic','CNIC','required');
  $this->form_validation->set_rules('password','Password','required');
  if ($this->form_validation->run()== FALSE) {
      $errors = $this->form_validation->error_array();
      $this->session->set_flashdata('login_fail',$errors);
      redirect($_SERVER['HTTP_REFERER']);
  }else {
  $user_login=array(

  'cnic'=>$this->input->post('cnic'),
  'password'=>md5($this->input->post('password'))
    ); 
    $data['employee']=$this->Employee_model->login_user($user_login);
     if($data['employee'])
      {  
        $this->session->set_userdata('user_login',true);
        $this->session->set_userdata('user_id',$data['employee']->id);
        $this->session->set_userdata('user_email',$data['employee']->email);
        $this->session->set_userdata('user_name',$data['employee']->name);
        $this->session->set_userdata('user_verified',$data['employee']->is_verified);
        $this->session->set_userdata('user_mobile',$data['employee']->mobile); 
        $this->session->set_userdata('user_cnic',$data['employee']->cnic); 
        $this->session->set_userdata('user_profile_complete',$data['employee']->is_profile_complete); 
        if($data['employee']->is_profile_complete)
        {
         redirect('dashboard/employee_profile');
        }
        redirect('dashboard/add_employee_profile');
     }
     else{
       $this->session->set_flashdata('error', 'No Record Found.');
       $this->load->view("login");
     }
    }
}

public function user_logout(){

  $this->session->sess_destroy();
  redirect('dashboard');
}

public function email_working(){
  $message = '<html>
  <head>
      <title>Dispatch</title>
  </head>
  <body>






  Test Mail




  </body>
</html>
';
  // $config = Array(
  //   'mailtype' => 'html',
  //   'protocol' => 'smtp',
  //   'smtp_host' => 'ssl://smtp.googlemail.com',
  //   'smtp_port' => 587,
  //   'smtp_user' => 'websonwork@gmail.com',
  //   'smtp_pass' => 'Hassan@749488',
  //   'charset' => 'iso-8859-1',
  //     'wordwrap' => TRUE
  // );
  $config = array(
    'protocol' => 'smtp',
    'smtp_host' => 'mail.provider.net',
    'smtp_port' => 25,
    'smtp_user' => 'websonwork@gmail.com',
    'smtp_pass' => 'Hassan@749488',
    'charset' => 'utf-8',
    'mailtype' => 'html'
     );
     $this->load->library('email', $config);
//       $this->load->library('email');
// $this->email->initialize($config);
      $this->email->set_newline("\r\n");
      $this->email->from('websonwork@gmail.com','Websonwork');
      $this->email->to('hali6338@gmail.com');
      $this->email->subject('Forget password email from PRABOR');
      $this->email->message($message);
      //dd($this->email->send());
      if($this->email->send())
      {
          dd('kjljl');
           echo 'Email sent.';
      }
      else
      {
          show_error($this->email->print_debugger());
      }
}

}

?>