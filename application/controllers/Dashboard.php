<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');

    }

    public function index()
    {
        $this->load->model(array('Slider_model','Count_visitor_model'));
        $main_slider_where = array('is_main_slider'=>1);
        $slider_where = array('is_main_slider'=>0);
        $data['main_silder_images'] = $this->Slider_model->get_all_slider_imgs($main_slider_where);
        $data['slider_images'] = $this->Slider_model->get_all_slider_imgs($slider_where);
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'index';
        $data['page_title'] = 'BOR Punjab';
        $this->Count_visitor_model->check_visitor();
        $this->load->view('index', $data);
    }

    public function contact_directory()
    {
        $this->load->model('public_info_officers_model');
        $data['phfmc_head_office'] = $this->public_info_officers_model->phfmc_head_office(); 
        $data['phfmc_board'] = $this->public_info_officers_model->phfmc_board();
        $data['phfmc_hospitals'] = $this->public_info_officers_model->phfmc_hospitals();
        $data['phfmc_rural_health_centers'] = $this->public_info_officers_model->phfmc_rural_health_centers();
        $data['phfmc_basic_health_centers'] = $this->public_info_officers_model->phfmc_basic_health_centers();
        $data['phfmc_mobile_health_centers'] = $this->public_info_officers_model->phfmc_mobile_health_centers();
        $data['govf'] = $this->public_info_officers_model->get_govf();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'contact_directory';
        $data['page_title'] = 'Contact Us';
        $this->load->view('index', $data);
    }
     public function add_employee_profile()
     {
        $is_profile_complete = ($this->session->userdata('user_profile_complete'))? $this->session->userdata('user_profile_complete'):0;
         if($this->session->userdata('user_login'))
         {
             if($is_profile_complete!=1)
             {
                $this->load->model(array('Country_model','districts_model','Employee_model','General_model'));
                if($this->input->post('add_employee_detail')){
                    // dd($this->input->post());
                    $this->load->library('form_validation');
                    $this->form_validation->set_rules('cnic','Cnic','required');
                    $this->form_validation->set_rules('mobile_no','Mobile No','required');
                    $this->form_validation->set_rules('name','Name','required');
                    $this->form_validation->set_rules('father_name','Father Name','required');
                    $this->form_validation->set_rules('gender','Gender','required');
                    $this->form_validation->set_rules('date_of_birth','Date of Birth','required');
                    $this->form_validation->set_rules('domicile','Domicile','required');
                    $this->form_validation->set_rules('marital_status','Marital Status','required');
                    $this->form_validation->set_rules('nationality','Nationality','required');
                    $this->form_validation->set_rules('whatsapp_no','Whatsapp No','required');
                    // $this->form_validation->set_rules('residential_phone_no','Residential Phone No','required');
                    $this->form_validation->set_rules('permanent_address','Permanent Address','required');
                    $this->form_validation->set_rules('present_address','Present Address','required');
                    $this->form_validation->set_rules('highest_qualification','Highest Qualification','required');
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('year_of_passing','Year of passing','required');
                    $this->form_validation->set_rules('grade','Grade','required');
                    $this->form_validation->set_rules('institute','Institute','required');
                    $this->form_validation->set_rules('date_of_entry_in_service','Date of entry in service','required');
                    $this->form_validation->set_rules('designation_at_service_entry','Designation at service entry','required');
                    $this->form_validation->set_rules('pay_scales','Pay Scales','required|is_natural|less_than_equal_to[20]');
                    $this->form_validation->set_rules('name_of_office_joined','Name of office joined','required');
                    $this->form_validation->set_rules('place_of_first_posting_district','Place of first posting district','required');
                    $this->form_validation->set_rules('place_of_first_posting_tehsil','Place of first posting Tehsil','required');
                    $this->form_validation->set_rules('current_designation','Current designation','required');
                    $this->form_validation->set_rules('current_pay_scale','Current Pay Scale','required|is_natural|less_than_equal_to[20]');
                    $this->form_validation->set_rules('name_of_current_office','Name of current office','required');
                    $this->form_validation->set_rules('place_of_current_posting_district','Place of current posting district','required');
                    $this->form_validation->set_rules('place_of_current_posting_tehsil','Place of current posting tehsil','required');
                    $this->form_validation->set_rules('date_of_last_promotion','Date of last promotion','required');
                    $this->form_validation->set_rules('emergency_name','Name','required');
                    $this->form_validation->set_rules('relationship','Relationship','required');
                    $this->form_validation->set_rules('emergency_mobile_no','Mobile No','required');
                    $this->form_validation->set_rules('postal_address','Postal Address','required');
                    // $this->form_validation->set_rules('training_program_1','Name of training program 1','required');
                    // $this->form_validation->set_rules('training_program_2','Name of training program 2','required');
                    // $this->form_validation->set_rules('training_program_3','Name of training program 3','required');
                    // $this->form_validation->set_rules('training_program_4','Name of training program 4','required');
                    // $this->form_validation->set_rules('training_program_5','Name of training program 5','required');
                    $this->form_validation->set_rules('name_of_program','Name of program','required');
                    if (empty($_FILES['profile_img']['name']))
                    {
                        $this->form_validation->set_rules('profile_img','Profile Image','required');

                    }

                    if ($this->form_validation->run() == FALSE) {
                        $errors = $this->form_validation->error_array();
                        $this->session->set_flashdata('error',$errors);
                        $data['districts'] = $this->districts_model->get_all();
                        $data['countries'] = $this->Country_model->get_all();
                        $data['relations'] = $this->General_model->get_relations();
                        $data['qualifications'] = $this->General_model->get_qualifications();
                        $data['marital_status'] = $this->General_model->get_marital_status();
                        $data['designations'] = $this->General_model->get_designations();
                        $data['offices'] = $this->General_model->get_offices();
                        $data['programs'] = $this->General_model->get_programs();
                        $data['header_name'] = 'header_main';
                        $data['folder_name'] = 'dashboard';
                        $data['page_name'] = 'employee_profile';
                        $data['page_title'] = 'Employee Profile';
                        $this->load->view('index', $data);
                        // $this->load->view('dashboard/employee_profile',$data);
                        // redirect($_SERVER['HTTP_REFERER']);
                    }else {
                        $data = $this->input->post();
                        $upload_path = 'images/employee_profile/';
                        if (!is_dir($upload_path)) {
                            mkdir($upload_path, 0777, TRUE);
                        }
                        if (!empty($_FILES['profile_img']['name']) && $_FILES['profile_img']['error'] == 0) {
                            $type = pathinfo($_FILES['profile_img']['name'], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['profile_img']['name'], PATHINFO_FILENAME);
                            $_FILES['profile_img']['name'] = str_replace(' ','_',$file_name). '_' . uniqid() . "." . strtolower($type);
                            move_uploaded_file($_FILES['profile_img']['tmp_name'], $upload_path . $_FILES['profile_img']['name']);
                            $data['profile_img'] = $upload_path . $_FILES['profile_img']['name'];
                        }
                        if(isset($data['place_of_first_posting_district']) || isset($data['place_of_first_posting_tehsil']))
                        {
                                $first_disrtict =  isset($data['place_of_first_posting_district'])? $data['place_of_first_posting_district']:'';
                                $first_tehsil = isset($data['place_of_first_posting_tehsil'])? $data['place_of_first_posting_tehsil']:'';
                                $data['place_of_first_posting'] = $first_disrtict.','.$first_tehsil;
                        }

                        if(isset($data['place_of_current_posting_district']) || isset($data['place_of_current_posting_tehsil']))
                        {
                            $current_disrtict =  isset($data['place_of_current_posting_district'])? $data['place_of_current_posting_district']:'';
                            $current_tehsil = isset($data['place_of_current_posting_tehsil'])? $data['place_of_current_posting_tehsil']:'';
                            $data['place_of_current_posting'] = $current_disrtict.','.$current_tehsil;
                        }
                        $affected_rows = $this->Employee_model->add_employee($data);
                        $this->session->set_flashdata('success', 'Data has been saved and shown in non-editable.');
                        $this->session->set_userdata('user_profile_complete','1'); 

                        redirect('dashboard/employee_profile');
                    }
                    
                }else{
                $data['districts'] = $this->districts_model->get_all();
                $data['countries'] = $this->Country_model->get_all();
                $data['relations'] = $this->General_model->get_relations();
                $data['qualifications'] = $this->General_model->get_qualifications();
                $data['marital_status'] = $this->General_model->get_marital_status();
                $data['designations'] = $this->General_model->get_designations();
                $data['offices'] = $this->General_model->get_offices();
                $data['programs'] = $this->General_model->get_programs();
                $data['header_name'] = 'header_main';
                $data['folder_name'] = 'dashboard';
                $data['page_name'] = 'employee_profile';
                $data['page_title'] = 'Employee Profile';
                $this->load->view('index', $data);
                }
             }else{
                 redirect('dashboard/employee_profile');
             }
        }else{
            redirect('login');
        }
     }

     public function get_tehsil()
     {
        $district_name = $this->input->post('district_name');
        $this->load->model('districts_model');
         $data = $this->districts_model->get_district_with_tehsil($district_name);
         echo json_encode($data);

     }

     public function get_programs()
     {
        $designations = $this->input->post('designation');
        // dd($designations);
        $this->load->model('Programs_model');
         $data = $this->Programs_model->get_programs_by_designations($designations);
        //  dd($data);
         echo json_encode($data);

     }

     public function employee_profile(){
        if($this->session->userdata('user_login'))
         {
             $id = $this->session->userdata('user_id');
            $this->load->model(array('Country_model','districts_model','Employee_model'));
            $data['districts'] = $this->districts_model->get_all();
            $data['countries'] = $this->Country_model->get_all();
            $get_employee_detail = $this->Employee_model->employee_detail($id);
            $data['employee_detail'] = isset($get_employee_detail)? $get_employee_detail:NULL;
            $data['header_name'] = 'header_main';
            $data['folder_name'] = 'dashboard';
            $data['page_name'] = 'employee_profile_detail';
            $data['page_title'] = 'Employee Profile';
            $this->load->view('index', $data);
         }else{
            redirect('login');
        }
     }
     public function emp_authentication()
     {
        
        $this->load->model('Employee_verification_model');
    
        if($this->input->post('emp_auth')){
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('cnic','Cnic','required');
            $this->form_validation->set_rules('mobile','Mobile','required');
            $this->form_validation->set_rules('name','Name','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if ($this->form_validation->run()== FALSE) {
                $errors = $this->form_validation->error_array();
                $this->session->set_flashdata('error',$errors);
                redirect($_SERVER['HTTP_REFERER']);
            }else {
            
                $data = $this->input->post();
                $digits = 5;
                $this->session->set_userdata('pin_code',rand(pow(10, $digits-1), pow(10, $digits)-1));
                $result = $this->Employee_verification_model->employee_auth($data);
                if($result['already_verified']){
                    $this->session->set_flashdata('success','You are already enrolled, Please use Login link to feed/view your profile.');
                    redirect($_SERVER['HTTP_REFERER']);
                }elseif($result['id'])
                {
                    
                    $this->session->set_flashdata('verify','true');
                    $this->session->set_userdata('cnic',$data['cnic']);
                    $mail = email_send($result['email'],$this->session->userdata('pin_code'),'PRABOR Verification Code');
                    if(!$mail->Send())
                    {
                        $this->session->set_flashdata('message','Something went wrong. While sending you email.');
                    }else{
                        $this->session->set_flashdata('message','A verification code has been send to your email.Check your email.');

                    }
                    redirect('dashboard/emp_verification');
                }else{
                    $this->session->set_flashdata('fail','Something went wrong during the registration.');
                    redirect($_SERVER['HTTP_REFERER']);

                }
            }
        }else{
            $data['header_name'] = 'header_main';
            $data['folder_name'] = 'dashboard';
            $data['page_name'] = 'emp_authentication';
            $data['page_title'] = 'Employee Authentication';
            $this->load->view('index', $data);
        }
         
     }


     public function emp_verification()
     {
         if($this->input->post('code_verification')){
            //  dd($this->input->post());
            $this->load->library('form_validation');
            $this->form_validation->set_rules('pin_code','Pin Code','required');
            if ($this->form_validation->run()== FALSE) {
                $errors = $this->form_validation->error_array();
                $this->session->set_flashdata('error',$errors['pin_code']);
                redirect($_SERVER['HTTP_REFERER']);
            }else {
                $this->load->model('Employee_model');
                $data = $this->input->post();
                $result = $this->Employee_model->verify_code($data);
                if($result){
                    $this->session->set_userdata('user_login',true);
                    $this->session->set_userdata('user_id',$result->id);
                    $this->session->set_userdata('user_email',$result->email);
                    $this->session->set_userdata('user_name',$result->name);
                    $this->session->set_userdata('user_verified',$result->is_verified);
                    $this->session->set_userdata('user_mobile',$result->mobile); 
                    $this->session->set_userdata('user_cnic',$result->cnic); 
                    $this->session->set_userdata('user_profile_complete',$result->is_profile_complete); 
                    if($result->is_profile_complete)
                    {
                    redirect('dashboard/employee_profile');
                    }
                    redirect('dashboard/add_employee_profile');
                }else{
                        $this->session->set_flashdata('error','No Record Found.');
                        redirect($_SERVER['HTTP_REFERER']);
                      }
                }
         }else{
            $data['header_name'] = 'header_main';
            $data['folder_name'] = 'dashboard';
            $data['page_name'] = 'emp_verify';
            $data['page_title'] = 'Employee Verification';
            $this->load->view('index', $data);
         }
        
     }

    public function manuals()
    {
        $this->load->model(array('Manuals_model'));
        $data['manuals'] = $this->Manuals_model->get_all();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'manuals';
        $data['page_title'] = 'Manuals';
        $this->load->view('index', $data);
    }
    
    public function experience_certificates_submenu()
    {
        $this->load->model('Page_model');
        $data['good'] = $this->Page_model->get_static_page_by_slug('good');
        $data['average'] = $this->Page_model->get_static_page_by_slug('average');
        $data['poor'] = $this->Page_model->get_static_page_by_slug('poor');

        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'experience_certificates_submenu';
        $data['page_title'] = 'Experience Certificates';
        $this->load->view('index', $data);
    }
    public function master_traning()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'master_traning';
        $data['page_title'] = 'Master Traning';
        $this->load->view('index', $data);
    }
    public function patwari_profile()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'patwari_profile';
        $data['page_title'] = 'Patwari Profile';
        $this->load->view('index', $data);
    }
    public function reach_us()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'reach_us';
        $data['page_title'] = 'How To Reach Us';
        $this->load->view('index', $data);
    }
    public function district_offices()
    {
        $this->load->model('Public_info_officers_model');
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'district_offices';
        $data['page_title'] = 'District Offices Contact';
        $data['district_offices'] = $this->Public_info_officers_model->district_offices();
        $this->load->view('index', $data);
    }

    public function head_office()
    {
        $this->load->model('Public_info_officers_model');
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'head_office';
        $data['page_title'] = 'Head Office Contact';
        $data['head_office'] = $this->Public_info_officers_model->head_office();
        $this->load->view('index', $data);
    }
    
    public function eveluation_report()
    {
        $this->load->model('Reports_model');
        $data['reports'] = $this->Reports_model->get_all();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'eveluation_report';
        $data['page_title'] = 'Evaluation Report';
        $this->load->view('index', $data);
    }
    public function tender()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'tender_document';
        $data['page_title'] = 'BOR Tenders';
        $this->load->view('index', $data);
    }

    public function jobs()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'jobs';
        $data['page_title'] = 'Careers/Jobs';
        $this->load->view('index', $data);
    }
    public function pr()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'pr';
        $data['page_title'] = 'Punjab Revenue Academy';
        $this->load->view('index', $data);
    }
    public function bor()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'bor';
        $data['page_title'] = 'Board of Revenue';
        $this->load->view('index', $data);
    }
    public function fp()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'fp';
        $data['page_title'] = 'Future Plane';
        $this->load->view('index', $data);
    }
    public function galview()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'galview';
        $data['page_title'] = 'District Wise Gallary View';
        $this->load->view('index', $data);
    }
    public function training_program()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'dashboard';
        $data['page_name'] = 'training_program';
        $data['page_title'] = 'Trainig_Program';
        $this->load->view('index', $data);
    }


    function events()
    {
        $this->load->model('event_gallery_model');
        $data['events'] = $this->event_gallery_model->get_latest_events();
        $data['folder_name'] = 'dashboard';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'events';
        $data['page_title'] = 'Current News';
        $this->load->view('index', $data);
    }

    function event_detail()
    {
        $this->load->model('event_gallery_model');
        $this->load->model('event_gallery_detail_model');
        $event_id = $this->input->get('event_id');
        $data['event'] = $this->event_gallery_model->get_by_id($event_id);
        $data['event_gallery'] = $this->event_gallery_detail_model->get_by_event_id($event_id);
        $data['folder_name'] = 'dashboard';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'event_detail';
        $data['page_title'] = $data['event']->title;
        $this->load->view('index', $data);
    }

    function search_results()
    {
        $query = $this->input->get('q');

        $data['query'] = $query;
        $data['folder_name'] = 'dashboard';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'search_details';
        $data['page_title'] = 'Search Results';
        $this->load->view('index', $data);
    }


    public function search()
    {
        $keyword = $this->input->get('keyword');
        if ($keyword) {
            $this->load->model('banners_model');
            $this->load->model('projects_model');

            $results = $this->banners_model->search_by_keyword($keyword);
            if ($results) {
                $json['results'] = $results;
                $json['totalCount'] = sizeof($results);
            } else {
                $json['results'] = [];
                $json['totalCount'] = 0;
            }
            $json['message'] = 'OK';
            $json['topAgency'] = null;
            print_r(json_encode($json));
        }
    }

}

?>