﻿<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aboutus extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
    }

    public function laws()
    {
        $this->load->model('law_and_rules_model');
        $data['law'] = $this->law_and_rules_model->get_law();
        // echo "<pre>";print_r($data);die;
        $data['rules'] = $this->law_and_rules_model->get_rules();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'aboutus';
        $data['page_name'] = 'laws';
        $data['page_title'] = 'Laws & Policy';
        $this->load->view('index', $data);
    }

    public function about_us()
    {
        $this->load->model('about_us_model');
        $data['functions'] = $this->about_us_model->get_functions();
        $data['overview'] = $this->about_us_model->get_overview();
        $data['background'] = $this->about_us_model->get_background();
        $data['policy'] = $this->about_us_model->get_policy();
        $data['folder_name'] = 'aboutus';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'about_us';
        $data['page_title'] = 'About Us';
        $this->load->view('index', $data);
    }

    public function policy()
    {
        $data['folder_name'] = 'aboutus';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'policy';
        $this->load->view('index', $data);
    }
}

?>