<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
    }

    public function index()
    {
redirect();
    }

    function listing_page($meta_page_slug = null){

        $this->load->model('Page_model');
        if(!(isset($meta_page_slug)&& $meta_page_slug!="")){
            redirect();
        }else{
            $data['meta_page'] = $this->Page_model->get_meta_page_by_slug($meta_page_slug);
            if(!isset($data['meta_page'])){
                redirect();
            }
        }
        $data['items'] = $this->Page_model->get_all_items_by_meta_slug($meta_page_slug);
        if(count($data['items'])==1){
            $this->item_detail($data['items'][0]->id);
            redirect('page/page_detail/'.$meta_page_slug.'/'.$data['items'][0]->id);
        }
        $data['folder_name'] = 'page';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'item';
        $data['page_title'] = $data['meta_page']->page_title;
        $this->load->view('index', $data);
    }
    function item_detail($item_id = null){

        if (isset($item_id) && !empty($item_id)) {
            $this->load->model('Page_model');
            $data['item'] = $this->Page_model->get_item_by_id($item_id);
            $data['meta_page']=$this->Page_model->get_meta_page_by_item_id($item_id);
            if (isset($data['item']) && !empty($data['item'])) {
                $data['item_images'] = $this->Page_model->get_item_images_by_id($item_id);
                $data['item_files'] = $this->Page_model->get_item_files_by_id($item_id);
                $data['folder_name'] = 'page';
                $data['header_name'] = 'header_main';
                $data['page_name'] = 'item_detail';
                $data['page_title'] = $data['item']->title;
                $this->load->view('index', $data);
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('page/item_listing');
            }
        } else {
            $this->session->set_flashdata('error', "No Record Selected");
            redirect('page/page_listing');

        }
    }
    function page_detail($slug = null , $item_id = null){

        if (isset($item_id) && !empty($item_id)) {
            $this->load->model('Page_model');
            $data['item'] = $this->Page_model->get_item_by_id($item_id);
            // echo "<pre>";print_r($data);die;
            if(isset($slug)&& $slug!=''){
                $data['meta_page']=$this->Page_model->get_meta_page_by_slug($slug);
                // echo "<pre>";print_r($data);die;
            }else{
                $data['meta_page']=$this->Page_model->get_meta_page_by_item_id($item_id);
            }
            if (isset($data['item']) && !empty($data['item'])) {
                $data['item_images'] = $this->Page_model->get_item_images_by_id($item_id);
                
                $data['item_files'] = $this->Page_model->get_item_files_by_id($item_id);
                $data['folder_name'] = 'page';
                $data['header_name'] = 'header_main';
                $data['page_name'] = 'item_detail';
                $data['page_title'] = $data['item']->title;
                $this->load->view('index', $data);
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                if(isset($slug)){
                    redirect('page/listing_page/'.$slug);
                }else{
                    redirect('page/listing_page');
                }
            }
        } else {
            $this->session->set_flashdata('error', "No Record Selected");
            redirect('page/listing_page');

        }
    }

    function pages($slug = null)
    {

        if (isset($slug) && !empty($slug)) {
            $this->load->model('Page_model');
            $page = $this->Page_model->get_static_page_by_slug($slug);
            if(!isset($page)){
                redirect(404);
            }
                $data['page'] = $page;
                $data['folder_name'] = 'page';
                $data['header_name'] = 'header_main';
                $data['page_name'] = 'static_page';
                $data['page_title'] = $page->page_title;
                $this->load->view('index', $data);
        } else {
            redirect();

        }
    }

    function footer_page($slug = null)
    {
        if (isset($slug) && !empty($slug)) {
            $this->load->model('footer_model');
            $page = $this->footer_model->get_static_page_by_slug($slug);
            if(!isset($page)){
                redirect(404);
            }
                $data['page'] = $page;
                $data['folder_name'] = 'page';
                $data['header_name'] = 'header_main';
                $data['page_name'] = 'static_page';
                $data['page_title'] = $page->page_title;
                $this->load->view('index', $data);
        } else {
            redirect();

        }
    }

    public function slider_detail()
    {
        $data['folder_name'] = 'page';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'slider_detail';
        $data['page_title'] = 'Slider Detail';
        $this->load->view('index', $data);
    }

}