<?php

class Public_info_officers_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function head_office()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Head Office');
        $query = $this->db->get();
        // echo "<pre>",print_r($query);die;
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function district_offices()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Districts Offices');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function dispensaries()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Dispensaries');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function rural_health_centres()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Rural Health Centres (RHCs)');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function basic_health_units()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Basic Health Units (BHUs)');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function hospitals()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Hospitals');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function filter_clinics()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Filter Clinics');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function health_facilities()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', '24/ 7 Health Facilities');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function mother_child_healthcare()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Mother & Child Healthcare Centres (MCH)');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function mobile_health_facilities()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Mobile Health Facilities');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function unani_tibi_dispensaries()
    {
        $this->db->select('*');
        $this->db->from('public_info_officers');
        $this->db->where('category', 'Unani Tibi Dispensaries');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}