<?php

class About_us_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_overview()
    {
        $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('type', '1');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_background()
    {
        $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('type', '2');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_functions()
    {
        $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('type', '4');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_policy()
    {
        $this->db->select('*');
        $this->db->from('about_us');
        $this->db->where('type', '3');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}