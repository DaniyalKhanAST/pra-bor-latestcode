<?php

class Supportive_material_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_booklets()
    {
        $this->db->select('*');
        $this->db->from('supportive_material');
        $this->db->where('sup_material_cat_id', 1);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_supplements()
    {
        $this->db->select('*');
        $this->db->from('supportive_material');
        $this->db->where('sup_material_cat_id', 2);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_posters()
    {
        $this->db->select('*');
        $this->db->from('supportive_material');
        $this->db->where('sup_material_cat_id', 3);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}