<?php
class Count_visitor_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function check_visitor()
    {
        $ip = $this->input->ip_address();

        $records = $this->db->where(['ip'=>$ip])->from("count_visitor")->count_all_results();

        if ($records<1)
        {
            // Never visited - add
            $this->db->insert('count_visitor', array('ip' => $ip) );
        }
    }

    public function get_counts(){
        $counts = $this->db->count_all('count_visitor');
        return $counts;
    }
}