<?php

class General_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_relations()
    {
        $this->db->select('*');
        $this->db->from('relations');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_qualifications()
    {
        $this->db->select('*');
        $this->db->from('qualifications');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_marital_status()
    {
        $this->db->select('*');
        $this->db->from('marital_status');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_designations()
    {
        $this->db->select('*');
        $this->db->from('designations');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_offices()
    {
        $this->db->select('*');
        $this->db->from('offices');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }


    function get_programs()
    {
        $this->db->select('*');
        $this->db->from('programs');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}