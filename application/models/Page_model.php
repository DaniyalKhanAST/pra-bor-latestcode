<?php

class Page_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_meta_page_by_slug($slug)
    {
        $this->db->select('*');
        $this->db->from('meta_page_table');
        $this->db->where('slug', $slug);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_all_items_by_meta_slug($slug)
    {
        $this->db->select('*,page_info_table.id as id');
        $this->db->from('meta_page_table');
        $this->db->join('page_info_table', 'meta_page_table.id = page_info_table.meta_page_id', 'INNER');
        $this->db->join('page_images_table', 'page_images_table.page_id = page_info_table.id', 'left');
        $this->db->where('meta_page_table.slug', $slug);
        $this->db->group_by('page_info_table.id');
        $this->db->order_by('page_info_table.id', 'desc');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_item_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('page_info_table');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_item_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('page_images_table');
        $this->db->where('page_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_item_files_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('page_files_table');
        $this->db->where('page_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_meta_page_by_item_id($item_id)
    {
        $this->db->select('*');
        $this->db->from('page_info_table');
        $this->db->join('meta_page_table', 'page_info_table.meta_page_id=meta_page_table.id', 'INNER');
        $this->db->where('page_info_table.id', $item_id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_static_page_by_slug($slug)
    {
        $this->db->select('*');
        $this->db->from('static_pages');
        $this->db->where('page_slug', $slug);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

}