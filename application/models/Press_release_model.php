<?php

class Press_release_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('press_release');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_download_careers()
    {
        $this->db->select('*');
        $this->db->from('download_careers');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}