<?php

class Section_statistics_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('section_statistics');
        $this->db->where('isactive', 1);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}