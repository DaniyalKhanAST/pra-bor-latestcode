<?php

class Useful_links_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_academics()
    {
        $this->db->select('*');
        $this->db->from('useful_links');
        $this->db->where('category', 1);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_industrial()
    {
        $this->db->select('*');
        $this->db->from('useful_links');
        $this->db->where('category', 2);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_national()
    {
        $this->db->select('*');
        $this->db->from('useful_links');
        $this->db->where('category', 3);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_international()
    {
        $this->db->select('*');
        $this->db->from('useful_links');
        $this->db->where('category', 4);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_miscellaneous_articles()
    {
        $this->db->select('*');
        $this->db->from('useful_links');
        $this->db->where('category', 5);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}