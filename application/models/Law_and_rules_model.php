<?php

class Law_and_rules_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_law()
    {
        $this->db->select('*');
        $this->db->from('law_and_rules');
        $this->db->where('type', 0);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_rules()
    {
        $this->db->select('*');
        $this->db->from('law_and_rules');
        $this->db->where('type', 1);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}