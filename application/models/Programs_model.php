<?php

class Programs_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_programs_by_designations($slug)
    {
        $this->db->select('*');
        $this->db->from('programs');
        $this->db->where('designations', $slug);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {

            $this->db->select('*');
            $this->db->from('programs');
            $this->db->where('designations','all');
            $result = $this->db->get();
            
            return $result->result_array();
        } else {
            return $query->result_array();
        }
    }
}