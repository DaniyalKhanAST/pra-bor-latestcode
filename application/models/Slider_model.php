<?php

class Slider_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('flood_advertisement');
		$this->db->order_by('id','DESC');
		$this->db->limit(7);
		$query = $this->db->get();

		if ($query->num_rows() < 1) {
			return null;
		} else {
			return $query->result();
		}
	}

	function get_all_slider_imgs($where)
	{
		$this->db->select("*");
		$this->db->from('main_slider');
		$this->db->where('is_active', 1);
		if($where)
		{
		$this->db->where($where);

		}
		$this->db->order_by('slider_number', 'ASC');

		$query = $this->db->get();
		// dd($this->db->last_query());
		if ($query->num_rows() < 1) {
			return null;
		} else {
			return $query->result();
		}
	}
}