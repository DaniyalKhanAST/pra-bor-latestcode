<?php

class Footer_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('footer');
        $query = $this->db->get();
        
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }

    }

    function get_static_page_by_slug($slug)
    {
        $this->db->select('*');
        $this->db->from('footer');
        $this->db->where('page_slug', $slug);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
}