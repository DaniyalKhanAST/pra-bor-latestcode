<?php
class Districts_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}


	function get_all()
	{
		$this->db->select('*');
		$this->db->from('district_and_tehsil');
		$this->db->group_by('district');
		$query = $this->db->get();
		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_district_with_tehsil($name)
	{
		$this->db->select('tehsil');
		$this->db->from('district_and_tehsil');
		$this->db->where('district',$name);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
}