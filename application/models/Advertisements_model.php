<?php

class Advertisements_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all_medicines()
    {
        $this->db->select('country_medicine.id,country_medicine.title,country_medicine.description,country_medicine.file_link,country_medicine_images.image_url');
        $this->db->from('country_medicine');
        $this->db->join('country_medicine_images', 'country_medicine_images.medicine_id = country_medicine.id', 'left');
        $this->db->group_by('country_medicine.id');
        $this->db->order_by('country_medicine.id', 'desc');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_medicine_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('country_medicine');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_medicine_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('country_medicine_images');
        $this->db->where('medicine_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_job_postings()
    {
        $this->db->select("job_postings.*,IF(job_postings.close_date < NOW() - INTERVAL 1 DAY,'close','open') AS job_status");
        $this->db->from('job_postings');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_photos()
    {
        $this->db->select('*');
        $this->db->from('multimedia');
        $this->db->where('type', 1);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_videos()
    {
        $this->db->select('*');
        $this->db->from('multimedia');
        $this->db->where('type', 2);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_stories()
    {
        $this->db->select('*');
        $this->db->from('multimedia');
        $this->db->where('type', 3);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_games()
    {
        $this->db->select('*');
        $this->db->from('multimedia');
        $this->db->where('type', 4);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}