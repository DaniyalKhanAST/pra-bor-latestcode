<?php
class Reports_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select('reports.*,report_category.name as category_name');
		$this->db->from('reports');
        $this->db->join("report_category", "report_category.id = reports.report_cat_id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
}