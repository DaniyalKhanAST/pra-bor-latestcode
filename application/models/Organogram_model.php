<?php

class Organogram_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_by_id1()
    {
        $this->db->select('*');
        $this->db->from('organogram');
        $this->db->where('category', 1);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_by_id2()
    {
        $this->db->select('*');
        $this->db->from('organogram');
        $this->db->where('category', 2);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_by_id3()
    {
        $this->db->select('*');
        $this->db->from('organogram');
        $this->db->where('category', 3);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
}