<?php

class Event_gallery_detail_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_by_event_id($id)
    {
        $this->db->select('*');
        $this->db->from('event_gallery_detail');
        $this->db->where('gallery_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}