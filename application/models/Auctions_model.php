<?php

class Auctions_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $this->db->select("auctions.*,directorates.name as directorate_name,auction_type.name as type_name,IF(auctions.closing_date < NOW() - INTERVAL 1 DAY,'close','open') AS auc_status", false);
        $this->db->from('auctions');
        $this->db->join("directorates", "auctions.directorate_id = directorates.id", "LEFT");
		$this->db->join("auction_type", "auctions.type = auction_type.id", "LEFT");
        $this->db->order_by("auctions.id", "desc");
        $query = $this->db->get();


        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}