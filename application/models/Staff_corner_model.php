<?php

class Staff_corner_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all_transfer_posting()
    {
        $this->db->select('*');
        $this->db->from('staff_corner');
        $this->db->where('staff_type_id', "1");
        $this->db->order_by("id", "desc");
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_suspension()
    {
        $this->db->select('*');
        $this->db->from('staff_corner');
        $this->db->where('staff_type_id', "2");
        $this->db->order_by("id", "desc");
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_notification()
    {
        $this->db->select('*');
        $this->db->from('staff_corner');
        $this->db->where('staff_type_id', "3");
        $this->db->order_by("id", "desc");
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_promotions()
    {
        $this->db->select('*');
        $this->db->from('staff_corner');
        $this->db->where('staff_type_id', "4");
        $this->db->order_by("id", "desc");
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_appointments()
    {
        $this->db->select('staff_corner.*,directorates.name');
        $this->db->from('staff_corner');
        $this->db->join('directorates', 'staff_corner.directorate_id = directorates.id', 'left');
        $this->db->where('staff_type_id', "5");
        $this->db->order_by("id", "desc");
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}