<?php

class Suggestion_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($item)
	{
		$data = array(
			'name' => $item['name'],
			'email' => $item['Email'],
			'mobile_no' => $item['number'],
			'district' => $item['country'],
			'feedback' => $item['subject']
			 );

		$this->db->insert('suggestions', $data);
	}
    
}