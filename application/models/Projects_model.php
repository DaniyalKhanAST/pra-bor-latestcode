<?php

class Projects_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all_new()
    {
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->where('project_cat_id', 1);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_ongoing()
    {
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->where('project_cat_id', 2);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_limited_completed()
    {
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->where('project_cat_id', 3);
        $this->db->limit(3);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_completed()
    {
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->where('project_cat_id', 3);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}