<?php

class Development_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_provincial_budget()
    {
        $this->db->select('*');
        $this->db->from('provisional_budget');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}