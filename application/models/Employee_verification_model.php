<?php

class Employee_verification_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function employee_auth($data)
    {
        $cnic = $data['cnic'];
        $email = $data['email'];
        $mobile = $data['mobile'];

        $array = array('cnic' => $cnic, 'mobile' => $mobile);
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where($array); 
        $records = $this->db->get();
        if($records->num_rows() < 1)
        {
          $array['pin_code'] = $this->session->userdata('pin_code');
          $array['name'] = $data['name'];
          $array['email'] = $data['email'];
          unset($array['is_active']);
          $this->db->insert('employees', $array);
          $insert_id = $this->db->insert_id();
         
          if($insert_id != 0)
          {
            $result['id'] = $insert_id;
            $result['email'] = $email;
            return $result;
          }else{
            $result['id'] = 0;
            return $result;
            // $this->db->select('*');
            // $this->db->from('employees');
            // $this->db->where(array('cnic' => $cnic, 'email' => $email, 'mobile' => $mobile)); 
            // $result = $this->db->get();
            // return $result->num_rows();
          }
        }else{
            $result['already_verified'] = true;
            return $result;
        }

    }
}