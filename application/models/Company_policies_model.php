<?php
class Company_policies_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('company_policies');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
}