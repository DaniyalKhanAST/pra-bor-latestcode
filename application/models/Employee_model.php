<?php

class Employee_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function employee_auth($data)
    {
        $cnic = $data['cnic'];
        $email = $data['email'];
        $mobile = $data['mobile'];

        $array = array('cnic' => $cnic, 'email' => $email, 'mobile' => $mobile);
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where($array); 
        $records = $this->db->get();
        if($records->num_rows())
        {
            $this->db->where($array);
            $update_rows = array('pin_code'=>$this->session->userdata('pin_code'),'name'=>$data['name']);
           $result =  $this->db->update('employees',$update_rows);
           return $result;
        }else{
            return 0;
        }

    }

    public function verify_code($data)
    {
        $array = array('cnic' => $data['cnic'], 'pin_code' => $this->session->userdata('pin_code'));
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where($array); 
        $records = $this->db->get();
        if($records->num_rows())
        {
            $this->db->where($array);
            $update_rows = array('password'=>md5($this->session->userdata('pin_code')),'is_verified'=>'1');
            $result =  $this->db->update('employees',$update_rows);
            return $records->row();
        }
        return 0;
    }

    public function add_employee($item)
    {
        
        $data = array(
            'name' => $item['name'],
            'father_name' => $item['father_name'],
            'cnic' => $item['cnic'],
            'email' => $item['email'],
            'gender' => $item['gender'],
            'date_of_birth' => isset($item['date_of_birth'])? $item['date_of_birth'] : NULL,
            'mobile' => isset($item['mobile_no'])? $item['mobile_no'] : NULL,
            'profile_img' => isset($item['profile_img'])? $item['profile_img'] : NULL,
            'domicile' => isset($item['domicile'])? $item['domicile'] : NULL,
            'marital_status' => $item['marital_status'],
            'nationality' => isset($item['nationality'])? $item['nationality'] : NULL,
            'ip_address' => $this->input->ip_address(),
            'blood_group' => isset($item['blood_group'])? $item['blood_group'] : NULL,
            'religion' => isset($item['religion'])? $item['religion'] : NULL,
            'whatsapp_no' => isset($item['whatsapp_no'])? $item['whatsapp_no'] : NULL,
            'residential_phone_no' => isset($item['residential_phone_no'])? $item['residential_phone_no'] : NULL,
            'permanent_address' => isset($item['permanent_address'])? $item['permanent_address'] : NULL,
            'present_address' => isset($item['present_address'])? $item['present_address'] : NULL,
            'highest_qualification' => isset($item['highest_qualification'])? $item['highest_qualification'] : NULL,
            'year_of_passing' => isset($item['year_of_passing'])? $item['year_of_passing'] : NULL,
            'grade' => isset($item['grade'])? $item['grade'] : NULL,
            'institute' => isset($item['institute'])? $item['institute'] : NULL,
            'date_of_entry_in_service' => isset($item['date_of_entry_in_service'])? $item['date_of_entry_in_service'] : NULL,
            'designation_at_service_entry' => isset($item['designation_at_service_entry'])? $item['designation_at_service_entry'] : NULL,
            'pay_scales' => isset($item['pay_scales'])? $item['pay_scales'] : NULL,
            'name_of_office_joined' => isset($item['name_of_office_joined'])? $item['name_of_office_joined'] : NULL,
            'place_of_first_posting' => isset($item['place_of_first_posting'])? $item['place_of_first_posting'] : NULL,
            'current_designation' => isset($item['current_designation'])? $item['current_designation'] : NULL,
            'current_pay_scale' => isset($item['current_pay_scale'])? $item['current_pay_scale'] : NULL,
            'name_of_current_office' => isset($item['name_of_current_office'])? $item['name_of_current_office'] : NULL,
            'place_of_current_posting' => isset($item['place_of_current_posting'])? $item['place_of_current_posting'] : NULL,
            'date_of_last_promotion' => isset($item['date_of_last_promotion'])? $item['date_of_last_promotion'] : NULL,
            'emergency_contact_name' => isset($item['emergency_name'])? $item['emergency_name'] : NULL,
            'relationship' => isset($item['relationship'])? $item['relationship'] : NULL,
            'emergency_mobile_no' => isset($item['emergency_mobile_no'])? $item['emergency_mobile_no'] : NULL,
            'postal_address' => isset($item['postal_address'])? $item['postal_address'] : NULL,
            'training_program_1' => isset($item['training_program_1'])? $item['training_program_1'] : NULL,
            'training_program_2' => isset($item['training_program_2'])? $item['training_program_2'] : NULL,
            'training_program_3' => isset($item['training_program_3'])? $item['training_program_3'] : NULL,
            'training_program_4' => isset($item['training_program_4'])? $item['training_program_4'] : NULL,
            'training_program_5' => isset($item['training_program_5'])? $item['training_program_5'] : NULL,
            'name_of_program' => isset($item['name_of_program'])? $item['name_of_program'] : NULL,
            'is_profile_complete'=>1
        );
        $this->db->where('id',$this->session->userdata('user_id'));
        $this->db->update('employees',$data);
        return $this->db->affected_rows();
    }

    public function employee_detail($id)
    {
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where('id',$id); 
        $records = $this->db->get();
        if($records->num_rows())
        {
            return $records->row();
        }
            return 0;
    }

    public function login_user($array){
         $this->db->select('*');
         $this->db->from('employees');
        $this->db->where('cnic',$array['cnic']);
        $this->db->where('password',$array['password']);
        $query=$this->db->get();
         if($query->num_rows())
         {
             return $query->row();
         }
         else{
           return false;
         }
       }

       public function cnic_check($cnic){
 
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where('cnic',$cnic);
        $query=$this->db->get();
       
        if($query->num_rows()>0){
          return false;
        }else{
          return true;
        }
       
      }

      public function forget_password($array){
        $this->db->select('*');
        $this->db->from('employees');
       $this->db->where('cnic',$array['cnic']);
       $this->db->where('mobile',$array['mobile']);
       $query=$this->db->get();
        if($query->num_rows())
        {
            return $query->row();
        }
        else{
          return false;
        }
      }
}