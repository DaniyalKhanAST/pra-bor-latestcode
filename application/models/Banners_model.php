<?php

class Banners_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function search_by_keyword($keyword)
    {
        $this->db->select("title as name,description,CONCAT('http://www.livestockpunjab.gov.pk/dashboard/event_detail?event_id=',id) AS url");
        $this->db->from('event_gallery');
        $this->db->like('title', $keyword);
        $this->db->or_like('description', $keyword);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}