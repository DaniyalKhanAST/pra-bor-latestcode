<?php

class Downloads_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_breeding()
    {
        $this->db->select('*');
        $this->db->from('downloads');
        $this->db->where('category', 1);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function mobile()
    {
        $this->db->select('*');
        $this->db->from('downloads');
        $this->db->where('category', 2);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
}