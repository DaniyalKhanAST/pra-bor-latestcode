<?php

class Event_gallery_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_by_id($id)
    {
        $this->db->select('event_gallery.*,districts.name as district_name,directorates.name as directorate_name');
        $this->db->from('event_gallery');
        $this->db->join("districts", "event_gallery.district_id = districts.id", "LEFT");
        $this->db->join("directorates", "event_gallery.directorate_id = directorates.id", "LEFT");
        $this->db->where('event_gallery.id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_latest_events()
    {
        $this->db->select('*');
        $this->db->from('event_gallery');
        $this->db->order_by('id', 'desc');
//		$this->db->limit(8);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_latest_news_events()
    {
        $this->db->select('id,title,SUBSTR(description,1,100) as description');
        $this->db->from('event_gallery');
        $this->db->order_by('id', 'desc');
        $this->db->limit(10);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}