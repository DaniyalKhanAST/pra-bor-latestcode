<?php
class Walk_in_interview_merit_list_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('walkin_interview_merit_list');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
}