var root = location.protocol + '//' + location.host;
var base_url = root + '/PRABORAdmin/';
$(document).ready(function () {

    // banner //
    $('#add_baner').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            banner_url: {
                validators: {
                    notEmpty: {
                        message: 'Image is required'
                    }
                }
            },
            status: {
                validators: {
                    notEmpty: {
                        message: 'Status is required'
                    }
                }
            }
        }
    });
    $('.add_slider_banner').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            priority: {
                validators: {
                    notEmpty: {
                        message: 'Priority is required'
                    },
                    numeric:{
                        message: 'Digits Only'
                    }
                }
            },
            banner_url: {
                validators: {
                    notEmpty: {
                        message: 'Image is required'
                    },
                    file: {
                        extension: 'jpg',
                        type: 'image/jpeg',
                        message: 'Please choose a jpg file'
                    }
                }
            }
        }
    });

    $('#add_slider_priority').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            priority: {
                validators: {
                    notEmpty: {
                        message: 'Priority is required'
                    },
                    numeric:{
                        message: 'Digits Only'
                    }
                }
            }
        }
    });
    $('#add_dev').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            category: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },
            development_url: {
                validators: {
                    notEmpty: {
                        message: 'File is required'
                    },
                    file: {
                        extension: 'jpeg,jpg,png,pdf',
                        type: 'image/jpeg,image/png,application/pdf',
                        message: 'The selected file is not valid'
                    }
                }
            },
            status: {
                validators: {
                    notEmpty: {
                        message: 'Status is required'
                    }
                }
            }
        }
    });
    $('#add_careers_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            careers_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            careers_image: {
                validators: {
                    notEmpty: {
                        message: 'File is required'
                    },
                    file: {
                        extension: 'jpeg,jpg,png,pdf',
                        type: 'image/jpeg,image/png,application/pdf',
                        message: 'The selected file is not valid'
                    }
                }
            }
        }
    });
    $('#edit_careers_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            careers_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            careers_image: {
                validators: {
                    notEmpty: {
                        message: 'File is required'
                    },
                    file: {
                        extension: 'jpeg,jpg,png,pdf',
                        type: 'image/jpeg,image/png,application/pdf',
                        message: 'The selected file is not valid'
                    }
                }
            }
        }
    });
    $('#edit_baner').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },

            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},
            status: {
                validators: {
                    notEmpty: {
                        message: 'Status is required'
                    }
                }
            }
        }
    });
    // Event
    $('#add_submit_events').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            banner_url: {
                validators: {
                    notEmpty: {
                        message: 'Thumbnail is required'
                    }
                }
            }
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},

        }
    });
        $('#add_country_medicine').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });


    $('.name_field_validate').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            }
        }
    });


    $('.employee_form_validate').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'Mobile No is required'
                    }
                }
            },
            cnic: {
                validators: {
                    notEmpty: {
                        message: 'CNIC is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Eamil is required'
                    },
                    email: true
                }
            }

        }
    });

    $('#edit_country_medicine').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#add_item').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#edit_item').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#add_ostrich_article').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#edit_ostrich_article').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#add_duck_article').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#edit_duck_article').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });

    $('#add_cock_article').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#edit_cock_article').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });

    $('#add_training').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#edit_training').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });

    $('#add_poultry_farm').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });
    $('#edit_poultry_farm').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }
        }
    });

    //add_duck_article
    $('#edit_submit_events').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            }

            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},

        }
    });
    //add_press_form
    $('#add_press_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            press_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            press_description: {
                validators: {
                    notEmpty: {
                        message: 'Description is required'
                    }
                }
            },
            press_image: {
                validators: {
                    notEmpty: {
                        message: 'PDF File is required'
                    }
                }
            }

        }
    });
    $('#edit_press_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            press_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            press_description: {
                validators: {
                    notEmpty: {
                        message: 'Description is required'
                    }
                }
            }

        }
    });
    $('#add_job_posting').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            adv_date: {
                validators: {
                    notEmpty: {
                        message: 'Advertisement Date is required'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
                    }
                }
            },
            close_date: {
                validators: {
                    notEmpty: {
                        message: 'Closing Date is required'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
                    }
                }
            },
            attachment_link: {
                validators: {
                    notEmpty: {
                        message: 'Attachment is required'
                    },
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        message: 'Please choose a PDF file'
                    }
                }
            }

        }
    });
    $('#edit_job_posting').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            adv_date: {
                validators: {
                    notEmpty: {
                        message: 'Advertisement Date is required'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
                    }
                }
            },
            close_date: {
                validators: {
                    notEmpty: {
                        message: 'Closing Date is required'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
                    }
                }
            },
            attachment_link: {
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        message: 'Please choose a PDF file'
                    }
                }
            }

        }
    });

    //    Auction ////
    $('#add_auction_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_auction_form').formValidation('revalidateField', 'closing_date');
        });
    $('#add_auction_form')
        .find('[name="directorate_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_auction_form').formValidation('revalidateField', 'directorate_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                type: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }
                    }
                },
                closing_date: {
                    validators: {
                        notEmpty: {
                            message: 'Closing Date is required'
                        }
                    }
                },
                pdf_link: {
                    validators: {
                        notEmpty: {
                            message: 'Pdf Link is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},
                directorate_id: {
                    validators: {
                        notEmpty: {
                            message: 'Directorate id is required'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'Status is required'
                        }
                    }
                },
            }
        });
    $('#edit_auction_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_auction_form').formValidation('revalidateField', 'closing_date');
        });

    $('#edit_auction_form')
        .find('[name="directorate_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_auction_form').formValidation('revalidateField', 'directorate_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                type: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }
                    }
                },
                closing_date: {
                    validators: {
                        notEmpty: {
                            message: 'Closing Date is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},
                directorate_id: {
                    validators: {
                        notEmpty: {
                            message: 'Directorate is required'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'Status is required'
                        }
                    }
                },
            }
        });


    // Video //
    $('#add_video_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            image_url: {
                validators: {
                    notEmpty: {
                        message: 'Image is required'
                    }
                }
            },
            video_link: {
                validators: {
                    notEmpty: {
                        message: 'Video link is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //}
        }
    });
    $('#edit_video_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            image_url: {
                validators: {
                    notEmpty: {
                        message: 'Image is required'
                    }
                }
            },
            video_link: {
                validators: {
                    notEmpty: {
                        message: 'Video link is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //}
        }
    });
//    Category Project ////
    $('#add_category_project').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            }

        }
    });
    $('#edit_project_category').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            }

        }
    });

//    add_staff_form
    $('#add_staff_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_staff_form').formValidation('revalidateField', 'date');
        });


    $('#add_staff_form')
        .find('[name=""]')
        .selectpicker()
        .change(function (e) {
            $('#add_staff_form').formValidation('revalidateField', 'staff_cat_id');
        })
        .end()
        .find('[name="directorate_id"]')
        .selectpicker()
        .change(function (e) {
            $('#add_staff_form').formValidation('revalidateField', 'directorate_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                staff_cat_id: {
                    validators: {
                        notEmpty: {
                            message: 'Staff Type is required'
                        }
                    }
                },
                name_of_officer: {
                    validators: {
                        notEmpty: {
                            message: 'Name of Officer/Official is required'
                        }
                    }
                },
                //transfer_from: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Transfer From is required'
                //        }
                //    }
                //},
                //designation_from: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Designation is required'
                //        }
                //    }
                //},
                //transfer_to: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Transfer To is required'
                //        }
                //    }
                //},
                //designation_to: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Designation is required'
                //        }
                //    }
                //},
                date: {
                    validators: {
                        notEmpty: {
                            message: 'Dated is required'
                        }
                    }
                },
                status_link: {
                    validators: {
                        notEmpty: {
                            message: 'Status is required'
                        }
                    }
                },
                //order_no: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Order No is required'
                //        }
                //    }
                //},
                //scale: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Scale is required'
                //        }
                //    }
                //},
                //instructions_texts: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Instruction Text is required'
                //        }
                //    }
                //},
                notification_no: {
                    validators: {
                        notEmpty: {
                            message: 'Notification No is required'
                        }
                        //numeric:{
                        //    message: 'Digits Only'
                        //}

                    }
                },
                directorate_id: {
                    validators: {
                        notEmpty: {
                            message: 'Directorate is required'
                        }
                    }
                }

            }
        });
    $('#edit_staff_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_staff_form').formValidation('revalidateField', 'date');
        });


    $('#edit_staff_form')
        .find('[name=""]')
        .selectpicker()
        .change(function (e) {
            $('#edit_staff_form').formValidation('revalidateField', 'staff_cat_id');
        })
        .end()
        .find('[name=""]')
        .selectpicker()
        .change(function (e) {
            $('#edit_staff_form').formValidation('revalidateField', 'directorate_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                staff_cat_id: {
                    validators: {
                        notEmpty: {
                            message: 'Staff Type is required'
                        }
                    }
                },
                name_of_officer: {
                    validators: {
                        notEmpty: {
                            message: 'Name of Officer/Official is required'
                        }
                    }
                },
                //transfer_from: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Transfer From is required'
                //        }
                //    }
                //},
                //designation_from: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Designation is required'
                //        }
                //    }
                //},
                //transfer_to: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Transfer To is required'
                //        }
                //    }
                //},
                //designation_to: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Designation is required'
                //        }
                //    }
                //},
                date: {
                    validators: {
                        notEmpty: {
                            message: 'Dated is required'
                        }
                    }
                },

                //order_no: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Order No is required'
                //        }
                //    }
                //},
                //scale: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Scale is required'
                //        }
                //    }
                //},
                //instructions_texts: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Instruction Text is required'
                //        }
                //    }
                //},
                notification_no: {
                    validators: {
                        notEmpty: {
                            message: 'Notification No is required'
                        }
                    }
                }


            }
        });




//    add_sector_camps
    $('#add_sector_camps')
        .find('[name="district_id"]')
        .selectpicker()
        .change(function (e) {
            $('#add_sector_camps').formValidation('revalidateField', 'district_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                district_id: {
                    validators: {
                        notEmpty: {
                            message: 'District is required'
                        }
                    }
                },
                flood_sector: {
                    validators: {
                        notEmpty: {
                            message: 'Flood Sectors is required'
                        }
                    }
                },
                flood_sub_sector: {
                    validators: {
                        notEmpty: {
                            message: 'Flood Sub Sectors is required'
                        }
                    }
                },
                flood_relief_camp: {
                    validators: {
                        notEmpty: {
                            message: 'Flood Relief Camp is required'
                        }
                    }
                },
                veterinary_officers: {
                    validators: {
                        notEmpty: {
                            message: 'Veterinary Officers is required'
                        }
                    }
                },
                veterinary_assistants: {
                    validators: {
                        notEmpty: {
                            message: 'Veterinary Assistants is required'
                        }
                    }
                },
                mobile_veterinary_dispenceries: {
                    validators: {
                        notEmpty: {
                            message: 'Mobile Veterinary Dispensories is required'
                        }
                    }
                },

            }
        });
    $('#edit_sector_camps')
        .find('[name="district_id"]')
        .selectpicker()
        .change(function (e) {
            $('#edit_sector_camps').formValidation('revalidateField', 'district_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                district_id: {
                    validators: {
                        notEmpty: {
                            message: 'District is required'
                        }
                    }
                },
                flood_sector: {
                    validators: {
                        notEmpty: {
                            message: 'Flood Sectors is required'
                        }
                    }
                },
                flood_sub_sector: {
                    validators: {
                        notEmpty: {
                            message: 'Flood Sub Sectors is required'
                        }
                    }
                },
                flood_relief_camp: {
                    validators: {
                        notEmpty: {
                            message: 'Flood Relief Camp is required'
                        }
                    }
                },
                veterinary_officers: {
                    validators: {
                        notEmpty: {
                            message: 'Veterinary Officers is required'
                        }
                    }
                },
                veterinary_assistants: {
                    validators: {
                        notEmpty: {
                            message: 'Veterinary Assistants is required'
                        }
                    }
                },
                mobile_veterinary_dispenceries: {
                    validators: {
                        notEmpty: {
                            message: 'Mobile Veterinary Dispensories is required'
                        }
                    }
                },

            }
        });


//    Project //

    $('#add_project_form')
        .find('[name="project_cat_id"]')
        .selectpicker()
        .change(function (e) {
            $('#add_project_form').formValidation('revalidateField', 'project_cat_id');
        })
        .end()
        .find('[name="from_year"]')
        .selectpicker()
        .change(function (e) {
            $('#add_project_form').formValidation('revalidateField', 'from_year');
        })
        .end()
        .find('[name="to_year"]')
        .selectpicker()
        .change(function (e) {
            $('#add_project_form').formValidation('revalidateField', 'to_year');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                project_cat_id: {
                    validators: {
                        notEmpty: {
                            message: 'Project cat id is required'
                        }
                    }
                },
                GS_no: {
                    validators: {
                        notEmpty: {
                            message: 'GS no is required'
                        }
                    }
                },
                name_of_scheme: {
                    validators: {
                        notEmpty: {
                            message: 'Name of scheme is required'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'Status is required'
                        }
                    }
                },
                cost_in_million: {
                    validators: {
                        notEmpty: {
                            message: 'Estimated Cost(Million) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                cost_in_pk: {
                    validators: {
                        notEmpty: {
                            message: 'Estimated Cost(PKR) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                duration: {
                    validators: {
                        notEmpty: {
                            message: 'Duration is required'
                        }
                    }
                },
                from_year: {
                    validators: {
                        notEmpty: {
                            message: 'From year is required'
                        }
                    }
                },
                to_year: {
                    validators: {
                        notEmpty: {
                            message: 'To year is required'
                        }
                    }
                },

            }
        });

    $('#edit_project_form')
        .find('[name="project_cat_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_project_form').formValidation('revalidateField', 'project_cat_id');
        })
        .end()
        .find('[name="from_year"]')
        .selectpicker()
        .change(function (e) {
            $('#add_project_form').formValidation('revalidateField', 'from_year');
        })
        .end()
        .find('[name="to_year"]')
        .selectpicker()
        .change(function (e) {
            $('#add_project_form').formValidation('revalidateField', 'to_year');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                project_cat_id: {
                    validators: {
                        notEmpty: {
                            message: 'Project cat id is required'
                        }
                    }
                },
                GS_no: {
                    validators: {
                        notEmpty: {
                            message: 'GS no is required'
                        }
                    }
                },
                name_of_scheme: {
                    validators: {
                        notEmpty: {
                            message: 'Name of scheme is required'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'Status is required'
                        }
                    }
                },
                cost_in_million: {
                    validators: {
                        notEmpty: {
                            message: 'Estimated Cost(Million) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                cost_in_pk: {
                    validators: {
                        notEmpty: {
                            message: 'Estimated Cost(PKR) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },

                from_year: {
                    validators: {
                        notEmpty: {
                            message: 'From year is required'
                        }
                    }
                },
                to_year: {
                    validators: {
                        notEmpty: {
                            message: 'To year is required'
                        }
                    }
                },

            }
        });


//    add_research_Form //
    $('#add_research_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            dissertation_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            dissertation__by: {
                validators: {
                    notEmpty: {
                        message: 'Dissertation by is required'
                    }
                }
            },
            current_place_of_posting: {
                validators: {
                    notEmpty: {
                        message: 'Current place of posting is required'
                    }
                }
            }
        }
    });
    $('#editresearch_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            dissertation_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            dissertation__by: {
                validators: {
                    notEmpty: {
                        message: 'Dissertation by is required'
                    }
                }
            },
            current_place_of_posting: {
                validators: {
                    notEmpty: {
                        message: 'Current place of posting is required'
                    }
                }
            }
        }
    });
//    Poultry Form //
    $('#add_poultry_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            poultry_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            poultry_qty: {
                validators: {
                    notEmpty: {
                        message: 'Quantity is required'
                    },
                    regexp: {
                        regexp: '^[0-9]*[.]?[0-9]*$',
                        message: 'Must be numbers'
                    }
                }
            }

        }
    });
    $('#edit_poultry_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            poultry_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            poultry_qty: {
                validators: {
                    notEmpty: {
                        message: 'Quantity is required'
                    },
                    regexp: {
                        regexp: '^[0-9]*[.]?[0-9]*$',
                        message: 'Must be numbers'
                    }
                }
            }

        }
    });
//    speciesForm ////
    $('#add_species_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            species_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            }

        }
    });
    $('#edit_species_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            species_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            }

        }
    });
    // report_category_Form //
    $('#add_report_category_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            reportcategory_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            }

        }
    });
    $('#edit_report_category_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            reportcategory_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            }

        }
    });
//    report_Form //

    $('#add_report_Form')
        .find('[name="report_cat_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_report_Form').formValidation('revalidateField', 'report_cat_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                report_cat_id: {
                    validators: {
                        notEmpty: {
                            message: 'Report cat id is required'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }
                    }
                },
                pdf_link: {
                    validators: {
                        notEmpty: {
                            message: 'Pdf Link is required'
                        }
                    }
                },
                detail: {
                    validators: {
                        notEmpty: {
                            message: 'Description is required'
                        }
                    }
                },

            }
        });


    $('#edit_report_Form')
        .find('[name="report_cat_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_report_Form').formValidation('revalidateField', 'report_cat_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                report_cat_id: {
                    validators: {
                        notEmpty: {
                            message: 'Report cat id is required'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }
                    }
                },
                detail: {
                    validators: {
                        notEmpty: {
                            message: 'Description is required'
                        }
                    }
                },

            }
        });
//    aboutsUS Form
    $('#add_aboutus_Form')
        .find('[name="type"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_aboutus_Form').formValidation('revalidateField', 'type');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},

                type: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },

            }
        });
    $('#edit_AboutUs_Form')
        .find('[name="type"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_AboutUs_Form').formValidation('revalidateField', 'type');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},
                type: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },

            }
        });


//    poultry_product_Form

    $('#add_poultry_pr_Form')
        .find('[name="poultry_item_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_poultry_pr_Form').formValidation('revalidateField', 'poultry_item_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                poultry_item_id: {
                    validators: {
                        notEmpty: {
                            message: 'Poultry item  is required'
                        }
                    }
                },
                pak_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Pak) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                punjab_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Punjab) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                share: {
                    validators: {
                        notEmpty: {
                            message: 'Share is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                }

            }
        });

    $('#edit_poultry_pr_Form')
        .find('[name="poultry_item_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_poultry_pr_Form').formValidation('revalidateField', 'poultry_item_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                poultry_item_id: {
                    validators: {
                        notEmpty: {
                            message: 'Poultry item is required'
                        }
                    }
                },
                pak_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Pak) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                punjab_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Punjab) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                share: {
                    validators: {
                        notEmpty: {
                            message: 'Share is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                }

            }
        });

//    population_Form

    $('#add_population_Form')
        .find('[name="species_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_population_Form').formValidation('revalidateField', 'species_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                species_id: {
                    validators: {
                        notEmpty: {
                            message: 'Species is required'
                        }
                    }
                },
                pak_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Pak) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                punjab_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Punjab) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                share: {
                    validators: {
                        notEmpty: {
                            message: 'Share is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                }

            }
        });

    $('#edit_Population_Form')
        .find('[name="species_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_Population_Form').formValidation('revalidateField', 'species_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                species_id: {
                    validators: {
                        notEmpty: {
                            message: 'Species is required'
                        }
                    }
                },
                pak_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Pak) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                punjab_qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Punjab) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                share: {
                    validators: {
                        notEmpty: {
                            message: 'Share is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                }

            }
        });
//    add_Stock_Form
    $('#add_Stock_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            herd_size: {
                validators: {
                    notEmpty: {
                        message: 'Herd size is required'
                    }
                }
            },
            pak_qty: {
                validators: {
                    notEmpty: {
                        message: 'Quantity(Pak) is required'
                    },
                    regexp: {
                        regexp: '^[0-9]*[.]?[0-9]*$',
                        message: 'Must be numbers'
                    }
                }
            },
            punjab_qty: {
                validators: {
                    notEmpty: {
                        message: 'Quantity(Punjab) is required'
                    },
                    regexp: {
                        regexp: '^[0-9]*[.]?[0-9]*$',
                        message: 'Must be numbers'
                    }
                }
            },

            source: {
                validators: {
                    notEmpty: {
                        message: 'Source is required'
                    },

                }
            }

        }
    });
    $('#edit_Stock_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            herd_size: {
                validators: {
                    notEmpty: {
                        message: 'Herd size is required'
                    }
                }
            },
            pak_qty: {
                validators: {
                    notEmpty: {
                        message: 'Quantity(Pak) is required'
                    },
                    regexp: {
                        regexp: '^[0-9]*[.]?[0-9]*$',
                        message: 'Must be numbers'
                    }
                }
            },
            punjab_qty: {
                validators: {
                    notEmpty: {
                        message: 'Quantity(Punjab) is required'
                    },
                    regexp: {
                        regexp: '^[0-9]*[.]?[0-9]*$',
                        message: 'Must be numbers'
                    }
                }
            },

            source: {
                validators: {
                    notEmpty: {
                        message: 'Source is required'
                    },

                }
            }

        }
    });
//    add_Export_Form
    $('#add_Export_Form')
        .find('[name="species_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_Export_Form').formValidation('revalidateField', 'species_id');
        })
        .end()
        .find('[name="poultry_item_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_Export_Form').formValidation('revalidateField', 'poultry_item_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                species_id: {
                    validators: {
                        notEmpty: {
                            message: 'Species is required'
                        }
                    }
                },
                poultry_item_id: {
                    validators: {
                        notEmpty: {
                            message: 'Poultry Item is required'
                        }
                    }
                },
                unity: {
                    validators: {
                        notEmpty: {
                            message: 'Unit is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },

                qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Punjab) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                value: {
                    validators: {
                        notEmpty: {
                            message: 'Value is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                },

            }
        });
    $('#edit_Export_Form')
        .find('[name="species_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_Export_Form').formValidation('revalidateField', 'species_id');
        })
        .end()
        .find('[name="poultry_item_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_Export_Form').formValidation('revalidateField', 'poultry_item_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                species_id: {
                    validators: {
                        notEmpty: {
                            message: 'Species is required'
                        }
                    }
                },
                poultry_item_id: {
                    validators: {
                        notEmpty: {
                            message: 'Poultry Item is required'
                        }
                    }
                },
                unity: {
                    validators: {
                        notEmpty: {
                            message: 'Unit is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },

                qty: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity(Punjab) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                value: {
                    validators: {
                        notEmpty: {
                            message: 'Value is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                },

            }
        });
//    add_Publication_Form

    $('#add_Publication_Form')
        .find('[name="year"]')
        .selectpicker()
        .change(function (e) {
            $('#add_Publication_Form').formValidation('revalidateField', 'year');
        })
        .end()
        .find('[name="version_no"]')
        .selectpicker()
        .change(function (e) {
            $('#add_Publication_Form').formValidation('revalidateField', 'version_no');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                title: {
                    validators: {
                        notEmpty: {
                            message: 'Title is required'
                        }
                    }
                },
                author_publisher: {
                    validators: {
                        notEmpty: {
                            message: 'Author/Publisher is required'
                        }
                    }
                },
                year: {
                    validators: {
                        notEmpty: {
                            message: 'Year is required'
                        }
                    }
                },

                version_no: {
                    validators: {
                        notEmpty: {
                            message: 'Version no is required'
                        }
                    }
                },
                value: {
                    validators: {
                        notEmpty: {
                            message: 'Value is required'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                },

            }
        });


    $('#edit_Publication_Form')
        .find('[name="year"]')
        .selectpicker()
        .change(function (e) {
            $('#edit_Publication_Form').formValidation('revalidateField', 'year');
        })
        .end()
        .find('[name="version_no"]')
        .selectpicker()
        .change(function (e) {
            $('#add_Publication_Form').formValidation('revalidateField', 'version_no');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                title: {
                    validators: {
                        notEmpty: {
                            message: 'Title is required'
                        }
                    }
                },
                author_publisher: {
                    validators: {
                        notEmpty: {
                            message: 'Author/Publisher is required'
                        }
                    }
                },
                year: {
                    validators: {
                        notEmpty: {
                            message: 'Year is required'
                        }
                    }
                },

                version_no: {
                    validators: {
                        notEmpty: {
                            message: 'Version no is required'
                        }
                    }
                },
                value: {
                    validators: {
                        notEmpty: {
                            message: 'Value is required'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Source is required'
                        },

                    }
                },

            }
        });


//add_public_form
    $('#add_public').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            designation: {
                validators: {
                    notEmpty: {
                        message: 'Designation is required'
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },

            /*    public_email: {
             validators: {
             notEmpty: {
             message: 'Email is required'
             }
             }
             },
             public_contact: {
             validators: {
             notEmpty: {
             message: 'Contact Number is required'
             }
             }
             },
             */
        }
    });
    $('#edit_public_form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            designation: {
                validators: {
                    notEmpty: {
                        message: 'Designation is required'
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },

            /*   public_email: {
             validators: {
             notEmpty: {
             message: 'Email is required'
             }
             }
             },
             public_contact: {
             validators: {
             notEmpty: {
             message: 'Contact Number is required'
             }
             }
             },*/

        }
    });


//    add_Agreement_Form
    $('#add_Agreement_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},
            link_url: {
                validators: {
                    notEmpty: {
                        message: 'PDF File is required'
                    }
                }
            },

        }
    });
    $('#edit_Agreement_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},

        }
    });
//    add_formguideline_Form
    $('#add_formguideline_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            formguideline_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            formguideline_pdf_link: {
                validators: {
                    notEmpty: {
                        message: 'Pdf Link is required'
                    }
                }
            },


        }
    });
    $('#edit_formguideline_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            formguideline_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
        }
    });
//    add_download_Form
    $('#add_download_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            download_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            download_link_url: {
                validators: {
                    notEmpty: {
                        message: 'Link Url is required'
                    }
                }
            },
            download_description: {
                validators: {
                    notEmpty: {
                        message: 'Description is required'
                    }
                }
            },


        }
    });
    $('#edit_download_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            download_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            download_description: {
                validators: {
                    notEmpty: {
                        message: 'Description is required'
                    }
                }
            },


        }
    });
//    add_useful_link_Form
    $('#add_useful_link_Form').formValidation({
        framework: 'bootstrap',
excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
useful_category: {
               validators: {
                   notEmpty: {
                       message: 'Category is required'
                   }
               }
           },
            useful_link_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            useful_link_url: {
                validators: {
                    notEmpty: {
                        message: 'Link Url is required'
                    }
                }
            }


        }
    });
    $('#edit_useful_link_Form').formValidation({
        framework: 'bootstrap',
excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
useful_category: {
               validators: {
                   notEmpty: {
                       message: 'Category is required'
                   }
               }
           },
            useful_link_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            useful_link_url: {
                validators: {
                    notEmpty: {
                        message: 'Link Url is required'
                    }
                }
            }


        }
    });
//    add_materialcategory_Form
    $('#add_materialcategory_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },

        }
    });
    $('#edit_Materialcategory_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },

        }
    });
//    add_MaterialSupport_Form
    $('#add_MaterialSupport_Form')
        .find('[name="sup_material_cat_id"]')
        .selectpicker()
        .change(function (e) {
            $('#add_MaterialSupport_Form').formValidation('revalidateField', 'sup_material_cat_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                sup_material_cat_id: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},
                pdf_link: {
                    validators: {
                        notEmpty: {
                            message: 'PDF File is required'
                        }
                    }
                },

            }
        });
    $('#edit_MaterialSupport_Form')
        .find('[name="district_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_MaterialSupport_Form').formValidation('revalidateField', 'district_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                district_id: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},

            }
        });
//    add_Survey_Form
    $('#add_Survey_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            /* link_url: {
             validators: {
             notEmpty: {
             message: 'PDF File is required'
             }
             }
             },*/

        }
    });
    $('#edit_Survey_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },


        }
    });
    //    add_vaccines_Form
    $('#add_vaccines_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            vaccines_type: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },
            vaccines_url: {
                validators: {
                    notEmpty: {
                        message: 'Image is required'
                    }
                }
            },

        }
    });
    $('#edit_vaccines_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            vaccines_type: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },


        }
    });
//    Advertisements

    $('#add_advertisements_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            advertisement_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            advertisement_image_url: {
                validators: {
                    notEmpty: {
                        message: 'Image is required'
                    }
                }
            },

        }
    });
    $('#edit_advertisements_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            advertisement_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },

        }
    });
//    add_virtual_governance_Form
    $('#add_virtual_governance_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            virtual_gov_text: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            virtual_url: {
                validators: {
                    notEmpty: {
                        message: 'Pdf file is required'
                    }
                }
            },

        }
    });
    $('#edit_virtual_gov_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            virtual_gov_text: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
        }
    })
//    addsquad_where_aboutForm

    $('#add_squad_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_squad_where_aboutForm').formValidation('revalidateField', 'squad_where_about_date');
        });
    $('#add_squad_where_aboutForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                squad_where_about_name: {
                    validators: {
                        notEmpty: {
                            message: 'Nmae is required'
                        }
                    }
                },
                squad_where_about_designation: {
                    validators: {
                        notEmpty: {
                            message: 'Designation is required'
                        }
                    }
                },
                squad_where_about_date: {
                    validators: {
                        notEmpty: {
                            message: 'Date is required'
                        }
                    }
                },
                squad_where_about: {
                    validators: {
                        notEmpty: {
                            message: 'Where about is required'
                        }
                    }
                },
                /*  squad_where_about_remarks: {
                 validators: {
                 notEmpty: {
                 message: 'Remarks is required'
                 }
                 }
                 },*/

            }
        });
    $('#edit_squad_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_squad_where_about').formValidation('revalidateField', 'squad_where_about_date');
        });

    $('#edit_squad_where_about').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            squad_where_about_name: {
                validators: {
                    notEmpty: {
                        message: 'Nmae is required'
                    }
                }
            },
            squad_where_about_designation: {
                validators: {
                    notEmpty: {
                        message: 'Designation is required'
                    }
                }
            },
            squad_where_about_date: {
                validators: {
                    notEmpty: {
                        message: 'Date is required'
                    }
                }
            },
            squad_where_about: {
                validators: {
                    notEmpty: {
                        message: 'Where about is required'
                    }
                }
            },
            /*  squad_where_about_remarks: {
             validators: {
             notEmpty: {
             message: 'Remarks is required'
             }
             }
             },*/

        }
    });
//    add_daily_activity_Form

    $('#add_daily_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_daily_activity_Form').formValidation('revalidateField', 'activity_date');
        });

    $('#add_daily_activity_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            activity_date: {
                validators: {
                    notEmpty: {
                        message: 'Activity date is required'
                    }
                }
            },
            districtwise_report_link: {
                validators: {
                    notEmpty: {
                        message: 'Districtwise report is required'
                    }
                }
            },
            /*   consolidate_report_link: {
             vaonslidators: {
             notEmpty: {
             message: 'Consolidate report is required'
             }
             }
             }*/

        }
    });

    $('#edit_daily_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_daily_activity_Form').formValidation('revalidateField', 'activity_date');
        });

    $('#edit_daily_activity_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            activity_date: {
                validators: {
                    notEmpty: {
                        message: 'Activity date is required'
                    }
                }
            },


        }
    });
//    add_OfficersContact_Form
    $('#add_OfficersContact_Form')
        .find('[name="district_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_OfficersContact_Form').formValidation('revalidateField', 'district_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                district_id: {
                    validators: {
                        notEmpty: {
                            message: 'District is required'
                        }
                    }
                },
                name_of_DLO: {
                    validators: {
                        notEmpty: {
                            message: 'Name of DLO is required'
                        }
                    }
                },
                contact_no: {
                    validators: {
                        notEmpty: {
                            message: 'Contact No is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[,]?[0-9]*[,]?[0-9]*[,]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },

            }
        });
    $('#edit_OfficerContact_Form')
        .find('[name="district_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_OfficersContact_Form').formValidation('revalidateField', 'district_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                district_id: {
                    validators: {
                        notEmpty: {
                            message: 'District is required'
                        }
                    }
                },
                name_of_DLO: {
                    validators: {
                        notEmpty: {
                            message: 'Name of DLO is required'
                        }
                    }
                },
                contact_no: {
                    validators: {
                        notEmpty: {
                            message: 'Contact No is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[,]?[0-9]*[,]?[0-9]*[,]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
            }
        });
//    staff_form


//    add_visit_activity_Form
    $('#add_visit_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_visit_activity_Form').formValidation('revalidateField', 'date');
        });

    $('#add_visit_activity_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            date: {
                validators: {
                    notEmpty: {
                        message: 'Date is required'
                    }
                }
            },
            name_of_officer: {
                validators: {
                    notEmpty: {
                        message: 'Name of Officer is required'
                    }
                }
            },
            designation: {
                validators: {
                    notEmpty: {
                        message: 'Designation is required'
                    }
                }
            },
            place_of_supervision: {
                validators: {
                    notEmpty: {
                        message: 'Place of Supervision is required'
                    }
                }
            },
            link_url: {
                validators: {
                    notEmpty: {
                        message: 'PDF File is required'
                    }
                }
            },

        }
    });

    $('#edit_visit_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_visit_Activity_Form').formValidation('revalidateField', 'visit_date');
        });

    $('#edit_visit_Activity_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            visit_date: {
                validators: {
                    notEmpty: {
                        message: 'Date is required'
                    }
                }
            },
            name_of_officer: {
                validators: {
                    notEmpty: {
                        message: 'Name of Officer is required'
                    }
                }
            },
            designation: {
                validators: {
                    notEmpty: {
                        message: 'Designation is required'
                    }
                }
            },
            place_of_supervision: {
                validators: {
                    notEmpty: {
                        message: 'Place of Supervision is required'
                    }
                }
            },


        }
    });
//    add_flood_advertisement
    $('#add_flood_advertisement').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            image_url: {
                validators: {
                    notEmpty: {
                        message: 'image is required'
                    },
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        message: 'The selected file is not valid'
                    }
                }
            }
        }
    });
    $('#edit_FloodAdvertisement_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {}
    });
//    add_station_Form
    $('#add_station_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},


        }
    });
    $('#edit_Station_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},


        }
    });
//    add_DistributionWanda_Form

    $('#add_wanda_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_DistributionWanda_Form').formValidation('revalidateField', 'date');
        });

    $('#add_DistributionWanda_Form')
        .find('[name="district_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_DistributionWanda_Form').formValidation('revalidateField', 'district_id');
        })
        .end()
        .find('[name="station_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_DistributionWanda_Form').formValidation('revalidateField', 'station_id');
        })
        .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                district_id: {
                    validators: {
                        notEmpty: {
                            message: 'District is required'
                        }
                    }
                },
                total_qty_kg: {
                    validators: {
                        notEmpty: {
                            message: 'Total Amount(KG) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                date: {
                    validators: {
                        notEmpty: {
                            message: 'Date is required'
                        }
                    }
                },
                transport_number: {
                    validators: {
                        notEmpty: {
                            message: 'Transport Number is required'
                        },
                        stringLength: {
                            max: 30,
                            message: 'maximum 30 characters long'
                        }
                    }
                },
                station_id: {
                    validators: {
                        notEmpty: {
                            message: 'Station is required'
                        }
                    }
                },
            }
        });
    //edit_WandaDistribution_Form
    $('#edit_wanda_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_WandaDistribution_Form').formValidation('revalidateField', 'date');
        });

    $('#edit_WandaDistribution_Form')
        .find('[name="district_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_WandaDistribution_Form').formValidation('revalidateField', 'district_id');
        })
        .end()
        .find('[name="station_id"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_WandaDistribution_Form').formValidation('revalidateField', 'station_id');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                district_id: {
                    validators: {
                        notEmpty: {
                            message: 'District is required'
                        }
                    }
                },
                total_qty_kg: {
                    validators: {
                        notEmpty: {
                            message: 'Total Amount(KG) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                date: {
                    validators: {
                        notEmpty: {
                            message: 'Date is required'
                        }
                    }
                },
                transport_number: {
                    validators: {
                        notEmpty: {
                            message: 'Transport Number is required'
                        },
                        stringLength: {
                            max: 30,
                            message: 'maximum 30 characters long'
                        }
                    }
                },
                station_id: {
                    validators: {
                        notEmpty: {
                            message: 'Station is required'
                        }
                    }
                },
            }
        });
//    add_new_budget_record

    $('#add_new_budget_record')
        .find('[name="from_year"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_new_budget_record').formValidation('revalidateField', 'from_year');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                from_year: {
                    validators: {
                        notEmpty: {
                            message: 'Year is required'
                        }
                    }
                },
                development_price: {
                    validators: {
                        notEmpty: {
                            message: 'Development Price(Million) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                non_development_price: {
                    validators: {
                        notEmpty: {
                            message: 'Non Development Price(Million) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },

            }
        });

    $('#edit_budget_about')
        .find('[name="from_year"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_BudgetListing_Form').formValidation('revalidateField', 'from_year');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                from_year: {
                    validators: {
                        notEmpty: {
                            message: 'Year is required'
                        }
                    }
                },
                development_price: {
                    validators: {
                        notEmpty: {
                            message: 'Development Price(Million) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },
                non_development_price: {
                    validators: {
                        notEmpty: {
                            message: 'Non Development Price(Million) is required'
                        },
                        regexp: {
                            regexp: '^[0-9]*[.]?[0-9]*$',
                            message: 'Must be numbers'
                        }
                    }
                },

            }
        });


//    add_about_us_Form
    $('#add_about_usForm').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            abt_us_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            //abt_us_description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},
            abt_us_link_url: {
                validators: {
                    notEmpty: {
                        message: 'PDF File is required'
                    }
                }
            },

        }
    });
    $('#edit_about_usForm').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            abt_us_name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            //abt_us_description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},

        }
    });
//    add_autonomous_Form
    $('#add_autonomous_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},
            link_url: {
                validators: {
                    notEmpty: {
                        message: 'Link Url is required'
                    }
                }
            },

        }
    });
    $('#edit_autonomous_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            //description: {
            //    validators: {
            //        notEmpty: {
            //            message: 'Description is required'
            //        }
            //    }
            //},


        }
    });
//    add_LawAndRule_Form

    $('#add_LawAndRule_Form')
        .find('[name="type"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#add_LawAndRule_Form').formValidation('revalidateField', 'type');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                title: {
                    validators: {
                        notEmpty: {
                            message: 'Title is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},
                file_link: {
                    validators: {
                        notEmpty: {
                            message: 'Pdf Link is required'
                        }
                    }
                },
                type: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },

            }
        });


    $('#edit_LawAndRule_Form')
        .find('[name="type"]')
        .selectpicker()
        .change(function (e) {
            /* Revalidate the language when it is changed */
            $('#edit_LawAndRule_Form').formValidation('revalidateField', 'type');
        })
        .end()

        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                title: {
                    validators: {
                        notEmpty: {
                            message: 'Title is required'
                        }
                    }
                },
                //description: {
                //    validators: {
                //        notEmpty: {
                //            message: 'Description is required'
                //        }
                //    }
                //},

                type: {
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                },

            }
        });


//    add_Secretaries_Form
    $('#add_from_Secretaries_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_Secretaries_Form').formValidation('revalidateField', 'from_date');
        });
    $('#add_to_Secretaries_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#add_Secretaries_Form').formValidation('revalidateField', 'to_date');
        });

    $('#add_Secretaries_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            from_date: {
                validators: {
                    notEmpty: {
                        message: 'Form Date is required'
                    }
                }
            },

            /* designation: {
             validators: {
             notEmpty: {
             message: 'Designation is required'
             }
             }
             },
             qualification: {
             validators: {
             notEmpty: {
             message: 'Qualification is required'
             }
             }
             },
             image_url: {
             validators: {
             notEmpty: {
             message: 'Image is required'
             }
             }
             },*/
            type: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },

        }
    });
    $('#edit_from_Secretaries_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_Secretaries_Form').formValidation('revalidateField', 'from_date');
        });
    $('#edit_to_Secretaries_datePicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#edit_Secretaries_Form').formValidation('revalidateField', 'to_date');
        });
    $('#edit_Secretaries_Form').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            from_date: {
                validators: {
                    notEmpty: {
                        message: 'Form Date is required'
                    }
                }
            },

            /*
             designation: {
             validators: {
             notEmpty: {
             message: 'Designation is required'
             }
             }
             },
             qualification: {
             validators: {
             notEmpty: {
             message: 'Qualification is required'
             }
             }
             },*/

            type: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },

        }
    });

    $('#add_Multimedia_Form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            type: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            link_url: {
                validators: {
                    notEmpty: {
                        message: 'Attachment is required'
                    }
                }
            },

        }
    });

    $('#edit_Multimedia_Form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            type: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    }
                }
            },
            link_url: {
                validators: {
                    notEmpty: {
                        message: 'Attachment is required'
                    }
                }
            },

        }
    });

    $('#upload_employee_file').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            add_employee: {
                validators: {
                    notEmpty: {
                        message: 'Attachment is required'
                    },
                    // file: {
                    //     extension: '.xlsx',
                    //     // type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    //     message: 'Please choose a .xlsx file'
                    // }
                }
            }

        }
    });


});

