-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2022 at 05:44 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `astcompk_9211compk`
--

-- --------------------------------------------------------

--
-- Table structure for table `public_info_officers`
--

CREATE TABLE `public_info_officers` (
  `id` int(11) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `name` varchar(250) NOT NULL,
  `department` varchar(100) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `public_info_officers`
--

INSERT INTO `public_info_officers` (`id`, `designation`, `name`, `department`, `email_address`, `contact_number`, `category`) VALUES
(1, 'developer', 'Massam', 'IT', 'massam@gmail.com', '03135416', 6),
(2, 'csdc', 'acas', 'cacsa', 'asda@dssd.dgdrg', '3132534', 2),
(3, 'csdc', 'hjgghhj', 'cacsa', 'asda@dssd.dgdrg', '3132534', 3),
(4, 'abc', 'acas', 'cacsa', 'asda@dssd.dgdrg', '3132534', 4),
(5, 'csdc', 'acas', 'cacsa', 'asda@dssd.dgdrg', '3132534', 5),
(6, 'abc', 'hjgghhj', 'scsd', 'asda@dssd.dgdrg', '3132534', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `public_info_officers`
--
ALTER TABLE `public_info_officers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `public_info_officers`
--
ALTER TABLE `public_info_officers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
