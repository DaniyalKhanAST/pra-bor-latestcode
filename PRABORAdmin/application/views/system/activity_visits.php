<style>
    .datepicker{z-index:9999 !important}
</style>

<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_visit_activity_btn" data-toggle="modal">Add
                    Visit Activity</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Flood Visit Activity</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Officer Name</th>
                                <th>Designation</th>
                                <th>Place Of Supervision</th>
                                <th>PDF Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($visit_activities)) {
                                $i = 1;
                                foreach ($visit_activities as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->date ?></td>
                                        <td><?= $row->name_of_officer ?></td>
                                        <td><?= $row->designation ?></td>
                                        <td><?= $row->place_of_supervision ?></td>
                                        <td><a href="<?= $row->link_url ?>">View</a></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_visit_activities" data-toggle="modal"
                                                           data-visit_id="<?= $row->id ?>"
                                                           data-visit_date="<?= $row->date ?>"
                                                           data-visit_designation="<?= $row->designation ?>"
                                                           data-visit_officer="<?= $row->name_of_officer ?>"
                                                           data-visit_place_supervision="<?= $row->place_of_supervision ?>"
                                                           data-visit_link_url="<?= $row->link_url ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_flood_activity_visits?visit_id=<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addVisitActivityForm').modal('toggle');
                                    $('#add_visit_activity_Form')[0].reset();
                                    $('#add_visit_activity_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editVisitActivityForm').modal('toggle');
                                    $('#edit_visit_Activity_Form')[0].reset();
                                    $('#edit_visit_Activity_Form').data('formValidation').resetForm();
                                });

                            });
                            $(".add_visit_activity_btn").click(function () {
                                $('#addVisitActivityForm').modal('show');
                            });
                            $(".edit_visit_activities").click(function () {
                                $("#visit_id_hidden").val($(this).data('visit_id'));
                                $("#visit_date").val($(this).data('visit_date'));
                                $("#visit_designation").val($(this).data('visit_designation'));
                                $("#visit_officer").val($(this).data('visit_officer'));
                                $("#visit_place_supervision").val($(this).data('visit_place_supervision'));
                                $("#old_visit_link_url").val($(this).data('visit_link_url'));
                                $('#editVisitActivityForm').modal('show');
                            });

                        </script>
                        <script>
                            $(document).ready(function () {
                                $('.datepicker').datepicker({
                                    format: 'yyyy-mm-dd',
                                    autoclose: true
                                });
                            });
                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>