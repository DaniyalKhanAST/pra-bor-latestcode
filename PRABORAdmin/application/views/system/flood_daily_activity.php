<style>
    .datepicker{z-index:9999 !important}
</style>

<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_daily_activity_btn" data-toggle="modal">Add
                    Daily Activity</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Flood Daily Activities</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Activity Date</th>
                                <th>DistrictWise Report</th>
                                <th>Consolidate Report</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($daily_activity)) {
                                $i = 1;
                                foreach ($daily_activity as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->activity_date ?></td>
                                        <td><a href="<?= $row->districtwise_report_link; ?>">View</a></td>
                                        <td><?php if(!empty($row->consolidate_report_link)){ ?><a href="<?= $row->consolidate_report_link; ?>">View</a> <?php } ?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_daily_activity" data-toggle="modal"
                                                           data-daily_id="<?= $row->id ?>"
                                                           data-daily_activity_date="<?= $row->activity_date ?>"
                                                           data-daily_districtwise="<?= $row->districtwise_report_link ?>"
                                                           data-daily_consolidate="<?= $row->consolidate_report_link ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_daily_activity?activity_id=<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#addDailyActivityForm').modal('toggle');
                                    $('#add_daily_activity_Form')[0].reset();
                                    $('#add_daily_activity_Form').data('formValidation').resetForm();
                                });

                                $('.edit_clearform').click(function () {
                                    $('#editDailyActivityForm').modal('toggle');
                                    $('#edit_daily_activity_Form')[0].reset();
                                    $('#edit_daily_activity_Form').data('formValidation').resetForm();
                                });
                            });
                            $(".add_daily_activity_btn").click(function () {
                                $('#addDailyActivityForm').modal('show');
                            });
                            $(".edit_daily_activity").click(function () {
                                $("#daily_activity_id").val($(this).data('daily_id'));
                                $("#daily_activity_date").val($(this).data('daily_activity_date'));
                                $("#daily_districtwise").val($(this).data('daily_districtwise'));
                                $("#daily_consolidate").val($(this).data('daily_consolidate'));
                                $('#editDailyActivityForm').modal('show');
                            });

                        </script>
                        <script>
                            $(document).ready(function () {
                                $('.datepicker').datepicker({
                                    format: 'yyyy-mm-dd',
                                    autoclose: true
                                });
                            });
                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>