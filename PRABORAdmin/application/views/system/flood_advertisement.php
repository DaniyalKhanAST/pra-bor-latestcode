<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_advertisement_btn" data-toggle="modal">Add
                    Advertisement</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Flood Advertisement</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($advertisement)) {
                                $i = 1;
                                foreach ($advertisement as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><img src="<?= $row->image_url ?>" style="width: 100px; height: 100px" ></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_advertisement" data-toggle="modal"
                                                           data-advertisement_id="<?= $row->id ?>"
                                                           data-advertisement_image_url="<?= $row->image_url ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_flood_advertisement?advertisement_id=<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#addFloodAdvertisementForm').modal('toggle');
                                    $('#add_flood_advertisement')[0].reset();
                                    $('#add_flood_advertisement').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editFloodAdvertisementForm').modal('toggle');
                                    $('#edit_FloodAdvertisementForm_Form')[0].reset();
                                    $('#edit_FloodAdvertisementForm_Form').data('formValidation').resetForm();
                                });
                            });
                            $(".add_advertisement_btn").click(function () {
                                $('#addFloodAdvertisementForm').modal('show');
                            });
                            $(".edit_advertisement").click(function () {
                                $("#flood_advertisement_id_hidden").val($(this).data('advertisement_id'));
                                $("#old_flood_advertisement_image_url").val($(this).data('advertisement_image_url'));
                                $("#flood_advertisement_image_url").attr('src', $(this).data('advertisement_image_url'));
                                $('#editFloodAdvertisementForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>