<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_officer_contact_btn" data-toggle="modal">Add
                    Officers Contact</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Flood Officers Contact</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <section class="boxpadding">
                        <div class="table-box">
                            <table class="display table example" id="">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>District</th>
                                    <th>Name Of DLO</th>
                                    <th>Contact No</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($officers_contact)) {
                                    $k = 1;
                                    foreach ($officers_contact as $row) {
                                            ?>
                                            <tr class="">
                                                <td><?= $k ?></td>
                                                <td><?= $row->district_name; ?></td>
                                                <td><?= $row->name_of_DLO; ?></td>
                                                <td><?= $row->contact_no ?></td>
                                                <td class="btn-group" style="width: 100px">
                                                    <div class="dropdown">
                                                        <button
                                                            class="btn btn-primary dropdown-toggle"
                                                            type="button" data-toggle="dropdown">
                                                            Action
                                                            <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href=""
                                                                   class="edit_officers_contact"
                                                                   data-toggle="modal"
                                                                   data-id="<?= $row->id ?>"
                                                                   data-officer_district_id="<?= $row->district_id ?>"
                                                                   data-officer_dlo="<?= $row->name_of_DLO ?>"
                                                                   data-officer_contact_no="<?= $row->contact_no ?>"
                                                                   value="Edit">Edit</a></li>
                                                            <li>
                                                                <a href="<?= base_url() ?>index.php/system/delete_officer_contact?contact_id=<?= $row->id ?>"
                                                                   class=""
                                                                   onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>

                                            </tr>
                                            <?php

                                        $k++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </section>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, $("tfoot input").index(this));
        });


        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });

        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });

        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

</script>
<script>
    $(".add_officer_contact_btn").click(function () {
        $('#addOfficerContactForm').modal('show');
    });
    $(".edit_officers_contact").click(function () {
        $("#officer_id_hidden").val($(this).data('id'));
        $("#updateofficer_district_id").val($(this).data('officer_district_id'));
        $("#updateofficer_district_id").selectpicker('refresh');
        $("#officer_dlo_updated").val($(this).data('officer_dlo'));
        $("#updated_officer_contact_no").val($(this).data('officer_contact_no'));
        $('#editOfficerContactForm').modal('show');
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });

        $('.clearform').click(function () {
            $('#addOfficerContactForm').modal('toggle');
            $('#add_OfficersContact_Form')[0].reset();
            $('#add_OfficersContact_Form').data('formValidation').resetForm();
        });

        $('.edit_clearform').click(function () {
            $('#editOfficerContactForm').modal('toggle');
            $('#edit_OfficerContact_Form')[0].reset();
            $('#edit_OfficerContact_Form').data('formValidation').resetForm();
        });
    });
</script>