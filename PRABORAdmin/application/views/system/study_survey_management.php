<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_survey_btn" data-toggle="modal">Add
                    Survey</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Study & Survey Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>PDF Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($agreements)) {
                                $i = 1;
                                foreach ($agreements as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td><a href="<?= $row->link_url ?>">View</a></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_survey" data-toggle="modal"
                                                           data-survey_id="<?= $row->id ?>"
                                                           data-surey_name="<?= $row->name ?>"
                                                           data-survey_link_url="<?= $row->link_url ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_study_survey?survey_id=<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addSurveyForm').modal('toggle');
                                    $('#add_Survey_Form')[0].reset();
                                    $('#add_Survey_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editSurveyForm').modal('toggle');
                                    $('#edit_Survey_Form')[0].reset();
                                    $('#edit_Survey_Form').data('formValidation').resetForm();
                                });

                            });
                            $(".add_survey_btn").click(function () {
                                $('#addSurveyForm').modal('show');
                            });
                            $(".edit_survey").click(function () {
                                $("#survey_id_hidden").val($(this).data('survey_id'));
                                $("#survey_name").val($(this).data('surey_name'));
                                $("#old_survey_pdf_link").val($(this).data('survey_link_url'));
                                $('#editSurveyForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>