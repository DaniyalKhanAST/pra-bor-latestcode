<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_useful_link_btn" data-toggle="modal">Add
                    Useful Link</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Useful Link Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Category</th>
                                <th>Name</th>
                                <th>Link Url</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($useful_link)) {
                                $i = 1;
                                foreach ($useful_link as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?php if($row->category == 1){echo "Academics";} elseif($row->category == 2){echo "Industrial";}elseif($row->category == 3){echo "National";}elseif($row->category == 4){echo "International";}elseif($row->category == 5){echo "Miscellaneous & Articles";} ?></td>
                                        <td><?= $row->name ?></td>
                                        <td><?= $row->link_url ?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" type="submit" class="edit_useful_link"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-useful_link_name="<?= $row->name ?>"
                                                           data-useful_link_url="<?= $row->link_url ?>" data-cat_id="<?= $row->category?>" value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_useful_link/<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#add_useful_link').modal('toggle');
                                    $('#add_useful_link_Form')[0].reset();
                                    $('#add_useful_link_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#edit_useful_linkForm').modal('toggle');
                                    $('#edit_useful_link_Form')[0].reset();
                                    $('#edit_useful_link_Form').data('formValidation').resetForm();
                                });

                                $(".add_useful_link_btn").click(function () {
                                    $('#add_useful_link').modal('show');
                                });
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>