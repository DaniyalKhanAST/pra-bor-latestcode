<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_publication_btn" data-toggle="modal">Add
                    Publication</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Publication Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Author/Publisher</th>
                                <th>Year</th>
                                <th>version_no</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($publications)) {
                                $i = 1;
                                foreach ($publications as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->title ?></td>
                                        <td><?= $row->author_publisher ?></td>
                                        <td><?= $row->year ?></td>
                                        <td><?php if ($row->version_no == 1) {
                                                echo "New";
                                            } else {
                                                echo "Old";
                                            } ?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_publications"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-pub_title="<?= $row->title ?>"
                                                           data-author_publisher="<?= $row->author_publisher ?> "
                                                           data-pub_year="<?= $row->year ?> "
                                                           data-version_no="<?= $row->version_no ?> "
                                                           value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_publications?publication_id=<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addPublicationForm').modal('toggle');
                                    $('#add_Publication_Form')[0].reset();
                                    $('#add_Publication_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editPublicationForm').modal('toggle');
                                    $('#edit_Publication_Form')[0].reset();
                                    $('#edit_Publication_Form').data('formValidation').resetForm();
                                });
                            });
                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>
    $(".add_publication_btn").click(function () {
        $('#addPublicationForm').modal('show');
    });
    $(".edit_publications").click(function () {
        $("#publication_id").val($(this).data('id'));
        $("#pub_title").val($(this).data('pub_title'));
        $("#author_publisher").val($(this).data('author_publisher'));
        $("#publish_year").val($(this).data('pub_year'));
        $("#publish_year").selectpicker('refresh');
        $("#updatedpubversion_no").val($(this).data('version_no'));
        $("#updatedpubversion_no").selectpicker('refresh');
        $('#editPublicationForm').modal('show');
    });

</script>
