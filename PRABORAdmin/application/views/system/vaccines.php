<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_vaccines_btn" data-toggle="modal">Add
                    Vaccines</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Vaccines Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($vaccines)) {
                                $i = 1;
                                foreach ($vaccines as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?php if ($row->type == 0) {
                                                echo "Price";
                                            } else {
                                                echo "Schedule";
                                            } ?></td>
                                        <td><img src="<?= $row->image_link ?>" height="100px" width="100px"></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a type="submit" class="edit_vaccines"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-upvaccines_type="<?= $row->type ?>"
                                                           data-vaccines_url="<?= $row->image_link ?>"
                                                           value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_vaccines/<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addvaccinesForm').modal('toggle');
                                    $('#add_vaccines_Form')[0].reset();
                                    $('#add_vaccines_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#edit_vaccinesForm').modal('toggle');
                                    $('#edit_vaccines_Form')[0].reset();
                                    $('#edit_vaccines_Form').data('formValidation').resetForm();
                                });

                                $(".add_vaccines_btn").click(function () {
                                    $('#addvaccinesForm').modal('show');
                                });
                                $(".edit_vaccines").click(function () {
                                    $("#vaccines_id").val($(this).data('id'));
                                    $("#updatedvaccines_type").val($(this).data('upvaccines_type'));
                                    $("#updatedvaccines_type").selectpicker('refresh');
                                    $("#old_vaccines_url").val($(this).data('vaccines_url'));
                                    $("#selected_vaccine_image").attr('src', $(this).data('vaccines_url'));
                                    $('#edit_vaccinesForm').modal('show');
                                });
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>