<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_aboutus_record_btn" data-toggle="modal">Add
                    Aboutus Record</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">About Us Listing</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>name</th>
                                <th>Description</th>
                                <th>File</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($aboutus_listing)) {
                                $i = 1;
                                foreach ($aboutus_listing as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name; ?></td>
                                        <td><?= $row->description; ?></td>
                                        <td><?php if(!empty($row->link_url)){ ?><a href="<?= $row->link_url ?>" >View</a> <?php } ?></td>
                                        <td><?php if($row->type == 1){echo"Overview";}elseif($row->type == 2){echo"Background";}else{echo"Policies";}  ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class=" edit_aboutus_listing_detail"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-aboutus_name="<?= $row->name ?>"
                                                           data-aboutus_description="<?= $row->description ?>"
                                                           data-aboutus_file_url="<?= $row->link_url ?>"
                                                           data-aboutus_type="<?= $row->type ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_about_us?aboutus_id=<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $(document).on('click','.clearform',function(){
                                    $('#addAboutUsForm').modal('toggle');
                                    $('#add_aboutus_Form')[0].reset();
                                    $('#add_aboutus_Form').data('formValidation').resetForm();
                                });
                                $(document).on('click','.edit_clearform',function(){
                                    $('#editAboutUsForm').modal('toggle');
                                    $('#edit_AboutUs_Form')[0].reset();
                                    $('#edit_AboutUs_Form').data('formValidation').resetForm();
                                });

                                $(document).on('click','.add_aboutus_record_btn',function(){
                                    $('#addAboutUsForm').modal('show');
                                });
                                //        squad where about ////
                                $(document).on('click','.edit_aboutus_listing_detail',function(){
                                    $("#aboutus_id").val($(this).data('id'));
                                    $("#aboutus_name").val($(this).data('aboutus_name'));
                                    $("#aboutus_description").val($(this).data('aboutus_description'));
                                    $("#aboutus_type").val($(this).data('aboutus_type'));
                                    $("#aboutus_type").selectpicker('refresh');
                                    $("#old_aboutus_file_url").val($(this).data('aboutus_file_url'));
                                    $("#aboutus_file_url").attr('src', $(this).data('aboutus_file_url'));
                                    $('#editAboutUsForm').modal('show');
                                });
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>