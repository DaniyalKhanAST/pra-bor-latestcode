<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_budget_record_btn" data-toggle="modal">Add
                    Budget Record</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Budget Listing</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Year</th>
                                <th>Development Price</th>
                                <th>Non Developmeent Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($budget_listing)) {
                                $i = 1;
                                foreach ($budget_listing as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->from_year.'-'.$row->to_year; ?></td>
                                        <td><?= $row->development_price ?></td>
                                        <td><?= $row->non_development_price ?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a type="submit" class=" edit_budget_listing_detail"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-budget_from_year="<?= $row->from_year ?>"
                                                           data-budget_to_year="<?= $row->to_year ?>"
                                                           data-budget_development_price="<?= $row->development_price ?>"
                                                           data-budget_non_development_price="<?= $row->non_development_price ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_budget_record?budget_id=<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addaboutus_budgetForm').modal('toggle');
                                    $('#add_new_budget_record')[0].reset();
                                    $('#add_new_budget_record').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editBudgetListingForm').modal('toggle');
                                    $('#edit_budget_about')[0].reset();
                                    $('#edit_budget_about').data('formValidation').resetForm();
                                });
                                $(".add_budget_record_btn").click(function () {
                                    $('#addaboutus_budgetForm').modal('show');
                                });
                                //        squad where about ////
                                $(".edit_budget_listing_detail").click(function () {
                                    $("#budget_id").val($(this).data('id'));
                                    $("#budget_from_year").val($(this).data('budget_from_year'));
                                    $("#budget_from_year").selectpicker('refresh');
                                    $("#budget_to_year").val($(this).data('budget_to_year'));
                                    $("#budget_to_year").selectpicker('refresh');
                                    $("#budget_development_price").val($(this).data('budget_development_price'));
                                    $("#budget_non_development_price").val($(this).data('budget_non_development_price'));
                                    $('#editBudgetListingForm').modal('show');
                                });
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>