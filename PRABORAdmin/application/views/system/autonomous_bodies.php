<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_autonomous_bodies_btn" data-toggle="modal">Add
                    autonomous Record</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Autonomous Bodies</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Link Url</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($autonomous_bodies)) {
                                $i = 1;
                                foreach ($autonomous_bodies as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td><?= $row->description ?></td>
                                        <td><?= $row->link_url ?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a type="submit" class="edit_autonomous_link"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-autonomous_name="<?= $row->name ?>"
                                                           data-autonomous_description="<?= $row->description ?>"
                                                           data-autonomous_link_url="<?= $row->link_url ?>" value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_autonomous_bodies?autonomous_id=<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#AddAutonomousBodies').modal('toggle');
                                    $('#add_autonomous_Form')[0].reset();
                                    $('#add_autonomous_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editAutonomousForm').modal('toggle');
                                    $('#edit_autonomous_Form')[0].reset();
                                    $('#edit_autonomous_Form').data('formValidation').resetForm();
                                });
                                $(".add_autonomous_bodies_btn").click(function () {
                                    $('#AddAutonomousBodies').modal('show');
                                });
                                $(".edit_autonomous_link").click(function () {
                                    $("#autonomous_id").val($(this).data('id'));
                                    $("#autonomous_name").val($(this).data('autonomous_name'));
                                    $("#autonomous_description").val($(this).data('autonomous_description'));
                                    $("#autonomous_link_url").val($(this).data('autonomous_link_url'));
                                    $('#editAutonomousForm').modal('show');
                                });
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>