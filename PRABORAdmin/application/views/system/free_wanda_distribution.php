<style>
    .datepicker{z-index:9999 !important}
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_DistributionWanda_btn" data-toggle="modal">Add
                    Record</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Free Wanda Distribution</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <section class="boxpadding">
                        <div class="table-box">
                            <table class="display table example" id="">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>District</th>
                                    <th>Amount(KG)</th>
                                    <th>Date</th>
                                    <th>Transport</th>
                                    <th>Station</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($distributions)) {
                                    $k = 1;
                                    foreach ($distributions as $row) {
                                            ?>
                                            <tr class="">
                                                <td><?= $k ?></td>
                                                <td><?= $row->district_name; ?></td>
                                                <td><?= $row->total_qty_kg; ?></td>
                                                <td><?= $row->date; ?></td>
                                                <td><?= $row->transport_number; ?></td>
                                                <td><?= $row->station_name ?></td>
                                                <td class="btn-group" style="width: 100px">
                                                    <div class="dropdown">
                                                        <button
                                                            class="btn btn-primary dropdown-toggle"
                                                            type="button" data-toggle="dropdown">
                                                            Action
                                                            <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href=""
                                                                   class="edit_free_distribution"
                                                                   data-toggle="modal"
                                                                   data-id="<?= $row->id ?>"
                                                                   data-distribution_district_id="<?= $row->district_id ?>"
                                                                   data-distribution_station_id="<?= $row->station_id ?>"
                                                                   data-distribution_qty="<?= $row->total_qty_kg ?>"
                                                                   data-distribution_transport_number="<?= $row->transport_number ?>"
                                                                   data-date="<?= $row->date ?>"
                                                                   value="Edit">Edit</a></li>
                                                            <li>
                                                                <a href="<?= base_url() ?>index.php/system/delete_free_wanda_distribution?distribution_id=<?= $row->id ?>"
                                                                   class=""
                                                                   onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>

                                            </tr>
                                            <?php

                                        $k++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </section>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, $("tfoot input").index(this));
        });


        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });

        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });

        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

</script>
<script>
    $(".add_DistributionWanda_btn").click(function () {
        $('#addDistributionWandaForm').modal('show');
    });
    $(".edit_free_distribution").click(function () {
        $("#distribution_id_hidden").val($(this).data('id'));
        $("#distribution_district_id").val($(this).data('distribution_district_id'));
        $("#distribution_district_id").selectpicker('refresh');
        $("#distribution_station_id").val($(this).data('distribution_station_id'));
        $("#distribution_station_id").selectpicker('refresh');
        $("#distribution_qty").val($(this).data('distribution_qty'));
        $("#distribution_transport_number").val($(this).data('distribution_transport_number'));
        $("#distribution_date").val($(this).data('date'));
        $('#editDistributionWandaForm').modal('show');
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
        $('.clearform').click(function () {
            $('#addDistributionWandaForm').modal('toggle');
            $('#add_DistributionWanda_Form')[0].reset();
            $('#add_DistributionWanda_Form').data('formValidation').resetForm();
        });
        $('.edit_clearform').click(function () {
            $('#editDistributionWandaForm').modal('toggle');
            $('#edit_WandaDistribution_Form')[0].reset();
            $('#edit_WandaDistribution_Form').data('formValidation').resetForm();
        });

    });
</script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>
