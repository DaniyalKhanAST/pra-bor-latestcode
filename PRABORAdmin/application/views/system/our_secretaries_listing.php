
<style>
    .datepicker{z-index:9999 !important}
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_secretaries_record_btn" data-toggle="modal">Add
                    Secretary Record</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Secretaries Listing</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>name</th>
                                <th>Year</th>
                                <th>Image</th>
                                <th>Designation</th>
                                <th>Qualification</th>
                                <th>Core Member</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($secretaries_listing)) {
                                $i = 1;
                                foreach ($secretaries_listing as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name; ?></td>
                                        <td><?= $row->from_date.'-'.$row->to_date; ?></td>
                                        <td><img src="<?= $row->image_url ?>" style="height: 100px; width: 100px;"> </td>
                                        <td><?= $row->designation ?></td>
                                        <td><?= $row->qualification ?></td>
                                        <td><?php if($row->type == 0){}else{echo"Core Member";}  ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class=" edit_secretaries_listing_detail"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-secretaries_name="<?= $row->name ?>"
                                                           data-secretaries_from_year="<?= $row->from_date ?>"
                                                           data-secretaries_to_year="<?= $row->to_date ?>"
                                                           data-secretaries_image_url="<?= $row->image_url ?>"
                                                           data-secretaries_designation="<?= $row->designation ?>"
                                                           data-secretaries_qualification="<?= $row->qualification ?>"
                                                           data-secretaries_type="<?= $row->type ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_secretaries_record?secretary_id=<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addSecretariesForm').modal('toggle');
                                    $('#add_Secretaries_Form')[0].reset();
                                    $('#add_Secretaries_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editSecretariesForm').modal('toggle');
                                    $('#edit_Secretaries_Form')[0].reset();
                                    $('#edit_Secretaries_Form').data('formValidation').resetForm();
                                });

                                $(".add_secretaries_record_btn").click(function () {
                                    $('#addSecretariesForm').modal('show');
                                });
                                //        squad where about ////

                                $(".edit_secretaries_listing_detail").click(function () {
                                    $("#secretaries_id").val($(this).data('id'));
                                    $("#secretaries_name").val($(this).data('secretaries_name'));
                                    $("#secretaries_from_year").val($(this).data('secretaries_from_year'));
                                    $("#secretaries_to_year").val($(this).data('secretaries_to_year'));
                                    $("#secretaries_designation").val($(this).data('secretaries_designation'));
                                    $("#secretaries_qualification").val($(this).data('secretaries_qualification'));
                                    $("#secretaries_type").val($(this).data('secretaries_type'));
                                    $("#secretaries_type").selectpicker('refresh');
                                    $("#old_secretaries_image_url").val($(this).data('secretaries_image_url'));
                                    $("#secretaries_image_url").attr('src', $(this).data('secretaries_image_url'));
                                    $('#editSecretariesForm').modal('show');
                                });
                            });

                        </script>
                        <script>
                            $(document).ready(function () {
                                $('.datepicker').datepicker({
                                    format: 'yyyy-mm-dd',
                                    autoclose: true
                                });
                            });
                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>