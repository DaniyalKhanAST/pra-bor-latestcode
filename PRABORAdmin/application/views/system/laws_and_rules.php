<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_lawsrules_btn" data-toggle="modal">Add
                    Record</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Laws/Rules</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>File Link</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($laws_and_rules)) {
                                $i = 1;
                                foreach ($laws_and_rules as $row) {

                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->title ?></td>
                                        <td><?= $row->description ?></td>
                                        <td><a href="<?= $row->file_link ?>">View</a></td>
                                        <td><?php if ($row->type == 0) {
                                                echo "Law";
                                            } else {
                                                echo "Rule";
                                            } ?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_law_and_rule" data-toggle="modal"
                                                           data-id="<?= $row->id ?>"
                                                           data-lawrule_description="<?= $row->description ?>"
                                                           data-lawrule_title="<?= $row->title ?>"
                                                           data-lawrule_type="<?= $row->type ?>"
                                                           data-lawrule_file_link="<?= $row->file_link ?>" value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/system/delete_laws_and_rules?law_rule_id=<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });
        $('.clearform').click(function () {
            $('#addLawAndRule').modal('toggle');
            $('#add_LawAndRule_Form')[0].reset();
            $('#add_LawAndRule_Form').data('formValidation').resetForm();
        });
        $(document).on('click', '.edit_clearform', function () {
            $('#editLawAndRuleForm').modal('toggle');
            $('#edit_LawAndRule_Form')[0].reset();
            $('#edit_LawAndRule_Form').data('formValidation').resetForm();
        });

        $(".add_lawsrules_btn").click(function () {
            $('#addLawAndRule').modal('show');
        });
        $(document).on('click', '.edit_law_and_rule', function () {
            $("#lawrule_id").val($(this).data('id'));
            $("#lawrule_description").val($(this).data('lawrule_description'));
            $("#lawrule_title").val($(this).data('lawrule_title'));
            $("#lawrule_type").val($(this).data('lawrule_type'));
            $("#lawrule_type").selectpicker('refresh');
            $("#lawrule_file_link").val($(this).data('lawrule_file_link'));
            $('#editLawAndRuleForm').modal('show');
        });
    });

</script>