<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_material_btn" data-toggle="modal">Add
                    Supportive Material</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Supportive Material Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <section class="boxpadding">
                        <div class="table-box">
                            <table class="display table example" id="">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Pdf Link</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($materials)) {
                                    $k = 1;
                                    foreach ($materials as $row) { ?>
                                        <tr class="">
                                            <td><?= $k ?></td>
                                            <td><?php if (!empty($material_cats)) {
                                                    foreach ($material_cats as $row_data) {
                                                        if ($row->sup_material_cat_id == $row_data->id) {
                                                            echo $row_data->name;
                                                        }
                                                    }
                                                } ?>

                                            </td>
                                            <td><?= $row->description; ?></td>
                                            <td><a href="<?= $row->link_url ?>">View</a></td>
                                            <td class="btn-group" style="">
                                                <div class="dropdown">
                                                    <button
                                                        class="btn btn-primary dropdown-toggle"
                                                        type="button" data-toggle="dropdown">
                                                        Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href=""
                                                               class="edit_material_supportive"
                                                               data-toggle="modal"
                                                               data-id="<?= $row->id ?>"
                                                               data-material_description="<?= $row->description ?>"
                                                               data-material_cat="<?= $row->sup_material_cat_id ?>"
                                                               data-material_link_url="<?= $row->link_url ?>"
                                                               value="Edit">Edit</a></li>
                                                        <li>
                                                            <a href="<?= base_url() ?>index.php/system/delete_material_supportive?material_id=<?= $row->id ?>"
                                                               class=""
                                                               onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>

                                        </tr>
                                        <?php

                                        $k++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </section>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, $("tfoot input").index(this));
        });


        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });

        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });

        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

</script>
<script>
    $(".add_material_btn").click(function () {
        $('#addMaterialSupportForm').modal('show');
    });
    $(".edit_material_supportive").click(function () {
        $("#material_id_hidden").val($(this).data('id'));
        $("#material_name_id").val($(this).data('material_description'));
        $("#material_cat_id_updated").val($(this).data('material_cat'));
        $("#material_cat_id_updated").selectpicker('refresh');
        $("#old_pdf_link_supp").val($(this).data('material_link_url'));
        $('#editMaterialForm').modal('show');
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
        $('.clearform').click(function () {
            $('#addMaterialSupportForm').modal('toggle');
            $('#add_MaterialSupport_Form')[0].reset();
            $('#add_MaterialSupport_Form').data('formValidation').resetForm();
        });
        $('.edit_clearform').click(function () {
            $('#editMaterialForm').modal('toggle');
            $('#edit_MaterialSupport_Form')[0].reset();
            $('#edit_MaterialSupport_Form').data('formValidation').resetForm();
        });
    });
</script>