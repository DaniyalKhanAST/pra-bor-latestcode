<style>
    .siteHeading3{
        color: #346276;
        font-size: 1.25rem;
        font-weight: 700;
        line-height: 1;
    }
</style>
<div class="container-liquid">
    <div class="col-md-12">
        <header>
            <h1 class="page-title">Edit <?= $page->page_title ?> Page</h1>
        </header>

        <div class="contents">
            <form id="edit_item" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Enter URL : </label>

                        <div class="col-sm-8">
                            <input name="page_url" type="text" class="form-control" value="<?=$page->page_url?>"/>
                            <label class="" style="font-weight: bold;">(Either add URL or Description)</label>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div> 

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Enter Tooltip : </label>

                        <div class="col-sm-8">
                            <input name="page_tooltip" type="text"  class="form-control" value="<?=$page->page_tooltip?>"/>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Description : </label>

                        <div class="col-sm-8">
                            <textarea class="form-control" id="texteditorfroala"
                                      name="description" ><?= $page->page_detail ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="form-group">
                        <input type="submit" class="btn btn-success pull-right" name="edit_page" value="Save Changes">
                    </div>
                </div>
                <div class="clearfix"></div>

            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {


        $(function() {
            var changeDirection = function (dir, align) {
                this.selection.save();
                var elements = this.selection.blocks();
                for (var i = 0; i < elements.length; i++) {
                    var element = elements[i];
                    if (element != this.$el.get(0)) {
                        $(element)
                            .css('direction', dir)
                            .css('text-align', align);
                    }
                }

                this.selection.restore();
            };

            $.FroalaEditor.DefineIcon('rightToLeft', {NAME: 'long-arrow-left'});
            $.FroalaEditor.RegisterCommand('rightToLeft', {
                title: 'Direction RTL',
                focus: true,
                undo: true,
                refreshAfterCallback: true,
                callback: function () {
                    changeDirection.apply(this, ['rtl', 'right']);
                }
            });

            $.FroalaEditor.DefineIcon('leftToRight', {NAME: 'long-arrow-right'});
            $.FroalaEditor.RegisterCommand('leftToRight', {
                title: 'Direction LTR',
                focus: true,
                undo: true,
                refreshAfterCallback: true,
                callback: function () {
                    changeDirection.apply(this, ['ltr', 'left']);
                }
            });
            $.FroalaEditor.DefineIcon('accordion', {NAME: 'list-alt'});
            $.FroalaEditor.RegisterCommand('accordion', {
                title: 'Insert Accordion',
                focus: true,
                undo: true,
                refreshAfterCallback: true,
                callback: function () {
                    this.html.insert('<h2 class="toggle-element"> </h2>');
                }
            });

            $('#texteditorfroala').froalaEditor({
                heightMin: 300,
                imageUploadParam: 'image_url',
                imageUploadURL: '<?= base_url(); ?>' + 'index.php/system/upload_editor_image',
                imageUploadMethod: 'POST',
                imageMaxSize: 1024 * 1024 * 5,
                imageAllowedTypes: ['jpeg', 'jpg', 'png'],
                fileUploadParam: 'file_url',
                fileUploadURL: '<?= base_url(); ?>' + 'index.php/system/upload_editor_file',
                fileUploadMethod: 'POST',
                fileMaxSize: 1024 * 1024 * 30,
                fileAllowedTypes: ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'],
                tabSpaces: 4,
                fontFamilySelection: true,
                fontSizeSelection: true,
                paragraphFormatSelection: true,
                pastePlain: true,
                linkList: [
                    {
                        text: 'LivestockPunjab',
                        href: 'http://livestockpunjab.gov.pk/',
                        target: '_blank'
                    },
                    {
                        text: 'VRI Lahore',
                        href: 'http://livestockpunjab.gov.pk/page/pages/vri_lahore',
                        target: '_blank'
                    },
                    {
                        text: 'Director Planning and Evaluation',
                        href: 'http://livestockpunjab.gov.pk/page/pages/director_planning_and_evaluation',
                        target: '_blank'
                    },
                    {
                        text: 'Project Directorate of Diagnostic Labs Punjab',
                        href: 'http://livestockpunjab.gov.pk/page/pages/disease_diagnostic_lab',
                        target: '_blank'
                    },
                    {
                        text: 'Director Livestock Farms',
                        href: 'http://livestockpunjab.gov.pk/page/pages/director_livestock_farms',
                        target: '_blank'
                    },
                    {
                        text: 'Divisional Directors Livestock',
                        href: 'http://livestockpunjab.gov.pk/page/pages/divisional_directors_livestock',
                        target: '_blank'
                    },
                    {
                        text: 'Director Communication and Extension',
                        href: 'http://livestockpunjab.gov.pk/page/pages/director_communication_and_extension',
                        target: '_blank'
                    }
                ],
                toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo', '|', 'leftToRight', 'rightToLeft', 'accordion']
            });
        });

    });

</script>