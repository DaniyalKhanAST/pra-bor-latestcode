<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <?php if (!empty($meta_pages)): ?>
                    Select Page: <select name="new_page" id="new_page" class="selectpicker" data-live-search="true" style="padding:10px;">
                        <?php foreach ($meta_pages as $row): ?>
                            <option value="<?= $row->slug ?>" <?= ($row->slug == $meta_page->slug) ? "Selected" : ""; ?> > <?= $row->page_title ?> </option>
                        <?php endforeach; ?>
                    </select>
                <?php endif; ?>
            </div>
            <div class="pull-right"><a href="<?= base_url() . 'index.php/page/add_item/' . $meta_page->id ?>" class="btn btn-success">Add <?= $meta_page->page_title ?></a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading"><?= $meta_page->page_title ?> Listing</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="listing_page_dt">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Images</th>
                                <th>Files</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($items)) {
                                $i = 1;
                                foreach ($items as $row) {

                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->title; ?></td>
                                        <td><?= substr(strip_tags($row->description), 0, 200); ?></td>


                                        <td>
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Images
                                                    <span class="caret"></span></button>

                                                <ul class="dropdown-menu">
                                                    <?php $count = 0; ?>
                                                    <?php if (isset($row->images) && !empty($row->images)) {
                                                        foreach ($row->images as $img): ?>
                                                            <?php $tempimg=explode("/",$img->image_url);
                                                            $image_name=$tempimg[count($tempimg)-1];?>

                                                            <li><a href="<?= $img->image_url ?>" target="_blank"> <?= $image_name ?> </a></li>
                                                        <?php endforeach;
                                                    } ?>

                                                </ul>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Attachments
                                                    <span class="caret"></span></button>

                                                <ul class="dropdown-menu">
                                                    <?php $count = 0; ?>
                                                    <?php if (isset($row->files) && !empty($row->files)) {
                                                        foreach ($row->files as $file): ?>
                                                            <?php $tempfile=explode("/",$file->file_link);
                                                            $file_name=$tempfile[count($tempfile)-1];?>
                                                            <li><a href="<?= $file->file_link ?>" target="_blank">  <?= $file_name; ?> </a></li>
                                                        <?php endforeach;
                                                    } ?>

                                                </ul>

                                            </div>
                                        </td>

                                        <td>
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?= base_url() ?>index.php/page/edit_item/<?= $row->id ?>">Edit</a></li>

                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/page/delete_item/<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#listing_page_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });
    });

    $(document).on('change', '#new_page', function () {
        var item_id = $(this).val();
        window.location.replace("<?=base_url()?>page/page_listing/" + item_id);
    });
</script>