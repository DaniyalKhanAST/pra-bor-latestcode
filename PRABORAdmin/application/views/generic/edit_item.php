<style>
    .input-group-lg > .form-control, .input-group-lg > .input-group-addon, .input-group-lg > .input-group-btn > .btn {
        height: 60px !important;
        padding: 5px 20px 0px 25px !important;
    }

    .glow {
        color: red;
    }
</style>
<div class="container-liquid">
    <div class="col-md-12">
        <header>
            <h1 class="page-title">Edit <?=$meta_page->page_title?></h1>
        </header>
        <div class="contents">
            <form id="edit_item" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Title</label>

                        <div class="col-sm-8">
                            <input name="title" value="<?= $item->title ?>" type="text" required class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Description</label>

                        <div class="col-sm-8">
                            <textarea id="texteditorfroala" class="form-control" name="description"><?= $item->description ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Replace File</label>

                        <div class="col-sm-2">
                            <input name="file_link[]" type="file" multiple class=""/>
                        </div>
                        <div class="panel_body files_view">
                            <?php if ( !empty($item_files)) { ?>
                                <div class="col-sm-4">
                                    <ul>
                                        <?php foreach($item_files as $file):?>
                                            <?php $file_name=explode('/',$file->file_link);?>
                                            <div class="">
                                                <li  style="margin:5px;">
                                                    <a href="<?=$file->file_link?>" target="_blank"><?= $file_name[count($file_name)-1]; ?></a>
                                                    <span class="close2 delete_file"
                                                          style="right:18px;position:absolute;background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;cursor:pointer;"
                                                          id="<?= $item->id."_".$file->id ?>">x</span>
                                                </li>

                                            </div>
                                        <?php endforeach;?>
                                    </ul>

                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Image Gallery</label>

                        <div class="col-sm-8">
                            <div class="panel-body">
                                <input id="input-6" name="image_url[]" type="file" multiple>

                                <div id="dropzone_previews_here">
                                    <?php
                                    if (!empty($item_images)) {
                                        foreach ($item_images as $image) { ?>

                                            <div class="col-sm-4" style="margin-top: 10px;">
                                                <div class="ImageEdit">
                                                    <img src="<?= $image->image_url ?>" style="height: 120px; width: 165px;">
                                                    <span class="close2 delete_p_image"
                                                          style="right:18px;position:absolute;background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;cursor:pointer;"
                                                          id="<?= $image->id ?>">x</span>

                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <br>
                                <br>

                                <div class="clearfix"></div>
                                <div class="col-lg-12 pull-right" style="margin-top: 10px" align="right">
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary pull-right" name="edit_item" value="Edit <?=$meta_page->page_title?>">
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="form-group"></div>
                    </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#texteditorfroala').froalaEditor({
            heightMin: 300,
            imageUploadParam: 'image_url',
            imageUploadURL: '<?= base_url(); ?>'+'system/upload_editor_image',
            imageUploadMethod: 'POST',
            imageMaxSize: 1024 * 1024,
            imageAllowedTypes: ['jpeg', 'jpg', 'png']
        });

        $('#dropzone_previews_here').on('click', '.delete_p_image', function (e) {

            if (confirm("Want to delete this Image?")) {
                $('#dropzone_previews_here').empty();
                var p_banner_id = $(this).attr('id');
                $data = 'p_banner_id=' + p_banner_id;
                $.ajax({
                    url: '<?= base_url() ?>page/delete_item_images/' +  <?php echo $item->id; ?>,
                    data: $data,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data != null) {
                            $('#dropzone_previews_here').append(data);

                        }
                    }
                });
            }
        });

        //$r_d_biologics

        $(document).on('click', '.delete_file', function (e) {

            if (confirm("Want to delete this File?")) {
                $('.files_view').empty();
                var p_banner_id = $(this).attr('id');
                p_banner_id = p_banner_id.split('_');
                p_banner_id = p_banner_id[1];
                $data = 'p_banner_id=' + p_banner_id;
                $.ajax({
                    url: '<?= base_url() ?>page/delete_item_files/' +  <?php echo $item->id; ?>,
                    data: $data,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data != null) {
                            $('.files_view').append(data);

                        }
                    }
                });
            }
        });
    });
</script>