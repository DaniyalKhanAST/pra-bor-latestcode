<?php
$user = $this->ion_auth->get_users_groups()->row();
$group_id = $user->id;
?>
<style>
    #nav2 li {
        cursor: pointer;
    }
</style>
<aside class="sidebar">
    <div class="">
        <header>
            <div class="logo">
                <h3 style="margin-top: 0px; color: white;">BOR Admin Panel</h3>
            </div>
            <a href="#" class="togglemenu">&nbsp;</a>

            <div class="clearfix"></div>
        </header>
        <nav class="navigation">
            <ul class="navi-acc" id="nav2">
                <li class="<?php if ($file_name == 'dashboard') echo 'active'; ?>">
                    <a href="<?= base_url() ?>index.php/dashboard" class="dashboard menu_dispaly">Dashboard</a>
                </li>
                <li class="<?php if ($file_name == 'main_slider_management') echo 'active'; ?>"><a class="dashboard menu_dispaly"
                                href="<?= base_url() ?>index.php/dashboard/main_slider_management">Main Slider</a></li>
                <li class="<?php if ($file_name == 'slider_management') echo 'active'; ?>"><a class="dashboard menu_dispaly"
                                href="<?= base_url() ?>index.php/dashboard/slider_management">Slider Management</a></li>
                <li class="<?php if ($file_name == 'quick_overview' || $file_name == 'services_at_glance' || $file_name == 'organogram' || $file_name == 'corporate_governance' 
                        || $file_name == 'future_plans' || $file_name == 'leadership' ||  $file_name == 'our_budget' || $file_name == 'our_partners' 
                        ||  $file_name == 'our_team' || $file_name == 'board_of_directors' || $file_name == 'bod_committees' || $file_name == 'corporate_social_responsibility'  
                        || $file_name == 'audit_reports' || $file_name == 'company_policies') echo 'active'; ?>">
                        <a class="charts menu_dispaly">About Us</a>
                        <ul class=" menu_tab <?php if ($file_name == 'quick_overview' || $file_name == 'services_at_glance' || $file_name == 'corporate_governance' 
                                || $file_name == 'organogram' || $file_name == 'future_plans' || $file_name == 'leadership' ||  $file_name == 'our_budget' 
                                || $file_name == 'our_partners'  || $file_name == 'our_team' || $file_name == 'board_of_directors' || $file_name == 'bod_committees' 
                                || $file_name == 'corporate_social_responsibility' || $file_name == 'audit_reports' || $file_name == 'company_policies'
                                ) echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'quick_overview') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/quick_overview">Overview</a>
                        </li>
                        <li class="<?php if ($file_name == 'future_plans') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/future_plans">Future Plans</a>
                        </li>
                        <li class="<?php if ($file_name == 'our_team') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/our_team">Our Team</a>
                        </li>
                        <li class="<?php if ($file_name == 'organogram') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/organogram">Organogram</a>
                        </li>
                        <li class="<?php if ($file_name == 'leadership') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/leadership">Leadership</a>
                        </li>
                    </ul>
                </li>
                
                <li class="<?php if ($file_name == 'registration_form' || $file_name == 'code_of_conduct' || $file_name == 'helpline') echo 'active'; ?>">
                    <a class="charts menu_dispaly">Course Registration</a>
                    <ul class=" menu_tab <?php if ($file_name == 'registration_form' || $file_name == 'code_of_conduct' || $file_name == 'helpline') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'registration_form') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/registration_form">Registration Form</a>
                        </li>
                        <li class="<?php if ($file_name == 'code_of_conduct') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/code_of_conduct">Code of Conduct</a>
                        </li>
                        <li class="<?php if ($file_name == 'helpline') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/helpline">Helpline</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($file_name == 'post_induction_trainings' || $file_name == 'demand_based_trainings' 
                        || $file_name == 'short_courses' || $file_name == 'e_learning' || $file_name == 'hands_on_training' || $file_name == 'field_trainings' 
                        || $file_name == 'course_designing' || $file_name == 'content_development' || $file_name == 'toolkits' || $file_name == 'examinations' 
                        || $file_name == 'workshops' || $file_name == 'symposia' || $file_name == 'domain_support' || $file_name == 'r_n_d') echo 'active'; ?>">
                    <a class="charts menu_dispaly">We Offer</a>
                    <ul class=" menu_tab <?php if ($file_name == 'post_induction_trainings' || $file_name == 'demand_based_trainings' 
                        || $file_name == 'short_courses' || $file_name == 'e_learning' || $file_name == 'hands_on_training' || $file_name == 'field_trainings' 
                        || $file_name == 'course_designing' || $file_name == 'content_development' || $file_name == 'toolkits' || $file_name == 'examinations' 
                        || $file_name == 'workshops' || $file_name == 'symposia' || $file_name == 'domain_support' || $file_name == 'r_n_d') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'post_induction_trainings') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/post_induction_trainings">Post Induction Trainings</a>
                        </li>
                        <li class="<?php if ($file_name == 'demand_based_trainings') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/demand_based_trainings">Demand Based Trainings</a>
                        </li>
                        <li class="<?php if ($file_name == 'short_courses') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/short_courses">Short Courses</a>
                        </li>
                        <li class="<?php if ($file_name == 'e_learning') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/e_learning">E-Learning</a>
                        </li>
                        <li class="<?php if ($file_name == 'hands_on_training') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/hands_on_training">Practical/ Hands-on Training</a>
                        </li>
                        <li class="<?php if ($file_name == 'field_trainings') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/field_trainings">Field Trainings</a>
                        </li>
                        <li class="<?php if ($file_name == 'course_designing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/course_designing">Course Designing</a>
                        </li>
                        <li class="<?php if ($file_name == 'content_development') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/content_development">Content Development</a>
                        </li>
                        <li class="<?php if ($file_name == 'toolkits') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/toolkits">Toolkits</a>
                        </li>
                        <li class="<?php if ($file_name == 'examinations') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/examinations">Examinations</a>
                        </li>
                        <li class="<?php if ($file_name == 'workshops') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/workshops">Workshops</a>
                        </li>
                        <li class="<?php if ($file_name == 'symposia') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/symposia">Symposia</a>
                        </li>
                        <li class="<?php if ($file_name == 'domain_support') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/domain_support">Domain Support</a>
                        </li>
                        <li class="<?php if ($file_name == 'r_n_d') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/r_n_d">R&D</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($file_name == 'main_campus' || $file_name == 'virtual_campus' || $file_name == 'satellite_campuses' || $file_name == 'libraries' || $file_name == 'hostel_facilities') echo 'active'; ?>">
                    <a class="charts menu_dispaly">Our Campuses</a>
                    <ul class=" menu_tab <?php if ($file_name == 'main_campus' || $file_name == 'virtual_campus' || $file_name == 'satellite_campuses' || $file_name == 'libraries' || $file_name == 'hostel_facilities') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'main_campus') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/main_campus">Main Campus</a>
                        </li>
                        <li class="<?php if ($file_name == 'virtual_campus') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/virtual_campus">Virtual Campus</a>
                        </li>
                        <li class="<?php if ($file_name == 'satellite_campuses') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/satellite_campuses">Satellite Campuses</a>
                        </li>
                        <li class="<?php if ($file_name == 'libraries') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/libraries">Libraries</a>
                        </li>
                        <li class="<?php if ($file_name == 'hostel_facilities') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/hostel_facilities">Hostel Facilities</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($file_name == 'publication_listing' || $file_name == 'press_release' || $file_name == 'policies' || $file_name == 'laws' 
                                || $file_name == 'rules' || $file_name == 'manuals' || $file_name == 'notifications' 
                                || $file_name == 'events_listing'
                                || $file_name == 'useful_links' 
                                || $file_name == 'faqs' || $file_name == 'public_info_officers') echo 'active'; ?>">
                    <a class="charts menu_dispaly">Info Desk</a>
                    <ul class=" menu_tab <?php if ($file_name == 'publication_listing' || $file_name == 'press_release' || $file_name == 'policies' || $file_name == 'laws' 
                                || $file_name == 'rules' || $file_name == 'manuals' || $file_name == 'notifications' 
                                || $file_name == 'events_listing'
                                || $file_name == 'useful_links' 
                                || $file_name == 'faqs' || $file_name == 'public_info_officers') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'publication_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/system/publication_management">Other Publications</a>
                        </li>
                        <li class="<?php if ($file_name == 'press_release') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/press_release">News/Events</a>
                        </li>
                        <li class="<?php if ($file_name == 'policies') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/policies" >Policies</a>
                        </li>
                        <li class="<?php if ($file_name == 'laws') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/laws">Laws</a>
                        </li>
                        <li class="<?php if ($file_name == 'rules') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/rules">Rules</a>
                        </li>
                        <li class="<?php if ($file_name == 'manuals') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/manuals_listing">Manuals</a>
                        </li>
                        <li class="<?php if ($file_name == 'notifications') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/notifications">Notifications</a>
                        </li>
                        <li class="<?php if ($file_name == 'events_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/events_listing">Gallery</a>
                        </li>
                        <li class="<?php if ($file_name == 'useful_links') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/useful_links ">Useful Links</a>
                        </li>
                        <li class="<?php if ($file_name == 'faqs') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/faqs">FAQs</a>
                        </li>
                        <li class="<?php if ($file_name == 'public_info_officers') echo 'active'; ?>">
                                <a href="<?= base_url() ?>index.php/dashboard/public_info">Contacts</a>
                        </li>
                    </ul>
                </li>

                <li class="<?php if ($file_name == 'twitter' || $file_name == 'facebook' || $file_name == 'instagram'
                        || $file_name == 'blogs' || $file_name == 'youtube' || $file_name == 'helpline' || $file_name == 'chief_minister' || $file_name == 'minister' || $file_name == 'smbr' 
                        || $file_name == 'collaborations' || $file_name == 'public_info_officers' || $file_name == 'site_map' || $file_name == 'faqs' || $file_name == 'download_careers'
                        || $file_name == 'copy_right'  || $file_name == 'career_jobs' || $file_name == 'collaborations'
                        || $file_name == 'public_notifications' || $file_name == 'our_hr' || $file_name == 'vacant_posts_jobs' || $file_name == 'locum' || $file_name == 'volunteering'
                        || $file_name == 'promotions' || $file_name == 'walk_in_interviews' || $file_name == 'general' || $file_name == 'appointments_notifications'
                        || $file_name == 'conclusion_termination_contracts' || $file_name == 'transfers_postings' || $file_name == 'experience_certificates' || $file_name == 'good'
                        || $file_name == 'average' || $file_name == 'poor' || $file_name == 'download_career_jobs' || $file_name == 'tender_documents' || $file_name == 'procurement_plans' || $file_name == 'evaluation_reports') 
                        echo 'active'; ?>">
                    <a class="charts menu_dispaly">FOOTER MANAGEMENT</a>
                    <ul class=" menu_tab <?php if ($file_name == 'twitter' || $file_name == 'facebook' || $file_name == 'instagram'
                        || $file_name == 'blogs' || $file_name == 'youtube' || $file_name == 'helpline' || $file_name == 'chief_minister' || $file_name == 'minister' || $file_name == 'smbr' 
                        || $file_name == 'collaborations' || $file_name == 'public_info_officers' || $file_name == 'site_map' || $file_name == 'faqs' || $file_name == 'download_careers'
                        || $file_name == 'copy_right'  || $file_name == 'career_jobs' || $file_name == 'collaborations'
                        || $file_name == 'public_notifications' || $file_name == 'our_hr' || $file_name == 'vacant_posts_jobs' || $file_name == 'locum' || $file_name == 'volunteering'
                        || $file_name == 'promotions' || $file_name == 'walk_in_interviews' || $file_name == 'general' || $file_name == 'appointments_notifications'
                        || $file_name == 'conclusion_termination_contracts' || $file_name == 'transfers_postings' || $file_name == 'experience_certificates' || $file_name == 'good'
                        || $file_name == 'average' || $file_name == 'poor' || $file_name == 'download_career_jobs' || $file_name == 'tender_documents' || $file_name == 'procurement_plans' || $file_name == 'evaluation_reports') 
                        echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'twitter') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/twitter">Twitter</a>
                        </li>
                        <li class="<?php if ($file_name == 'facebook') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/facebook">Facebook</a>
                        </li>
                        <li class="<?php if ($file_name == 'instagram') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/instagram">Instagram</a></li>
                        <li class="<?php if ($file_name == 'blogs') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/blogs">Blogs</a>
                        </li>

                        
                        <li class="<?php if ($file_name == 'youtube') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/youtube">YouTube</a>
                        </li>
                        <li class="<?php if ($file_name == 'helpline') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/helpline">Helpline</a>
                        </li>
                        
                        <li class="<?php if ($file_name == 'chief_minister') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/chief_minister">Chief Minister</a>
                        </li>
                        <li class="<?php if ($file_name == 'minister') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/minister">Minister</a>
                        </li>
                        <li class="<?php if ($file_name == 'smbr') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/smbr">SMBR</a>
                        </li>
                        <li class="<?php if ($file_name == 'collaborations') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/collaborations">Our Partners</a>
                        </li>
                        
                        <li class="<?php if ($file_name == 'public_info_officers') echo 'active'; ?>">
                                <a href="<?= base_url() ?>index.php/dashboard/public_info">Contact Us</a>
                        </li>
                        <li class="<?php if ( $file_name == 'our_hr' || $file_name == 'vacant_posts_jobs' || $file_name == 'locum' || $file_name == 'volunteering'
                        || $file_name == 'promotions' || $file_name == 'walk_in_interviews' || $file_name == 'general' || $file_name == 'appointments_notifications'
                        || $file_name == 'conclusion_termination_contracts' || $file_name == 'transfers_postings' || $file_name == 'experience_certificates' || $file_name == 'good'
                        || $file_name == 'average' || $file_name == 'poor' || $file_name == 'download_career_jobs') echo 'active'; ?>">
                        <a class="menu_dispaly">Careers/Jobs</a>
                        <ul class=" menu_tab <?php if ($file_name == 'our_hr' || $file_name == 'vacant_posts_jobs' || $file_name == 'locum' || $file_name == 'volunteering'
                                || $file_name == 'promotions' || $file_name == 'walk_in_interviews' || $file_name == 'general' || $file_name == 'appointments_notifications'
                                || $file_name == 'conclusion_termination_contracts' || $file_name == 'transfers_postings' || $file_name == 'experience_certificates' || $file_name == 'good'
                                || $file_name == 'average' || $file_name == 'poor' || $file_name == 'download_career_jobs') echo 'active'; ?>">
                                <li class="<?php if ($file_name == 'our_hr') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/our_hr">Our HR</a></li>
                                <li class="<?php if ($file_name == 'vacant_posts_jobs') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/vacant_posts_jobs">Vacant Posts/ Jobs</a>
                                </li>
                                <li class="<?php if ($file_name == 'locum') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/locum">Locum</a>
                                </li>
                                <li class="<?php if ($file_name == 'volunteering') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/volunteering">Volunteering</a>
                                </li>

                                <li class="<?php if ($file_name == 'promotions') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/promotions">Promotions</a>
                                </li>
                                <li class="<?php if ($file_name == 'walk_in_interviews') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/walk_in_interviews">Merit Lists (Walk-in Interviews)</a>
                                </li>

                                <li class="<?php if ($file_name == 'general') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/general">Merit Lists (General)</a>
                                </li>
                                <li class="<?php if ($file_name == 'appointments_notifications') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/appointments_notifications">Appointments Notifications</a>
                                </li>

                                <li class="<?php if ($file_name == 'transfers_postings') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/transfers_postings">Transfers/ Postings</a>
                                </li>
                                <li class="<?php if ($file_name == 'conclusion_termination_contracts') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/page/static_page/conclusion_termination_contracts">Conclusion/ Termination of Contracts</a>
                                </li>
                                <li class="<?php if ( $file_name == 'good' || $file_name == 'average' || $file_name == 'poor') echo 'active'; ?>">
                                        <a class=" menu_dispaly">Experience Certificates</a>
                                        <ul class=" menu_tab <?php if ($file_name == 'good' || $file_name == 'average' || $file_name == 'poor') echo 'active'; ?>">
                                                <li class="<?php if ($file_name == 'good') echo 'active'; ?>"><a
                                                        href="<?= base_url() ?>index.php/page/static_page/good">Good</a></li>
                                                <li class="<?php if ($file_name == 'average') echo 'active'; ?>"><a
                                                        href="<?= base_url() ?>index.php/page/static_page/average">Average</a>
                                                </li>
                                                <li class="<?php if ($file_name == 'poor') echo 'active'; ?>"><a
                                                        href="<?= base_url() ?>index.php/page/static_page/poor">Poor</a>
                                                </li>
                                        </ul>
                                </li>
                                <li class="<?php if ($file_name == 'download_career_jobs') echo 'active'; ?>"><a
                                        href="<?= base_url() ?>index.php/dashboard/download_career_jobs" >Downloads Careers/Jobs</a>
                                </li>
                        </ul>
                        </li>
                        <li class="<?php if ( $file_name == 'tender_documents' || $file_name == 'procurement_plans' || $file_name == 'evaluation_reports') echo 'active'; ?>">
                    <a class="menu_dispaly">Tenders</a>
                    <ul class=" menu_tab <?php if ( $file_name == 'tender_documents' || $file_name == 'procurement_plans' || $file_name == 'evaluation_reports') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'tender_documents') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/auction_tender_listing">Tender Documents</a>
                        </li>
                        <li class="<?php if ($file_name == 'procurement_plans') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/procurement_plan_listing">Procurement Plans</a>
                        </li>
                        <li class="<?php if ($file_name == 'evaluation_reports') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/reports_management">Evaluation Reports</a>
                        </li>
                    </ul>
                </li>
                        <li class="<?php if ($file_name == 'site_map') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/site_map">Site Map</a></li>
                        <li class="<?php if ($file_name == 'faqs') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/faqs">FAQs</a>
                        </li>
                        <li class="<?php if ($file_name == 'download_careers') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/download_careers">Downloads</a>
                        </li>
                        <li class="<?php if ($file_name == 'copy_right') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/footer_page/copy_right">Copyrigt</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($file_name == 'marital_status_listing') echo 'active'; ?>">
                    <a class="charts menu_dispaly">Reference Management</a>
                    <ul class=" menu_tab <?php if ($file_name == 'marital_status_listing' || $file_name == 'programs_listing' || $file_name == 'offices_listing' || $file_name == 'qualifications_listing' || $file_name == 'relations_listing' || $file_name =='designations_listing' || $file_name == 'directorates_listing') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'marital_status_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/marital_status_management">Marital Status</a>
                        </li>
                        <li class="<?php if ($file_name == 'programs_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/programs_management">Programs</a>
                        </li>
                        <li class="<?php if ($file_name == 'offices_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/offices_management">Offices</a>
                        </li>
                        <li class="<?php if ($file_name == 'qualifications_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/qualifications_management">Qualifications</a>
                        </li>
                        <li class="<?php if ($file_name == 'relations_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/relations_management">Relations</a>
                        </li>
                        <li class="<?php if ($file_name == 'designations_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/designations_management">Designations</a>
                        </li>
                        <li class="<?php if ($file_name == 'directorates_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/dashboard/directorates_management">Directorates</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($file_name == 'employee_management') echo 'active'; ?>"><a class="dashboard menu_dispaly"
                                href="<?= base_url() ?>index.php/dashboard/employee_listing">Employee Management</a></li>
                <li class="<?php if ($file_name == 'employee_listing' ) echo 'active'; ?>">
                    <a class="charts menu_dispaly">MIS</a>
                    <ul class=" menu_tab <?php if ($file_name == 'employee_listing') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'employee_listing') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/employee">List of Employees</a>
                        </li>
                    </ul>
                </li>

                <li class="<?php if ($file_name == 'examination_procedure' ) echo 'active'; ?>">
                    <a class="charts menu_dispaly">Slider Static Page Management</a>
                    <ul class=" menu_tab <?php if ($file_name == 'examination_procedure') echo 'active'; ?>">
                        <li class="<?php if ($file_name == 'examination_procedure') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/examination_procedure">Examination Procedure</a>
                        </li>
                        <li class="<?php if ($file_name == '9211_virtual_governance') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/9211_virtual_governance">V-Governance</a>
                        </li>
                        <li class="<?php if ($file_name == 'social_audit') echo 'active'; ?>"><a
                                href="<?= base_url() ?>index.php/page/static_page/social_audit">Social Audit</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </nav>
        <span class="shadows"></span>
    </div>

</aside>
<script>
    $(document).on('click', '.menu_dispaly', function (e) {
        $('.menu_tab').removeClass('active');
    });

</script>