<link href="<?=base_url() ?>assets/css/toastr.css" rel="stylesheet" media="screen" />
<link href="<?= base_url() ?>assets/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/formValidation.css"/>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/datepicker.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/datepicker3.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link href="<?=base_url() ?>assets/css/style.css" rel="stylesheet" media="screen" />
<link href="<?=base_url() ?>assets/css/bootstrapp.css" rel="stylesheet" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-fileupload/fileinput.css"/>
<link href="<?= base_url();?>assets/js/dropzone/css/dropzone.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/froala/froala_editor.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/froala/froala_style.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/froala/code_view.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/froala/colors.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/froala/fullscreen.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/froala/table.css" />

<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.js"></script>