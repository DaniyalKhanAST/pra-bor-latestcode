<!-- SHOW TOASTR NOTIFIVATION -->
<?php if ($this->session->flashdata('success') != ""): ?>

    <script type="text/javascript">
        $(document).ready(function () {
            toastr.success('<?php echo $this->session->flashdata("success"); ?>');
        });
    </script>
<?php endif; ?>
<?php if ($this->session->flashdata('error') != ""): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            toastr.error('<?php echo $this->session->flashdata("error"); ?>');
        });
    </script>
<?php endif; ?> 

<script>
    $(document).ready(function () {
        $('#textedit').wysihtml5();
        $('.textedit').wysihtml5();
    });
    $('.datepicker').datepicker({
        format: 'YYYY-MM-DD'
    });
</script>
