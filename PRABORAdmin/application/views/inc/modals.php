<?php if ($file_name == 'banner_management') { ?>

    <div id="addBannerForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_banner" id="add_baner" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/banner_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Banner</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Banner Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="banner_url"
                                               name="banner_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>

                                <div class="col-sm-6 skin skin-flat">
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled-checked" checked
                                               name="status" value="1">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="square-radio-disabled-checked">Active</label>
                                    </div>
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled" name="status"
                                               value="0">
                                    </div>
                                    <div class="col-sm-1 ">
                                        <label for="square-radio-disabled">InActive</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_banner" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'marital_status_listing') { ?>

<div id="addMaritalStatusForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/marital_status_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Marital Status</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="add_new_marital_status" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editMaritalStatusForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/marital_status_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Marital Status</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" id="marital_status_name" class="form-control"/>
                            </div>
                            <input type="hidden" name="marital_status_id" id="marital_status_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform"  data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="edit_marital_status" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>

<?php if ($file_name == 'programs_listing') { ?>

<div id="addProgramForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/programs_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Program</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="add_new_program" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editProgramForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/programs_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Program</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" id="program_name" class="form-control"/>
                            </div>
                            <input type="hidden" name="program_id" id="program_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform"  data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="edit_program" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>

<?php if ($file_name == 'offices_listing') { ?>

<div id="addOfficeForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/offices_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Office</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="add_new_office" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editOfficeForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/offices_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Office</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" id="office_name" class="form-control"/>
                            </div>
                            <input type="hidden" name="office_id" id="office_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform"  data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="edit_office" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>


<?php if ($file_name == 'qualifications_listing') { ?>

<div id="addQualificationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/qualifications_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Qualification</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="add_new_qualification" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editQualificationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/qualifications_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Qualification</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" id="qualification_name" class="form-control"/>
                            </div>
                            <input type="hidden" name="qualification_id" id="qualification_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform"  data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="edit_qualification" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>


<?php if ($file_name == 'relations_listing') { ?>

<div id="addRelationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/relations_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Relation</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="add_new_relation" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editRelationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/relations_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Relation</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" id="relation_name" class="form-control"/>
                            </div>
                            <input type="hidden" name="relation_id" id="relation_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform"  data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="edit_relation" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>


<?php if ($file_name == 'designations_listing') { ?>

<div id="addDesignationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/designations_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Designation</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="add_new_designation" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editDesignationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/designations_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Designation</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" id="designation_name" class="form-control"/>
                            </div>
                            <input type="hidden" name="designation_id" id="designation_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform"  data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="edit_designation" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>


<?php if ($file_name == 'directorates_listing') { ?>

<div id="addDirectorateForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/directorates_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Directorate</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="add_new_directorate" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editDirectorateForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form  class="name_field_validate" method="POST"
                  action="<?= base_url() ?>index.php/dashboard/directorates_management"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Directorate</h4>
                </div>
                <div class="modal-body col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2" style="font-weight: bold;">Title</label>

                            <div class="col-sm-6">
                                <input name="name" type="text" id="directorate_name" class="form-control"/>
                            </div>
                            <input type="hidden" name="directorate_id" id="directorate_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default clearform"  data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="edit_directorate" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>



<?php if ($file_name == 'main_slider_management' || $file_name =='slider_management') { ?>

    <div id="" class="modal fade addSliderBannerForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_slider_banner" id="add_slider_banner" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/<?= $file_name?>"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Slider Image</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="title" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Priority</label>

                                <div class="col-sm-6">
                                    <input name="priority" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <?php if($file_name =='slider_management'): ?>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Select Slider Number</label>

                                <div class="col-sm-6">
                                    <select class="form-control" name="slider_number">
                                        <option value="1" selected>Slider 1</option>
                                        <option value="2">Slider 2</option>
                                        <option value="3">Slider 3</option>
                                        <option value="4">Slider 4</option>
                                        <option value="5">Slider 5</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <?php endif; ?>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                            <input type="file" id="banner_url" name="banner_url">
                                        </span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_slider_img" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="addSliderBannerPriorityForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="setSliderPriorityForm" id="add_slider_priority" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/main_slider_priority"
                      enctype="multipart/form-data">
                    <input type="hidden" id="main_slider_id_priority"  name="main_slider_id" value="">
                    <input type="hidden" id="priority"  name="priority_redirect" value="<?=$file_name;?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Set Main Slider Priority</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Priority</label>

                                <div class="col-sm-6">
                                    <input name="priority" id="main_slider_priority_value" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="set_slider_priority" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php } ?>

<?php if ($file_name == 'organogram_management') { ?>

    <div id="addorganogramForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_organogram" id="add_baner" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/organogram_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New organogram</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="category" class="form-control "
                                            title="Select Directorate">
                                        <option value="">Select Type</option>
                                        <option value="1">Secretariat</option>
                                        <option value="2">DG Extension</option>
                                        <option value="3">DG Research</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">organogram Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="organogram_url"
                                               name="organogram_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>

                                <div class="col-sm-6 skin skin-flat">
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled-checked" checked
                                               name="status" value="1">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="square-radio-disabled-checked">Active</label>
                                    </div>
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled" name="status"
                                               value="0">
                                    </div>
                                    <div class="col-sm-1 ">
                                        <label for="square-radio-disabled">InActive</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_organogram" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'section_statistics_management') { ?>

    <div id="addsection_statisticsForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_section_statistics" id="add_baner" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/section_statistics_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Section Statistics</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;"> Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="section_statistics_url"
                                               name="section_statistics_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>

                                <div class="col-sm-6 skin skin-flat">
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled-checked" checked
                                               name="status" value="1">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="square-radio-disabled-checked">Active</label>
                                    </div>
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled" name="status"
                                               value="0">
                                    </div>
                                    <div class="col-sm-1 ">
                                        <label for="square-radio-disabled">InActive</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_section_statistics" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'development_management') { ?>

    <div id="adddevelopmentForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_development" id="add_dev" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/development_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add </h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="category" class="form-control dev_cat"
                                            title="Select Directorate">
                                        <option value="">Select Type</option>
                                        <option value="1">Key Initiatives</option>
                                        <option value="2">Livestock Cholistan</option>
                                        <option value="3">The Provincial Budget</option>
                                        <option value="4">Special Package (Kisan Package)</option>
                                        <option value="5">Development Data Catalogue</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;"> Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="development_url"
                                               name="development_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>

                                <div class="col-sm-6 skin skin-flat">
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled-checked" checked
                                               name="status" value="1">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="square-radio-disabled-checked">Active</label>
                                    </div>
                                    <div class="col-sm-1 ">
                                        <input tabindex="11" type="radio" id="square-radio-disabled" name="status"
                                               value="0">
                                    </div>
                                    <div class="col-sm-1 ">
                                        <label for="square-radio-disabled">InActive</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default clearform" id="" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_development" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
    <script>
        $("[name='category']").on('change', function () {
         if($('.dev_cat option:selected').val()==2){
         var select  = $(this).closest('.row').clone();
         select.find('label').html("Chlistan Category");
         select.find('.dev_cat option').remove();
         select.find('.dev_cat').append(new Option( "Select Type",""));
         select.find('.dev_cat').append(new Option( "A BIRD EYE VIEW OF CHOLISTAN","1"));
         /*select.find('.dev_cat').append(new Option( "Test2","2"));
         select.find('.dev_cat').append(new Option( "Test3","3"));
         select.find('.dev_cat').append(new Option( "Test4","4"));*/
         select.find('.dev_cat').attr('name','cholistan_cat');
         select.find('.dev_cat').attr('class','form-control cho_cat');
         select.insertAfter($(this).closest('.row'));

         }else{
         $('.cho_cat').closest(".row").remove();
         }
         });


    </script>

<?php if ($file_name == 'auc_ten_listing') { ?>
    <div id="addAuctionForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_auction_form" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/auction_tender_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Tender</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="type" class="form-control selectpicker"
                                            title="Select Directorate">
                                        <option value="">Select Type</option>
                                        <?php
                                        if (!empty($auction_type)) {
                                            foreach ($auction_type as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Closing Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="add_auction_datePicker">
                                        <input type="text" class="form-control" name="closing_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Advertisement</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Bid Document</label>

                                <div class="col-sm-6">
                                    <input name="bid_doc" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Department</label>

                                <div class="col-sm-6">
                                    <select name="directorate_id" class="form-control selectpicker"
                                            title="Select Directorate">
                                        <option value="">Select Department</option>
                                        <?php
                                        if (!empty($directorates)) {
                                            foreach ($directorates as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-1 ">
                                        <input type="radio" class="bluecheckradios" checked
                                               name="status" value="1">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="">Open</label>
                                    </div>
                                    <div class="col-sm-1 ">
                                        <input type="radio" class="bluecheckradios" name="status"
                                               value="0">
                                    </div>
                                    <div class="col-sm-1 ">
                                        <label for="">Close</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_auc" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editAuctionForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_auction_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Tender</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="auc_id" type="hidden" class="form-control" id="auc_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="type" class="form-control selectpicker" id="auc_type_id"
                                            title="Select Directorate">
                                        <option value="">Select Type</option>
                                        <?php
                                        if (!empty($auction_type)) {
                                            foreach ($auction_type as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control" id="auc_name"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Closing Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="edit_auction_datePicker">
                                        <input type="text" class="form-control" id="auc_closing_date" name="closing_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_pdf_link" type="text" readonly id="old_auc_pdf_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" id="auc_pdf_link" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Bid Document</label>

                                <div class="col-sm-6">
                                    <input name="bid_doc" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Department</label>

                                <div class="col-sm-6">
                                    <select name="directorate_id" id="auc_directorate_id" class="form-control selectpicker"
                                            title="Select Directorate">
                                        <option value="">Select Department</option>
                                        <?php
                                        if (!empty($directorates)) {
                                            foreach ($directorates as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-1 ">
                                        <input type="radio" id="auc_open_status" class="bluecheckradios"
                                               name="status" value="1">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="">Open</label>
                                    </div>
                                    <div class="col-sm-1 ">
                                        <input type="radio" class="bluecheckradios" id="auc_close_status" name="status"
                                               value="0">
                                    </div>
                                    <div class="col-sm-1 ">
                                        <label for="">Close</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_auc" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'preliminary_list_candidates') { ?>
    <div id="addMLForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_ml_form" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/preliminary_list_candidates"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Preliminary List of Interested Candidates</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                <input name="district" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_ml" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editMLForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_ml_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit New Preliminary List of Interested Candidates</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="ml_id" type="hidden" class="form-control" id="ml_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                <input name="district" id="ml_district" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" id="ml_description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_pdf_link" type="text" readonly id="old_ml_pdf_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_ml" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'procurement_plans') { ?>
    <div id="addPPForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_pp_form" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/procurement_plan_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Procurement Plan</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Year</label>

                                <div class="col-sm-6">
                                <input name="year" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_pp" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editPPForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_pp_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit New Procurement Plan</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="pp_id" type="hidden" class="form-control" id="pp_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Year</label>

                                <div class="col-sm-6">
                                <input name="year" id="pp_year" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" id="pp_description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_pdf_link" type="text" readonly id="old_pp_pdf_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_pp" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'company_policies') { ?>
    <div id="addCompanyPolicyForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_company_policy_form" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/company_policies_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Company Policy</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Policy Name</label>

                                <div class="col-sm-6">
                                <input name="name" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Policy Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_company_policy" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editCompanyPolicyForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_company_policy_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Company Policy</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="cp_id" type="hidden" class="form-control" id="cp_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Policy Name</label>

                                <div class="col-sm-6">
                                <input name="name" id="cp_name" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_pdf_link" type="text" readonly id="old_cp_pdf_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_cp" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'manuals') { ?>
    <div id="addManualsForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_manuals_form" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/manuals_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Manual</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Manual Name</label>

                                <div class="col-sm-6">
                                <input name="name" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Manual Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_manual" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editManualForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_manual_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Manual</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="manual_id" type="hidden" class="form-control" id="manual_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Manual Name</label>

                                <div class="col-sm-6">
                                <input name="name" id="manual_name" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_pdf_link" type="text" readonly id="old_manual_pdf_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Manual Document</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_manual" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>


<?php if ($file_name == 'audit_report_listing') { ?>
    <div id="addReportForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_audit_report_form" method="POST"
                      action="<?= base_url() ?>index.php/dashboard/audit_reports"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Audit Report</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="title" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Year</label>

                                <div class="col-sm-6">
                                    <input name="year" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;" >Upload Report</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_audit_report" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editAuditReportForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_audit_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Audit Report</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="audit_report_id" type="hidden" class="form-control" id="audit_report_id"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="title" type="text" class="form-control" id="title" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Year</label>

                                <div class="col-sm-6">
                                    <input name="year" type="text" class="form-control" id="year" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Upload Report</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" id="audit_pdf_link" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_audit" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'job_postings') { ?>
    <div id="addNewJob" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_job_posting" id="add_job_posting" method="POST"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Job Posting</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="title" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Advertisement Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" >
                                        <input type="text" class="form-control datepicker" id="add_job_adv_datePicker" name="adv_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Closing Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" >
                                        <input type="text" class="form-control datepicker" id="add_job_close_datePicker" name="close_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Attachment</label>

                                <div class="col-sm-6">
                                    <input name="attachment_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_job" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editNewJob" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_job_posting" id="edit_job_posting" method="POST" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Job Posting</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="job_id" type="hidden" class="form-control" id="job_id"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="title" type="text" class="form-control" id="job_title"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Advertisement Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="edit_job_adv_datePicker">
                                        <input type="text" class="form-control datepicker" id="edit_adv_date" name="adv_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Closing Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="edit_job_close_datePicker">
                                        <input type="text" class="form-control datepicker" id="edit_closing_date" name="close_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old Attachment</label>

                                <div class="col-sm-6">
                                    <input name="old_attachment_link" type="text" readonly id="edit_attachment_link"
                                           class="form-control"/>
                                </div>
                                <div class="col-sm-1">
                                    <a class="btn btn-sm btn-info" target="_blank" href="#" id="old_attach_view">View</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Attachment</label>

                                <div class="col-sm-6">
                                    <input name="attachment_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_job" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'public_info_officers') { ?>
    <div id="add_publicform" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_public" id="add_public" method="POST" action="<?php base_url('dashboard/public_info')?>"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Contact</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Category</label>

                                <div class="col-sm-6">
                                    <select name="category" class="form-control selectpicker"
                                            title="Select Directorate" required>
                                        <option value="" disabled>Select Type</option>
                                        <option value="Head Office">Head Office</option>
                                        <option value="Districts Offices">Districts Offices</option>
                                        <option value="Dispensaries">Dispensaries</option>
                                        <option value="Basic Health Units (BHUs)">Basic Health Units (BHUs)</option>
                                        <option value="Rural Health Centres (RHCs)">Rural Health Centres (RHCs)</option>
                                        <option value="Filter Clinics">Filter Clinics</option>
                                        <option value="Hospitals">Hospitals</option>
                                        <option value="24/ 7 Health Facilities">24/ 7 Health Facilities</option>
                                        <option value="Mother & Child Healthcare Centres (MCH)">Mother & Child Healthcare Centres (MCH)</option>
                                        <option value="Mobile Health Facilities">Mobile Health Facilities</option>
                                        <option value="Unani Tibi Dispensaries">Unani Tibi Dispensaries</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name of Office/Facility</label>

                                <div class="col-sm-6">
                                    <input name="name_of_office_facility" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <input name="district" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Email</label>

                                <div class="col-sm-6">
                                    <input name="email" type="email" class="form-control" id="email" required/>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row input_fields_wrap">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Email</label>

                                <div class="col-sm-6">
                                    <input type="email" class="form-control email_address_add" id="email" name="email"/>

                                </div>


                                 <div class="col-sm-4">
                                    <button type="button" class="add_field_button btn btn-info glowOnClick">Add</button>
                                </div>
                                <div style="display:none; color:red ;padding-left: 18px;" class="col-lg-offset-2  error" id="">Invalid Email</div>

                            </div>
                        </div> -->

                        <div class="row input_contact_wrap">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Mobile No.</label>

                                <div class="col-sm-6">
                                    <input type="text" name="mobile_no" class="form-control contact_number_add" required/>
                                    <span id="spnPhoneStatus"></span>
                                </div>
                                <!-- <div class="col-sm-4">
                                    <button type="button" value="add_contact" class="add_contact_button btn btn-info glowOnClick1">Add</button>
                                    <span id="add_contact_val"></span>
                                </div> -->

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Phone No.</label>

                                <div class="col-sm-6">
                                    <input name="phone_no" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Latitude</label>

                                <div class="col-sm-6">
                                    <input name="latitude" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Longitude</label>

                                <div class="col-sm-6">
                                    <input name="longitude" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>

                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="public_add" name="add_public" disabled="disabled" value="Add">
                    </div>

                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <div id="edit_publicform" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_public" id="edit_public_form" method="POST" action="<?php base_url('dashboard/public_info')?>"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Contact</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <input name="info_id" type="hidden" id="info_id"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Category</label>

                                <div class="col-sm-6">
                                    <select name="category" class="form-control selectpicker"
                                            title="Select Directorate" required>
                                        <option value="" disabled>Select Type</option>
                                        <option value="Head Office">Head Office</option>
                                        <option value="Districts Offices">Districts Offices</option>
                                        <option value="Dispensaries">Dispensaries</option>
                                        <option value="Basic Health Units (BHUs)">Basic Health Units (BHUs)</option>
                                        <option value="Rural Health Centres (RHCs)">Rural Health Centres (RHCs)</option>
                                        <option value="Filter Clinics">Filter Clinics</option>
                                        <option value="Hospitals">Hospitals</option>
                                        <option value="24/ 7 Health Facilities">24/ 7 Health Facilities</option>
                                        <option value="Mother & Child Healthcare Centres (MCH)">Mother & Child Healthcare Centres (MCH)</option>
                                        <option value="Mobile Health Facilities">Mobile Health Facilities</option>
                                        <option value="Unani Tibi Dispensaries">Unani Tibi Dispensaries</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name of Office/Facility</label>

                                <div class="col-sm-6">
                                    <input name="name_of_office_facility" type="text" class="form-control" id="name_of_office_facility" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation" type="text" class="form-control" id="designation" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <input name="district" type="text" class="form-control" id="district" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Email</label>

                                <div class="col-sm-6">
                                    <input name="email" type="email" class="form-control" class="email" required/>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row input_fields_wrap">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Email</label>

                                <div class="col-sm-6">
                                    <input type="email" class="form-control email_address_add" id="email" name="email"/>

                                </div>


                                 <div class="col-sm-4">
                                    <button type="button" class="add_field_button btn btn-info glowOnClick">Add</button>
                                </div>
                                <div style="display:none; color:red ;padding-left: 18px;" class="col-lg-offset-2  error" id="">Invalid Email</div>

                            </div>
                        </div> -->

                        <div class="row input_contact_wrap">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Mobile No.</label>

                                <div class="col-sm-6">
                                    <input type="text" name="mobile_no" class="form-control contact_number_add" id="mobile_no" required/>
                                    <span id="spnPhoneStatus"></span>
                                </div>
                                <!-- <div class="col-sm-4">
                                    <button type="button" value="add_contact" class="add_contact_button btn btn-info glowOnClick1">Add</button>
                                    <span id="add_contact_val"></span>
                                </div> -->

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Phone No.</label>

                                <div class="col-sm-6">
                                    <input name="phone_no" type="text" class="form-control" id="phone_no" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Latitude</label>

                                <div class="col-sm-6">
                                    <input name="latitude" type="text" class="form-control" id="latitude"  required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Longitude</label>

                                <div class="col-sm-6">
                                    <input name="longitude" type="text" class="form-control" id="longitude" required/>
                                </div>
                            </div>
                        </div>

                        <div class="row input_contact_wraps"></div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="edit_public" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'press_release') { ?>
    <div id="add_pressform" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_press" id="add_press_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add News/Events</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">


                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-6">
                                    <input name="press_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="press_description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="press_image" type="file" class=""/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_press" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editpressform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="editpress" id="edit_press_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit News/Events</h4>
                    </div>

                    <div class="modal-body col-lg-offset-1">

                        <input type="hidden" id="press_ids" name="press_id">

                        <input type="hidden" id="image_links" name="image_link">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-6">
                                    <input name="press_name" id="press_names" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="press_description" id="press_descriptions" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;"> File</label>

                                <div class="col-sm-6">
                                    <input name="press_image" type="file" class=""/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="editpress" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'download_careers') { ?>
    <div id="add_careersform" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_careers" id="add_careers_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Downloads</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">


                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-6">
                                    <input name="careers_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="careers_description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="careers_image" type="file" class=""/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_careers" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editcareersform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="editcareers" id="edit_careers_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Download Careers</h4>
                    </div>

                    <div class="modal-body col-lg-offset-1">

                        <input type="hidden" id="careers_ids" name="careers_id">

                        <input type="hidden" id="image_links" name="image_link">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-6">
                                    <input name="careers_name" id="careers_names" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="careers_description" id="careers_descriptions" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;"> File</label>

                                <div class="col-sm-6">
                                    <input name="careers_image" type="file" class=""/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="editcareers" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'download_career_jobs') { ?>
    <div id="add_career_job_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_careers" id="add_career_job_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Downloads</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">


                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-6">
                                    <select name="job_name" class="form-control selectpicker"
                                            title="Select Name" required>
                                        <option value="" disabled>Select Name</option>

                                        <option value="Application Form for Jobs">Application Form for Jobs</option>
                                        <option value="Application Form for Locum">Application Form for Locum</option>
                                        <option value="Application for Volunteering">Application for Volunteering</option>
                                        <option value="Interview Call Letters">Interview Call Letters</option>
                                        <option value="Application Form for filing of objections on merit list">Application Form for filing of objections on merit list</option>
                                        <option value="Application Form for Leave">Application Form for Leave</option>
                                        <option value="Application Form for Transfer Requests">Application Form for Transfer Requests</option>
                                        <option value="Application Form for Experience Certificate">Application Form for Experience Certificate</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="careers_image" type="file" required class=""/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_careers" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editcareer_job_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="editcareers" id="edit_career_job_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Download Career Jobs</h4>
                    </div>

                    <div class="modal-body col-lg-offset-1">

                        <input type="hidden" id="careers_ids" name="careers_id">

                        <input type="hidden" id="image_links" name="careers_image">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-6">
                                    <select name="job_name" id="job_name" class="form-control selectpicker"
                                            title="Select Name" required>
                                        <option value="" disabled>Select Name</option>

                                        <option value="Application Form for Jobs">Application Form for Jobs</option>
                                        <option value="Application Form for Locum">Application Form for Locum</option>
                                        <option value="Application for Volunteering">Application for Volunteering</option>
                                        <option value="Interview Call Letters">Interview Call Letters</option>
                                        <option value="Application Form for filing of objections on merit list">Application Form for filing of objections on merit list</option>
                                        <option value="Application Form for Leave">Application Form for Leave</option>
                                        <option value="Application Form for Transfer Requests">Application Form for Transfer Requests</option>
                                        <option value="Application Form for Experience Certificate">Application Form for Experience Certificate</option>

                                    </select>
                                
                                </div>
                            </div>
                        </div>
                    

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;"> File</label>

                                <div class="col-sm-6">
                                    <input name="careers_image" type="file" class="" required/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="editcareers" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'video_management') { ?>
    <div id="addvideoForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_video" method="POST" class="form-horizontal" id="add_video_form"
                      action="<?= base_url() ?>index.php/dashboard/video_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Video</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Video Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-6">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="" name="image_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Video Link</label>

                                <div class="col-sm-6">
                                    <input name="video_link" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_video" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editvideo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_video" method="POST" action="<?= base_url() ?>index.php/dashboard/edit_video"
                      id="edit_video_form"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Video</h4>
                    </div>

                    <div class="modal-body col-lg-offset-1">
                        <input type="hidden" id="id" name="id">
                        <input type="hidden" id="old_image_url" name="old_image_url">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Video Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" id="name" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="col-sm-7">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" id="selected_image_url"
                                                 alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" id="old_image_url"
                                               name="image_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Video Link</label>

                                <div class="col-sm-6">
                                    <input name="video_link" id="video_link" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" id="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="editvideo" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'project_category') { ?>
    <div id="add_Pro_cat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_category_project" id="add_category_project" method="POST" class="form-horizontal"
                      action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Project Category</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Category Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_category" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editProCat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_project_category" id="edit_project_category" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Project Category</h4>
                    </div>

                    <div class="modal-body col-lg-offset-1">
                        <input type="hidden" id="p_cat_id" name="pcat_id">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Category Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" id="p_cat_name" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="editpcat" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

    <div id="addProjectForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_project_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Project</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Project Type</label>

                                <div class="col-sm-6">
                                    <select name="project_cat_id" id="project_type_id" class="form-control"
                                            title="Select Project Type">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($project_cats)) {
                                            foreach ($project_cats as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group pro_GS_status" style="display: none">
                                <label class="col-sm-3" style="font-weight: bold;">GS No</label>

                                <div class="col-sm-6">
                                    <input name="GS_no" type="text" class="form-control gs_no" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name of Scheme</label>

                                <div class="col-sm-6">
                                    <input name="name_of_scheme" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row sceheme_status" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Status</label>

                                <div class="col-sm-6">
                                    <select name="status" class="form-control selectpicker"
                                            title="Select Status">
                                        <option value="0">Approved</option>
                                        <option value="1">Un Approved</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Estimated Cost(Million)</label>

                                <div class="col-sm-6">
                                    <input name="cost_in_million" type="text" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>

                        <div class="row project_duration" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Duration</label>

                                <div class="col-sm-6">
                                    <select name="duration" class="form-control selectpicker pro_duration"
                                            title="Select Status">
                                        <option value="01">1 Year</option>
                                        <option value="02">2 Year</option>
                                        <option value="03">3 Year</option>
                                        <option value="04">4 Year</option>
                                        <option value="05">5 Year</option>
                                        <option value="06">6 Year</option>
                                        <option value="07">7 Year</option>
                                        <option value="08">8 Year</option>
                                        <option value="09">9 Year</option>
                                        <option value="10">10 Year</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row project_expected_du" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Duration</label>

                                <div class="col-sm-3">
                                    <select name="from_year" class="form-control selectpicker "
                                            title="From Year">
                                        <option value="2001-02">2001-02</option>
                                        <option value="2002-03">2002-03</option>
                                        <option value="2003-04">2003-04</option>
                                        <option value="2004-05">2004-05</option>
                                        <option value="2005-06">2005-06</option>
                                        <option value="2006-07">2006-07</option>
                                        <option value="2007-08">2007-08</option>
                                        <option value="2008-09">2008-09</option>
                                        <option value="2009-10">2009-10</option>
                                        <option value="2010-11">2010-11</option>
                                        <option value="2011-12">2011-12</option>
                                        <option value="2012-13">2012-13</option>
                                        <option value="2013-14">2013-14</option>
                                        <option value="2014-15">2014-15</option>
                                        <option value="2015-16">2015-16</option>
                                        <option value="2016-17">2016-17</option>
                                        <option value="2017-18">2017-18</option>
                                        <option value="2018-19">2018-19</option>
                                        <option value="2019-20">2019-20</option>
                                        <option value="2020-21">2020-21</option>
                                        <option value="2021-22">2021-22</option>
                                        <option value="2022-23">2022-23</option>

                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select name="to_year" class="form-control selectpicker"
                                            title="To Year">
                                        <option value="2001-02">2001-02</option>
                                        <option value="2002-03">2002-03</option>
                                        <option value="2003-04">2003-04</option>
                                        <option value="2004-05">2004-05</option>
                                        <option value="2005-06">2005-06</option>
                                        <option value="2006-07">2006-07</option>
                                        <option value="2007-08">2007-08</option>
                                        <option value="2008-09">2008-09</option>
                                        <option value="2009-10">2009-10</option>
                                        <option value="2010-11">2010-11</option>
                                        <option value="2011-12">2011-12</option>
                                        <option value="2012-13">2012-13</option>
                                        <option value="2013-14">2013-14</option>
                                        <option value="2014-15">2014-15</option>
                                        <option value="2015-16">2015-16</option>
                                        <option value="2016-17">2016-17</option>
                                        <option value="2017-18">2017-18</option>
                                        <option value="2018-19">2018-19</option>
                                        <option value="2019-20">2019-20</option>
                                        <option value="2020-21">2020-21</option>
                                        <option value="2021-22">2021-22</option>
                                        <option value="2022-23">2022-23</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group " style="">
                                <label class="col-sm-3" style="font-weight: bold;">Link Type</label>

                                <div class="col-sm-6">
                                    <input name="link_type" type="radio" value="1" class="link_type_radio"
                                           /> File
                                    <input name="link_type" type="radio" Value="2" class=" link_type_radio"
                                           /> Video

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group " style="">
                                <label class="col-sm-3" style="font-weight: bold;">Link URL</label>

                                <div class="col-sm-6">
                                    <input name="link_url" class="form-control link_url_class"  type="text" />

                                </div>
                            </div>
                        </div>
                        <!--                    <div class="row">-->
                        <!--                        <div class="form-group">-->
                        <!--                            <label class="col-sm-2 control-label">Status</label>-->
                        <!---->
                        <!--                            <div class="col-sm-8">-->
                        <!--                                <div class="col-sm-1 ">-->
                        <!--                                    <input type="radio" class="bluecheckradios" checked-->
                        <!--                                           name="is_completed" value="1">-->
                        <!--                                </div>-->
                        <!--                                <div class="col-sm-3">-->
                        <!--                                    <label for="">Completed</label>-->
                        <!--                                </div>-->
                        <!--                                <div class="col-sm-1 ">-->
                        <!--                                    <input type="radio" class="bluecheckradios" name="is_completed"-->
                        <!--                                           value="0">-->
                        <!--                                </div>-->
                        <!--                                <div class="col-sm-3">-->
                        <!--                                    <label for="">InComplete</label>-->
                        <!--                                </div>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                        <!--                    </div>-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" id="clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_project" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_project" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_project_form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Project</h4>
                    </div>
                    <input name="project_id" id="pro_id_hidden" type="hidden" class="form-control"/>

                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Project Type</label>

                                <div class="col-sm-6">
                                    <select name="project_cat_id" id="project_type_id_updated" class="form-control"
                                            title="Select Project Type">
                                        <option value="">Select Project Type</option>
                                        <?php
                                        if (!empty($project_cats)) {
                                            foreach ($project_cats as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group pro_GS_status" style="display: none">
                                <label class="col-sm-3" style="font-weight: bold;">GS No</label>

                                <div class="col-sm-6">
                                    <input name="GS_no" id="pro_gs_no_updated" type="text" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name of Scheme</label>

                                <div class="col-sm-6">
                                    <input name="name_of_scheme" id="pro_scheme_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row sceheme_status" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Status</label>

                                <div class="col-sm-6">
                                    <select name="status" id="pro_status" class="form-control selectpicker sceh_status"
                                            title="Select Status">
                                        <option value="0">Approved</option>
                                        <option value="1">Un Approved</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Estimated Cost(Million)</label>

                                <div class="col-sm-6">
                                    <input name="cost_in_million" id="pro_cost_million" type="text" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <!--  <div class="row" style="display: none;">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Estimated Cost(PKR)</label>

                                <div class="col-sm-6">
                                    <input name="cost_in_pk" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>-->
                        <div class="row project_duration" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Duration</label>

                                <div class="col-sm-6">
                                    <select name="duration" id="pro_duration" class="form-control "
                                            title="Select Status">
                                        <option value="01">1 Year</option>
                                        <option value="02">2 Year</option>
                                        <option value="03">3 Year</option>
                                        <option value="04">4 Year</option>
                                        <option value="05">5 Year</option>
                                        <option value="06">6 Year</option>
                                        <option value="07">7 Year</option>
                                        <option value="08">8 Year</option>
                                        <option value="09">9 Year</option>
                                        <option value="10">10 Year</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row project_expected_du" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Duration</label>

                                <div class="col-sm-3">
                                    <select name="from_year" id="pro_from_year" class="form-control  "
                                            title="From Year">
                                        <option value="2001-02">2001-02</option>
                                        <option value="2002-03">2002-03</option>
                                        <option value="2003-04">2003-04</option>
                                        <option value="2004-05">2004-05</option>
                                        <option value="2005-06">2005-06</option>
                                        <option value="2006-07">2006-07</option>
                                        <option value="2007-08">2007-08</option>
                                        <option value="2008-09">2008-09</option>
                                        <option value="2009-10">2009-10</option>
                                        <option value="2010-11">2010-11</option>
                                        <option value="2011-12">2011-12</option>
                                        <option value="2012-13">2012-13</option>
                                        <option value="2013-14">2013-14</option>
                                        <option value="2014-15">2014-15</option>
                                        <option value="2015-16">2015-16</option>
                                        <option value="2016-17">2016-17</option>
                                        <option value="2017-18">2017-18</option>
                                        <option value="2018-19">2018-19</option>
                                        <option value="2019-20">2019-20</option>
                                        <option value="2020-21">2020-21</option>
                                        <option value="2021-22">2021-22</option>
                                        <option value="2022-23">2022-23</option>

                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select name="to_year" id="pro_to_year" class="form-control selectpicker"
                                            title="To Year">
                                        <option value="2001-02">2001-02</option>
                                        <option value="2002-03">2002-03</option>
                                        <option value="2003-04">2003-04</option>
                                        <option value="2004-05">2004-05</option>
                                        <option value="2005-06">2005-06</option>
                                        <option value="2006-07">2006-07</option>
                                        <option value="2007-08">2007-08</option>
                                        <option value="2008-09">2008-09</option>
                                        <option value="2009-10">2009-10</option>
                                        <option value="2010-11">2010-11</option>
                                        <option value="2011-12">2011-12</option>
                                        <option value="2012-13">2012-13</option>
                                        <option value="2013-14">2013-14</option>
                                        <option value="2014-15">2014-15</option>
                                        <option value="2015-16">2015-16</option>
                                        <option value="2016-17">2016-17</option>
                                        <option value="2017-18">2017-18</option>
                                        <option value="2018-19">2018-19</option>
                                        <option value="2019-20">2019-20</option>
                                        <option value="2020-21">2020-21</option>
                                        <option value="2021-22">2021-22</option>
                                        <option value="2022-23">2022-23</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group " style="">
                                <label class="col-sm-3" style="font-weight: bold;">Link Type</label>

                                <div class="col-sm-6">
                                    <input name="link_type" type="radio" value="1" class="link_type_radio"
                                    /> File
                                    <input name="link_type" type="radio" Value="2" class=" link_type_radio"
                                    /> Video

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group " style="">
                                <label class="col-sm-3" style="font-weight: bold;">Link URL</label>

                                <div class="col-sm-6">
                                    <input name="link_url"  class="form-control link_url_class"   type="text"  />

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="edit_project" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php if ($file_name == 'staff_corners') { ?>
    <div id="addstaffForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" method="POST" action=""
                      id="add_staff_form" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Staff</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Staff Type</label>

                                <div class="col-sm-6">
                                    <select name="staff_cat_id" id="staff_type_id" class="form-control staff_type_id_change"
                                            title="Select Project Type">
                                        <option value="">Select</option>
                                        <option value="1">Transfer Posting</option>
                                        <option value="2">Suspension/Termination Orders</option>
                                        <option value="3">Notification</option>
                                        <option value="4">Promotion Orders</option>
                                        <option value="5">Appointment Orders</option>
                                        <!--                                    --><?php
                                        //                                    if (!empty($project_cats)) {
                                        //                                        foreach ($project_cats as $row) { ?>
                                        <!--                                            <option value="-->
                                        <? //= $row->id ?><!--">--><? //= $row->name ?><!--</option>-->
                                        <!--                                        --><?php //}
                                        //                                    } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row name_of_officer">
                            <div class="form-group ">
                                <label class="col-sm-2" style="font-weight: bold;">Name of Officer/Official</label>

                                <div class="col-sm-6">
                                    <input name="name_of_officer" type="text" class="form-control officer_name_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row transfer_from">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Transfer From</label>

                                <div class="col-sm-6">
                                    <input name="transfer_from" type="text" class="form-control transfer_from_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row designation_from">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation_from" type="text" class="form-control designation_from_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row transfer_too">
                            <div class="form-group">
                                <label class="col-sm-2 transfer_to" style="font-weight: bold;">Transfer To</label>

                                <div class="col-sm-6">
                                    <input name="transfer_to" type="text" class="form-control transfer_to_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row designation_to">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation_to" type="text" class="form-control designation_to_field"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="add_staff_datePicker">
                                        <input type="text" class="form-control" name="date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row status_link">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">File</label>

                                <div class="col-sm-6">
                                    <input name="status_link" type="file" class=" status_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row order_no" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Order No</label>

                                <div class="col-sm-6">
                                    <input name="order_no" type="text" class="form-control order_no_field" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row scale" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Scale</label>

                                <div class="col-sm-6">
                                    <input name="scale" type="text" class="form-control scale_field" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row instructions_texts" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Instruction Text</label>

                                <div class="col-sm-6">
                                    <input name="instructions_texts" type="text"
                                           class="form-control instruction_text_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row notification_no" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Notification No</label>

                                <div class="col-sm-6">
                                    <input name="notification_no"
                                           type="text" class="form-control notification_no_field"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row directorate_id" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Directorate</label>

                                <div class="col-sm-6">
                                    <select name="directorate_id" id="directorate_id"
                                            class="form-control selectpicker directorate_id_field"
                                            title="Select Directorate"/>
                                    <?php
                                    if (!empty($directorates)) {
                                        foreach ($directorates as $row) { ?>
                                            <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                        <?php }
                                    } ?>
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_staff" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_staff" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_staff" method="POST" action=""
                      id="edit_staff_form" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Project</h4>
                    </div>
                    <input name="staff_id" id="staff_id" type="hidden"/>
                    <input name="old_status_links" id="old_status_link" type="hidden"/>

                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Staff Type</label>

                                <div class="col-sm-6">
                                    <select name="staff_cat_id" id="staff_type_ids"
                                            class="form-control staff_type_id_change"
                                            title="Select Project Type">
                                        <option value="">Select</option>
                                        <option value="1">Transfer Posting</option>
                                        <option value="2">Suspension/Termination Orders</option>
                                        <option value="3">Notification</option>
                                        <option value="4">Promotion Orders</option>
                                        <option value="5">Appointment Orders</option>
                                        <!--                                    --><?php
                                        //                                    if (!empty($project_cats)) {
                                        //                                        foreach ($project_cats as $row) { ?>
                                        <!--                                            <option value="-->
                                        <? //= $row->id ?><!--">--><? //= $row->name ?><!--</option>-->
                                        <!--                                        --><?php //}
                                        //                                    } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row name_of_officer">
                            <div class="form-group ">
                                <label class="col-sm-2" style="font-weight: bold;">Name of Officer/Official</label>

                                <div class="col-sm-6">
                                    <input name="name_of_officer" id="name_of_officer" type="text" class="form-control officer_name_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row transfer_from">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Transfer From</label>

                                <div class="col-sm-6">
                                    <input name="transfer_from" id="transfer_from" type="text" class="form-control transfer_from_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row designation_from">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation_from" id="designation_from" type="text" class="form-control designation_from_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row transfer_too">
                            <div class="form-group">
                                <label class="col-sm-2 transfer_to" style="font-weight: bold;">Transfer To</label>

                                <div class="col-sm-6">
                                    <input name="transfer_to" id="transfer_to" type="text" class="form-control transfer_to_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row designation_to">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation_to" id="designation_to" type="text" class="form-control designation_to_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Dated</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="edit_staff_datePicker">
                                        <input type="text" class="form-control" id="date_staff" name="date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row status_link">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">File</label>

                                <div class="col-sm-6">
                                    <input name="status_link" type="file" class=" status_field"/>
                                </div>
                            </div>
                        </div>

                        <div class="row order_no" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Order No</label>

                                <div class="col-sm-6">
                                    <input name="order_no" id="order_no" type="text" class="form-control order_no_field"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row scale" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Scale</label>

                                <div class="col-sm-6">
                                    <input name="scale" id="scale" type="text" class="form-control scale_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row instructions_texts" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Instruction Text</label>

                                <div class="col-sm-6">
                                    <input name="instructions_texts" id="instructions_texts" type="text"
                                           class="form-control instruction_text_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row notification_no" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Notification No</label>

                                <div class="col-sm-6">
                                    <input name="notification_no" id="notification_no"
                                           type="text"
                                           class="form-control notification_no_field"/>
                                </div>
                            </div>
                        </div>
                        <div class="row directorate_id" style="display: none">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Directorate</label>

                                <div class="col-sm-6">
                                    <select name="directorate_id" id="directorate_ids" class="form-control selectpicker"
                                            title="Select Directorate">
                                        <option value="">Select Directorate</option>
                                        <?php
                                        if (!empty($directorates)) {
                                            foreach ($directorates as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="edit_staff" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'research_management') { ?>
    <div id="add_researchForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_research" method="POST" id="add_research_Form" class="form-horizontal"
                      action="<?= base_url() ?>index.php/dashboard/research_thesis_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Research</h4>
                    </div>
                    <div class="modal-body col-sm-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4" style="font-weight: bold;">Dissertation Name</label>

                                <div class="col-sm-7">
                                    <input name="dissertation_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4" style="font-weight: bold;">Dissertation By</label>

                                <div class="col-sm-7">
                                    <input name="dissertation__by" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4" style="font-weight: bold;">Current Place of Posting</label>

                                <div class="col-sm-7">
                                    <input name="current_place_of_posting" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_research" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_researchForm" class="modal fade" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="editresearch_form" class="form-horizontal" name="edit_research" method="post"
                          action="<?= base_url() ?>index.php/dashboard/research_thesis_listing"
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Research</h4>
                        </div>
                        <div class="modal-body col-sm-offset-1">
                            <input type="hidden" id="research_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4" style="font-weight: bold;">Dissertation Name</label>

                                    <div class="col-sm-7">
                                        <input name="dissertation_name" id="dissertation_name" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4" style="font-weight: bold;">Dissertation By</label>

                                    <div class="col-sm-7">
                                        <input name="dissertation__by" id="dissertation__by" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4" style="font-weight: bold;">Current Place of Posting</label>

                                    <div class="col-sm-7">
                                        <input name="current_place_of_posting" id="current_place_of_posting" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="editresearch" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <!--poultry-->
<?php if ($file_name == 'poultry_management') { ?>
    <div id="add_twitter_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_twitter" method="POST" id="add_twitter_form" class="form-horizontal"
                      action="<?= base_url() ?>index.php/dashboard/poultry_items_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Twitter URL</h4>
                    </div>
                    <div class="modal-body col-sm-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">URL : </label>

                                <div class="col-sm-6">
                                    <input name="poultry_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_poultry" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_poultryForm" class="modal fade" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_poultry_form" class="form-horizontal" name="edit_poultry" method="post"
                          action="<?= base_url() ?>index.php/dashboard/poultry_items_listing" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Poultry</h4>
                        </div>
                        <div class="modal-body col-sm-offset-1">
                            <input type="hidden" id="poultry_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-6">
                                        <input name="poultry_name" id="poultry_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Quantity</label>

                                    <div class="col-sm-6">
                                        <input name="poultry_qty" id="poultry_qty" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="editpoultry" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <!--species table-->
<?php if ($file_name == 'species_management') { ?>
    <div id="add_speciesForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_species" method="POST" id="add_species_Form" class="form-horizontal"
                      action="<?= base_url() ?>index.php/dashboard/species_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Species</h4>
                    </div>
                    <div class="modal-body col-sm-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="species_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_species" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_speciesForm" class="modal fade" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_species_Form" class="form-horizontal" name="edit_species" method="post"
                          action="<?= base_url() ?>index.php/dashboard/species_listing" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Species</h4>
                        </div>
                        <div class="modal-body col-sm-offset-1">
                            <input type="hidden" id="species_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-7">
                                        <input name="species_name" id="species_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="editspecies" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <!--Report Category-->
<?php if ($file_name == 'report_category_management') { ?>
    <div id="add_reportcategoryForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_report" method="POST" id="add_report_category_Form" class="form-horizontal"
                      action="<?= base_url() ?>index.php/dashboard/report_category_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Report Category</h4>
                    </div>
                    <div class="modal-body col-sm-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="reportcategory_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_reportcategory" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_reportcategoryForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_report_category_Form" class="form-horizontal" name="edit_reportcategory" method="post"
                          action="<?= base_url() ?>index.php/dashboard/report_category_listing"
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Report Category</h4>
                        </div>
                        <div class="modal-body col-sm-offset-1">
                            <input type="hidden" id="reportcategory_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-7">
                                        <input name="reportcategory_name" id="reportcategory_name" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="edit_reportcategory" value="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'evaluation_reports') { ?>
    <div id="addReportForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_report_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Report</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="report_cat_id" class="form-control selectpicker"
                                            title="Select Report Type">
                                        <option value="">Select Report Type</option>
                                        <?php
                                        if (!empty($reports_cats)) {
                                            foreach ($reports_cats as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="detail" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_report" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editReportForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_report_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Report</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="report_id" type="hidden" class="form-control" id="report_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="report_cat_id" class="form-control selectpicker"
                                            id="report_cat_id_updated"
                                            title="Select Directorate">
                                        <option value="">Select Type</option>
                                        <?php
                                        if (!empty($reports_cats)) {
                                            foreach ($reports_cats as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" id="report_name_id" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_pdf_link" id="old_pdf_link" readonly type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" id="pdf_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="detail" type="text" id="report_detail_updated" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_report" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'poultry_product_listing') { ?>
    <div id="addPProductForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_poultry_pr_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Poultry Product</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Poultry Item</label>

                                <div class="col-sm-6">
                                    <select name="poultry_item_id" class="form-control selectpicker"
                                            title="Select Poultry Item">
                                        <option value="">Select Poultry Item</option>
                                        <?php
                                        if (!empty($p_items)) {
                                            foreach ($p_items as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Pak)</label>

                                <div class="col-sm-6">
                                    <input name="pak_qty" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="punjab_qty" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Share in %</label>

                                <div class="col-sm-6">
                                    <input name="share" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_product" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editProductForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_poultry_pr_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Poultry Product</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="product_id" type="hidden" class="form-control" id="product_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Poultry Item</label>

                                <div class="col-sm-6">
                                    <select name="poultry_item_id" id="update_poultry_item"
                                            class="form-control selectpicker"
                                            title="Select Poultry Item">
                                        <option value="">Select Poultry Item</option>
                                        <?php
                                        if (!empty($p_items)) {
                                            foreach ($p_items as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Pak)</label>

                                <div class="col-sm-6">
                                    <input name="pak_qty" type="text" id="update_pak_qty" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="punjab_qty" type="text" id="update_punjab_qty" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Share in %</label>

                                <div class="col-sm-6">
                                    <input name="share" type="text" id="update_share" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" id="update_source" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_p_product" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'population_management') { ?>
    <div id="addPopulationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_population_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Population</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Species</label>

                                <div class="col-sm-6">
                                    <select name="species_id" class="form-control selectpicker"
                                            title="Select Species Item">
                                        <option value="">Select Species Item</option>
                                        <?php
                                        if (!empty($species)) {
                                            foreach ($species as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Pak)</label>

                                <div class="col-sm-6">
                                    <input name="pak_qty" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="punjab_qty" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Share in %</label>

                                <div class="col-sm-6">
                                    <input name="share" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_population" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editPopulationForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_Population_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Population</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="population_id" type="hidden" class="form-control" id="population_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Species</label>

                                <div class="col-sm-6">
                                    <select name="species_id" id="update_species_item" class="form-control selectpicker"
                                            title="Select Species Item">
                                        <option value="">Select Species Item</option>
                                        <?php
                                        if (!empty($species)) {
                                            foreach ($species as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Pak)</label>

                                <div class="col-sm-6">
                                    <input name="pak_qty" type="text" id="pupdate_pak_qty" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="punjab_qty" type="text" id="pupdate_punjab_qty" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Share in %</label>

                                <div class="col-sm-6">
                                    <input name="share" type="text" id="pupdate_share" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" id="pupdate_source" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_p_product" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'stockPattern_management') { ?>
    <div id="addStockForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_Stock_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Stock Pattern</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Herd Size</label>

                                <div class="col-sm-6">
                                    <select name="herd_size" class="form-control selectpicker"
                                            title="Select Herd Size">
                                        <option value="1-2">1-2</option>
                                        <option value="3-4">3-4</option>
                                        <option value="5-6">5-6</option>
                                        <option value="Above 6">Above 6</option>
                                        <option value="7-8">7-8</option>
                                        <option value="9-10">9-10</option>
                                        <option value="11-12">11-12</option>
                                        <option value="13-14">13-14</option>
                                        <option value="15-16">15-16</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Pak)</label>

                                <div class="col-sm-6">
                                    <input name="pak_qty" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="punjab_qty" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_stock" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editStockForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_Stock_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Stock Pattern</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="stock_id" type="hidden" class="form-control" id="stock_id_hidden"/>

                        <div class="row">
                            <div class="form-group ">
                                <label class="col-sm-3" style="font-weight: bold;">Herd Size</label>

                                <div class="col-sm-6">
                                    <select name="herd_size" id="update_herd_size" class="form-control selectpicker"
                                            title="Select Herd Size">
                                        <option value="1-2">1-2</option>
                                        <option value="3-4">3-4</option>
                                        <option value="5-6">5-6</option>
                                        <option value="Above 6">Above 6</option>
                                        <option value="7-8">7-8</option>
                                        <option value="9-10">9-10</option>
                                        <option value="11-12">11-12</option>
                                        <option value="13-14">13-14</option>
                                        <option value="15-16">15-16</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Pak)</label>

                                <div class="col-sm-6">
                                    <input name="pak_qty" type="text" id="supdate_pak_qty" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="punjab_qty" type="text" id="supdate_punjab_qty" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" id="supdate_source" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_stock_p" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'export_major') { ?>
    <div id="addExportForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_Export_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Major Export Commodities</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Species</label>

                                <div class="col-sm-6">
                                    <select name="species_id" class="form-control selectpicker"
                                            title="Select Species Item">
                                        <option value="">Select Species Item</option>
                                        <?php
                                        if (!empty($species)) {
                                            foreach ($species as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Poultry Item</label>

                                <div class="col-sm-6">
                                    <select name="poultry_item_id" class="form-control selectpicker"
                                            title="Select Poultry Item">
                                        <option value="">Select Poultry Item</option>
                                        <?php
                                        if (!empty($p_items)) {
                                            foreach ($p_items as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Unit</label>

                                <div class="col-sm-6">
                                    <input name="unity" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="qty" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Value</label>

                                <div class="col-sm-6">
                                    <input name="value" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_export" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editExportForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_export" id="edit_Export_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Major Export Commodities</h4>
                    </div>
                    <input name="export_id" type="hidden" class="form-control" id="export_id_hidden"/>

                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Species</label>

                                <div class="col-sm-6">
                                    <select name="species_id" id="update_species_export" class="form-control selectpicker"
                                            title="Select Species Item">
                                        <option value="">Select Species Item</option>
                                        <?php
                                        if (!empty($species)) {
                                            foreach ($species as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Poultry Item</label>

                                <div class="col-sm-6">
                                    <select name="poultry_item_id" id="update_poulty_export"
                                            class="form-control selectpicker"
                                            title="Select Poultry Item">
                                        <option value="">Select Poultry Item</option>
                                        <?php
                                        if (!empty($p_items)) {
                                            foreach ($p_items as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Unit</label>

                                <div class="col-sm-6">
                                    <input name="unity" type="text" id="update_unit_export" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Quantity(Punjab)</label>

                                <div class="col-sm-6">
                                    <input name="qty" type="text" id="update_qty_export" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Value</label>

                                <div class="col-sm-6">
                                    <input name="value" type="text" id="update_value_export" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Source</label>

                                <div class="col-sm-6">
                                    <input name="source" type="text" id="update_source_export" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_new_export" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'publication_listing') { ?>
    <div id="addPublicationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_research" method="POST" id="add_Publication_Form" class="form-horizontal"
                      action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Publication</h4>
                    </div>
                    <div class="modal-body col-sm-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4" style="font-weight: bold;">Title</label>

                                <div class="col-sm-7">
                                    <input name="title" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4" style="font-weight: bold;">Author/Publisher</label>

                                <div class="col-sm-7">
                                    <input name="author_publisher" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4" style="font-weight: bold;">Year</label>

                                <div class="col-sm-7">
                                    <select name="year" class="form-control selectpicker"
                                            title="Select Year">
                                        <?php for ($i = 2000; $i < 2020; $i++) { ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4" style="font-weight: bold;">Version No</label>

                                <div class="col-sm-7">
                                    <select name="version_no" class="form-control selectpicker"
                                            title="Select Version">
                                        <option value="1">New</option>
                                        <option value="0">Old</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_publication" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editPublicationForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_Publication_Form" class="form-horizontal" name="edit_pu" method="post"
                          action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Publication</h4>
                        </div>
                        <div class="modal-body col-sm-offset-1">
                            <input type="hidden" id="publication_id" name="publication_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4" style="font-weight: bold;">Title</label>

                                    <div class="col-sm-7">
                                        <input name="title" id="pub_title" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4" style="font-weight: bold;">Author/Publisher</label>

                                    <div class="col-sm-7">
                                        <input name="author_publisher" id="author_publisher" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4" style="font-weight: bold;">Year</label>

                                    <div class="col-sm-7">
                                        <select name="year" id="publish_year" class="form-control selectpicker"
                                                title="Select Year ">
                                            <?php for ($i = 2000; $i < 2020; $i++) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4" style="font-weight: bold;">Version No</label>

                                    <div class="col-sm-7">
                                        <select name="version_no" id="updatedpubversion_no"
                                                class="form-control selectpicker"
                                                title="Select Version ">
                                            <option value="1">New</option>
                                            <option value="0">Old</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="editable_publication" value="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'publication_agreements') { ?>
    <div id="addAgreementForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_Agreement_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Agreement</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="link_url" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_agre" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editAgreementForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_Agreement_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Agreement</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="agreement_id" type="hidden" class="form-control" id="agre_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control" id="agre_name"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" id="agre_desc"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_link_url" type="text" readonly id="old_agre_pdf_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="link_url" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_agreement" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

    <div id="add_formguideline" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_formguideline" method="POST" id="add_formguideline_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Form And Guideline</h4>
                    </div>
                    <div class="modal-body col-sm-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="formguideline_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Pdf Link</label>

                                <div class="col-sm-7">
                                    <input name="formguideline_pdf_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_formguideline" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_formguidelineForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_formguideline_Form" class="form-horizontal" name="edit_formguideline" method="post"
                          action="" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Form And Guideline</h4>
                        </div>
                        <div class="modal-body col-sm-offset-1">
                            <input type="hidden" id="formguideline_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-7">
                                        <input name="formguideline_name" id="formguideline_name" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Old Pdf Link</label>

                                    <div class="col-sm-7">
                                        <input name="old_formguideline_pdf_link" id="formguideline_pdf_link" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Pdf Link</label>

                                    <div class="col-sm-7">
                                        <input name="formguideline_pdf_link" type="file" class=""/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="edit_formguideline" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>

<?php if ($file_name == 'download_management') { ?>
    <div id="add_download" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_download" method="POST" id="add_download_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Form Download</h4>
                    </div>
                    <div class="modal-body col-sm-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Category</label>

                                <div class="col-sm-7">
                                    <select name="category" class="form-control selectpicker"
                                            title="Select Type">
                                        <option value="">Select Category</option>

                                        <option value="1">Breeding Framework</option>
                                        <option value="2">Mobile Hospital</option>
                                        <option value="3">Poverty Alleviation</option>
                                        <option value="4">R&D Biologics</option>
                                        <option value=56">Mass Vaccination Campaign</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="download_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;"> Link Url</label>

                                <div class="col-sm-7">
                                    <input name="download_link_url" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                <div class="col-sm-7">
                                    <input name="download_description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_download" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_downloadForm" class="modal fade" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_download_Form" class="form-horizontal" name="edit_download" method="post" action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Download</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="download_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Category</label>

                                    <div class="col-sm-7">
                                        <select name="category" class="form-control selectpicker"
                                                title="Select Type">
                                            <option value="">Select Category</option>

                                            <option value="1">Breeding Framework</option>
                                            <option value="2">Mobile Hospital</option>
                                            <option value="3">Poverty Alleviation</option>
                                            <option value="4">R&D Biologics</option>
                                            <option value="5">Mass Vaccination Campaign</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-7">
                                        <input name="download_name" id="download_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Link Url</label>

                                    <div class="col-sm-7">
                                        <input name="old_download_link_url" id="download_link_url" readonly type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Link Url</label>

                                    <div class="col-sm-7">
                                        <input name="download_link_url" type="file" class=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                    <div class="col-sm-7">
                                        <input name="download_description" id="download_description" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="edit_download" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>

    <div id="add_useful_link" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_useful_link" method="POST" id="add_useful_link_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Useful Link</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Category</label>

                                <div class="col-sm-7">
                                    <select name="useful_category" class="form-control selectpicker"
                                            title="Select Type">
                                        <option value="">Select Category</option>

                                        <option value="1">Academics</option>
                                        <option value="2">Industrial</option>
                                        <option value="3">National</option>
                                        <option value="4">International</option>
<option value="5">Miscellaneous & Articles</option>


                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="useful_link_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;"> Link Url</label>

                                <div class="col-sm-7">
                                    <input name="useful_link_url" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_useful_link" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_useful_linkForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_useful_link_Form" class="form-horizontal" name="edit_useful_link" method="post" action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Useful Link</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="useful_link_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Category</label>

                                    <div class="col-sm-7">
                                        <select name="useful_category" class="form-control selectpicker"
                                                title="Select Type" id="useful_cat_name">
                                            <option value="">Select Category</option>
                                            <option value="1">Academics</option>
                                            <option value="2">Industrial</option>
                                            <option value="3">National</option>
                                            <option value="4">International</option>
<option value="5">Miscellaneous & Articles</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-7">
                                        <input name="useful_link_name" id="useful_link_name" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Link Url</label>

                                    <div class="col-sm-7">
                                        <input name="useful_link_url" id="useful_link_url" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="edit_useful_link" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>

<?php if ($file_name == 'material_category') { ?>
    <div id="addMaterialcategoryForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_report" method="POST" id="add_materialcategory_Form" class="form-horizontal"
                      action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Material Category</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_category" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editMaterialcategoryForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_Materialcategory_Form" class="form-horizontal" name="edit_materialcategory" method="post"
                          action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Report Category</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="materialcategory_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-7">
                                        <input name="name" id="materialcategory_name" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="edit_materialcategory" value="Update">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'supportive_material_management') { ?>
    <div id="addMaterialSupportForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_MaterialSupport_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Supportive Material</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="sup_material_cat_id" class="form-control selectpicker"
                                            title="Select Material Type">
                                        <option value="">Select Material Type</option>
                                        <?php
                                        if (!empty($material_cats)) {
                                            foreach ($material_cats as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_material_sup" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editMaterialForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_MaterialSupport_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Supportive Matrial</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="material_id" type="hidden" class="form-control" id="material_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="sup_material_cat_id" class="form-control selectpicker"
                                            id="material_cat_id_updated"
                                            title="Select Type">
                                        <option value="">Select Type</option>
                                        <?php
                                        if (!empty($material_cats)) {
                                            foreach ($material_cats as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-6">
                                    <input name="description" id="material_name_id" type="text" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_pdf_link" id="old_pdf_link_supp" readonly type="text"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="pdf_link" id="pdf_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_sup_material" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'multimedia_management') { ?>
    <div id="addMultimediaForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_multimedia" id="add_Multimedia_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Photo/Video/Story/Games</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>
                                <div class="col-sm-6">
                                    <select name="type" class="form-control  multimedia_type"
                                            title="Select Type" >
                                        <option value="">Select Type</option>
                                        <option value="1">Photo</option>
                                        <option value="2">Video</option>
                                        <option value="3">Story</option>
                                        <option value="4">Game</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="title" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Attachment</label>

                                <div class="col-sm-6">
                                    <input name="link_url" type="file" class="form-control link_url"/>
                                    <input name="link_url" type="text" class="form-control link_url_video" style="display:none;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_multimedia" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editMultimediaForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_Multimedia_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Photo/Video/Story/Games</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="multimedia_id" type="hidden" class="form-control" id="multimedia_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-6">
                                    <select name="type" class="form-control  multimedia_type"
                                            id="multimedia_type"
                                            title="Select Type">
                                        <option value="">Select Type</option>
                                        <option value="1">Photo</option>
                                        <option value="2">Video</option>
                                        <option value="3">Story</option>
                                        <option value="4">Game</option>

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="title" id="multimedia_title" type="text" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old Attachment</label>

                                <div class="col-sm-6">
                                    <input name="old_link_url" id="old_attachment" readonly type="text"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Attachment</label>

                                <div class="col-sm-6">
                                    <input name="link_url" id="link_url" type="file" class="link_url form-control"/>
                                    <input name="link_url" id="link_url_video" type="text" class="form-control link_url_video" style="display:none;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_multimedia" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>


<?php if ($file_name == 'study_survey_management') { ?>
    <div id="addSurveyForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_survey" id="add_Survey_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Survey</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="link_url" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_survey" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editSurveyForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_survey" id="edit_Survey_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Survey</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="survey_id" type="hidden" class="form-control" id="survey_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="name" type="text" class="form-control" id="survey_name"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_link_url" type="text" readonly id="old_survey_pdf_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="link_url" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_survey" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'vaccines_management') { ?>
    <div id="addvaccinesForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_vaccines" id="add_vaccines_Form" method="POST" action="" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Vaccines</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                <div class="col-sm-7">
                                    <select name="vaccines_type" class="form-control selectpicker">
                                        <option value="" selected>Select Type</option>
                                        <option value="0">Price</option>
                                        <option value="1">Schedule</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="vaccines_url" required="required"
                                               name="vaccines_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class=" col-sm-12">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_vaccines" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_vaccinesForm" class="modal fade" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_vaccines_Form" class="form-horizontal" name="edit_vaccines" method="post" action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Vaccines</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="vaccines_id" name="id">
                            <input type="hidden" id="old_vaccines_url" name="old_vaccines_url">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Type</label>

                                    <div class="col-sm-7">
                                        <select name="vaccines_type" id="updatedvaccines_type"
                                                class="form-control selectpicker" title="Select Vaccine Type">
                                            <option value="">Select</option>
                                            <option value="1">Schedule</option>
                                            <option value="0">Price</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                    <div class="col-sm-7">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img data-src="holder.js/100%x100%" id="selected_vaccine_image" alt="...">
                                            </div>
                                            <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="vaccines_url"
                                               name="vaccines_url"></span>
                                                <a href="#" class="btn btn-default fileinput-exists"
                                                   data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="edit_vaccines" value="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <!--advertisements-->
<?php if ($file_name == 'advertisements_management') { ?>
    <div id="addadvertisementsForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_advertisements" id="add_advertisements_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New advertisements</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-6">
                                    <input name="advertisement_name" type="text" class="form-control" id="survey_name"
                                           required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="advertisement_image_url" required="required"
                                               name="advertisement_image_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class=" col-sm-12">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_advertisements" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_advertisementsForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_advertisements_Form" class="form-horizontal" name="edit_advertisements" method="post"
                          action="" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Advertisements</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="advertisement_id" name="id">
                            <input type="hidden" id="old_advertisement_image_url" name="old_advertisement_image_url">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                    <div class="col-sm-6">
                                        <input name="advertisement_name" type="text" class="form-control"
                                               id="advertisement_name" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                    <div class="col-sm-7">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img data-src="holder.js/100%x100%" id="selected_advertisement_image"
                                                     alt="...">
                                            </div>
                                            <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" id="old_advertisement_image_url"
                                               name="advertisement_image_url"></span>
                                                <a href="#" class="btn btn-default fileinput-exists"
                                                   data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default" name="edit_advertisements" value="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

    <!--virtual governance-->
<?php if ($file_name == 'virtual_governance_management') { ?>
    <div id="addvitual_governanceForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_virtual_governance" id="add_virtual_governance_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Virtual Governance</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="virtual_gov_text" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="virtual_url" type="file" id="" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_virtual_governance" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editvitual_governanceForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_virtual_gov" id="edit_virtual_gov_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit virtual_governance</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="id" type="hidden" class="form-control" id="vitual_gov_id"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Title</label>

                                <div class="col-sm-6">
                                    <input name="virtual_gov_text" type="text" class="form-control" id="virtual_gov_text"
                                           required/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="previous_virtual_url" type="text" readonly id="old_vitual_gov_link"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="virtual_url" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="edit_virtual_governance" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'flood_daily_activity') { ?>
    <div id="addDailyActivityForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_daily_activity" id="add_daily_activity_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Daily Activity</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="add_daily_datePicker">
                                        <input type="text" class="form-control" name="activity_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Districtwise Report</label>

                                <div class="col-sm-6">
                                    <input name="districtwise_report_link" type="file" id="" class="" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Consolidate Report</label>

                                <div class="col-sm-6">
                                    <input name="consolidate_report_link" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_daily_activity" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editDailyActivityForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_daily_activity_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Daily Flood Activity </h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="daily_activity_id" type="hidden" class="form-control" id="daily_activity_id"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="edit_daily_datePicker">
                                        <input type="text" class="form-control" id="daily_activity_date" name="activity_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old Districtwise Report</label>

                                <div class="col-sm-6">
                                    <input name="old_districtwise_report_link" type="text" readonly id="daily_districtwise"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Districtwise Report</label>

                                <div class="col-sm-6">
                                    <input name="districtwise_report_link" type="file"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old Cosolidate Report</label>

                                <div class="col-sm-6">
                                    <input name="old_consolidate_report_link" type="text" readonly id="daily_consolidate"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Consolidate Report</label>

                                <div class="col-sm-6">
                                    <input name="consolidate_report_link" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_daily_activity" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($file_name == 'activity_visits') { ?>
    <div id="addVisitActivityForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_visit_activity" id="add_visit_activity_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Visit Activity</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="add_visit_datePicker">
                                        <input type="text" class="form-control" name="date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name of Officer</label>

                                <div class="col-sm-6">
                                    <input name="name_of_officer" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Place of Supervision</label>

                                <div class="col-sm-6">
                                    <input name="place_of_supervision" type="text" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="link_url" type="file" id="" class="" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_visit_activity" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editVisitActivityForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_visit" id="edit_visit_Activity_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Agreement</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="visit_id" type="hidden" class="form-control" id="visit_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="edit_visit_datePicker">
                                        <input type="text" class="form-control" id="visit_date" name="visit_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name of Officer</label>

                                <div class="col-sm-6">
                                    <input name="name_of_officer" id="visit_officer" type="text" class="form-control"
                                           required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Designation</label>

                                <div class="col-sm-6">
                                    <input name="designation" type="text" id="visit_designation" class="form-control"
                                           required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Place of Supervision</label>

                                <div class="col-sm-6">
                                    <input name="place_of_supervision" type="text" id="visit_place_supervision"
                                           class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Old PDF File</label>

                                <div class="col-sm-6">
                                    <input name="old_link_url" type="text" readonly id="old_visit_link_url"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">PDF File</label>

                                <div class="col-sm-6">
                                    <input name="link_url" type="file" id="" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_visit_activity" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'flood_officers_contact') { ?>
    <div id="addOfficerContactForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_OfficersContact_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Officer Contact</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <select name="district_id" class="form-control selectpicker"
                                            title="Select District">
                                        <option value="">Select District</option>
                                        <?php
                                        if (!empty($districts)) {
                                            foreach ($districts as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name of DLO</label>

                                <div class="col-sm-6">
                                    <input name="name_of_DLO" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Contact No</label>

                                <div class="col-sm-6">
                                    <input name="contact_no" type="text"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_officers_contact" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editOfficerContactForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="edit_OfficerContact_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Supportive Matrial</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="officer_id" type="hidden" class="form-control" id="officer_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <select name="district_id" class="form-control selectpicker"
                                            title="Select District" id="updateofficer_district_id">
                                        <option value="">Select District</option>
                                        <?php
                                        if (!empty($districts)) {
                                            foreach ($districts as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name of DLO</label>

                                <div class="col-sm-6">
                                    <input name="name_of_DLO" id="officer_dlo_updated" type="text" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Contact No</label>

                                <div class="col-sm-6">
                                    <input name="contact_no" id="updated_officer_contact_no"
                                           type="text"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_officers_contact" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'flood_advertisement') { ?>
    <div id="addFloodAdvertisementForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_flood_advertisement" id="add_flood_advertisement" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Flood Advertisement</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="form-group col-sm-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id=""
                                               name="image_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class=" col-sm-12">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_flood_advertisement" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editFloodAdvertisementForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_flood_advert" id="edit_FloodAdvertisementForm_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Survey</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input type="hidden" id="flood_advertisement_id_hidden" name="advertisement_id">
                        <input type="hidden" id="old_flood_advertisement_image_url"
                               name="old_flood_advertisement_image_url">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Image</label>

                                <div class="col-sm-7">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" id="flood_advertisement_image_url"
                                                 alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file"
                                               name="image_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_flood_advertis" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'stations_management') { ?>
    <div id="add_new_stations" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_stations" method="POST" id="add_station_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Station</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;"> Description</label>

                                <div class="col-sm-7">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_station" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_StationForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_Station_Form" class="form-horizontal" name="edit_Station_Form" method="post" action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Station</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="station_id" name="station_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                    <div class="col-sm-7">
                                        <input name="name" id="station_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Description</label>

                                    <div class="col-sm-7">
                                        <input name="description" id="station_description" type="text" class="form-control"
                                        />
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" name="edit_station_selected" value="Update">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'free_wanda_distribution') { ?>
    <div id="addDistributionWandaForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_auction" id="add_DistributionWanda_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Wanda Distribution Record</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <select name="district_id" class="form-control selectpicker"
                                            title="Select District">
                                        <option value="">Select District</option>
                                        <?php
                                        if (!empty($districts)) {
                                            foreach ($districts as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Total Amount(KG)</label>

                                <div class="col-sm-6">
                                    <input name="total_qty_kg" type="text" class="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="add_wanda_datePicker">
                                        <input type="text" class="form-control" name="date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Transport Number</label>

                                <div class="col-sm-6">
                                    <input name="transport_number" type="text"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Station</label>

                                <div class="col-sm-6">
                                    <select name="station_id" class="form-control selectpicker"
                                            title="Select Station">
                                        <option value="">Select District</option>
                                        <?php
                                        if (!empty($stations)) {
                                            foreach ($stations as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_free_dist" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editDistributionWandaForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_distribution" id="edit_WandaDistribution_Form" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Free Wanda Distribution Record</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input name="distribution_id" type="hidden" class="form-control" id="distribution_id_hidden"/>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <select name="district_id" class="form-control selectpicker"
                                            title="Select District" id="distribution_district_id">
                                        <option value="">Select District</option>
                                        <?php
                                        if (!empty($districts)) {
                                            foreach ($districts as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Total Amount(KG)</label>

                                <div class="col-sm-6">
                                    <input name="total_qty_kg" type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer" id="distribution_qty"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-6 date">
                                    <div class="input-group input-append date" id="edit_wanda_datePicker">
                                        <input type="text" class="form-control" id="distribution_date" name="date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Transport Number</label>

                                <div class="col-sm-6">
                                    <input name="transport_number" type="text"
                                           id="distribution_transport_number"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Station</label>

                                <div class="col-sm-6">
                                    <select name="station_id" class="form-control selectpicker" id="distribution_station_id"
                                            title="Select Station">
                                        <option value="">Select Station</option>
                                        <?php
                                        if (!empty($stations)) {
                                            foreach ($stations as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="update_wanda_distribution" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'squad_where_about_management') { ?>
    <div id="addsquad_where_aboutForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_squad_where_about" method="POST" id="add_squad_where_aboutForm" class="form-horizontal"
                      action="<?= base_url() ?>index.php/dashboard/squad_where_about_listing"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Squad Where About</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="squad_where_about_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;"> Designation</label>

                                <div class="col-sm-7">
                                    <input name="squad_where_about_designation" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 " style="font-weight: bold;">Date</label>

                                <div class="col-sm-7 date">
                                    <div class="input-group input-append date" id="add_squad_datePicker">
                                        <input type="text" class="form-control" name="squad_where_about_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Where About</label>

                                <div class="col-sm-7">
                                    <input name="squad_where_about" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;"> Remarks</label>

                                <div class="col-sm-7">
                                    <input name="squad_where_about_remarks" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_squad_where_about" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="edit_squad_where_aboutForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_squad_where_about" class="form-horizontal" name="edit_squad_where_about" method="post"
                          action="<?= base_url() ?>index.php/dashboard/squad_where_about_listing"
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Squad Where About</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="squad_where_about_id" name="id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                    <div class="col-sm-7">
                                        <input name="squad_where_about_name" id="squad_where_about_name" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Designation</label>

                                    <div class="col-sm-7">
                                        <input name="squad_where_about_designation" id="squad_where_about_designation"
                                               type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 " style="font-weight: bold;">Date</label>

                                    <div class="col-sm-7 date">
                                        <div class="input-group input-append date" id="edit_squad_datePicker">
                                            <input type="text" class="form-control" id="squad_where_about_date" name="squad_where_about_date"/>
                                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Where About</label>

                                    <div class="col-sm-7">
                                        <input name="squad_where_about" id="squad_where_about" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Remarks</label>

                                    <div class="col-sm-7">
                                        <input name="squad_where_about_remarks" id="squad_where_about_remarks" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="edit_squad_where_about" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'budget_listing') { ?>
    <div id="addaboutus_budgetForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_new_budget" method="POST" id="add_new_budget_record" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Budget Record</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Year</label>

                                <div class="col-sm-3">
                                    <select name="from_year" class="form-control selectpicker"
                                            title="From Year">
                                        <?php for ($i = 2015; $i < 2050; $i++) { ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select name="to_year" class="form-control selectpicker"
                                            title="To Year">
                                        <?php for ($i = 2015; $i < 2050; $i++) { ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Development Price(Million)</label>

                                <div class="col-sm-7">
                                    <input name="development_price" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Non Development Price(Million)</label>

                                <div class="col-sm-7">
                                    <input name="non_development_price" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_budget" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editBudgetListingForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_budget_about" class="form-horizontal" name="edit_budget_about" method="post"
                          action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Budget Record</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="budget_id" name="budget_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Year</label>

                                    <div class="col-sm-3">
                                        <select name="from_year" id="budget_from_year" class="form-control selectpicker"

                                                title="From Year">
                                            <?php for ($i = 2000; $i < 2020; $i++) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="to_year" id="budget_to_year" class="form-control selectpicker"

                                                title="To Year">
                                            <?php for ($i = 2001; $i < 2021; $i++) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Development Price(Million)</label>

                                    <div class="col-sm-7">
                                        <input name="development_price" id="budget_development_price" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Non Development
                                        Price(Million)</label>

                                    <div class="col-sm-7">
                                        <input name="non_development_price" id="budget_non_development_price" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="editaboutusbudget" value="submit">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'autonomous_bodies') { ?>
    <div id="AddAutonomousBodies" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_autonomous" method="POST" id="add_autonomous_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Autonomous Record</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                <div class="col-sm-7">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;"> Link Url</label>

                                <div class="col-sm-7">
                                    <input name="link_url" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_autonomous" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editAutonomousForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_autonomous_Form" class="form-horizontal" name="edit_autonomous_Form" method="post"
                          action=""
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Autonomous Record</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="autonomous_id" name="autonomous_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> Name</label>

                                    <div class="col-sm-7">
                                        <input name="name" id="autonomous_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                    <div class="col-sm-7">
                                        <input name="description" id="autonomous_description" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Link Url</label>

                                    <div class="col-sm-7">
                                        <input name="link_url" id="autonomous_link_url" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="edit_autonomous_records" value="Update">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'laws_and_rules') { ?>
    <div id="addLawAndRule" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="addLawAndRule" method="POST" id="add_LawAndRule_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Rule And Law</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Title</label>

                                <div class="col-sm-7">
                                    <input name="title" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                <div class="col-sm-7">
                                    <input name="description" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Pdf Link</label>

                                <div class="col-sm-7">
                                    <input name="file_link" type="file" class=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                <div class="col-sm-7">
                                    <select name="type" class="form-control selectpicker"
                                            title="Select Type">
                                        <option value="0">Law</option>
                                        <option value="1">Rule</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_lawrule" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editLawAndRuleForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_LawAndRule_Form" class="form-horizontal" name="edit_LawAndRule_Form" method="post"
                          action="" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Law And Rule</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="lawrule_id" name="law_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Title</label>

                                    <div class="col-sm-7">
                                        <input name="title" type="text" id="lawrule_title" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                    <div class="col-sm-7">
                                        <input name="description" id="lawrule_description" type="text" class="form-control"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Old File Link</label>

                                    <div class="col-sm-7">
                                        <input name="old_file_link" readonly id="lawrule_file_link" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;"> File Link</label>

                                    <div class="col-sm-7">
                                        <input name="file_link" type="file" class=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                    <div class="col-sm-7">
                                        <select name="type" id="lawrule_type" class="form-control selectpicker"
                                                title="Select Type">
                                            <option value="0">Law</option>
                                            <option value="1">Rule</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="edit_rulelawform" value="Update">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'our_secretaries_listing') { ?>
    <div id="addSecretariesForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_Secretaries_Form" method="POST" id="add_Secretaries_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New Secretary</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 " style="font-weight: bold;">From Date</label>

                                <div class="col-sm-7 date">
                                    <div class="input-group input-append date" id="add_from_Secretaries_datePicker">
                                        <input type="text" class="form-control" id="" name="from_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 " style="font-weight: bold;">T0 Date</label>

                                <div class="col-sm-7 date">
                                    <div class="input-group input-append date" id="add_to_Secretaries_datePicker">
                                        <input type="text" class="form-control" id="squad_where_about_date" name="to_date"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Designation</label>

                                <div class="col-sm-7">
                                    <input name="designation" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Qualification</label>

                                <div class="col-sm-7">
                                    <input name="qualification" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Image</label>

                                <div class="col-sm-7">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                            <img data-src="holder.js/100%x100%" alt="...">
                                        </div>
                                        <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id=""
                                               name="image_url"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                <div class="col-sm-7">
                                    <select name="type" class="form-control selectpicker" required
                                            title="Select Type">
                                        <option value="0">Not Core Member</option>
                                        <option value="1">Core Member</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_secretary" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editSecretariesForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_Secretaries_Form" class="form-horizontal" name="editLawAndRuleForm" method="post"
                          action="" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Secretary Record</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="secretaries_id" name="secretary_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                    <div class="col-sm-7">
                                        <input name="name" type="text" id="secretaries_name" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 " style="font-weight: bold;">From Date</label>

                                    <div class="col-sm-7 date">
                                        <div class="input-group input-append date" id="edit_from_Secretaries_datePicker">
                                            <input type="text" class="form-control" id="secretaries_from_year" name="from_date"/>
                                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 " style="font-weight: bold;">To Date</label>

                                    <div class="col-sm-7 date">
                                        <div class="input-group input-append date" id="edit_to_Secretaries_datePicker">
                                            <input type="text" class="form-control" id="secretaries_to_year" name="to_date"/>
                                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Designation</label>

                                    <div class="col-sm-7">
                                        <input name="designation" id="secretaries_designation" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Qualification</label>

                                    <div class="col-sm-7">
                                        <input name="qualification" id="secretaries_qualification" type="text"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <input name="old_image_url" id="old_secretaries_image_url" type="hidden" class="form-control"
                            />

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Image</label>

                                    <div class="col-sm-7">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img data-src="holder.js/100%x100%" id="secretaries_image_url" alt="...">
                                            </div>
                                            <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id=""
                                               name="image_url"></span>
                                                <a href="#" class="btn btn-default fileinput-exists"
                                                   data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                    <div class="col-sm-7">
                                        <select name="type" class="form-control selectpicker" id="secretaries_type" required
                                                title="Select Type">
                                            <option value="0">Not Core Member</option>
                                            <option value="1">Core Member</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="edit_Secretaryform" value="Update">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'about_us_listing') { ?>
    <div id="addAboutUsForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="addSAboutusForm" method="POST" id="add_aboutus_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add New AboutUs</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                <div class="col-sm-7">
                                <textarea class="form-control " rows="2" cols="2" id="textedit"
                                          name="description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">File</label>

                                <div class="col-sm-7">
                                    <input type="file" name="link_url">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                <div class="col-sm-7">
                                    <select name="type" class="form-control selectpicker" required
                                            title="Select Type">
                                        <option value="1">Overview</option>
                                        <option value="2">Background</option>
                                        <option value="3">Policies</option>
                                        <option value="4">Functions</option>


                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_about_us" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editAboutUsForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_AboutUs_Form" class="form-horizontal" name="editLawAndRuleForm" method="post"
                          action="" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit About Us Record</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="aboutus_id" name="aboutus_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                    <div class="col-sm-7">
                                        <input name="name" id="aboutus_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                    <div class="col-sm-7">
                                     <textarea class="form-control " rows="2" cols="2" id="aboutus_description"
                                               name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Old File</label>

                                    <div class="col-sm-7">
                                        <input type="text" name="old_link_url" id="old_aboutus_file_url" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">File</label>

                                    <div class="col-sm-7">
                                        <input type="file" name="link_url">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                    <div class="col-sm-7">
                                        <select name="type" id="aboutus_type" class="form-control selectpicker" required
                                                title="Select Type">
                                            <option value="1">Overview</option>
                                            <option value="2">Background</option>
                                            <option value="3">Policies</option>
                                            <option value="4">Functions</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="edit_aboutusform" value="Update">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if ($file_name == 'poverty_alleviation') { ?>
    <div id="addpoverty_budgetForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="addSAboutusForm" method="POST" id="add_aboutus_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                <div class="col-sm-7">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                <div class="col-sm-7">
                                <textarea class="form-control " rows="2" cols="2" id="textedit"
                                          name="description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">File</label>

                                <div class="col-sm-7">
                                    <input type="file" name="link_url">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                <div class="col-sm-7">
                                    <select name="type" class="form-control selectpicker" required
                                            title="Select Type">
                                        <option value="1">Overview</option>
                                        <option value="2">Background</option>
                                        <option value="3">Policies</option>
                                        <option value="4">Functions</option>


                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_poverty" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editpoverty_budgetForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="col-lg-12  ">
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="edit_poverty_Form" class="form-horizontal" name="editLawAndRuleForm" method="post"
                          action="" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="poverty_id" name="poverty_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Name</label>

                                    <div class="col-sm-7">
                                        <input name="name" id="aboutus_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Description</label>

                                    <div class="col-sm-7">
                                     <textarea class="form-control " rows="2" cols="2" id="aboutus_description"
                                               name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Old File</label>

                                    <div class="col-sm-7">
                                        <input type="text" name="old_link_url" id="old_aboutus_file_url" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">File</label>

                                    <div class="col-sm-7">
                                        <input type="file" name="link_url">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3" style="font-weight: bold;">Type</label>

                                    <div class="col-sm-7">
                                        <select name="type" id="aboutus_type" class="form-control selectpicker" required
                                                title="Select Type">
                                            <option value="1">Overview</option>
                                            <option value="2">Background</option>
                                            <option value="3">Policies</option>
                                            <option value="4">Functions</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="edit_povertyform" value="Update">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php if ($file_name == 'provisional_budget') { ?>
    <div id="addprovisional_budgetForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <form name="addSAboutusForm" method="POST" id="add_aboutus_Form" class="form-horizontal" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Company Budget</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                <div class="col-sm-8">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                <div class="col-sm-8">
                                <textarea class="form-control " rows="2" cols="2" id="texteditorfroala"
                                          name="description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">File</label>

                                <div class="col-sm-8">
                                    <input type="file" name="link_url">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_new_about_us" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editprovisional_budgetForm" class="modal fade" role="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 80%">
            <div class="col-lg-12 ">

                <div class="modal-content">
                    <form id="edit_AboutUs_Form" class="form-horizontal" name="editLawAndRuleForm" method="post"
                          action="" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Company Budget</h4>
                        </div>
                        <div class="modal-body col-lg-offset-1">
                            <input type="hidden" id="aboutus_id" name="aboutus_id">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Name</label>

                                    <div class="col-sm-8">
                                        <input name="name" id="aboutus_name" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Description</label>

                                    <div class="col-sm-8">
                                     <textarea class="form-control " rows="2" cols="2" id="editeditorfroala"
                                               name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">Old File</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="old_link_url" id="old_aboutus_file_url" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2" style="font-weight: bold;">File</label>

                                    <div class="col-sm-8">
                                        <input type="file" name="link_url">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" name="edit_aboutusform" value="Update">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <!--sector camps-->
<?php if ($file_name == 'sector_camps') { ?>
    <div id="addsectorcampsForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="add_sector_camps" id="add_sector_camps" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Sector And Camps</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <select name="district_id"
                                            class="form-control "
                                            title="Select District">
                                        <option value="">Select District</option>
                                        <?php
                                        if (!empty($districts)) {
                                            foreach ($districts as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Flood Sectors</label>

                                <div class="col-sm-6">
                                    <input name="flood_sector" type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Flood Sub Sectors</label>

                                <div class="col-sm-6">
                                    <input name="flood_sub_sector" type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Flood Relief Camp</label>

                                <div class="col-sm-6">
                                    <input name="flood_relief_camp" type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Veterinary Officers</label>

                                <div class="col-sm-6">
                                    <input name="veterinary_officers" type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Veterinary Assistants</label>

                                <div class="col-sm-6">
                                    <input name="veterinary_assistants" type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Mobile Veterinary Dispensories</label>

                                <div class="col-sm-6">
                                    <input name="mobile_veterinary_dispenceries" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="add_sector_camps" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="editsectorForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="edit_sector_camps" id="edit_sector_camps" method="POST" action=""
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Sector And Camps</h4>
                    </div>
                    <div class="modal-body col-lg-offset-1">
                        <input type="hidden" name="sector_id" id="sector_id">


                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">District</label>

                                <div class="col-sm-6">
                                    <select name="district_id" id="district_id"
                                            class="form-control selectpicker "
                                            title="Select District">
                                        <option value="">Select District</option>
                                        <?php
                                        if (!empty($districts)) {
                                            foreach ($districts as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Flood Sectors</label>

                                <div class="col-sm-6">
                                    <input name="flood_sector" id="flood_sector" type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Flood Sub Sectors</label>

                                <div class="col-sm-6">
                                    <input name="flood_sub_sector" id="flood_sub_sector" type="text" class="form-control"
                                           data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Flood Relief Camp</label>

                                <div class="col-sm-6">
                                    <input name="flood_relief_camp" id="flood_relief_camp" type="text" class="form-control"
                                           data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Veterinary Officers</label>

                                <div class="col-sm-6">
                                    <input name="veterinary_officers" id="veterinary_officers" type="text"
                                           class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Veterinary Assistants</label>

                                <div class="col-sm-6">
                                    <input name="veterinary_assistants" id="veterinary_assistants" type="text"
                                           class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2" style="font-weight: bold;">Mobile Veterinary Dispensories</label>

                                <div class="col-sm-6">
                                    <input name="mobile_veterinary_dispenceries" id="mobile_veterinary_dispenceries"
                                           type="text" class="form-control" data-fv-integer="true"
                                           data-fv-integer-message="The value is not an integer"/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default edit_clearform" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" name="edit_sector_camps" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>