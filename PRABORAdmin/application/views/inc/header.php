<header>
    <!-- User Section Start -->

    <div class="user col-lg-6" style="margin-top: 0px; padding-top: 0px;">
        <figure>
            <a href="<?=base_url()?>index.php/dashboard/user_profile/<?=$user_id;?>"><img src="<?=base_url() ?>assets/images/avatar11.jpg" alt="Adminise" /></a>
        </figure>
        <div class="welcome">
            <p>Welcome</p>
            <h5><a href="<?=base_url()?>index.php/dashboard/user_profile/<?=$user_id;?>"><?=ucfirst($name);?></a></h5>
        </div>
    </div>
    <!-- User Section End -->
    <!-- Search Section Start -->

    <!-- Search Section End -->
    <!-- Header Top Navigation Start -->
    <div class="col-lg-6 pull-right"  style="margin-top: 0px; padding-top: 0px;">
    <nav class="topnav">
        <ul id="nav1">
            <li class="settings">
                <a href="#"><i class="glyphicon glyphicon-wrench"></i>Settings</a>
                <div class="popdown popdown-right settings">
                    <nav>
                        <a href="<?=base_url()?>index.php/dashboard/user_profile/<?=$user_id;?>"><i class="glyphicon glyphicon-user"></i>Your Profile</a>
                        <a href="<?= base_url()?>index.php/users/edit_user_account/<?=$user_id;?>"><i class="glyphicon glyphicon-pencil"></i>Edit Account</a>
                        <a href="#" data-toggle="modal" class="change_pass"><i class="glyphicon glyphicon-lock"></i>Change Password</a>
                        <a href="#"><i class="glyphicon glyphicon-question-sign"></i>Get Help</a>
                        <a href="<?=base_url() ?>index.php/login/logout"><i class="glyphicon glyphicon-log-out"></i>Log out</a>
                    </nav>
                </div>
            </li>
        </ul>
    </nav>
    </div>
    <!-- Header Top Navigation End -->
    <div class="clearfix"></div>
</header>