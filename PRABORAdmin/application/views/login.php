<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin Panel</title>
        <!--// Stylesheets //-->
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet" media="screen" />
        <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet" media="screen" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/icheck.min.js"></script>
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- Wrapper Start -->
        <div class="loginwrapper">
            <div class="loginone">
                <header>
                    <p style="color: white;font-size: 28px; font-weight: bold;">BOR Admin Panel</p>
                    <p>Enter your credentials to login to your account</p>
                </header>
                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="alert  alert-danger">Please provide correct information</div>
                <?php } ?>
                <form method="post" action="<?= base_url() ?>index.php/login/check_user">
                    <div class="username">
                        <input type="text" name="identity" class="form-control" placeholder="Enter your username" />
                        <i class="glyphicon glyphicon-user"></i>
                    </div>
                    <div class="password">
                        <input type="password" name="password" class="form-control" placeholder="Enter your password" />
                        <i class="glyphicon glyphicon-lock"></i>
                    </div>
                    <div class="custom-radio-checkbox">
                        <input  type="hidden" value="0" name="rememberme" class="bluecheckradios">
                        <input tabindex="1" type="checkbox" value="1" name="rememberme" class="bluecheckradios">
                        <label>Remember me</label>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $('.bluecheckradios').iCheck({
                                checkboxClass: 'icheckbox_flat-blue',
                                radioClass: 'iradio_flat-blue',
                                increaseArea: '20%' // optional
                            });
                        });
                    </script>
                    <input type="submit" class="btn btn-primary style2" value="Sign In" />
                </form>
<!--                <footer>
                    <a href="#" class="forgot pull-left">I forgot my password</a>
                    <a href="#" class="register pull-right">Create account</a>
                    <div class="clear"></div>
                </footer>-->
            </div>
        </div>
        <!-- Wrapper End -->

    </body>
</html>
