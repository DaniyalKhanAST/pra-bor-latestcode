<style>
    .datepicker{z-index:9999 !important}
</style>

<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_squad_where_about_btn" data-toggle="modal">Add
                    Squad Where About</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Squad Where About Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Date</th>
                                <th>Where About</th>
                                <th>Remarks</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($squad_where_about)) {
                                $i = 1;
                                foreach ($squad_where_about as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td><?= $row->designation ?></td>
                                        <td><?= $row->date ?></td>
                                        <td><?= $row->where_about ?></td>
                                        <td><?= $row->remarks ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a type="submit" class=" editsquad_where_about"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-squad_where_about_name="<?= $row->name ?>"
                                                           data-squad_where_about_designation="<?= $row->designation ?>"
                                                           data-squad_where_about_date="<?= $row->date ?>"
                                                           data-squad_where_about="<?= $row->where_about ?>"
                                                           data-squad_where_about_remarks="<?= $row->remarks ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_squad_where_about/<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                        <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>

                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addsquad_where_aboutForm').modal('toggle');
                                    $('#add_squad_where_aboutForm')[0].reset();
                                    $('#add_squad_where_aboutForm').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#edit_squad_where_aboutForm').modal('toggle');
                                    $('#edit_squad_where_about')[0].reset();
                                    $('#edit_squad_where_about').data('formValidation').resetForm();
                                });
                                $(".add_squad_where_about_btn").click(function () {
                                    $('#addsquad_where_aboutForm').modal('show');
                                });
                                //        squad where about ////
                                $(".editsquad_where_about").click(function () {
                                    $("#squad_where_about_id").val($(this).data('id'));
                                    $("#squad_where_about_name").val($(this).data('squad_where_about_name'));
                                    $("#squad_where_about_designation").val($(this).data('squad_where_about_designation'));
                                    $("#squad_where_about_date").val($(this).data('squad_where_about_date'));
                                    $("#squad_where_about").val($(this).data('squad_where_about'));
                                    $("#squad_where_about_remarks").val($(this).data('squad_where_about_remarks'));
                                    $('#edit_squad_where_aboutForm').modal('show');
                                });
//                                $('#squad_where_about_date').datepicker({dateFormat: "yyyy-mm-dd"}).val();

                            });

                        </script>


                        <script>
                            $(document).ready(function () {
                                $('.datepicker').datepicker({
                                    format: 'yyyy-mm-dd',
                                    autoclose: true
                                })

                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>