<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_aboutus_record_btn" data-toggle="modal">Add
                    Company Budget Record</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Company Budget Listing</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>name</th>
                                <th>Description</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($aboutus_listing)) {
                                $i = 1;
                                foreach ($aboutus_listing as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name; ?></td>
                                        <td><?= $row->description; ?></td>
                                        <td><?php if(!empty($row->link_url)){ ?><a href="<?= $row->link_url ?>" >View</a> <?php } ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class=" edit_aboutus_listing_detail"
                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                           data-aboutus_name="<?= $row->name ?>"
                                                           data-aboutus_description='<?= $row->description ?>'
                                                           data-aboutus_file_url="<?= $row->link_url ?>"
                                                           data-aboutus_type="<?= $row->type ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_provisional_budget?aboutus_id=<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#addAboutUsForm').modal('toggle');
                                    $('#add_aboutus_Form')[0].reset();
                                    $('#add_aboutus_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editAboutUsForm').modal('toggle');
                                    $('#edit_AboutUs_Form')[0].reset();
                                    $('#edit_AboutUs_Form').data('formValidation').resetForm();
                                });
                                $('#aboutus_description').wysihtml5();

                                $(".add_aboutus_record_btn").click(function () {
                                    $('#addprovisional_budgetForm').modal('show');
                                });
                                //        squad where about ////
                                $(".edit_aboutus_listing_detail").click(function () {
                                    $("#aboutus_id").val($(this).data('id'));
                                    $("#aboutus_name").val($(this).data('aboutus_name'));
                                    $('#editeditorfroala').froalaEditor('html.set',$(this).data('aboutus_description') );
                                    $("#aboutus_type").val($(this).data('aboutus_type'));
                                    $("#aboutus_type").selectpicker('refresh');
                                    $("#old_aboutus_file_url").val($(this).data('aboutus_file_url'));
                                    $("#aboutus_file_url").attr('src', $(this).data('aboutus_file_url'));
                                    $('#editprovisional_budgetForm').modal('show');
                                });

                                $('#texteditorfroala').froalaEditor({
                                    heightMin: 300
                                });
                                $('#editeditorfroala').froalaEditor({
                                    heightMin: 300
                                });

                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>