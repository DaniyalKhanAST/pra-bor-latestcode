<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_development_btn" data-toggle="modal" >Add development</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Development Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($development)) {
                                $i = 1;
                                foreach ($development as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->title ?></td>
                                        <td><img src="<?= $row->image_url ?>" height="100px" width="100px" ></td>
                                        <td><?= $row->description ?></td>
                                        <td><?php if($row->isactive == 1){echo"Active";}else{ echo"In-Active";} ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li> <a href="<?= base_url() ?>index.php/dashboard/edit_development?development_id=<?= $row->id ?>"  >Edit</a></li>
                                                    <li>  <a href="<?= base_url() ?>index.php/dashboard/delete_development?development_id=<?= $row->id ?>"  onclick="return confirm('Are you sure to delete');" >Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });



                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init")
                                    {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "")
                                    {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#adddevelopmentForm').modal('toggle');
                                    $('#add_baner')[0].reset();
                                    $('#add_baner').data('formValidation').resetForm();
                                });


                            });
                            $(".add_development_btn").click(function () {
                                $('#adddevelopmentForm').modal('show');
                            });






                        </script>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>