<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_project_btn" data-toggle="modal">Add
                    Project</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Projects Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="tabs-section">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#new_project" data-toggle="tab">New Project</a></li>
                            <li><a href="#ongoing_project" data-toggle="tab">Ongoing Project</a></li>
                            <li><a href="#completed_project" data-toggle="tab">Completed Project</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="new_project">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>GS No</th>
                                                <th>Name of Scheme</th>
                                                <th>Status</th>
                                                <th>Cost</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($new_project)) {
                                                $i = 1;
                                                foreach ($new_project as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->GS_no; ?></td>
                                                        <td><?= $row->name_of_scheme; ?>

                                                        </td>
                                                        <td><?php if ($row->status == 0) {
                                                                echo "Approved";
                                                            } else {
                                                                echo "Un-Approved";
                                                            } ?></td>
                                                        <td><?= $row->cost_in_million ?></td>
                                                        <td class="btn-group" style="">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class="edit_project"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-name_of_scheme="<?= $row->name_of_scheme ?>"
                                                                           data-is_completed="<?= $row->is_completed ?>"
                                                                           data-pro_status="<?= $row->status ?>"
                                                                           data-project_cat_id="<?= $row->project_cat_id ?>"
                                                                           data-proj_gs_no="<?= $row->GS_no ?>"
                                                                           data-cost_in_million="<?= $row->cost_in_million ?>"
                                                                           data-pro_duration="<?= $row->duration ?>"
                                                                           data-to_year="<?= $row->to_year ?>"
                                                                           data-from_year="<?= $row->from_year ?>"
                                                                           data-link_type="<?= $row->link_type ?>"
                                                                           data-link_url="<?= $row->link_url ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_project?project_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });


                                                /*
                                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                                 * the footer
                                                 */
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });

                                                $(".add_pro_cat_btn").click(function () {
                                                    $('#add_Pro_cat').modal('show');
                                                });
                                                $(".edit_pro_cat").click(function () {
                                                    $("#p_cat_id").val($(this).data('id'));
                                                    $("#p_cat_name").val($(this).data('name'));
                                                    $('#editProCat').modal('show');
                                                });
                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane" id="ongoing_project">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example2">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>GS No</th>
                                                <th>Name of Scheme</th>
                                                <th>Status</th>
                                                <th>Cost</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($ongoing_project)) {
                                                $i = 1;
                                                foreach ($ongoing_project as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->GS_no; ?></td>
                                                        <td><?= $row->name_of_scheme; ?>
                                                        </td>
                                                        <td><?php if ($row->status == 0) {
                                                                echo "Approved";
                                                            } else {
                                                                echo "Un-Approved";
                                                            } ?></td>
                                                        <td><?= $row->cost_in_million ?></td>
                                                        <td class="btn-group" style="">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class=" edit_project"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-name_of_scheme="<?= $row->name_of_scheme ?>"
                                                                           data-is_completed="<?= $row->is_completed ?>"
                                                                           data-pro_status="<?= $row->status ?>"
                                                                           data-project_cat_id="<?= $row->project_cat_id ?>"
                                                                           data-proj_gs_no="<?= $row->GS_no ?>"
                                                                           data-cost_in_million="<?= $row->cost_in_million ?>"
                                                                           data-pro_duration="<?= $row->duration ?>"
                                                                           data-to_year="<?= $row->to_year ?>"
                                                                           data-from_year="<?= $row->from_year ?>"
                                                                           data-link_type="<?= $row->link_type ?>"
                                                                           data-link_url="<?= $row->link_url ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_project?project_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example2').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });


                                                /*
                                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                                 * the footer
                                                 */
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });
                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane" id="completed_project">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example3">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Duration</th>
                                                <th>Status</th>
                                                <th>Name of Scheme</th>
                                                <th>Cost</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($completed_project)) {
                                                $i = 1;
                                                foreach ($completed_project as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->duration . ' Year'; ?></td>
                                                        <td> (<?= $row->from_year; ?> to <?= $row->to_year; ?>)</td>
                                                        <td><?= $row->name_of_scheme; ?> </td>
                                                        <td><?= $row->cost_in_million ?></td>
                                                        <td class="btn-group" style="width: 125px">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class=" edit_project"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-name_of_scheme="<?= $row->name_of_scheme ?>"
                                                                           data-is_completed="<?= $row->is_completed ?>"
                                                                           data-pro_status="<?= $row->status ?>"
                                                                           data-project_cat_id="<?= $row->project_cat_id ?>"
                                                                           data-proj_gs_no="<?= $row->GS_no ?>"
                                                                           data-cost_in_million="<?= $row->cost_in_million ?>"
                                                                           data-pro_duration="<?= $row->duration ?>"
                                                                           data-to_year="<?= $row->to_year ?>"
                                                                           data-from_year="<?= $row->from_year ?>"
                                                                           data-link_type="<?= $row->link_type ?>"
                                                                           data-link_url="<?= $row->link_url ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_project?project_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example3').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });


                                                /*
                                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                                 * the footer
                                                 */
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });

                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>
    $(document).ready(function () {
    $('.clearform').click(function () {
        $('#addProjectForm').modal('toggle');
        $('#add_project_form')[0].reset();
        $('#add_project_form').data('formValidation').resetForm();
    });
        $('.edit_clearform').click(function () {
            $('#edit_project').modal('toggle');
            $('#edit_project_form')[0].reset();
            $('#edit_project_form').data('formValidation').resetForm();
        });

        $(document).on('change','.link_type_radio',function(){
            if($(this).val() == 1){
                $('.link_url_class').attr('type','file');
            }else{
                $('.link_url_class').attr('type','text');
            }
        });
    });

</script>
<script>

    $(".add_project_btn").click(function () {
        $('#addProjectForm').modal('show');
    });
    $(".edit_project").click(function () {

        $("#pro_id_hidden").val($(this).data('id'));
        $("#project_type_id_updated").val($(this).data('project_cat_id'));
        $("#pro_gs_no_updated").val($(this).data('proj_gs_no'));
        $("#pro_scheme_name").val($(this).data('name_of_scheme'));
        $("#pro_cost_million").val($(this).data('cost_in_million'));
        $("#pro_status").val($(this).data('pro_status'));
        $("#pro_status").selectpicker('refresh');
        $("#pro_duration").val($(this).data('pro_duration'));
        $("#pro_duration").selectpicker('refresh');
        $("#pro_from_year").val($(this).data('from_year'));
        $("#pro_from_year").selectpicker('refresh');
        $("#pro_to_year").val($(this).data('to_year'));
        $("#pro_to_year").selectpicker('refresh');
        $('.old_link_url').remove();
        if($(this).data('link_type')==1){
            $("#edit_project_form .link_type_radio[value=1]").attr('checked','checked');
            $('#edit_project_form .link_type_radio[value="1"]').trigger('change');
        }else{
            $("#edit_project_form .link_type_radio[value=2]").attr('checked','checked');
            $('#edit_project_form .link_type_radio[value="2"]').trigger('change');
            $("#edit_project_form .link_url_class").val($(this).data('link_url'));
        }
        if($(this).data('link_type')==1 && $(this).data('link_url') != undefined){
            $('<a href="'+$(this).data('link_url')+'" target="_blank" style="color:blue;" class="old_link_url">View Old File</a>').insertAfter('#edit_project_form .link_url_class');
        }
        var pro_cats = $(this).data('project_cat_id');

        if (pro_cats == 1) {
            $('.pro_GS_status').show();
            $('.gs_no').attr("disabled", false);
            $('.sceheme_status').show();
            $('.sceh_status').attr("disabled", false);
            $('.project_duration').hide();
            $('.pro_duration').attr("disabled", "disabled");
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        } else if (pro_cats == '2') {
            $('.pro_GS_status').show();
            $('.gs_no').attr("disabled", false);
            $('.sceheme_status').show();
            $('.sceh_status').attr("disabled", false);
            $('.project_duration').hide();
            $('.pro_duration').attr("disabled", "disabled");
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        } else {
            $('.pro_GS_status').hide();
            $('.gs_no').attr("disabled", "disabled");
            $('.sceheme_status').hide();
            $('.sceh_status').attr("disabled", "disabled");
            $('.project_duration').show();
            $('.pro_duration').attr("disabled", false);
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        }
        $('#edit_project').modal('show');

    });


</script>

<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });

    });
</script>