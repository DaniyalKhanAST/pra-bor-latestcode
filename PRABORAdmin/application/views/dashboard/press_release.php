<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_pr_btn" data-toggle="modal" >Add News/Events</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">News/Events</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image Link</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($press_release)) {
                                $i = 1;
                                foreach ($press_release as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td><?= $row->description ?></td>
                                        <td><?= $row->image_link ?></td>

                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="" class="edit_press_modal" data-toggle="modal" data-press_id="<?=$row->id ?>" data-press_name="<?=$row->name ?>" data-press_description="<?=$row->description ?>"  data-image_link="<?=$row->image_link ?>" >Edit</a></li>
                                                    <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_press?press_id=<?= $row->id ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });



                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init")
                                    {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "")
                                    {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#add_pressform').modal('toggle');
                                    $('#add_press_form')[0].reset();
                                    $('#add_press_form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editpressform').modal('toggle');
                                    $('#edit_press_form')[0].reset();
                                    $('#edit_press_form').data('formValidation').resetForm();
                                });

                            });
                            $(".add_pr_btn").click(function () {
                                $('#add_pressform').modal('show');
                            });
                            $(".edit_press_modal").click(function () {
                                console.log("he");
                                $("#press_ids").val($(this).data('press_id'));
                                $("#press_names").val($(this).data('press_name'));
                                $("#press_descriptions").val($(this).data('press_description'));
                                $("#image_links").val($(this).data('image_link'));

                                $('#editpressform').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>