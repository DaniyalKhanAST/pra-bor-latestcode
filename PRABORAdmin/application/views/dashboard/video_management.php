<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_video_btn" data-toggle="modal" >Add Video</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Video Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Video Link</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($video_fetch)) {
                                $i = 1;
                                foreach ($video_fetch as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name?></td>
                                        <td><?= $row->video_link ?></td>
                                        <td><?= $row->description ?></td>
                                        <td><img src="<?= $row->image_url ?>" height="100px" width="100px" ></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                   <li> <a type="button" class=" editvideo" data-toggle="modal" data-id="<?=$row->id ?>" data-name="<?=$row->name ?>" data-video_link="<?=$row->video_link ?> " data-description="<?=$row->description ?>" data-image_url="<?=$row->image_url ?>" value="Edit">Edit</a></li>
                                                    <li><a href="<?= base_url() ?>index.php/dashboard/delete_video/<?= $row->id ?>" class="" onclick="return confirm('Are you sure to delete?')" >Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });



                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init")
                                    {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "")
                                    {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#addvideoForm').modal('toggle');
                                    $('#add_video_form')[0].reset();
                                    $('#add_video_form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editvideo').modal('toggle');
                                    $('#edit_video_form')[0].reset();
                                    $('#edit_video_form').data('formValidation').resetForm();
                                });

                                $(".add_video_btn").click(function () {
                                    $('#addvideoForm').modal('show');
                                });
                                $(".editvideo").click(function () {

                                    $("#id").val($(this).data('id'));
                                    $("#name").val($(this).data('name'));
                                     $("#old_image_url").val($(this).data('image_url'));
                                     $("#selected_image_url").attr('src', $(this).data('image_url'));

                                    $("#video_link").val($(this).data('video_link'));
                                    $("#description").val($(this).data('description'));
                                    $('#editvideo').modal('show');
                                });
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>
