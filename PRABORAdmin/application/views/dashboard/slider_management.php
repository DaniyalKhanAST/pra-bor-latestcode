<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-success add_slider_img_btn" data-toggle="modal" >Add Slider Image</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Slider Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="slider_banner_dt">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Priority</th>
                                <th>Slider Number</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($slider_images)) {
                                $i = 1;
                                foreach ($slider_images as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->title ?></td>
                                        <td><img src="<?=base_url().$row->banner_url ?>" height="100px" width="100px" ></td>
                                        <td><?=$row->priority ?></td>
                                        <td><?=$row->slider_number ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>  <a href="<?= base_url() ?>index.php/dashboard/delete_slider_banner/<?= $row->id ?>/slider"  onclick="return confirm('Are you sure to delete');" >Delete</a></li>
                                                    <li>  <a href="#" class="add_slider_priority_btn" id="<?='id_'.$row->id.'_'.$row->priority?>" >Set Priority</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#slider_banner_dt').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                            });
                            $(".add_slider_img_btn").click(function () {
                                $('.add_slider_banner').formValidation('resetForm');
                                $('.addSliderBannerForm').modal('show');
                            });

                            $(".add_slider_priority_btn").click(function () {
                                $('#add_slider_priority')[0].reset();
                                var main_slider_id_mixed=$(this).attr('id');
                                main_slider_id=main_slider_id_mixed.split('_');
                                if(main_slider_id[2]!=undefined && main_slider_id[2]!=="_"){
                                    $('#main_slider_priority_value').val(main_slider_id[2]);
                                }
                                $('#main_slider_id_priority').val(main_slider_id[1]);
                                $('#add_slider_priority').formValidation('resetForm');
                                $('#addSliderBannerPriorityForm').modal('show');

                            });
                        </script>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>