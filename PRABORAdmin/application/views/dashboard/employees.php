<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="<?= base_url() ?>index.php/dashboard/add_employee" class="btn btn-success" data-toggle="modal" >Add Employee</a></div>
            <div class="pull-right"><a href="<?= base_url() ?>index.php/dashboard/import_employee" class="btn btn-success">Upload File</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Employee Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="employee_dt">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>CNIC</th>
                                <th>Email</th>
                                <th>Mobile No</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($employees)) {
                                $i = 1;
                                foreach ($employees as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td><?=$row->cnic ?></td>
                                        <td><?=$row->email ?></td>
                                        <td><?=$row->mobile ?></td>
                                        <td><?=($row->is_active=='1')? 'Active':"In Active"?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>  <a href="<?= base_url() ?>index.php/dashboard/edit_employee/<?= $row->id ?>" class="edit_employee_btn">Edit</a></li>
                                                    <li>  <a href="<?= base_url() ?>index.php/dashboard/delete_employee/<?= $row->id ?>"  onclick="return confirm('Are you sure to delete');" >Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#employee_dt').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                            });
                        </script>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>