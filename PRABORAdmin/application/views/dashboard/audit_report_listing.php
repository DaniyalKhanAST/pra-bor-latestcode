<style>
    .datepicker{z-index:9999 !important}
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_audit_report_btn" data-toggle="modal" >Add Audit Report</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Audit Reports</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Year</th>
                                    <th>Pdf File</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($audit_reports)) {
                                    $i = 1;
                                    foreach ($audit_reports as $row) {
                                        ?>
                                        <tr class="">
                                            <td><?= $i ?></td>
                                            <td><?= $row->title ?></td>
                                            <td><?= $row->year ?></td>
                                            <td><a href="<?= $row->pdf_link	 ?>" >View</a></td>
                                            <td class="btn-group" style="">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" class="edit_audit" data-toggle="modal" data-audit_report_id="<?=$row->id ?>" data-title="<?=$row->title ?>"  data-year="<?=$row->year ?>" data-audit_pdf_link="<?=$row->pdf_link ?>" value="Edit">Edit</a></li>
                                                        <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_audit_report?report_id=<?= $row->id ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                        <script>
                                        var asInitVals = new Array();
                                        $(document).ready(function () {
                                            var oTable = $('#example').dataTable({
                                                "oLanguage": {
                                                    "sSearch": "Search all columns:"
                                                }
                                            });

                                            $("tfoot input").keyup(function () {
                                                /* Filter on the column (the index) of this element */
                                                oTable.fnFilter(this.value, $("tfoot input").index(this));
                                            });



                                            /*
                                             * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                             * the footer
                                             */
                                            $("tfoot input").each(function (i) {
                                                asInitVals[i] = this.value;
                                            });

                                            $("tfoot input").focus(function () {
                                                if (this.className == "search_init")
                                                {
                                                    this.className = "";
                                                    this.value = "";
                                                }
                                            });

                                            $("tfoot input").blur(function (i) {
                                                if (this.value == "")
                                                {
                                                    this.className = "search_init";
                                                    this.value = asInitVals[$("tfoot input").index(this)];
                                                }
                                            });
                                            $('.clearform').click(function () {
                                                $('#addAuctionForm').modal('toggle');
                                                $('#add_audit_report_form')[0].reset();
                                                $('#add_audit_report_form').data('formValidation').resetForm();
                                            });
                                            $('.edit_audit').click(function () {
                                                $('#editAuditReportForm').modal('toggle');
                                            });

                                        });
                                        $(".add_audit_report_btn").click(function () {
                                            $('#addReportForm').modal('show');
                                        });
                                        $(".edit_audit").click(function () {
                                            $("#audit_report_id").val($(this).data('audit_report_id'));
                                            $("#title").val($(this).attr('data-title'));
                                            $("#year").val($(this).data('year'));
                                            // $("#audit_pdf_link").val($(this).data('audit_pdf_link'));
                                            $('#editAuctionForm').modal('show');
                                        });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>