<div class="container-liquid">
    <div class="col-md-9">
        <header>
            <h1 class="page-title">Edit Banner</h1>
        </header>
        <form id="edit_baner" method="post" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12 col-lg-offset-1">
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Title</label>
                            <div class="col-sm-8">
                                <input name="title" value="<?= $banner->title ?>" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">New Banner</label>
                            <div class="form-group col-sm-8">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                         style="width: 200px; height: 150px;">
                                        <img data-src="holder.js/100%x100%" src="<?= $banner->image_url ?>" alt="...">
                                    </div>
                                    <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="banner_url"
                                               name="banner_url"></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Previous Banner</label>
                            <div class="col-sm-8">
                                <img src="<?= $banner->image_url ?>" style="width: 100px; height: 100px;">
                                <input name="old_banner_url" value="<?= $banner->image_url ?>" type="hidden" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Discription</label>
                            <div class="col-sm-8">
                                <input name="description" value="<?= $banner->description ?>" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Status</label>
                            <div class="">
                                <div class="col-sm-1 ">
                                    <input  type="radio" class="bluecheckradios"
                                           name="isactive" <?php if($banner->isactive == 1){echo"checked";}else{} ?> value="1">
                                </div>
                                <div class="col-sm-2">
                                    <label for="square-radio-disabled-checked">Active</label>
                                </div>
                                <div class="col-sm-1 ">
                                    <input  type="radio" <?php if($banner->isactive == 0){echo"checked";}else{} ?>  class="bluecheckradios"  name="isactive"
                                           value="0">
                                </div>
                                <div class="col-sm-1 ">
                                    <label for="square-radio-disabled">InActive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <input type="submit" class="btn btn-primary pull-right" name="edit_banner" value="Edit Banner">
            </div>
        </form>
        <!--/#form-submit-->
    </div><!-- /.col -->
</div>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
        $('.clearform').click(function () {
            $('#editbaner').modal('toggle');
            $('#edit_baner')[0].reset();
            $('#edit_baner').data('formValidation').resetForm();
        });


    });
</script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>



