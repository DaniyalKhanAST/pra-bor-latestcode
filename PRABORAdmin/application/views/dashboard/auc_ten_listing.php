<style>
    .datepicker{z-index:9999 !important}
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_auct_tend_btn" data-toggle="modal" >Add Tender</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Tenders</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Department</th>
                                    <th>Name</th>
                                    <th>Closing Date</th>
                                    <th>Status</th>
                                    <th>Pdf File</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($auction)) {
                                    $i = 1;
                                    foreach ($auction as $row) {
                                        ?>
                                        <tr class="">
                                            <td><?= $i ?></td>
                                            <td><?= $row->type_name ?></td>
                                            <td><?= $row->directorate_name ?></td>
                                            <td><?= $row->name ?></td>
                                            <td><?= $row->closing_date ?></td>
                                            <td><?php if($row->status == 1){echo"Open";}else{echo"Close";} ?></td>
                                            <td><a href="<?= $row->pdf_link	 ?>" >View</a></td>
                                            <td class="btn-group" style="">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" class="edit_auc" data-toggle="modal" data-auc_id="<?=$row->id ?>" data-auc_name="<?=$row->name ?>"  data-auc_type="<?=$row->type ?>" data-auc_directorate="<?=$row->directorate_id ?>" data-auc_closing_date="<?=$row->closing_date ?>" data-auc_status="<?=$row->status ?>" data-auc_pdf="<?=$row->pdf_link ?>" value="Edit">Edit</a></li>
                                                        <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_auction?auc_id=<?= $row->id ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                        <script>
                                        var asInitVals = new Array();
                                        $(document).ready(function () {
                                            var oTable = $('#example').dataTable({
                                                "oLanguage": {
                                                    "sSearch": "Search all columns:"
                                                }
                                            });

                                            $("tfoot input").keyup(function () {
                                                /* Filter on the column (the index) of this element */
                                                oTable.fnFilter(this.value, $("tfoot input").index(this));
                                            });



                                            /*
                                             * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                             * the footer
                                             */
                                            $("tfoot input").each(function (i) {
                                                asInitVals[i] = this.value;
                                            });

                                            $("tfoot input").focus(function () {
                                                if (this.className == "search_init")
                                                {
                                                    this.className = "";
                                                    this.value = "";
                                                }
                                            });

                                            $("tfoot input").blur(function (i) {
                                                if (this.value == "")
                                                {
                                                    this.className = "search_init";
                                                    this.value = asInitVals[$("tfoot input").index(this)];
                                                }
                                            });
                                            $('.clearform').click(function () {
                                                $('#addAuctionForm').modal('toggle');
                                                $('#add_auction_form')[0].reset();
                                                $('#add_auction_form').data('formValidation').resetForm();
                                            });
                                            $('.edit_clearform').click(function () {
                                                $('#editAuctionForm').modal('toggle');
                                                $('#edit_auction_form')[0].reset();
                                                $('#edit_auction_form').data('formValidation').resetForm();
                                            });

                                        });
                                        $(".add_auct_tend_btn").click(function () {
                                            $('#addAuctionForm').modal('show');
                                        });
                                        $(".edit_auc").click(function () {
                                            $("#auc_id_hidden").val($(this).data('auc_id'));
                                            $("#auc_name").val($(this).data('auc_name'));
                                            $("#auc_type_id").val($(this).data('auc_type'));
                                            $("#auc_type_id").selectpicker('refresh');
                                            $("#auc_directorate_id").val($(this).data('auc_directorate'));
                                            $("#auc_directorate_id").selectpicker('refresh');
                                            $("#auc_closing_date").val($(this).data('auc_closing_date'));
                                            var auc_status = $(this).data('auc_status');
                                            if(auc_status == 1){
                                                $('#auc_open_status').iCheck('check');
                                            }else{
                                                $('#auc_close_status').iCheck('check');
                                            }
                                            $("#old_auc_pdf_link").val($(this).data('auc_pdf'));
                                            $('#editAuctionForm').modal('show');
                                        });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>