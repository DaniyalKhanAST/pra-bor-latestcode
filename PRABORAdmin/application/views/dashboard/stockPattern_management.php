<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_stockpattern_btn" data-toggle="modal">Add
                    Stock Pattern</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Stock Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Herd Size</th>
                                <th>Quantity(Pak)</th>
                                <th>Quantity(Punjab)</th>
                                <th>Source</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($stocks)) {
                                $i = 1;
                                foreach ($stocks as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->herd_size ?></td>
                                        <td><?= $row->pak_qty ?></td>
                                        <td><?= $row->punjab_qty ?></td>
                                        <td><?= $row->source ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_stockpattren" data-toggle="modal"
                                                           data-id="<?= $row->id ?>"
                                                           data-herd_size="<?= $row->herd_size ?>"
                                                           data-pak_qty="<?= $row->pak_qty ?>"
                                                           data-punjab_qty="<?= $row->punjab_qty ?>"
                                                           data-p_source="<?= $row->source ?>" value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_stockpattern?stock_id=<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#addStockForm').modal('toggle');
                                    $('#add_Stock_Form')[0].reset();
                                    $('#add_Stock_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editStockForm').modal('toggle');
                                    $('#edit_Stock_Form')[0].reset();
                                    $('#edit_Stock_Form').data('formValidation').resetForm();
                                });
                            });
                            $(".add_stockpattern_btn").click(function () {
                                $('#addStockForm').modal('show');
                            });
                            $(".edit_stockpattren").click(function () {
                                $("#stock_id_hidden").val($(this).data('id'));
                                $("#supdate_pak_qty").val($(this).data('pak_qty'));
                                $("#update_herd_size").val($(this).data('herd_size'));
                                $("#update_herd_size").selectpicker('refresh');
                                $("#supdate_punjab_qty").val($(this).data('punjab_qty'));
                                $("#supdate_source").val($(this).data('p_source'));
                                $('#editStockForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>