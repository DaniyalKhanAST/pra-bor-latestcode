<div class="container-liquid">
    <div class="col-md-9">
        <header>
            <h1 class="page-title">Edit organogram</h1>
        </header>
        <form id="edit_baner" method="post" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12 col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3" style="font-weight: bold;">Type</label>

                            <div class="col-sm-8">
                                <select name="category" class="form-control "
                                        title="Select Directorate">
                                    <option value="">Select Type</option>
                                    <option value="1" <?php if($organogram->category == 1){echo("selected");}?>>Secretariat</option>
                                    <option value="2" <?php if($organogram->category == 2){echo("selected");}?>>DG Extension</option>
                                    <option value="3" <?php if($organogram->category == 3){echo("selected");}?> >DG Research</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Title</label>
                            <div class="col-sm-8">
                                <input name="title" value="<?= $organogram->title ?>" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">New organogram</label>
                            <div class="form-group col-sm-8">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                         style="width: 200px; height: 150px;">
                                        <img data-src="holder.js/100%x100%" src="<?= $organogram->image_url ?>" alt="...">
                                    </div>
                                    <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="organogram_url"
                                               name="organogram_url"></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Previous organogram</label>
                            <div class="col-sm-8">
                                <img src="<?= $organogram->image_url ?>" style="width: 100px; height: 100px;">
                                <input name="old_organogram_url" value="<?= $organogram->image_url ?>" type="hidden" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Discription</label>
                            <div class="col-sm-8">
                                <input name="description" value="<?= $organogram->description ?>" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Status</label>
                            <div class="">
                                <div class="col-sm-1 ">
                                    <input  type="radio" class="bluecheckradios"
                                            name="isactive" <?php if($organogram->isactive == 1){echo"checked";}else{} ?> value="1">
                                </div>
                                <div class="col-sm-2">
                                    <label for="square-radio-disabled-checked">Active</label>
                                </div>
                                <div class="col-sm-1 ">
                                    <input  type="radio" <?php if($organogram->isactive == 0){echo"checked";}else{} ?>  class="bluecheckradios"  name="isactive"
                                            value="0">
                                </div>
                                <div class="col-sm-1 ">
                                    <label for="square-radio-disabled">InActive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <input type="submit" class="btn btn-primary pull-right" name="edit_organogram" value="Edit organogram">
            </div>
        </form>
        <!--/#form-submit-->
    </div><!-- /.col -->
</div>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
        $('.clearform').click(function () {
            $('#editbaner').modal('toggle');
            $('#edit_baner')[0].reset();
            $('#edit_baner').data('formValidation').resetForm();
        });


    });
</script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>



