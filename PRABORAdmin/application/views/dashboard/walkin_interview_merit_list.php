<style>
    .datepicker{z-index:9999 !important}
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_ml_btn" data-toggle="modal" >Add Walk-in Interview Merit List</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Walk-in Interview Merit List</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>District</th>
                                    <th>Description</th>
                                    <th>Pdf File</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($walkin_interview_merit_list)) {
                                    $i = 1;
                                    foreach ($walkin_interview_merit_list as $row) {
                                        ?>
                                        <tr class="">
                                            <td><?= $i ?></td>
                                            <td><?= $row->district ?></td>
                                            <td><?= $row->description ?></td>
                                            <td><a href="<?= $row->pdf_link	 ?>" >View</a></td>
                                            <td class="btn-group" style="">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" class="edit_ml" data-toggle="modal" data-ml_id="<?=$row->id ?>" data-ml_district="<?=$row->district ?>" data-ml_description="<?=$row->description ?>"  data-ml_pdf="<?=$row->pdf_link ?>" value="Edit">Edit</a></li>
                                                        <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_walkin_interview_merit_list?ml_id=<?= $row->id ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                        <script>
                                        var asInitVals = new Array();
                                        $(document).ready(function () {
                                            var oTable = $('#example').dataTable({
                                                "oLanguage": {
                                                    "sSearch": "Search all columns:"
                                                }
                                            });

                                            $("tfoot input").keyup(function () {
                                                /* Filter on the column (the index) of this element */
                                                oTable.fnFilter(this.value, $("tfoot input").index(this));
                                            });



                                            /*
                                             * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                             * the footer
                                             */
                                            $("tfoot input").each(function (i) {
                                                asInitVals[i] = this.value;
                                            });

                                            $("tfoot input").focus(function () {
                                                if (this.className == "search_init")
                                                {
                                                    this.className = "";
                                                    this.value = "";
                                                }
                                            });

                                            $("tfoot input").blur(function (i) {
                                                if (this.value == "")
                                                {
                                                    this.className = "search_init";
                                                    this.value = asInitVals[$("tfoot input").index(this)];
                                                }
                                            });
                                            $('.clearform').click(function () {
                                                $('#addMLForm').modal('toggle');
                                                $('#add_ml_form')[0].reset();
                                                $('#add_ml_form').data('formValidation').resetForm();
                                            });
                                            $('.edit_clearform').click(function () {
                                                $('#editMLForm').modal('toggle');
                                                $('#edit_ml_form')[0].reset();
                                                $('#edit_ml_form').data('formValidation').resetForm();
                                            });

                                        });
                                        $(".add_ml_btn").click(function () {
                                            $('#addMLForm').modal('show');
                                        });
                                        $(".edit_ml").click(function () {
                                            $("#ml_id_hidden").val($(this).data('ml_id'));
                                            $("#ml_district").val($(this).data('ml_district'));
                                            $("#ml_description").val($(this).data('ml_description'));
                                            $("#old_ml_pdf_link").val($(this).data('ml_pdf'));
                                            $('#editMLForm').modal('show');
                                        });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>