<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_population_btn" data-toggle="modal">Add
                    Poultry Product</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Population Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Species item</th>
                                <th>Quantity(Pak)</th>
                                <th>Quantity(Punjab)</th>
                                <th>Share</th>
                                <th>Source</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($populations)) {
                                $i = 1;
                                foreach ($populations as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->species_name ?></td>
                                        <td><?= $row->pak_qty ?></td>
                                        <td><?= $row->punjab_qty ?></td>
                                        <td><?= $row->share . '%' ?></td>
                                        <td><?= $row->source ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_populations" data-toggle="modal"
                                                           data-id="<?= $row->id ?>"
                                                           data-species_id="<?= $row->species_id ?>"
                                                           data-pak_qty="<?= $row->pak_qty ?>"
                                                           data-punjab_qty="<?= $row->punjab_qty ?>"
                                                           data-p_share="<?= $row->share ?>"
                                                           data-p_source="<?= $row->source ?>" value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_population_product?population_id=<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addPopulationForm').modal('toggle');
                                    $('#add_population_Form')[0].reset();
                                    $('#add_population_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editPopulationForm').modal('toggle');
                                    $('#edit_Population_Form')[0].reset();
                                    $('#edit_Population_Form').data('formValidation').resetForm();
                                });

                            });
                            $(".add_population_btn").click(function () {
                                $('#addPopulationForm').modal('show');
                            });
                            $(".edit_populations").click(function () {
                                $("#population_id_hidden").val($(this).data('id'));
                                $("#pupdate_pak_qty").val($(this).data('pak_qty'));
                                $("#update_species_item").val($(this).data('species_id'));
                                $("#update_species_item").selectpicker('refresh');
                                $("#pupdate_punjab_qty").val($(this).data('punjab_qty'));
                                $("#pupdate_source").val($(this).data('p_source'));
                                $("#pupdate_share").val($(this).data('p_share'));
                                $('#editPopulationForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>