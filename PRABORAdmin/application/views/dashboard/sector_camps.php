<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_sector_camps_btn" data-toggle="modal" >Add Sector Camps</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Sector And Camps</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>District</th>
                                <th>Flood Sectors</th>
                                <th>Flood Sub Sectors</th>
                                <th>Flood Relief Camp</th>
                                <th>Veterinary Officers</th>
                                <th>Veterinary Assistants</th>
                                <th>Mobile Veterinary Dispensories</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($sector_camps)) {
                                $i = 1;
                                foreach ($sector_camps as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->district_name ?></td>
                                        <td><?= $row->flood_sector ?></td>
                                        <td><?= $row->flood_sub_sector ?></td>
                                        <td><?= $row->flood_relief_camp ?></td>
                                        <td><?= $row->veterinary_officers ?></td>
                                        <td><?= $row->veterinary_assistants ?></td>
                                        <td><?= $row->mobile_veterinary_dispenceries ?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_sector_camps" data-toggle="modal" data-sector_id="<?=$row->id ?>" data-district_id="<?=$row->district_id ?>" data-flood_sector="<?=$row->flood_sector ?>"  data-flood_sub_sector="<?=$row->flood_sub_sector ?>" data-flood_relief_camp="<?=$row->flood_relief_camp ?>" data-veterinary_officers="<?=$row->veterinary_officers ?>" data-veterinary_assistants="<?=$row->veterinary_assistants ?>" data-mobile_veterinary_dispenceries="<?=$row->mobile_veterinary_dispenceries ?>" value="Edit">Edit</a></li>
                                                    <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_sector?sector_id=<?= $row->id ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });



                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init")
                                    {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "")
                                    {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#addsectorcampsForm').modal('toggle');
                                    $('#add_sector_camps')[0].reset();
                                    $('#add_sector_camps').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editsectorForm').modal('toggle');
                                    $('#edit_sector_camps')[0].reset();
                                    $('#edit_sector_camps').data('formValidation').resetForm();
                                });
                            });
                            $(".add_sector_camps_btn").click(function () {
                                $('#addsectorcampsForm').modal('show');
                            });
                            $(".edit_sector_camps").click(function () {
                                $("#district_id").val($(this).data('district_id'));
                                $("#flood_sector").val($(this).data('flood_sector'));
                                $("#flood_sub_sector").val($(this).data('flood_sub_sector'));
                                $("#flood_relief_camp").val($(this).data('flood_relief_camp'));
                                $("#veterinary_officers").val($(this).data('veterinary_officers'));
                                $("#veterinary_assistants").val($(this).data('veterinary_assistants'));
                                $("#mobile_veterinary_dispenceries").val($(this).data('mobile_veterinary_dispenceries'));
                                $("#sector_id").val($(this).data('sector_id'));

                                $('#editsectorForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>