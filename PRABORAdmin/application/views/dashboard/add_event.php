<style>
    .input-group-lg > .form-control, .input-group-lg > .input-group-addon, .input-group-lg > .input-group-btn > .btn {
        height: 60px !important;
        padding: 5px 20px 0px 25px !important;
    }
    .glow{
        color:red;
    }
    input[type="file"] {
  display: block;
}

.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}

.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}

</style>
<html>
    <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    </head>
<div class="container-liquid">
    <div class="col-md-12">
        <header>
            <h1 class="page-title">Add Event</h1>
        </header>
        <div class="contents">
            <form id="add_submit_events" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Title</label>

                        <div class="col-sm-8">
                            <input name="title" type="text" required class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Thumbnail</label>

                        <div class="form-group col-sm-8">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                     style="width: 200px; height: 150px;">
                                    <img data-src="holder.js/100%x100%" alt="...">
                                </div>
                                <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="image_url" required
                                               name="banner_url"></span>
                                    <a href="#" class="btn btn-default fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Description</label>

                        <div class="col-sm-8">
                            <textarea class="form-control" rows="2" cols="2" id="textedit"
                                     required name="description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Directorate</label>

                        <div class="col-sm-8">
                            <select name="directorate_id" class="form-control selectpicker" required
                                    title="Select Directorate">
                                <option value="">Select Directorate</option>
                                <?php
                                if(!empty($directorates)){
                                foreach ($directorates as $row) { ?>
                                    <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                <?php } } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">District</label>

                        <div class="col-sm-8">
                            <select name="district_id" class="form-control selectpicker" required
                                    title="Select District">
                                <option value="">Select District</option>
                                <?php
                                if(!empty($districts)){
                                foreach ($districts as $row) { ?>
                                    <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                <?php } } ?>
                            </select>
                        </div>
                    </div>
                </div>
                  <!-- <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Event Gallery</label>

                        <div class="col-sm-8">
                            <input id="input-6" name="image_url[]" required type="file" multiple class="gal_image" data-allowed-file-extensions='["png", "jpg", "jpeg"]'>
                        </div>
                    </div>
                </div>   -->
                  <!-- <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Event Gallery</label>

                        <div class="col-sm-8">
                        <input type="file" id="files" name="files[]" multiple />
                        <div id="imggallary" style="justify-content: space-evenly;"></div>
                        
                        </div>
                    </div>
                </div>   -->
               
               
                <div class="row">
                    <div class="form-group"></div>
                </div>
  
             

      <table class="fileUploadTable" id="uploadArea" cellspacing="0">

       <tr>

        <td>

         <label for="imageFile">Image:</label> 

         <input type="file" id="daniyal" name="image_url[]" accept=".jpg,.jpeg,.png,.gif">

        </td>

        <td>

         <label for="imageCaption">Image Caption:</label>

         <input type="text" name="imageCaption[]">

        </td>

        <td width="150px">

          

        </td>

       </tr>

       <tr id="uploadSubmission">

        <td>

         <input type="button" value="Upload More" id="uploadMore">

        </td>

        <td>

         <!-- <input type="submit" value="Submit" name="addImage" id="dd"> -->

        </td>

        <td width="150px">

          

        </td>

       </tr>

      </table>


        <td width="150px">

          

        </td>

       </tr>

      </table> 
      <div class="clearfix"></div>
                <hr>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary pull-right submit_add_event" name="add_event" id="add_event"
                           value="Add Event">
                </div>
                <div class="clearfix"></div>
     </form>
            <!--/#form-submit-->
        </div>
    </div><!-- /.col -->
</div>

<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>
<script>
   $(function() {

 var scntDiv = $('#add_submit_events');

 var i = $('#p_scents tr td').size() + 1;



 $('#uploadMore').on('click', function() {

        $('<tr><td><label for="imageFile" id="dsa">Image:</label> <input type="file" name="image_url[]" accept=".jpg,.jpeg,.png,.gif" id="daniyal"></td><td><label for="imageCaption">Image Caption:</label> <input type="text" name="imageCaption[]"></td><td><a href="#" class="removeUpload" width="150px" style="text-align: center"></a></td></tr>').insertBefore( $('#uploadSubmission') );

         i++;

        return false;

 });



 $('.removeUpload').on('click', function() { 

         if( i > 1 ) {

                 $(this).parents('tr').remove();
                 i--;

        }

         return false;

 });

 });
 $(document).ready(function () {

// // Bind the click event to the function
 $("#add_event").click(function () {

// // Select all the elements with the
// // type of text
 $("#add_submit_events").each(function () {
 const inputImage=[] = $.map($('input[type=text][name="imageCaption[]"]'), function(el) { return el.value; });

 const inputCaption = []=$.map($('input[type=file][name="image_url[]"]'), function(el) { return el.value; });

 });

 })
 }); 
     $(document).on('ready', function() {
         $("#input-6").fileinput({
             showUpload: false,
             maxFileCount: 10,
             mainClass: "input-group-lg"
         });

     });
//     $(document).ready(function() {
//   // First define the Array where we will store all our files
//   var myFiles = [];
//   // now, every time the user selects new Files,
//   $("#files").on("change", function(e) {
//     var files = e.target.files, file;
//     // iterate through all the given files
//     for (var i = 0; i < files.length; i++) {
//       file = files[i];
//       myFiles.push(file); // store it in our array
//       $('<span class="pip">' +
//         '<img class="imageThumb" ' +
//         // no need for a FileReader here,
//         //  a blobURI is better (sync & uses less memory)
//         'src="' + URL.createObjectURL(file) + '" ' +
//         'title="' + file.name + '"/>' +
//         '<br/>' +'<input value="" name="inputgallary" id="eventgall" type="text">'+
//         +
//         '</span>')
//       .insertAfter("#files")
  
      
//     }
   
//   });

 
  
 
//}); 

</script>
</html>