<div class="container-liquid">
    <div class="col-md-9">
        <header>
            <h1 class="page-title">Edit Development</h1>
        </header>
        <form id="edit_baner" method="post" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12 col-lg-offset-1">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3" style="font-weight: bold;">Type</label>

                            <div class="col-sm-8">
                                <select name="category" class="form-control dev_cat"
                                        title="Select Directorate" required>
                                    <option value="">Select Type</option>
                                    <option value="1" <?php if($development->category == 1){echo("selected");}?>>Key Initiatives</option>
                                    <option value="2" <?php if($development->category == 2){echo("selected");}?>>Livestock Cholistan</option>
                                    <option value="3" <?php if($development->category == 3){echo("selected");}?> >The Provincial Budget</option>
                                    <option value="4" <?php if($development->category == 4){echo("selected");}?>>Special Package (Kisan Package)</option>
                                    <option value="5" <?php if($development->category == 5){echo("selected");}?>>Development Data Catalogue</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php if($development->category == 2): ?>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3" style="font-weight: bold;">Cholistan Type</label>

                                <div class="col-sm-8">
                                    <select name="cholistan_cat" class="form-control cho_cat"
                                            title="Select Directorate">
                                        <option value="">Select Type</option>
                                        <option value="1" <?php if($development->cholistan_category == 1){echo("selected");}?>>Livestock Cholistan</option>
<!--                                        <option value="2" --><?php //if($development->cholistan_category == 2){echo("selected");}?><!-->Test 2</option>-->
<!--                                        <option value="3" --><?php //if($development->cholistan_category == 3){echo("selected");}?><!-- >Test 3</option>-->
<!--                                        <option value="4" --><?php //if($development->cholistan_category == 4){echo("selected");}?><!-->Test 4</option>-->

                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Title</label>
                            <div class="col-sm-8">
                                <input name="title" value="<?= $development->title ?>" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">New development</label>
                            <div class="form-group col-sm-8">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                         style="width: 200px; height: 150px;">
                                        <img data-src="holder.js/100%x100%" src="<?= $development->image_url ?>" alt="...">
                                    </div>
                                    <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="development_url"
                                               name="development_url"></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Previous development</label>
                            <div class="col-sm-8">
                                <img src="<?= $development->image_url ?>" style="width: 100px; height: 100px;">
                                <input name="old_development_url" value="<?= $development->image_url ?>" type="hidden" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Discription</label>
                            <div class="col-sm-8">
                                <input name="description" value="<?= $development->description ?>" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" >
                            <label class="col-sm-3" style="font-weight: bold;">Status</label>
                            <div class="">
                                <div class="col-sm-1 ">
                                    <input  type="radio" class="bluecheckradios"
                                            name="isactive" <?php if($development->isactive == 1){echo"checked";}else{} ?> value="1">
                                </div>
                                <div class="col-sm-2">
                                    <label for="square-radio-disabled-checked">Active</label>
                                </div>
                                <div class="col-sm-1 ">
                                    <input  type="radio" <?php if($development->isactive == 0){echo"checked";}else{} ?>  class="bluecheckradios"  name="isactive"
                                            value="0">
                                </div>
                                <div class="col-sm-1 ">
                                    <label for="square-radio-disabled">InActive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <input type="submit" class="btn btn-primary pull-right" name="edit_development" value="Edit development">
            </div>
        </form>
        <!--/#form-submit-->
    </div><!-- /.col -->
</div>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
        $('.clearform').click(function () {
            $('#editbaner').modal('toggle');
            $('#edit_baner')[0].reset();
            $('#edit_baner').data('formValidation').resetForm();
        });


    });
</script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>



