<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-success add_relation_btn" data-toggle="modal" >Add Relation</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Relations Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="relation_dt">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($relations)) {
                                $i = 1;
                                foreach ($relations as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>  <a href="#" class="edit_relation_btn" data-id="<?=$row->id?>" data-name="<?=$row->name?>">Edit</a></li>
                                                    <li>  <a href="<?= base_url() ?>index.php/dashboard/delete_relation/<?= $row->id ?>"  onclick="return confirm('Are you sure to delete');" >Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#relation_dt').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                            });
                            $(".add_relation_btn").click(function () {
                                $('#addRelationForm').modal('show');
                            });
                            $(".edit_relation_btn").click(function () {
                                $('#editRelationForm').modal('show');
                                $("#relation_id").val($(this).data('id'));
                                $("#relation_name").val($(this).data('name'));
                            });

                        </script>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>