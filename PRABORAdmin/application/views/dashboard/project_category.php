<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_pro_cat_btn" data-toggle="modal" >Add category</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Project Category Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($project_cats)) {
                                $i = 1;
                                foreach ($project_cats as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name?></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                   <li> <a href="" class=" edit_pro_cat" data-toggle="modal" data-id="<?=$row->id ?>" data-name="<?=$row->name ?>"  value="Edit">Edit</a></li>
                                                    <li><a href="<?= base_url() ?>index.php/dashboard/delete_pro_cat?cat_id=<?= $row->id ?>" class="" onclick="return confirm('Are you sure to delete?')" >Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });



                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init")
                                    {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "")
                                    {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#add_Pro_cat').modal('toggle');
                                    $('#add_category_project')[0].reset();
                                    $('#add_category_project').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editProCat').modal('toggle');
                                    $('#edit_project_category')[0].reset();
                                    $('#edit_project_category').data('formValidation').resetForm();
                                });

                                $(".add_pro_cat_btn").click(function () {
                                    $('#add_Pro_cat').modal('show');
                                });
                                $(".edit_pro_cat").click(function () {
                                    $("#p_cat_id").val($(this).data('id'));
                                    $("#p_cat_name").val($(this).data('name'));
                                    $('#editProCat').modal('show');
                                });
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>