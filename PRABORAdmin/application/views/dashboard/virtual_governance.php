<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_virtual_gov_btn" data-toggle="modal">Add
                    Virtual Governance</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading"> Virtual Governance Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>PDF Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($vitual_gov)) {
                                $i = 1;
                                foreach ($vitual_gov as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->text ?></td>
                                        <td><a href="<?= $row->link_url ?>">View</a></td>
                                        <td class="btn-group" style="width: 100px">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_virtual_governance" data-toggle="modal"
                                                           data-id="<?= $row->id ?>"
                                                           data-virtual_gov_text="<?= $row->text ?>"
                                                           data-virtual_url="<?= $row->link_url ?>"
                                                           value="Edit">Edit</a></li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_virtual_governance/<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });

                                $('.clearform').click(function () {
                                    $('#addvitual_governanceForm').modal('toggle');
                                    $('#add_virtual_governance_Form')[0].reset();
                                    $('#add_virtual_governance_Form').data('formValidation').resetForm();
                                });

                                $('.edit_clearform').click(function () {
                                    $('#editvitual_governanceForm').modal('toggle');
                                    $('#edit_virtual_gov_Form')[0].reset();
                                    $('#edit_virtual_gov_Form').data('formValidation').resetForm();
                                });
                            });
                            $(".add_virtual_gov_btn").click(function () {
                                $('#addvitual_governanceForm').modal('show');
                            });
                            $(".edit_virtual_governance").click(function () {
                             var $id=  $("#vitual_gov_id").val($(this).data('id'));
                                console.log($id);
                                $("#virtual_gov_text").val($(this).data('virtual_gov_text'));
                                $("#old_vitual_gov_link").val($(this).data('virtual_url'));
                                $('#editvitual_governanceForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>