<style>
    button.glowOnClick.glow{
        -webkit-animation-name: pinButtonPulse;
        -webkit-animation-duration: .3s;
    }
    button.glowOnClick1.glow{
        -webkit-animation-name: pinButtonPulse;
        -webkit-animation-duration: .3s;
    }
    @-webkit-keyframes pinButtonPulse {
        from {
            background-color: transparent;
            -webkit-box-shadow: 0 0 9px #333;
        }
        50% {
            background-color: #ccc;
            -webkit-box-shadow: 0 0 18px #eee;
        }
        to {
            background-color: transparent;
            -webkit-box-shadow: 0 0 9px #333;
        }
    }
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_public_info" data-toggle="modal" >Add Contact</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Contacts</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Category</th>
                                <th>Name of Office/Facility</th>
                                <th>Designation</th>
                                <th>District</th>
                                <th>Email</th>
                                <th>Mobile No.</th>
                                <th>Phone No.</th>
                                <th>Latitude</th>
                                <th>Longitude</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($public_info)) {
                                // $i = 1;
                                foreach ($public_info as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $row->id ?></td>
                                        <td><?= $row->category ?></td>
                                        <td><?= $row->name_of_office_facility ?></td>
                                        <td><?= $row->designation ?></td>
                                        <td><?= $row->district ?></td>
                                        <td><?= $row->email ?></td>
                                        <td><?= $row->mobile_no ?></td>
                                        <td><?= $row->phone_no ?></td>
                                        <td><?= $row->latitude ?></td>
                                        <td><?= $row->longitude ?></td>

                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="" class="edit_info_modal" data-toggle="modal" data-info_id="<?=$row->id ?>"  data-email="<?=$row->email ?>" data-category="<?=$row->category ?>" data-name_of_office_facility="<?=$row->name_of_office_facility ?>" data-designation="<?=$row->designation ?>" data-district="<?=$row->district ?>"  data-mobile_no="<?=$row->mobile_no ?>"  data-phone_no="<?=$row->phone_no ?>" data-latitude="<?=$row->latitude ?>" data-longitude="<?=$row->longitude ?>">Edit</a></li>
                                                    <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_info?info_id=<?= $row->id ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    // $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });



                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init")
                                    {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "")
                                    {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                               /* $('.clearform').click(function () {
                                    $('#add_publicform').modal('toggle');
                                    $('#add_public')[0].reset();
                                    $('#add_public').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#edit_publicform').modal('toggle');
                                    $('#edit_public_form')[0].reset();
                                    $('#edit_public_form').data('formValidation').resetForm();
                                });*/

                            });
                            $(".add_public_info").click(function () {
                                $('#add_publicform').modal('show');
                            });
                            $(".edit_info_modal").click(function () {
                                // console.log($(this).data('email'));
                                $('.input_fields_wraps').empty();
                                $('.input_contact_wraps').empty();
                                $("#info_id").val($(this).data('info_id'));
                                // $("#category").val($(this).data('category'));
                                $("#name_of_office_facility").val($(this).data('name_of_office_facility'));
                                $("#designation").val($(this).data('designation'));
                                $("#district").val($(this).data('district'));
                                $(".email").val($(this).data('email'));
                                $("#mobile_no").val($(this).data('mobile_no'));
                                $("#phone_no").val($(this).data('phone_no'));
                                $("#latitude").val($(this).data('latitude'));
                                $("#longitude").val($(this).data('longitude'));
                                // var emails=$(this).data('email_address');
                                // var arr = emails.toString().split(',');
                                // if(arr != "") {
                                //     $.each(arr, function (index, value) {
                                //         //text box increment
                                //         $('.input_fields_wraps').append(' <div class=""><label class="col-sm-2" style="font-weight: bold;">Email</label><div class="col-sm-6"><input name="email[]" value="' + value + '" type="text" readonly class="form-control"/></div><div class="col-sm-4"> <button type="button" class="remove_field btn btn-info">Remove</button> </div></div>'); //add input box
                                //     });
                                // }
                                // var contact_number = $(this).data('contact_number');
                                // var arr2 = contact_number.toString().split(',');
                                // if(arr2 != "") {
                                //     $.each(arr2, function (index, value) {
                                //         //text box increment
                                //         $('.input_contact_wraps').append(' <div class="col-lg-12" style="padding: 0px;margin: 0px;"><label class="col-sm-2" style="font-weight: bold;">Contact Number</label><div class="col-sm-6"><input name="contact_number[]" readonly value="' + value + '" type="text" class="form-control"/></div><div class="col-sm-4"> <button type="button" class="remove_field_conact btn btn-info">Remove</button> </div></div>'); //add input box
                                //     });
                                // }

                                $('#edit_publicform').modal('show');
                            });


                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };

    $(document).ready(function() {

        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count



        $(add_button).on("click",function(e){ //on add input button click
            $(".email_address_add").prop('required', false);
            var emails_Address=$(".email_address_add").val();

            e.preventDefault();
            if(isValidEmailAddress( emails_Address ) ) {

                $('#email_error').hide();

                if (emails_Address != "") {
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper).append(' <div class=""><label class="col-sm-2" style="font-weight: bold;">Email</label><div class="col-sm-6"><input name="email[]" id="public_info_email" readonly type="text" value="' + emails_Address + '" class="form-control"/></div><div class="col-sm-4"> <button type="button" class="remove_field btn btn-info">Remove</button> </div></div>'); //add input box
                    }
                }
            }
            $(".email_address_add").val("");
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
        })
        var max_fields      = 10; //maximum input boxes allowed
        var contact         = $(".input_contact_wrap"); //Fields wrapper
        var add_contact_button      = $(".add_contact_button"); //Add button ID
        var y = 1; //initlal text box count

        $(add_contact_button).on("click",function(e){ //on add input button click
            $(".contact_number_add").prop('required', false);
            var contact_number = $(".contact_number_add").val();
            e.preventDefault();
            if(contact_number != "") {

                if (y < max_fields) { //max input box allowed
                    y++; //text box increment
                    $(contact).append(' <div class="col-lg-12" style="padding: 0px;margin: 0px;"><label class="col-sm-2" style="font-weight: bold;">Contact Number</label><div class="col-sm-6"><input name="contact_number[]" readonly value="' + contact_number + '" type="text" class="form-control"/></div><div class="col-sm-4"> <button type="button" class="remove_field_conact btn btn-info">Remove</button> </div></div>'); //add input box

                }

            }
            $(".contact_number_add").val("");
        });

        $(contact).on("click",".remove_field_conact", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); y--;
        })
        // $(document).on("submit", "#add_public", function(e){
        //     var currentButton = $(".glowOnClick");
        //     var currentButton1 = $(".glowOnClick1");
        //     if(x <=1) {
        //         currentButton.addClass('glow');
        //         setTimeout(function(){ currentButton.removeClass('glow');},500);
        //         e.preventDefault();
        //         return false;
        //     }else if(y<=1){
        //         currentButton1.addClass('glow');
        //         setTimeout(function(){ currentButton.removeClass('glow');},500);
        //         e.preventDefault();
        //         return false;
        //     }else{
        //         currentButton.removeClass('glow');
        //         currentButton1.removeClass('glow');
        //     }
        // });
    });

</script>
<script>
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrappers        = $(".input_fields_wrap"); //Fields wrapper
        var edit_button      = $(".edit_field_button"); //Add button ID

        var a = 1; //initial text box count
        $(edit_button).on("click",function(e){ //on add input button click
            $(".email_address_edit").prop('required', false);
            var emails_Address=$(".email_address_edit").val();
            if(emails_Address != "") {
                e.preventDefault();
                if (a < max_fields) { //max input box allowed
                    a++; //text box increment
                    $(wrappers).append(' <div class=""><label class="col-sm-2" style="font-weight: bold;">Email</label><div class="col-sm-6"><input name="email[]" readonly type="text" value="' + emails_Address + '" class="form-control"/></div><div class="col-sm-4"> <button type="button" class="remove_field btn btn-info">Remove</button> </div></div>'); //add input box
                }
            }
            $(".email_address_edit").val("");
        });

        $(document).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); a--;
        })
        var max_fields      = 10; //maximum input boxes allowed
        var contacts         = $(".input_contact_wrap"); //Fields wrapper
        var edit_contact_button      = $(".edit_contact_button"); //Add button ID

        var b = 1; //initlal text box count
        $(edit_contact_button).on("click",function(e){ //on add input button click
            $(".contact_number_edit").prop('required', false);
            var contact_number = $(".contact_number_edit").val();
            e.preventDefault();
            if(contact_number != "") {
                if (b < max_fields) { //max input box allowed
                    b++; //text box increment
                    $(contacts).append(' <div class="col-lg-12" style="padding: 0px;margin: 0px;"><label class="col-sm-2" style="font-weight: bold;">Contact Number</label><div class="col-sm-6"><input name="contact_number[]" readonly value="' + contact_number + '" type="text" class="form-control"/></div><div class="col-sm-4"> <button type="button" class="remove_field_conact btn btn-info">Remove</button> </div></div>'); //add input box
                }
            }
            $(".contact_number_edit").val("");
        });

        $(document).on("click",".remove_field_conact", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); b--;
        })


    });

</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>
<script>
    $(document).ready(function () {
        $('.clearform').click(function () {
            $('#add_publicform').modal('toggle');
            $('#add_public')[0].reset();
            $('#add_public').data('formValidation').resetForm();
        });
        $('.edit_clearform').click(function () {
            $('#edit_publicform').modal('toggle');
           // $('#edit_public')[0].reset();
            $('#edit_public').data('formValidation').resetForm();
        });


    });
</script>