<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_export_btn" data-toggle="modal">Add
                    Major Export</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Export Major Commodities</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Species/Poultry item</th>
                                <th>Unit</th>
                                <th>Quantity</th>
                                <th>Value</th>
                                <th>Source</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($export_major)) {
                                $i = 1;
                                foreach ($export_major as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?php if(!empty($row->species_id)){ echo $row->species_name;}else{ echo $row->poultry_item_name; }  ?></td>
                                        <td><?= $row->unity ?></td>
                                        <td><?= $row->qty ?></td>
                                        <td><?= $row->value ?></td>
                                        <td><?= $row->source ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_major_export" data-toggle="modal"
                                                           data-id="<?= $row->id ?>"
                                                           data-species_id="<?= $row->species_id ?>"
                                                           data-poultry_item_id="<?= $row->poultry_item_id ?>"
                                                           data-major_unity="<?= $row->unity ?>"
                                                           data-major_qty="<?= $row->qty ?>"
                                                           data-major_value="<?= $row->value ?>"
                                                           data-major_source="<?= $row->source ?>" value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_major_export?export_id=<?= $row->id ?>"
                                                           onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });


                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init") {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "") {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addExportForm').modal('toggle');
                                    $('#add_Export_Form')[0].reset();
                                    $('#add_Export_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#editExportForm').modal('toggle');
                                    $('#edit_Export_Form')[0].reset();
                                    $('#edit_Export_Form').data('formValidation').resetForm();
                                });


                            });
                            $(".add_export_btn").click(function () {
                                $('#addExportForm').modal('show');
                            });
                            $(".edit_major_export").click(function () {
                                $("#export_id_hidden").val($(this).data('id'));
                                $("#update_unit_export").val($(this).data('major_unity'));
                                $("#update_species_export").val($(this).data('species_id'));
                                $("#update_species_export").selectpicker('refresh');
                                $("#update_poulty_export").val($(this).data('poultry_item_id'));
                                $("#update_poulty_export").selectpicker('refresh');
                                $("#update_qty_export").val($(this).data('major_qty'));
                                $("#update_value_export").val($(this).data('major_value'));
                                $("#update_source_export").val($(this).data('major_source'));
                                $('#editExportForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>