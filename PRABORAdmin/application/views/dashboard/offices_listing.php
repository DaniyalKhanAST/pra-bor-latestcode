<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-success add_office_btn" data-toggle="modal" >Add Office</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Office Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="offices_dt">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($offices)) {
                                $i = 1;
                                foreach ($offices as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>  <a href="#" class="edit_office_btn" data-id="<?=$row->id?>" data-name="<?=$row->name?>">Edit</a></li>
                                                    <li>  <a href="<?= base_url() ?>index.php/dashboard/delete_office/<?= $row->id ?>"  onclick="return confirm('Are you sure to delete');" >Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#offices_dt').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                            });
                            $(".add_office_btn").click(function () {
                                $('#addOfficeForm').modal('show');
                            });
                            $(".edit_office_btn").click(function () {
                                $('#editOfficeForm').modal('show');
                                $("#office_id").val($(this).data('id'));
                                $("#office_name").val($(this).data('name'));
                            });

                        </script>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>