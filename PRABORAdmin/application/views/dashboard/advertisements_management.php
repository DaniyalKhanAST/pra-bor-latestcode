<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_advertisements_btn" data-toggle="modal" >Add Advertisements</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Advertisements Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($advertisements)) {
                                $i = 1;
                                foreach ($advertisements as $row) {

                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->name ?></td>
                                        <td><img src="<?= $row->image_url ?>" height="100px" width="100px" ></td>

                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a type="submit" class=" edit_advertisements" data-toggle="modal" data-id="<?=$row->id ?>" data-advertisement_name="<?=$row->name ?>" data-advertisement_image_url="<?=$row->image_url ?>" value="Edit">Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_advertisements/<?= $row->id ?>" class="" onclick="return confirm('Are you sure to delete?')" >Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            var asInitVals = new Array();
                            $(document).ready(function () {
                                var oTable = $('#example').dataTable({
                                    "oLanguage": {
                                        "sSearch": "Search all columns:"
                                    }
                                });

                                $("tfoot input").keyup(function () {
                                    /* Filter on the column (the index) of this element */
                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                });



                                /*
                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                 * the footer
                                 */
                                $("tfoot input").each(function (i) {
                                    asInitVals[i] = this.value;
                                });

                                $("tfoot input").focus(function () {
                                    if (this.className == "search_init")
                                    {
                                        this.className = "";
                                        this.value = "";
                                    }
                                });

                                $("tfoot input").blur(function (i) {
                                    if (this.value == "")
                                    {
                                        this.className = "search_init";
                                        this.value = asInitVals[$("tfoot input").index(this)];
                                    }
                                });
                                $('.clearform').click(function () {
                                    $('#addadvertisementsForm').modal('toggle');
                                    $('#add_advertisements_Form')[0].reset();
                                    $('#add_advertisements_Form').data('formValidation').resetForm();
                                });
                                $('.edit_clearform').click(function () {
                                    $('#edit_advertisementsForm').modal('toggle');
                                    $('#edit_advertisements_Form')[0].reset();
                                    $('#edit_advertisements_Form').data('formValidation').resetForm();
                                });

                            });
                            $(".add_advertisements_btn").click(function () {
                                $('#addadvertisementsForm').modal('show');
                            });

                            $(".edit_advertisements").click(function () {
                                $("#advertisement_id").val($(this).data('id'));
                                $("#advertisement_name").val($(this).data('advertisement_name'));
                                //$("#updatedvaccines_type").selectpicker('refresh');
                                $("#old_advertisement_image_url").val($(this).data('advertisement_image_url'));
                                $("#selected_advertisement_image").attr('src', $(this).data('advertisement_image_url'));

                                $('#edit_advertisementsForm').modal('show');
                            });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>