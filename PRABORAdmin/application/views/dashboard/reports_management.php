<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_report_btn" data-toggle="modal">Add
                    Report</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Reports Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="tabs-section">
                        <ul class="nav nav-tabs" id="myTab">
                            <?php
                            $i = 0;
                            if (!empty($reports_cats)) {
                                foreach ($reports_cats as $report_cat) { ?>
                                    <li class="<?php if ($i = 0) {
                                        echo "active";
                                    } ?>"><a href="#tab_<?= $report_cat->id ?>"
                                             data-toggle="tab"><?= $report_cat->name ?></a></li>
                                    <?php $i++;
                                }
                            } ?>
                        </ul>
                        <div class="tab-content">
                            <?php
                            $j = 0;
                            if (!empty($reports_cats)) {
                                foreach ($reports_cats as $report_cat) { ?>
                                    <div class="tab-pane <?php if ($j = 0) {
                                        echo "active";
                                    } ?>" id="tab_<?= $report_cat->id ?>">
                                        <section class="boxpadding">
                                            <div class="table-box">
                                                <table class="display table example" id="">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Detail</th>
                                                        <th>Pdf Link</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    if (!empty($reports)) {
                                                        $k = 1;
                                                        foreach ($reports as $row) {
                                                            if ($report_cat->id == $row->report_cat_id) {
                                                                ?>
                                                                <tr class="">
                                                                    <td><?= $k ?></td>
                                                                    <td><?= $row->name; ?></td>
                                                                    <td><?= $row->detail; ?></td>
                                                                    <td><?= $row->pdf_link ?></td>
                                                                    <td class="btn-group" style="">
                                                                        <div class="dropdown">
                                                                            <button
                                                                                class="btn btn-primary dropdown-toggle"
                                                                                type="button" data-toggle="dropdown">
                                                                                Action
                                                                                <span class="caret"></span></button>
                                                                            <ul class="dropdown-menu">
                                                                                <li><a href=""
                                                                                       class="edit_report"
                                                                                       data-toggle="modal"
                                                                                       data-id="<?= $row->id ?>"
                                                                                       data-report_name="<?= $row->name ?>"
                                                                                       data-report_cat="<?= $row->report_cat_id ?>"
                                                                                       data-report_detail="<?= $row->detail ?>"
                                                                                       data-report_pdf="<?= $row->pdf_link ?>"
                                                                                       value="Edit">Edit</a></li>
                                                                                <li>
                                                                                    <a href="<?= base_url() ?>index.php/dashboard/delete_report?report_id=<?= $row->id ?>"
                                                                                       class=""
                                                                                       onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>

                                                                </tr>
                                                                <?php

                                                            }
                                                            $k++;
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </section>
                                    </div>
                                    <?php $j++;
                                }
                            } ?>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, $("tfoot input").index(this));
        });


        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });

        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });

        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

</script>
<script>
    $(".add_report_btn").click(function () {
        $('#addReportForm').modal('show');
    });
    $(".edit_report").click(function () {
        $("#report_id_hidden").val($(this).data('id'));
        $("#report_name_id").val($(this).data('report_name'));
        $("#report_cat_id_updated").val($(this).data('report_cat'));
        $("#report_cat_id_updated").selectpicker('refresh');
        $("#report_detail_updated").val($(this).data('report_detail'));
        $("#old_pdf_link").val($(this).data('report_pdf'));
        $('#editReportForm').modal('show');
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
        $('.clearform').click(function () {
            $('#addReportForm').modal('toggle');
            $('#add_report_Form')[0].reset();
            $('#add_report_Form').data('formValidation').resetForm();
        });
        $('.edit_clearform').click(function () {
            $('#editReportForm').modal('toggle');
            $('#edit_report_Form')[0].reset();
            $('#edit_report_Form').data('formValidation').resetForm();
        });
    });
</script>