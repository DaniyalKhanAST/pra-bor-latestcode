<style>
    .siteHeading3{
        color: #346276;
        font-size: 1.25rem;
        font-weight: 700;
        line-height: 1;
    }
</style>
<div class="container-liquid">
    <div class="col-md-12">
        <header>
            <h1 class="page-title">Add Employee</h1>
        </header>
        <?php if($this->session->flashdata('fail')):?>
            <div class="alert alert-danger">
                <?=$this->session->flashdata('fail')?>
            </div>
            <?php endif;?>
        <div class="contents">
            <form id="upload_employee_file" method="post" enctype="multipart/form-data" action="<?=base_url()?>index.php/dashboard/import_employee">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Upload File : </label>

                        <div class="col-sm-8">
                            <input name="add_employee" id="add_employee" type="file" class="form-control" accept=".xlsx, .xls, .csv"/>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="form-group">
                        <input type="hidden" name="upload_file" value="save">
                        <input type="submit" class="btn btn-success pull-right" name="add_new_employee" value="Save Changes">
                    </div>
                </div>
                <div class="clearfix"></div>

            </form>
        </div>
    </div>
</div>