<style>
    .datepicker{z-index:9999 !important}
</style>

<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_staff_btn" data-toggle="modal">Add
                    Staff</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Staff Corner Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="tabs-section">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#transfer_posting" data-toggle="tab">Transfer Posting</a></li>
                            <li><a href="#suspension" data-toggle="tab">Suspension Orders</a></li>
                            <li><a href="#notifications" data-toggle="tab">Notifications</a></li>
                            <li><a href="#promotion" data-toggle="tab">Promotions</a></li>
                            <li><a href="#appointments" data-toggle="tab">Appointments Orders</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="transfer_posting">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name of Officer/Official</th>
                                                <th>Transfer From</th>
                                                <th>Designation From</th>
                                                <th>Transfer To</th>
                                                <th>Designation To</th>
                                                <th>Dated</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($transfer_posting)) {
                                                $i = 1;
                                                foreach ($transfer_posting as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->name_of_officer; ?></td>
                                                        <td><?= $row->transfer_from; ?></td>
                                                        <td><?php echo $row->designation_from; ?></td>
                                                        <td><?= $row->transfer_to ?></td>
                                                        <td><?= $row->designation_to ?></td>
                                                        <td><?= $row->date ?></td>
                                                        <td><?php $stringCut = substr($row->status_link, 0, 10);
                                                            echo $stringCut;
                                                            ?></td>
                                                        <td class="btn-group" style="">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class=" edit_staff"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-name_of_officer="<?= $row->name_of_officer ?>"
                                                                           data-transfer_from="<?= $row->transfer_from ?>"
                                                                           data-designation_from="<?= $row->designation_from ?>"
                                                                           data-transfer_to="<?= $row->transfer_to ?>"
                                                                           data-designation_to="<?= $row->designation_to ?>"
                                                                           data-date="<?= $row->date ?>"
                                                                           data-status_link="<?= $row->status_link ?>"
                                                                           data-staff_cat_id="<?= $row->staff_type_id ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_staff?staff_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                                        
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });
                                                
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });

                                                $(".add_pro_cat_btn").click(function () {
                                                    $('#add_Pro_cat').modal('show');
                                                });
                                                $(".edit_pro_cat").click(function () {
                                                    $("#p_cat_id").val($(this).data('id'));
                                                    $("#p_cat_name").val($(this).data('name'));
                                                    $('#editProCat').modal('show');
                                                });
                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane" id="suspension">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example2">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name of Officer</th>
                                                <th>Place of Posting</th>
                                                <th>Designation</th>
                                                <th>Scale</th>
                                                <th>Status </th>
                                                <th>Order No./Dated</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($all_suspension)) {
                                                $i = 1;
                                                foreach ($all_suspension as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->name_of_officer; ?></td>
                                                        <td><?= $row->transfer_to; ?></td>
                                                        <td><?= $row->designation_to; ?></td>
                                                        <td><?= $row->scale; ?></td>

                                                        <td><?= $row->status_link ?></td>
                                                        <td><?= $row->order_no ?>/<?= $row->date ?></td>
                                                        <td class="btn-group" style="width: 100px">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class=" edit_staff"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-name_of_officer="<?= $row->name_of_officer ?>"
                                                                           data-transfer_to="<?= $row->transfer_to ?>"
                                                                           data-designation_to="<?= $row->designation_to ?>"
                                                                           data-scale="<?= $row->scale ?>"
                                                                           data-status_link="<?= $row->status_link ?>"
                                                                           data-order_no="<?= $row->order_no ?>"
                                                                           data-date="<?= $row->date ?>"
                                                                           data-staff_cat_id="<?= $row->staff_type_id ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_staff?staff_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example2').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });


                                                
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });
                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane" id="notifications">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example3">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Notification No./Dated</th>
                                                <th>Instructions/ Text</th>
                                                <th>File</th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($all_notification)) {
                                                $i = 1;
                                                foreach ($all_notification as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->notification_no; ?>/<?= $row->date; ?></td>
                                                        <td><?= $row->instructions_texts; ?> </td>
                                                        <td><?= $row->status_link ?></td>
                                                        <td class="btn-group" style="width: 100px">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class=" edit_staff"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-notification_no="<?= $row->notification_no ?>"
                                                                           data-date="<?= $row->date ?>"
                                                                           data-instructions_texts="<?= $row->instructions_texts ?>"
                                                                           data-status_link="<?= $row->status_link ?>"
                                                                           data-staff_cat_id="<?= $row->staff_type_id ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_staff?staff_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example3').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });


                                                /*
                                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                                 * the footer
                                                 */
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });

                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane" id="promotion">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example4">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Notification No./Dated</th>
                                                <th>Instructions/ Text</th>
                                                <th>File</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($promotions)) {
                                                $i = 1;
                                                foreach ($promotions as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->notification_no; ?>/<?= $row->date; ?></td>
                                                        <td><?= $row->instructions_texts; ?> </td>
                                                        <td><?= $row->status_link ?></td>
                                                        <td class="btn-group" style="width: 100px">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class=" edit_staff"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-notification_no="<?= $row->notification_no ?>"
                                                                           data-date="<?= $row->date ?>"
                                                                           data-instructions_texts="<?= $row->instructions_texts ?>"
                                                                           data-status_link="<?= $row->status_link ?>"
                                                                           data-staff_cat_id="<?= $row->staff_type_id ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_staff?staff_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example4').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });


                                                /*
                                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                                 * the footer
                                                 */
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });

                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane" id="appointments">
                                <section class="boxpadding">
                                    <div class="table-box">
                                        <table class="display table" id="example5">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name of Officer / Official</th>
                                                <th>Designation</th>
                                                <th>Directorate</th>
                                                <th>Date</th>
                                                <th>View</th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($all_appointments)) {
                                                $i = 1;
                                                foreach ($all_appointments as $row) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?= $i ?></td>
                                                        <td><?= $row->name_of_officer; ?></td>
                                                        <td><?= $row->designation_to; ?> </td>
                                                        <td><?= $row->directorate_name; ?> </td>
                                                        <td><?= $row->date; ?> </td>
                                                        <td><?= $row->status_link; ?> </td>
                                                        <td class="btn-group" style="width: 100px">
                                                            <div class="dropdown">
                                                                <button class="btn btn-primary dropdown-toggle"
                                                                        type="button" data-toggle="dropdown">Action
                                                                    <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="" class=" edit_staff"
                                                                           data-toggle="modal" data-id="<?= $row->id ?>"
                                                                           data-name_of_officer="<?= $row->name_of_officer ?>"
                                                                           data-designation_to="<?= $row->designation_to ?>"
                                                                           data-date="<?= $row->date ?>"
                                                                           data-status_link="<?= $row->status_link ?>"
                                                                           data-staff_cat_id="<?= $row->staff_type_id ?>"
                                                                           data-directorate_id="<?= $row->directorate_id ?>"
                                                                           value="Edit">Edit</a></li>
                                                                    <li>
                                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_staff?staff_id=<?= $row->id ?>"
                                                                           class=""
                                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        
                                        <script>
                                            var asInitVals = new Array();
                                            $(document).ready(function () {
                                                var oTable = $('#example5').dataTable({
                                                    "oLanguage": {
                                                        "sSearch": "Search all columns:"
                                                    }
                                                });

                                                $("tfoot input").keyup(function () {
                                                    /* Filter on the column (the index) of this element */
                                                    oTable.fnFilter(this.value, $("tfoot input").index(this));
                                                });


                                                /*
                                                 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                                 * the footer
                                                 */
                                                $("tfoot input").each(function (i) {
                                                    asInitVals[i] = this.value;
                                                });

                                                $("tfoot input").focus(function () {
                                                    if (this.className == "search_init") {
                                                        this.className = "";
                                                        this.value = "";
                                                    }
                                                });

                                                $("tfoot input").blur(function (i) {
                                                    if (this.value == "") {
                                                        this.className = "search_init";
                                                        this.value = asInitVals[$("tfoot input").index(this)];
                                                    }
                                                });

                                            });

                                        </script>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>
    $(".add_staff_btn").click(function () {
        $('#addstaffForm').modal('show');
    });
    $(".edit_staff").click(function () {
        $("#staff_id").val($(this).data('id'));
        $("#name_of_officer").val($(this).data('name_of_officer'));
        $("#transfer_from").val($(this).data('transfer_from'));
        $("#designation_from").val($(this).data('designation_from'));
        $("#transfer_to").val($(this).data('transfer_to'));
        $("#designation_to").val($(this).data('designation_to'));
        $("#date_staff").val($(this).data('date'));
        $("#scale").val($(this).data('scale'));
        $("#old_status_link").val($(this).data('status_link'));
        $("#notification_date").val($(this).data('notification_date'));
        $("#instructions_texts").val($(this).data('instructions_texts'));
        $("#directorate_ids").val($(this).data('directorate_id'));
        $("#directorate_ids").selectpicker('refresh');
        var type_id=$(this).data('staff_cat_id');
        $("#staff_type_ids").val(type_id);
        $("#order_no").val($(this).data('order_no'));
        $("#notification_no").val($(this).data('notification_no'));

        var pro_cats = $(this).data('staff_cat_id');
//        console.log(pro_cats);
        if (pro_cats == '1') {
            $('.name_of_officer').show();
            $('.officer_name_field').attr("disabled", false);
            $('.transfer_to').text("Transfer To");
            $('.transfer_too').show();
            $('.transfer_to_field').attr("disabled", false);
            $('.date').show();
            $('.status_link').show();
            $('.status_field').attr("disabled", false);
            $('.designation_from').show();
            $('.designation_from_field').attr("disabled", false);
            $('.designation_to').show();
            $('.designation_to_field').attr("disabled", false);
            $('.transfer_from').show();
            $('.transfer_from_field').attr("disabled", false);
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').hide();
            $('.instruction_text_field').attr("disabled", "disabled");
            $('.notification_no').hide();
            $('.notification_no_field').attr("disabled", "disabled");
            $('.directorate_id').hide();
            $('.directorate_id_field').attr("disabled", "disabled");

        } else if (pro_cats == '2') {
            $('.status_link').show();
            $('.status_field').attr("disabled", false);
            $('.date').show();
            $('.name_of_officer').show();
            $('.officer_name_field').attr("disabled", false);
            $('.transfer_too').show();
            $('.transfer_to_field').attr("disabled", false);
            $('.transfer_to').text("Place Of Posting");
            $('.designation_to').show();
            $('.designation_to_field').attr("disabled", false);
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.order_no').show();
            $('.order_no_field').attr("disabled", false);
            $('.scale').show();
            $('.scale_field').attr("disabled", false);
            $('.instructions_texts').hide();
            $('.instruction_text_field').attr("disabled", "disabled");
            $('.notification_no').hide();
            $('.notification_no_field').attr("disabled", "disabled");
            $('.directorate_id').hide();
            $('.directorate_id_field').attr("disabled", "disabled");
        } else if(pro_cats == '3'){
            $('.name_of_officer').hide();
            $('.officer_name_field').attr("disabled", "disabled");
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.designation_to').hide();
            $('.designation_to_field').attr("disabled", "disabled");
            $('.transfer_too').hide();
            $('.transfer_to_field').attr("disabled", "disabled");
            $('.date').show();
//            $('.status_link').hide();
//            $('.status_field').attr("disabled", "disabled");
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').show();
            $('.instruction_text_field').attr("disabled", false);
            $('.notification_no').show();
            $('.notification_no_field').attr("disabled", false);
            $('.directorate_id').hide();

        }else if(pro_cats == '4'){
            $('.name_of_officer').hide();
            $('.officer_name_field').attr("disabled", "disabled");
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.designation_to').hide();
            $('.designation_to_field').attr("disabled", "disabled");
            $('.transfer_too').hide();
            $('.transfer_to_field').attr("disabled", "disabled");
            $('.date').show();
//            $('.status_link').hide();
//            $('.status_field').attr("disabled", "disabled");
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').show();
            $('.instruction_text_field').attr("disabled", false);
            $('.notification_no').show();
            $('.notification_no_field').attr("disabled", false);
            $('.directorate_id').hide();

        }else if(pro_cats == '5'){
            $('.name_of_officer').show();
            $('.officer_name_field').attr("disabled", false);
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.designation_to').show();
            $('.designation_to_field').attr("disabled", false);
            $('.transfer_too').hide();
            $('.transfer_to_field').attr("disabled", "disabled");
            $('.date').show();
            $('.status_link').show();
            $('.status_field').attr("disabled", false);
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').hide();
            $('.instruction_text_field').attr("disabled", "disabled");
            $('.notification_no').hide();
            $('.notification_no_field').attr("disabled", "disabled");
            $('.directorate_id').show();

        }
        $('#edit_staff').modal('show');

    });


    $(document).on('change', '.staff_type_id_change', function (e) {

        var staff_type_id = $(this).val();
        console.log(staff_type_id);
        if (staff_type_id == '1') {
            $('.name_of_officer').show();
            $('.officer_name_field').attr("disabled", false);
            $('.transfer_to').text("Transfer To");
            $('.transfer_too').show();
            $('.transfer_to_field').attr("disabled", false);
            $('.date').show();
            $('.status_link').show();
            $('.status_field').attr("disabled", false);
            $('.designation_from').show();
            $('.designation_from_field').attr("disabled", false);
            $('.designation_to').show();
            $('.designation_to_field').attr("disabled", false);
            $('.transfer_from').show();
            $('.transfer_from_field').attr("disabled", false);
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').hide();
            $('.instruction_text_field').attr("disabled", "disabled");
            $('.notification_no').hide();
            $('.notification_no_field').attr("disabled", "disabled");
            $('.directorate_id').hide();
            $('.directorate_id_field').attr("disabled", "disabled");


        } else if (staff_type_id == '2') {
            $('.status_link').show();
            $('.status_field').attr("disabled", false);
            $('.date').show();
            $('.name_of_officer').show();
            $('.officer_name_field').attr("disabled", false);
            $('.transfer_too').show();
            $('.transfer_to_field').attr("disabled", false);
            $('.transfer_to').text("Place Of Posting");
            $('.designation_to').show();
            $('.designation_to_field').attr("disabled", false);
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.order_no').show();
            $('.order_no_field').attr("disabled", false);
            $('.scale').show();
            $('.scale_field').attr("disabled", false);
            $('.instructions_texts').hide();
            $('.instruction_text_field').attr("disabled", "disabled");
            $('.notification_no').hide();
            $('.notification_no_field').attr("disabled", "disabled");
            $('.directorate_id').hide();
            $('.directorate_id_field').attr("disabled", "disabled");
        } else if(staff_type_id == '3'){
            $('.name_of_officer').hide();
            $('.officer_name_field').attr("disabled", "disabled");
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.designation_to').hide();
            $('.designation_to_field').attr("disabled", "disabled");
            $('.transfer_too').hide();
            $('.transfer_to_field').attr("disabled", "disabled");
            $('.date').show();
//            $('.status_link').hide();
//            $('.status_field').attr("disabled", "disabled");
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').show();
            $('.instruction_text_field').attr("disabled", false);
            $('.notification_no').show();
            $('.notification_no_field').attr("disabled", false);
            $('.directorate_id').hide();

        }else if(staff_type_id == '4'){
            $('.name_of_officer').hide();
            $('.officer_name_field').attr("disabled", "disabled");
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.designation_to').hide();
            $('.designation_to_field').attr("disabled", "disabled");
            $('.transfer_too').hide();
            $('.transfer_to_field').attr("disabled", "disabled");
            $('.date').show();
//            $('.status_link').hide();
//            $('.status_field').attr("disabled", "disabled");
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').show();
            $('.instruction_text_field').attr("disabled", false);
            $('.notification_no').show();
            $('.notification_no_field').attr("disabled", false);
            $('.directorate_id').hide();

        }else if(staff_type_id == '5'){
            $('.name_of_officer').show();
            $('.officer_name_field').attr("disabled", false);
            $('.transfer_from').hide();
            $('.transfer_from_field').attr("disabled", "disabled");
            $('.designation_from').hide();
            $('.designation_from_field').attr("disabled", "disabled");
            $('.designation_to').show();
            $('.designation_to_field').attr("disabled", false);
            $('.transfer_too').hide();
            $('.transfer_to_field').attr("disabled", "disabled");
            $('.date').show();
            $('.status_link').show();
            $('.status_field').attr("disabled", false);
            $('.order_no').hide();
            $('.order_no_field').attr("disabled", "disabled");
            $('.scale').hide();
            $('.scale_field').attr("disabled", "disabled");
            $('.instructions_texts').hide();
            $('.instruction_text_field').attr("disabled", "disabled");
            $('.notification_no').hide();
            $('.notification_no_field').attr("disabled", "disabled");
            $('.directorate_id').show();

        }
    });

</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });
        $('.clearform').click(function () {
            $('#addstaffForm').modal('toggle');
            $('#add_staff_form')[0].reset();
            $('#add_staff_form').data('formValidation').resetForm();
        });
        $('.edit_clearform').click(function () {
            $('#edit_staff').modal('toggle');
            $('#edit_staff_form')[0].reset();
            $('#edit_staff_form').data('formValidation').resetForm();
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>