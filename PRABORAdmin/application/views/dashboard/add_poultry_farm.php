<style>
    .input-group-lg > .form-control, .input-group-lg > .input-group-addon, .input-group-lg > .input-group-btn > .btn {
        height: 60px !important;
        padding: 5px 20px 0px 25px !important;
    }
    .glow{
        color:red;
    }
</style>
<div class="container-liquid">
    <div class="col-md-12">
        <header>
            <h1 class="page-title">Add Poultry Farm</h1>
        </header>
        <div class="contents">
            <form id="add_poultry_farm" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Title</label>

                        <div class="col-sm-8">
                            <input name="title" type="text" required class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Description</label>

                        <div class="col-sm-8">
                            <textarea class="form-control" id="texteditorfroala"
                                      name="description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">File</label>

                        <div class="col-sm-6">
                            <input name="file_link" type="file" class=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Image Gallery</label>

                        <div class="col-sm-8">
                            <input name="image_url[]" type="file" multiple class="gal_image" data-allowed-file-extensions='["png", "jpg", "jpeg"]'>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary pull-right" name="add_article"
                           value="Add Poultry Farm">
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).on('ready', function() {
        $('#texteditorfroala').froalaEditor({
            heightMin: 300
        });
    });
</script>