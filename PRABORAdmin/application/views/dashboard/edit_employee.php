<style>
    .siteHeading3{
        color: #346276;
        font-size: 1.25rem;
        font-weight: 700;
        line-height: 1;
    }
</style>
<div class="container-liquid">
    <div class="col-md-12">
        <header>
            <h1 class="page-title">Edit Employee</h1>
        </header>
        <?php if($this->session->flashdata('fail')):?>
            <div class="alert alert-danger">
                <?=$this->session->flashdata('fail')?>
            </div>
            <?php endif;?>
        <div class="contents">
            <form class="employee_form_validate" method="post" enctype="multipart/form-data" action="<?=base_url()?>index.php/dashboard/employee_listing">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Name : </label>

                        <div class="col-sm-8">
                            <input name="name" type="text" class="form-control" value="<?=isset($employee)? $employee->name:""?>"/>
                        </div>
                        <span style="color:red"><?=($this->session->flashdata('error')['name'])? $this->session->flashdata('error')['name'] :''?></span>
                    </div>
                </div>
                <div class="clearfix"></div> 

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">CNIC </label>

                        <div class="col-sm-8">
                            <input  type="text" class="form-control" readonly value="<?=isset($employee)? $employee->cnic:""?>"/>
                        </div>
                        <span style="color:red"><?=($this->session->flashdata('error')['cnic'])? $this->session->flashdata('error')['cnic'] :''?></span>

                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Email : </label>

                        <div class="col-sm-8">
                            <input  type="email" class="form-control" readonly value="<?=isset($employee)? $employee->email:""?>"/> 
                        </div>
                    </div>
                    <span style="color:red"><?php echo ($this->session->flashdata('error')['email'])? $this->session->flashdata('error')['email'] :''?></span>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Mobile No : </label>

                        <div class="col-sm-8">
                            <input name="mobile" type="text" class="form-control" value="<?=isset($employee)? $employee->mobile:""?>"/>
                        </div>
                        <span style="color:red"><?=($this->session->flashdata('error')['mobile'])? $this->session->flashdata('error')['mobile'] :''?></span>

                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Status </label>

                        <div class="col-sm-8">
                            <input name="is_active" type="text" class="form-control" value="<?=isset($employee)? $employee->is_active:""?>"/>
                            <label>Please add status between 0 and 1</label>
                        </div>
                        <!-- <span style="color:red"><?=($this->session->flashdata('error')['mobile'])? $this->session->flashdata('error')['mobile'] :''?></span> -->

                    </div>
                </div>

                <div class="row">

                    <div class="form-group">
                        <input type="hidden" name="employee_id" value="<?=isset($employee)? $employee->id:""?>">
                        <input type="submit" class="btn btn-success pull-right" name="edit_employee" value="Save Changes">
                    </div>
                </div>
                <div class="clearfix"></div>

            </form>
        </div>
    </div>
</div>