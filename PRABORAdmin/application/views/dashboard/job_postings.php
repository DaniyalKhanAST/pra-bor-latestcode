<style>
    .datepicker{z-index:9999 !important}
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a class="btn btn-success add_job_btn" data-toggle="modal" >Add Job Posting</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Job Postings</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="job_posting_dt">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Advertisement Date</th>
                                <th>Closing Date</th>
                                <th>Status</th>
                                <th>Attachment</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($jobs)) {
                                $i = 1;
                                foreach ($jobs as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->title ?></td>
                                        <td><?= $row->adv_date ?></td>
                                        <td><?= $row->close_date ?></td>
                                        <td><?= $row->job_status ?></td>

                                        <td><a class="btn btn-sm btn-info" href="<?= $row->attachment_link	 ?>" target="_blank">View</a></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="edit_job_btn" data-toggle="modal" data-job_id='<?=$row->id ?>' data-job_title='<?=$row->title ?>'  data-job_adv_date='<?=$row->adv_date ?>' data-job_close_date="<?=$row->close_date ?>" data-job_attachment_link="<?=$row->attachment_link ?>" value="Edit">Edit</a></li>
                                                    <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_job_posting/<?= $row->id ?>" onclick="return confirm('Delete this Job Posting?')">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function (e) {
            $($(this).closest('form')).formValidation('revalidateField', $(this).attr('name'));
        });
        $('#job_posting_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aaSorting": []
        });
        $(".add_job_btn").click(function () {
            $('#addNewJob').modal('show');
        });
        $(".edit_job_btn").click(function () {
            $("#job_id").val($(this).data('job_id'));
            $("#job_title").val($(this).data('job_title'));
            $("#edit_adv_date").datepicker('update',$(this).data('job_adv_date'));
            $("#edit_closing_date").datepicker('update',$(this).data('job_close_date'));
            $("#edit_attachment_link").val($(this).data('job_attachment_link'));
            $('#old_attach_view').attr('href',$(this).data('job_attachment_link'));
            $('#editNewJob').modal('show');
        });
    });


</script>