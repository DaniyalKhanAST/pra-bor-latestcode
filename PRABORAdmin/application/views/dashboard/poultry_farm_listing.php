<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="<?=base_url()?>index.php/dashboard/add_poultry_farm" class="btn btn-success">Add Poultry Farm</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Poultry Farm Listing</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($articles)) {
                                $i = 1;
                                foreach ($articles as $row) {
                                    ?>
                                    <tr class="">
                                        <td><?= $i ?></td>
                                        <td><?= $row->title; ?></td>
                                        <td><?= $row->description; ?></td>
                                        <td><?php if(!empty($row->file_link)){ ?><a href="<?= $row->file_link ?>" target="_blank">View</a> <?php } ?></td>
                                        <td class="btn-group" style="">
                                            <div class="dropdown">
                                                <button
                                                    class="btn btn-primary dropdown-toggle"
                                                    type="button" data-toggle="dropdown">
                                                    Action
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?= base_url() ?>index.php/dashboard/edit_poultry_farm/<?= $row->id ?>">Edit</a></li>

                                                    <li>
                                                        <a href="<?= base_url() ?>index.php/dashboard/delete_poultry_farm/<?= $row->id ?>"
                                                           class=""
                                                           onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });
    });
</script>