<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="<?= base_url() ?>index.php/dashboard/add_event" class="btn btn-primary" >Add Event</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Gallery Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="news_dt">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                    <th>Directorate</th>
                                    <th>District</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($events)) {
                                    $i = 1;
                                    foreach ($events as $row) {
                                        ?>
                                        <tr class="">
                                            <td><?= $i ?></td>
                                            <td><?= $row->title ?></td>
                                            <td><a class="btn btn-group-sm btn-default" href="<?= $row->image_url ?>" target="_blank">view</a></td>
                                            <td><?= strlen($row->description) > 100 ? substr(strip_tags($row->description),0,100).'...' : strip_tags($row->description); ?></td>
                                            <td><?=$row->directorate_name ?></td>
                                            <td><?=$row->district_name ?></td>
                                            <td class="btn-group" style="">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" style="min-width: 115px">
                                                        <li><a href="<?= base_url() ?>index.php/dashboard/edit_event?event_id=<?= $row->id ?>">Edit</a></li>
                                                        <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_event?event_id=<?= $row->id ?>" onclick=" return confirm('Are you sure to Delete?')">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var oTable = $('#news_dt').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });

    });

</script>