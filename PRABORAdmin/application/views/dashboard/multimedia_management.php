<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_multimedia_btn" data-toggle="modal">Add
                    Photo/Video/Stories/Games </a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Photo/Video/Stories/Games Management</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>

                    <section class="boxpadding">
                        <div class="table-box">
                            <table class="display table example" id="">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Attachments</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($items)) {
                                    $k = 1;
                                    foreach ($items as $row) { ?>
                                        <tr class="">
                                            <td><?= $k ?></td>
                                            <td><?= $row->title; ?></td>
                                            <td><?php
                                                        if ($row->type == 1) {
                                                            echo 'Photo';
                                                        }else if($row->type == 2){
                                                            echo 'Video';
                                                        }else if($row->type == 3){
                                                            echo 'Storie';
                                                        }else if($row->type == 4){
                                                            echo 'Game';
                                                        }
                                                 ?>

                                            </td>

                                            <td><a href="<?= $row->link_url ?>">View</a></td>
                                            <td class="btn-group" style="">
                                                <div class="dropdown">
                                                    <button
                                                        class="btn btn-primary dropdown-toggle"
                                                        type="button" data-toggle="dropdown">
                                                        Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">

                                                        <li>
                                                            <a href="<?= base_url() ?>index.php/dashboard/delete_multimedia?multimedia_id=<?= $row->id ?>"
                                                               class=""
                                                               onclick="return confirm('Are you sure to delete?')">Delete</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>

                                        </tr>
                                        <?php

                                        $k++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </section>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script>
    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            }
        });

        $("tfoot input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, $("tfoot input").index(this));
        });


        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });

        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });

        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

</script>
<script>
    $(".add_multimedia_btn").click(function () {
        $('#add_Multimedia_Form').formValidation('resetForm');
        $('#addMultimediaForm').modal('show');
    });
    $(".edit_material_supportive").click(function () {
        $("#multimedia_id_hidden").val($(this).data('id'));
        $("#multimedia_title").val($(this).data('title'));
        $("#multimedia_type").val($(this).data('type'));
        $("#multimedia_type").selectpicker('refresh');
        $("#old_attachment").val($(this).data('link_url'));
        $('#edit_Multimedia_Form').formValidation('resetForm');
        $('#editMultimediaForm').modal('show');
    });
</script>
<script>

    $(document).on('change','.multimedia_type',function(){
        var value = $(this).val();
        if(value==2){
            $('.link_url').attr('disabled',true);
            $('.link_url').hide();
            $('.link_url_video').attr('disabled',false);
            $('.link_url_video').show();
        }else{
            $('.link_url_video').attr('disabled',true);
            $('.link_url_video').hide();
            $('.link_url').attr('disabled',false);
            $('.link_url').show();
        }
    });

</script>