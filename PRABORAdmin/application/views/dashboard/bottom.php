<!--// Javascript //-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/formValidation.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/framework/bootstrap.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/validation.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.accordion.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.custom-scrollbar.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/icheck.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/selectnav.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/functions.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/toastr.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/wysihtml5-0.3.0_rc2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap-wysihtml5-0.0.2.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/froala_editor.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/align.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/code_beautifier.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/code_view.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/colors.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/draggable.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/font_family.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/font_size.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/fullscreen.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/link.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/lists.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/paragraph_format.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/paragraph_style.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/table.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/url.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/froala/entities.min.js"></script>
<?php if ($page_name == 'static_page') { ?>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/froala/image.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/froala/file.min.js"></script>
<?php } ?>

<script src="<?= base_url() ?>assets/js/bootstrap-timepicker.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.dataTables.min.js"></script>
<script type='text/javascript' src='<?= base_url() ?>assets/js/invoice.js'></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dropzone/dropzone.js" type="text/javascript"></script>


<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script>
    $(document).ready(function () {
//        $(".file-3").fileinput({
//            showUpload: false,
//            showCaption: false,
//            browseClass: "btn btn-primary btn-lg",
//            fileType: "any",
//            maxFilesNum: 4,
//            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
//        });

        $(document).on('click', '.edit_formguideline', function () {
            $("#formguideline_id").val($(this).data('id'));
            $("#formguideline_name").val($(this).data('formguideline_name'));
            $("#formguideline_pdf_link").val($(this).data('formguideline_pdf_link'));
            $("#formguideline_pdf_link").val($(this).data('formguideline_pdf_link'));
            $('#edit_formguidelineForm').modal('show');
        });
        $(document).on('click', '.edit_download', function () {
            $("#download_id").val($(this).data('id'));
            $("#download_name").val($(this).data('download_name'));
            $("#download_link_url").val($(this).data('download_link_url'));
            $("#download_description").val($(this).data('download_description'));
            $('#edit_downloadForm').modal('show');
        });
        $(document).on('click', '.edit_useful_link', function () {
            $("#useful_link_id").val($(this).data('id'));
            $("#useful_cat_name").selectpicker('val', $(this).data('cat_id'));
            $("#useful_link_name").val($(this).data('useful_link_name'));
            $("#useful_link_url").val($(this).data('useful_link_url'));
            $('#edit_useful_linkForm').modal('show');
        });
        $(document).on('click', '.edit_research', function () {
            $("#research_id").val($(this).data('id'));
            $("#dissertation_name").val($(this).data('dissertation_name'));
            $("#dissertation__by").val($(this).data('dissertation__by'));
            $("#current_place_of_posting").val($(this).data('current_place_of_posting'));
            $('#edit_researchForm').modal('show');
        });
        $(document).on('click', '.edit_poultry', function () {
            $("#poultry_id").val($(this).data('id'));
            $("#poultry_name").val($(this).data('poultry_name'));
            $("#poultry_qty").val($(this).data('poultry_qty'));
            $('#edit_poultryForm').modal('show');
        });
        $(document).on('click', '.edit_species', function () {
            $("#species_id").val($(this).data('id'));
            $("#species_name").val($(this).data('species_name'));
            $('#edit_speciesForm').modal('show');
        });
        $(document).on('click', '.edit_reportcategory', function () {
            $("#reportcategory_id").val($(this).data('id'));
            $("#reportcategory_name").val($(this).data('reportcategory_name'));
            $('#edit_reportcategoryForm').modal('show');
        });
        function confirmClose() {
            return confirm('Are you sure to delete?');
        }

    });

</script>
<script language="Javascript" type="text/javascript">

    function startyear(e, t) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#err").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    }
    function endyear(e, t) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    }

</script>
<script>

    $(document).on('change', '#project_type_id', function (e) {

        var project_type_ids = $(this).val();

        if (project_type_ids == '1') {

            $('.pro_GS_status').show();
            $('.gs_no').attr("disabled", false);
            $('.sceheme_status').show();
            $('.sceh_status').attr("disabled", false);
            $('.project_duration').hide();
            $('.pro_duration').attr("disabled", "disabled");
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        } else if (project_type_ids == '2') {
            $('.pro_GS_status').show();
            $('.gs_no').attr("disabled", false);
            $('.sceheme_status').show();
            $('.sceh_status').attr("disabled", false);
            $('.project_duration').hide();
            $('.pro_duration').attr("disabled", "disabled");
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        } else {
            $('.pro_GS_status').hide();
            $('.gs_no').attr("disabled", "disabled");
            $('.sceheme_status').hide();
            $('.sceh_status').attr("disabled", "disabled");
            $('.project_duration').show();
            $('.pro_duration').attr("disabled", false);
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        }
    });
    $(document).on('change', '#project_type_id_updated', function (e) {
        var project_type_id = $(this).val();
        console.log(project_type_id);
        if (project_type_id == '1') {
            $('.pro_GS_status').show();
            $('.gs_no').attr("disabled", false);
            $('.sceheme_status').show();
            $('.sceh_status').attr("disabled", false);
            $('.project_duration').hide();
            $('.pro_duration').attr("disabled", "disabled");
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        } else if (project_type_id == '2') {
            $('.pro_GS_status').show();
            $('.gs_no').attr("disabled", false);
            $('.sceheme_status').show();
            $('.sceh_status').attr("disabled", false);
            $('.project_duration').hide();
            $('.pro_duration').attr("disabled", "disabled");
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        } else {
            $('.pro_GS_status').hide();
            $('.gs_no').attr("disabled", "disabled");
            $('.sceheme_status').hide();
            $('.sceh_status').attr("disabled", "disabled");
            $('.project_duration').show();
            $('.pro_duration').attr("disabled", false);
            $('.project_expected_du').show();
            $('.project_duration').attr("disabled", false);
        }


    });
</script>

