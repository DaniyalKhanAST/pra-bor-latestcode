<style>
    .input-group-lg > .form-control, .input-group-lg > .input-group-addon, .input-group-lg > .input-group-btn > .btn {
        height: 60px !important;
        padding: 5px 20px 0px 25px !important;
    }
    .glow{
        color:red;
    }
</style>
<div class="container-liquid">
    <div class="col-md-12">
        <header>
            <h1 class="page-title">Edit Event</h1>
        </header>
        <div class="contents">
            <form id="edit_submit_events" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Title</label>

                        <div class="col-sm-8">
                            <input name="title" value="<?=$events->title ?>"  type="text" required class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Thumbnail</label>

                        <div class="form-group col-sm-8">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                     style="width: 200px; height: 150px;">
                                    <img data-src="<?=$events->image_url ?>" src="<?=$events->image_url ?>" alt="...">
                                </div>
                                <div>
                                    <span class="btn btn-default btn-file"><span
                                            class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                        <input type="file" id="image_url" name="banner_url"></span>
                                    <input type="hidden" value="<?=$events->image_url ?>" name="old_banner_url"></span>
                                    <a href="#" class="btn btn-default fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Description</label>

                        <div class="col-sm-8">
                            <textarea class="form-control" rows="2" cols="2" id="textedit"
                                     required name="description"><?=$events->description ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Directorate</label>

                        <div class="col-sm-8">
                            <select name="directorate_id" class="form-control selectpicker"
                                    title="Select Directorate">
                                <option value="">Select Directorate</option>
                                <?php
                                if(!empty($directorates)){
                                foreach ($directorates as $row) { ?>
                                    <option value="<?= $row->id ?>" <?php if($row->id == $events->directorate_id){echo"selected=selected";} ?>><?= $row->name ?></option>
                                <?php } } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">District</label>

                        <div class="col-sm-8">
                            <select name="district_id" class="form-control selectpicker"
                                    title="Select District">
                                <option value="">Select District</option>
                                <?php
                                if(!empty($districts)){
                                foreach ($districts as $row) { ?>
                                    <option value="<?= $row->id ?>" <?php if($row->id == $events->district_id){echo"selected=selected";} ?>><?= $row->name ?></option>
                                <?php } } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2" style="font-weight: bold;">Event Gallery</label>

                        <div class="col-sm-8">
                            
                            <div class="panel-body">
                                <!-- <input id="input-6" name="image_url[]"  type="file" multiple > -->
                               
                                <div id="dropzone_previews_here" >
                                    <?php
                                    if(!empty($event_gallery)){
                                        foreach ($event_gallery as $item) { ?>
                                    

                                            <div class="col-sm-4" style="margin-top: 10px;" >
                                                <div class="ImageEdit" id="ImageEdit">
                                                <?php  echo("<script>console.log('PHP: " . $item->image_url . "');</script>");?>
                                                    <input style="display:none" type="text"  name="image_url_old[]" value="<?=$item->image_url ?>">
                                                    <img src="<?=$item->image_url ?>" style="height: 120px; width: 165px;">
                                                    <span class="close2 delete_p_image" style="right:18px;position:absolute;background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;cursor:pointer;" id="<?=$item->id  ?>">x</span>
                                                    <input type="input" name="edit_des[]" value="<?=$item->image_desc ?>">
                                                    <input style="display:none" type="file"  name="image_url[]" value="<?=$item->image_url ?>">
                                                    <!-- <span class="close2 " style="right:18px;position:absolute;background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;cursor:pointer; margin-top: 131px;margin-top: 131px">x</span> -->
                                                </div>
                                            </div>
                                        <?php 
                                    }
                                    ?>
                                    <script>
                                        var input1 = document.getElementsByName('edit_des[]');
                                       for(let i = 0; i<=input1.length ; i++ ){
                                           console.log(input1[i])
                                       }
                                    </script>
                                    <?php
                                    } ?>

                                </div>
                                <br>
                                <br>

                                <div class="clearfix"></div>
                                <div class="col-lg-12 pull-right" style="margin-top: 10px" align="right">
<!--                                    <input class="btn btn-success" id="save_pBanner" type="submit" value="Update">-->
                                </div>
                            </div>
                            <div>
                        </div>
                    </div>
                    
                </div>
                <table class="fileUploadTable" id="uploadArea" cellspacing="0" style="display: initial;margin-left: 15px;">

<tr>

 <td>

  <label for="imageFile">Image:</label> 

  

 </td>

 <td>

 

  

 </td>

 <td width="150px">

   

 </td>

</tr>

<tr id="uploadSubmission">

 <td>

  <input type="button" value="Upload More" id="uploadMore">

 </td>

 <td>

  <!-- <input type="submit" value="Submit" name="addImage" id="dd"> -->

 </td>

 <td width="150px">

   

 </td>

</tr>

</table>


 <td width="150px">

   

 </td>

</tr>

</table> 
                <div class="clearfix"></div>
                <hr>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary pull-right submit_add_event" name="edit_event"
                           value="Edit Event">
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="form-group"></div>
                </div>
            </form>
            <!--/#form-submit-->
        </div>
    </div><!-- /.col -->
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload/jasny-bootstrap.js"></script>
<script>
    $(document).on('ready', function() {
        $("#input-6").fileinput({
            showUpload: false,
            maxFileCount: 10,
            mainClass: "input-group-lg"
        });
    });
    $( document ).ready(function() {
        $('#dropzone_previews_here').on('click', '.delete_p_image', function (e) {

            if(confirm("Want to delete your Gallery Image?")){
                $('#dropzone_previews_here').empty();
                var p_banner_id = $(this).attr('id');
                $data = 'p_banner_id=' + p_banner_id;
                $.ajax({
                    url: '<?= base_url() ?>index.php/dashboard/delete_gallery_images/' +  <?php echo $events->id; ?>,
                    data: $data,
                    type: 'POST',
                    dataType:'json',
                    success: function (data) {
                        if(data != null){
                            $('#dropzone_previews_here').append(data);

                        }
                    }
                });
            }
        });
    });
    $(function() {

var scntDiv = $('#edit_submit_events');

var i = $('#p_scents tr td').size() + 1;



$('#uploadMore').on('click', function() {

       $('<tr><td><label for="imageFile" id="dsa">Image:</label> <input type="file" name="image_url[]" accept=".jpg,.jpeg,.png,.gif" id="daniyal"></td><td><label for="imageCaption">Image Caption:</label> <input type="text" name="edit_des[]"></td><td><a href="#" class="removeUpload" width="150px" style="text-align: center"></a></td></tr>').insertBefore( $('#uploadSubmission') );

        i++;

       return false;

});



$('.removeUpload').on('click', function() { 

        if( i > 1 ) {

                $(this).parents('tr').remove();
                i--;

       }

        return false;

});

});
$(document).ready(function () {

// // Bind the click event to the function
$("#edit_submit_events").click(function () {

// // Select all the elements with the
// // type of text
$("#add_submit_events").each(function () {
const inputImage=[] = $.map($('input[type=text][name="imageCaption[]"]'), function(el) { return el.value; });
var cookieValue = document.getElementById('ImageEdit').getAttribute('value');
    alert(cookieValue);
const inputCaption = []=$.map($('input[type=file][name="image_url[]"]'), function(el) { return el.value; });

});

})
}); 
</script>