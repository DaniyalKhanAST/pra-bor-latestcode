<style>
    .datepicker{z-index:9999 !important}
</style>
<div class="container-liquid">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right"><a href="" class="btn btn-primary add_pp_btn" data-toggle="modal" >Add Procurement Plan</a></div>
        </div>
        <div class="col-xs-12">
            <div class="sec-box">
                <header>
                    <h2 class="heading">Procurement Plans</h2>
                </header>
                <div class="contents">
                    <a class="togglethis">Toggle</a>
                    <div class="table-box">
                        <table class="display table" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Year</th>
                                    <th>Description</th>
                                    <th>Pdf File</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($procurement_plans)) {
                                    $i = 1;
                                    foreach ($procurement_plans as $row) {
                                        ?>
                                        <tr class="">
                                            <td><?= $i ?></td>
                                            <td><?= $row->year ?></td>
                                            <td><?= $row->description ?></td>
                                            <td><a href="<?= $row->pdf_link	 ?>" >View</a></td>
                                            <td class="btn-group" style="">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" class="edit_pp" data-toggle="modal" data-pp_id="<?=$row->id ?>" data-pp_year="<?=$row->year ?>" data-pp_description="<?=$row->description ?>"  data-pp_pdf="<?=$row->pdf_link ?>" value="Edit">Edit</a></li>
                                                        <li> <a  href="<?= base_url() ?>index.php/dashboard/delete_procurement_plan?pp_id=<?= $row->id ?>" onclick="return confirm('Are you sure to Delete?')">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
                        <script>
                                        var asInitVals = new Array();
                                        $(document).ready(function () {
                                            var oTable = $('#example').dataTable({
                                                "oLanguage": {
                                                    "sSearch": "Search all columns:"
                                                }
                                            });

                                            $("tfoot input").keyup(function () {
                                                /* Filter on the column (the index) of this element */
                                                oTable.fnFilter(this.value, $("tfoot input").index(this));
                                            });



                                            /*
                                             * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
                                             * the footer
                                             */
                                            $("tfoot input").each(function (i) {
                                                asInitVals[i] = this.value;
                                            });

                                            $("tfoot input").focus(function () {
                                                if (this.className == "search_init")
                                                {
                                                    this.className = "";
                                                    this.value = "";
                                                }
                                            });

                                            $("tfoot input").blur(function (i) {
                                                if (this.value == "")
                                                {
                                                    this.className = "search_init";
                                                    this.value = asInitVals[$("tfoot input").index(this)];
                                                }
                                            });
                                            $('.clearform').click(function () {
                                                $('#addPPForm').modal('toggle');
                                                $('#add_pp_form')[0].reset();
                                                $('#add_pp_form').data('formValidation').resetForm();
                                            });
                                            $('.edit_clearform').click(function () {
                                                $('#editPPForm').modal('toggle');
                                                $('#edit_pp_form')[0].reset();
                                                $('#edit_pp_form').data('formValidation').resetForm();
                                            });

                                        });
                                        $(".add_pp_btn").click(function () {
                                            $('#addPPForm').modal('show');
                                        });
                                        $(".edit_pp").click(function () {
                                            $("#pp_id_hidden").val($(this).data('pp_id'));
                                            $("#pp_year").val($(this).data('pp_year'));
                                            $("#pp_description").val($(this).data('pp_description'));
                                            $("#old_pp_pdf_link").val($(this).data('pp_pdf'));
                                            $('#editPPForm').modal('show');
                                        });

                        </script>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row End -->
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.minimalcheckradios').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional
        });
        $('.bluecheckradios').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%' // optional
        });


    });
</script>