<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>BOR Punjab</title>
        <!--// Style sheets //-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <?php include 'inc/top.php'; ?>
    </head>
    <body>
        <div class="wrapper">
            <div class="structure-row">
                <?php include 'inc/left_menu.php'; ?>
                <div class="right-sec">
                    <?php include 'inc/header.php'; ?>
                    <div class="content-section">
                        <?php include $folder_name . '/' . $page_name . '.php'; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'inc/bottom.php'; ?>
        <?php include 'inc/footer.php'; ?>
         <?php include 'inc/modals.php'; ?>
    </body>
   
</html>