<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
    }


    public function page_listing($meta_page_slug = null)
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Generic_model');
            if (!(isset($meta_page_slug) && $meta_page_slug != "")) {
                $data['meta_page'] = $this->Generic_model->get_any_meta_page();
                $meta_page_id = $data['meta_page']->id;
            } else {
                $data['meta_page'] = $this->Generic_model->get_meta_page_by_slug($meta_page_slug);
                $meta_page_id = $data['meta_page']->id;
            }
            $data['meta_pages'] = $this->Generic_model->get_all_meta_page();
            $page_items = $this->Generic_model->get_all_items($meta_page_id);
            if (isset($page_items) && !empty($page_items)) {
                foreach ($page_items as $page_item) {
                    $page_item->files = $this->Generic_model->get_item_files_by_id($page_item->id);
                    $page_item->images = $this->Generic_model->get_item_images_by_id($page_item->id);
                    $page_item_ar[] = $page_item;
                }
            } else {
                $page_item_ar = null;
            }
            $data['items'] = $page_item_ar;
            $data['folder_name'] = 'generic';
            $data['file_name'] = 'generic';
            $data['page_name'] = 'item_listing';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_item($meta_page_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('generic_model');
            if ($this->input->post('add_item') && isset($meta_page_id)) {
                $upload_path_image = 'uploads/item_images/';
                $upload_path_file = 'uploads/item_files/';
                if (!is_dir($upload_path_image)) {
                    mkdir($upload_path_image, 0777, TRUE);
                }
                if (!is_dir($upload_path_file)) {
                    mkdir($upload_path_file, 0777, TRUE);
                }

                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $data_gal['meta_page_id'] = $meta_page_id;
                $item_id = $this->generic_model->add_item($data_gal);

                //////
                for ($i = 0; $i < sizeof($_FILES['file_link']['name']); $i++) {
                    if (!empty($_FILES['file_link']['name'][$i]) && $_FILES['file_link']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['file_link']['tmp_name'][$i], $upload_path_file . $_FILES['file_link']['name'][$i]);
                        $data_gl_detail['file_link'] = base_url() . $upload_path_file . $_FILES['file_link']['name'][$i];
                        $data_gl_detail['page_id'] = $item_id;
                        $this->generic_model->add_item_file($data_gl_detail);
                        unset($data_gl_detail);
                    }
                }
                //////
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path_image . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path_image . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['page_id'] = $item_id;
                        $this->generic_model->add_item_image($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Record Added');
                $meta_page_data = $this->generic_model->get_meta_page_by_id($meta_page_id);
                redirect('page/page_listing/' . $meta_page_data->slug);
            } else {
                $data['meta_page'] = $this->generic_model->get_meta_page_by_id($meta_page_id);
                $data['folder_name'] = 'generic';
                $data['file_name'] = 'generic';
                $data['page_name'] = 'add_item';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_item($item_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($item_id) && !empty($item_id)) {
                $this->load->model('generic_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_item')) {
                    $upload_path_image = 'uploads/item_images/';
                    $upload_path_file = 'uploads/item_files/';
                    if (!is_dir($upload_path_image)) {
                        mkdir($upload_path_image, 0777, TRUE);
                    }
                    if (!is_dir($upload_path_file)) {
                        mkdir($upload_path_file, 0777, TRUE);
                    }

                    $data_gal['title'] = $this->input->post('title');
                    $data_gal['description'] = $this->input->post('description');
                    $this->generic_model->update_item($item_id, $data_gal);

                    //////
                    for ($i = 0; $i < sizeof($_FILES['file_link']['name']); $i++) {
                        if (!empty($_FILES['file_link']['name'][$i]) && $_FILES['file_link']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['file_link']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['file_link']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['file_link']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['file_link']['tmp_name'][$i], $upload_path_file . $_FILES['file_link']['name'][$i]);
                            $data_gl_detail['file_link'] = base_url() . $upload_path_file . $_FILES['file_link']['name'][$i];
                            $data_gl_detail['page_id'] = $item_id;
                            $this->generic_model->add_item_file($data_gl_detail);
                        }
                    }
                    //////
                    for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                        if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path_image . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path_image . $_FILES['image_url']['name'][$i];
                            $data_gl_detail['page_id'] = $item_id;
                            $this->generic_model->add_item_image($data_gl_detail);
                        }
                    }
                    $meta_page = $this->generic_model->get_meta_page_by_item_id($item_id);
                    $this->session->set_flashdata('success', 'Record Updated');
                    redirect('page/page_listing/' . $meta_page->slug);
                } else {
                    $data['item'] = $this->generic_model->get_item_by_id($item_id);
                    $data['item_images'] = $this->generic_model->get_item_images_by_id($item_id);
                    $data['item_files'] = $this->generic_model->get_item_files_by_id($item_id);
                    $data['meta_page'] = $this->generic_model->get_meta_page_by_item_id($item_id);
                    $data['folder_name'] = 'generic';
                    $data['file_name'] = 'generic';
                    $data['page_name'] = 'edit_item';
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Record Selected");
                redirect('page/page_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function static_page($slug = null)
    {
        
        if ($this->ion_auth->logged_in()) {
            if (isset($slug) && !empty($slug)) {
                $this->load->model('generic_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_page')) {
                    $page_detail = $this->input->post('description');
                    $this->generic_model->update_static_page($slug,$page_detail);
                    $this->session->set_flashdata('success', 'Page Details Updated');
                    redirect('page/static_page/' .$slug);
                } else {
                    $data['page'] = $this->generic_model->get_static_page_by_slug($slug);
                    $data['folder_name'] = 'generic';
                    $data['page_name'] = 'static_page';
                    $data['file_name'] = $slug;
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Page Found");
                redirect();
            }
        } else {
            redirect('login');
        }
    }

    public function footer_page($slug = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($slug) && !empty($slug)) {
                $this->load->model('footer_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_page')) {
                    $page_url = $this->input->post('page_url');
                    $page_tooltip = $this->input->post('page_tooltip');
                    $page_detail = $this->input->post('description');
                    $this->footer_model->update_footer_page($page_url, $page_tooltip, $page_detail, $slug);
                    $this->session->set_flashdata('success', 'Page Details Updated');
                    redirect('page/footer_page/' .$slug);
                } else {
                    $data['page'] = $this->footer_model->get_footer_page_by_slug($slug);
                    $data['folder_name'] = 'generic';
                    $data['page_name'] = 'footer_page';
                    $data['file_name'] = $slug;
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Page Found");
                redirect();
            }
        } else {
            redirect('login');
        }
    }

    public function delete_item_images($item_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('generic_model');
            $image_id = $this->input->post('p_banner_id');
            $this->generic_model->delete_item_image($image_id);
            $p_banner = $this->generic_model->get_item_images_by_id($item_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_item_files($item_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('generic_model');
            $file_id = $this->input->post('p_banner_id');
            $this->generic_model->delete_item_file($file_id);
            $p_banner = $this->generic_model->get_item_files_by_id($item_id);
            $html = '';
            if (!empty($p_banner)) {
                $html .= '<div class="col-sm-4"><ul>';
                foreach ($p_banner as $banner) {
                    $file_name = explode('/', $banner->file_link);
                    $html .= '<li style="margin:5px;">' . $file_name[count($file_name) - 1] . '
                        <span class="close2 delete_file"
                              style="right:18px;position:absolute;background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;cursor:pointer;"
                              id="' . $item_id . '_' . $banner->id . '">x</span></li>';
                }
                $html .= '</ul></div>';
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_item($item_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($item_id) && !empty($item_id)) {
                $this->load->model('generic_model');
                $meta_page = $this->generic_model->get_meta_page_by_item_id($item_id);
                $this->generic_model->delete_all_item_images($item_id);
                $this->generic_model->delete_item($item_id);
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('page/page_listing/' . $meta_page->slug);
            } else {
                $this->session->set_flashdata('error', "No Record Id Found");
                redirect('page/page_listing');

            }
        } else {
            redirect('login');
        }
    }

    public function random_string($length = 5)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


}
