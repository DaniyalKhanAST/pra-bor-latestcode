<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class System extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
    }

    public function index()
    {
        if ($this->is_logged_in()) {
            $data['folder_name'] = 'system';
            $data['file_name'] = 'system';
            $data['page_name'] = 'index';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function publication_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'publication_management';
            $this->load->model('publications_model');

            if ($this->input->post('add_new_publication')) {
                $data = $this->input->post();
                $this->publications_model->create($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/publication_management');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('system/publication_management');
                }
            } else if ($this->input->post('editable_publication')) {
                $data = $this->input->post();
                $id = $this->input->post('publication_id');
                $this->publications_model->update($id, $data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/publication_management');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('system/publication_management');
                }
            }
            $data['publications'] = $this->publications_model->get_all();
            $data['folder_name'] = 'system';
            $data['file_name'] = 'publication_listing';
            $data['page_name'] = 'publication_listing';
            $this->load->view('index', $data);

        } else {
            redirect('login');
        }
    }

    public function delete_publications()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('publications_model');
            $publication_id = $this->input->get('publication_id');
            $this->publications_model->delete($publication_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/publication_management');
        } else {
            redirect('login');
        }
    }

    public function publication_agreements()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('publication_agreement_model');
            if ($this->input->post('add_new_agre')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Publication_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                    $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                    $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                    $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                    $this->publication_agreement_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('system/publication_agreements');
            } else if ($this->input->post('update_agreement')) {
                $data = $this->input->post();
                $agreement_id = $this->input->post('agreement_id');
                $upload_path = 'uploads/Publication_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                    $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                    $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                    $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                } else {
                    $data['link_url'] = $this->input->post('old_link_url');
                }
                $this->publication_agreement_model->update($agreement_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('system/publication_agreements');
            } else {
                $data['agreements'] = $this->publication_agreement_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'publication_agreements';
                $data['page_name'] = 'publication_agreements';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_agreement()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('publication_agreement_model');
            $agreement_id = $this->input->get('agreement_id');
            $this->publication_agreement_model->delete($agreement_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/publication_agreements');
        } else {
            redirect('login');
        }
    }


    public function from_and_guideline_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'form_and_guideline';
            $this->load->model('form_and_guideline_model');
            $data['form_guideline'] = $this->form_and_guideline_model->get_all();
            if ($this->input->post('edit_formguideline')) {
                $data = $this->input->post();
                $id = $this->input->post('id');
                $upload_path = 'uploads/Guidelines_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['formguideline_pdf_link']['name']) && $_FILES['formguideline_pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['formguideline_pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['formguideline_pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['formguideline_pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['formguideline_pdf_link']['tmp_name'], $upload_path . $_FILES['formguideline_pdf_link']['name']);
                    $data['formguideline_pdf_link'] = base_url() . $upload_path . $_FILES['formguideline_pdf_link']['name'];
                } else {
                    $data['formguideline_pdf_link'] = $this->input->post('old_formguideline_pdf_link');
                }
                $this->form_and_guideline_model->update($id, $data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/from_and_guideline_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('system/from_and_guideline_listing');
                }
            } else if ($this->input->post('add_new_formguideline')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Guidelines_Documents/';
                if ($data) {
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['formguideline_pdf_link']['name']) && $_FILES['formguideline_pdf_link']['error'] == 0) {
                        $type = pathinfo($_FILES['formguideline_pdf_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['formguideline_pdf_link']['name'], PATHINFO_FILENAME);
                        $_FILES['formguideline_pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['formguideline_pdf_link']['tmp_name'], $upload_path . $_FILES['formguideline_pdf_link']['name']);
                        $data['formguideline_pdf_link'] = base_url() . $upload_path . $_FILES['formguideline_pdf_link']['name'];
                        $this->form_and_guideline_model->create($data);
                    }
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/from_and_guideline_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('system/from_and_guideline_listing');
                }
            }
            $data['folder_name'] = 'system';
            $data['file_name'] = 'form_and_guideline_management';
            $data['page_name'] = 'form_and_guideline';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function delete_from_and_guideline()
    {
        
        if ($this->ion_auth->logged_in()) {
            
            $user_data = $this->ion_auth->user()->row();
            //echo "<pre>";print_r($user_data);die;
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('form_and_guideline_model');
            $formguideline_id = $this->uri->segment(3);
            $record = $this->form_and_guideline_model->get_by_id($formguideline_id);
            if(empty($record))
            {
                $this->session->set_flashdata('error', 'Record not found');
                redirect('system/from_and_guideline_listing');
            }
            $this->form_and_guideline_model->delete($formguideline_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/from_and_guideline_listing');
        } else {
            redirect('login');
        }
    }

    public function download_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'download';
            $this->load->model('downloads_model');
            $data['download'] = $this->downloads_model->get_all();
            if ($this->input->post('edit_download')) {
                $data = $this->input->post();
                if ($data) {
                    $id = $this->input->post('id');
                    $upload_path = 'uploads/Download_Documents/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['download_link_url']['name']) && $_FILES['download_link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['download_link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['download_link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['download_link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['download_link_url']['tmp_name'], $upload_path . $_FILES['download_link_url']['name']);
                        $data['download_link_url'] = base_url() . $upload_path . $_FILES['download_link_url']['name'];
                    } else {
                        $data['download_link_url'] = $this->input->post('old_download_link_url');
                    }
                    $this->downloads_model->update($id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/download_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('system/download_listing');
                }
            } else if ($this->input->post('add_new_download')) {
                $data = $this->input->post();
                if ($data) {
                    $upload_path = 'uploads/Download_Documents/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }

                    if (!empty($_FILES['download_link_url']['name']) && $_FILES['download_link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['download_link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['download_link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['download_link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['download_link_url']['tmp_name'], $upload_path . $_FILES['download_link_url']['name']);
                        $data['download_link_url'] = base_url() . $upload_path . $_FILES['download_link_url']['name'];
                    }
                    $this->downloads_model->create($data);

                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/download_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('system/download_listing');
                }
            }
            $data['folder_name'] = 'system';
            $data['file_name'] = 'download_management';
            $data['page_name'] = 'download_management';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function delete_download()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('downloads_model');
            $download_id = $this->uri->segment(3);
            $this->downloads_model->delete($download_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/download_listing');
        } else {
            redirect('login');
        }
    }

    public function useful_links_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'useful_links_listing';
            $this->load->model('useful_links_model');
            $data['useful_link'] = $this->useful_links_model->get_all();
            if ($this->input->post('edit_useful_link')) {
                $data = $this->input->post();
                // print_r($data); exit;
                $id = $this->input->post('id');
                $this->useful_links_model->update($id, $data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/useful_links_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('system/useful_links_listing');
                }
            } else if ($this->input->post('add_new_useful_link')) {
                $data = $this->input->post();
                $this->useful_links_model->create($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/useful_links_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('system/useful_links_listing');
                }
            }
            $data['folder_name'] = 'system';
            $data['file_name'] = 'useful_links_management';
            $data['page_name'] = 'useful_links';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function delete_useful_link()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('useful_links_model');
            $useful_links_id = $this->uri->segment(3);
            $this->useful_links_model->delete($useful_links_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/useful_links_listing');
        } else {
            redirect('login');
        }
    }


    public function material_category_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'report_category';
            $this->load->model('supportive_material_category_model');
            $data['material_category'] = $this->supportive_material_category_model->get_all();
            if ($this->input->post('edit_materialcategory')) {
                $data = $_POST;
                $id = $this->input->post('id');
                if ($data) {
                    $this->supportive_material_category_model->update($id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/material_category_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('system/material_category_listing');
                }
            } else if ($this->input->post('add_new_category')) {
                $data = $_POST;
                if ($data) {
                    $this->supportive_material_category_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/material_category_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('system/material_category_listing');
                }
            }
            $data['folder_name'] = 'system';
            $data['file_name'] = 'material_category';
            $data['page_name'] = 'material_category';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }


    public function delete_material_category()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('supportive_material_category_model');
            $category_id = $this->uri->segment(3);
            $this->supportive_material_category_model->delete($category_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/material_category_listing');
        } else {
            redirect('login');
        }
    }


    public function supportive_material_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'project_category';
            $this->load->model('supportive_material_category_model');
            $this->load->model('supportive_material_model');
            if ($this->input->post('add_new_material_sup')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Material_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['link_url'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                    $this->supportive_material_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('system/supportive_material_management');
            } else if ($this->input->post('update_sup_material')) {
                $data = $this->input->post();
                $material_id = $this->input->post('material_id');
                $upload_path = 'uploads/Material_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['link_url'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['link_url'] = $this->input->post('old_pdf_link');
                }
                $this->supportive_material_model->update($material_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('system/supportive_material_management');
            } else {
                $data['material_cats'] = $this->supportive_material_category_model->get_all();
                $data['materials'] = $this->supportive_material_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'supportive_material_management';
                $data['page_name'] = 'supportive_material_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }


    public function delete_material_supportive()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('supportive_material_model');
            $material_id = $this->input->get('material_id');
            $this->supportive_material_model->delete($material_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/supportive_material_management');
        } else {
            redirect('login');
        }
    }

    public function study_survey_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('study_survey_model');
            if ($this->input->post('add_new_survey')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Survey_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                    $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                    $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                    $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                }
                $this->study_survey_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('system/study_survey_management');
            } else if ($this->input->post('update_survey')) {
                $data = $this->input->post();
                $survey_id = $this->input->post('survey_id');
                $upload_path = 'uploads/Survey_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                    $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                    $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                    $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                } else {
                    $data['link_url'] = $this->input->post('old_link_url');
                }
                $this->study_survey_model->update($survey_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('system/study_survey_management');
            } else {
                $data['agreements'] = $this->study_survey_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'study_survey_management';
                $data['page_name'] = 'study_survey_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_study_survey()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('study_survey_model');
            $survey_id = $this->input->get('survey_id');
            $this->study_survey_model->delete($survey_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/study_survey_management');
        } else {
            redirect('login');
        }
    }

    public function vaccines_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $search = 'vaccines_listing';

            $this->load->model('vaccines_model');
            $data['vaccines'] = $this->vaccines_model->get_all();
            if ($this->input->post('add_new_vaccines')) {
                $data = $this->input->post();
                $upload_path = 'uploads/vaccines_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['vaccines_url']['name']) && $_FILES['vaccines_url']['error'] == 0) {
                    $type = pathinfo($_FILES['vaccines_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['vaccines_url']['name'], PATHINFO_FILENAME);
                    $_FILES['vaccines_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['vaccines_url']['tmp_name'], $upload_path . $_FILES['vaccines_url']['name']);
                    $data['vaccines_url'] = base_url() . $upload_path . $_FILES['vaccines_url']['name'];
                }
                $this->vaccines_model->create($data);
                redirect('system/vaccines_listing');
            } else if ($this->input->post('edit_vaccines')) {
                $data = $this->input->post();
                $upload_path = 'uploads/vaccines_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['vaccines_url']['name']) && $_FILES['vaccines_url']['error'] == 0) {
                    $type = pathinfo($_FILES['vaccines_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['vaccines_url']['name'], PATHINFO_FILENAME);
                    $_FILES['vaccines_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['vaccines_url']['tmp_name'], $upload_path . $_FILES['vaccines_url']['name']);
                    $data['vaccines_url'] = base_url() . $upload_path . $_FILES['vaccines_url']['name'];
                } else {
                    $data['vaccines_url'] = $this->input->post('old_vaccines_url');
                }
                $vaccines_id = $this->input->post('id');
                $this->vaccines_model->update($vaccines_id, $data);
                redirect('system/vaccines_listing');
            }
            $data['folder_name'] = 'system';
            $data['file_name'] = 'vaccines_management';
            $data['page_name'] = 'vaccines';
            $this->load->view('index', $data);

        } else {
            redirect('login');
        }
    }

    public function delete_vaccines()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('vaccines_model');
            $vaccines_id = $this->uri->segment(3);
            $this->vaccines_model->delete($vaccines_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/vaccines_listing');
        } else {
            redirect('login');
        }
    }

    public function flood_daily_activity()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('daily_flood_activity_model');
            if ($this->input->post('add_new_daily_activity')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Flood_Reports/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['districtwise_report_link']['name']) && $_FILES['districtwise_report_link']['error'] == 0) {
                    $type = pathinfo($_FILES['districtwise_report_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['districtwise_report_link']['name'], PATHINFO_FILENAME);
                    $_FILES['districtwise_report_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['districtwise_report_link']['tmp_name'], $upload_path . $_FILES['districtwise_report_link']['name']);
                    $data['districtwise_report_link'] = base_url() . $upload_path . $_FILES['districtwise_report_link']['name'];
                }
                if (!empty($_FILES['consolidate_report_link']['name']) && $_FILES['consolidate_report_link']['error'] == 0) {
                    $type = pathinfo($_FILES['consolidate_report_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['consolidate_report_link']['name'], PATHINFO_FILENAME);
                    $_FILES['consolidate_report_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['consolidate_report_link']['tmp_name'], $upload_path . $_FILES['consolidate_report_link']['name']);
                    $data['consolidate_report_link'] = base_url() . $upload_path . $_FILES['consolidate_report_link']['name'];
                } else {
                    $data['consolidate_report_link'] = NULL;
                }
                $this->daily_flood_activity_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('system/flood_daily_activity');
            } else if ($this->input->post('update_daily_activity')) {
                $data = $this->input->post();
                $daily_id = $this->input->post('daily_activity_id');
                $upload_path = 'uploads/Flood_Reports/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['districtwise_report_link']['name']) && $_FILES['districtwise_report_link']['error'] == 0) {
                    $type = pathinfo($_FILES['districtwise_report_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['districtwise_report_link']['name'], PATHINFO_FILENAME);
                    $_FILES['districtwise_report_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['districtwise_report_link']['tmp_name'], $upload_path . $_FILES['districtwise_report_link']['name']);
                    $data['districtwise_report_link'] = base_url() . $upload_path . $_FILES['districtwise_report_link']['name'];
                } else {
                    $data['districtwise_report_link'] = $this->input->post('old_districtwise_report_link');
                }
                $consolidate_report = $this->input->post('old_consolidate_report_link');
                if (!empty($_FILES['consolidate_report_link']['name']) && $_FILES['consolidate_report_link']['error'] == 0) {
                    $type = pathinfo($_FILES['consolidate_report_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['consolidate_report_link']['name'], PATHINFO_FILENAME);
                    $_FILES['consolidate_report_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['consolidate_report_link']['tmp_name'], $upload_path . $_FILES['consolidate_report_link']['name']);
                    $data['consolidate_report_link'] = base_url() . $upload_path . $_FILES['consolidate_report_link']['name'];
                } elseif (!empty($consolidate_report)) {
                    $data['consolidate_report_link'] = $this->input->post('old_consolidate_report_link');;
                } else {
                    $data['consolidate_report_link'] = NULL;
                }
                $this->daily_flood_activity_model->update($daily_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('system/flood_daily_activity');
            } else {
                $data['daily_activity'] = $this->daily_flood_activity_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'flood_daily_activity';
                $data['page_name'] = 'flood_daily_activity';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_daily_activity()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('daily_flood_activity_model');
            $activity_id = $this->input->get('activity_id');
            $this->daily_flood_activity_model->delete($activity_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/flood_daily_activity');
        } else {
            redirect('login');
        }
    }

    public function flood_officers_contact()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'project_category';
            $this->load->model('districts_model');
            $this->load->model('flood_officers_contacts_model');
            if ($this->input->post('add_officers_contact')) {
                $data = $this->input->post();
                if ($data) {
                    $this->flood_officers_contacts_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/flood_officers_contact');
            } else if ($this->input->post('update_officers_contact')) {
                $data = $this->input->post();
                if ($data) {
                    $officers_contact_id = $this->input->post('officer_id');
                    $this->flood_officers_contacts_model->update($officers_contact_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/flood_officers_contact');
            } else {
                $data['districts'] = $this->districts_model->get_all();
                $data['officers_contact'] = $this->flood_officers_contacts_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'flood_officers_contact';
                $data['page_name'] = 'flood_officers_contact';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_officer_contact()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('flood_officers_contacts_model');
            $contact_id = $this->input->get('contact_id');
            $this->flood_officers_contacts_model->delete($contact_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/flood_officers_contact');
        } else {
            redirect('login');
        }
    }

    public function flood_activity_visits()
    {
        if ($this->ion_auth->logged_in()) {

            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('flood_activity_visits_model');
            if ($this->input->post('add_new_visit_activity')) {
                $data = $this->input->post();
                if ($data) {
                    $upload_path = 'uploads/Flood_Reports/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                    }
                    $this->flood_activity_visits_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/flood_activity_visits');
            } else if ($this->input->post('update_visit_activity')) {
                $data = $this->input->post();
                if ($data) {
                    $visit_id = $this->input->post('visit_id');
                    $upload_path = 'uploads/Publication_Documents/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                    } else {
                        $data['link_url'] = $this->input->post('old_link_url');
                    }
                    $this->flood_activity_visits_model->update($visit_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/flood_activity_visits');
            } else {
                $data['visit_activities'] = $this->flood_activity_visits_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'activity_visits';
                $data['page_name'] = 'activity_visits';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_flood_activity_visits()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('flood_activity_visits_model');
            $visit_id = $this->input->get('visit_id');
            $this->flood_activity_visits_model->delete($visit_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/flood_activity_visits');
        } else {
            redirect('login');
        }
    }

    public function flood_advertisement()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('flood_advertisement_model');
            if ($this->input->post('add_flood_advertisement')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Flood_Reports/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['image_url']['name']) && $_FILES['image_url']['error'] == 0) {
                    $type = pathinfo($_FILES['image_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['image_url']['name'], PATHINFO_FILENAME);
                    $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                    move_uploaded_file($_FILES['image_url']['tmp_name'], $upload_path . $_FILES['image_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'];
                    $this->flood_advertisement_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }

                redirect('system/flood_advertisement');
            } else if ($this->input->post('update_flood_advertis')) {
                $data = $this->input->post();
                $advertisement_id = $this->input->post('advertisement_id');
                if ($advertisement_id) {
                    $upload_path = 'uploads/Flood_Reports/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['image_url']['name']) && $_FILES['image_url']['error'] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['image_url']['tmp_name'], $upload_path . $_FILES['image_url']['name']);
                        $data['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'];
                    } else {
                        $data['image_url'] = $this->input->post('old_flood_advertisement_image_url');
                    }
                    $this->flood_advertisement_model->update($advertisement_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/flood_advertisement');
            } else {
                $data['advertisement'] = $this->flood_advertisement_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'flood_advertisement';
                $data['page_name'] = 'flood_advertisement';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_flood_advertisement()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('flood_advertisement_model');
            $advertisement_id = $this->input->get('advertisement_id');
            $this->flood_advertisement_model->delete($advertisement_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/flood_advertisement');
        } else {
            redirect('login');
        }
    }


    public function station_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'useful_links_listing';
            $this->load->model('stations_model');
            $data['stations'] = $this->stations_model->get_all();
            if ($this->input->post('edit_station_selected')) {
                $data = $this->input->post();
                if ($data) {
                    $station_id = $this->input->post('station_id');
                    $this->stations_model->update($station_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/station_management');
                } else {
                    $this->session->set_flashdata('error', 'Data not Updated');
                    redirect('system/station_management');
                }
            } else if ($this->input->post('add_new_station')) {
                $data = $this->input->post();
                if ($data) {
                    $this->stations_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/station_management');
                } else {
                    $this->session->set_flashdata('error', 'Data not added');
                    redirect('system/station_management');
                }
            }
            $data['folder_name'] = 'system';
            $data['file_name'] = 'stations_management';
            $data['page_name'] = 'stations_management';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function delete_station()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('stations_model');
            $station_id = $this->input->get('station_id');
            $this->stations_model->delete($station_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/station_management');
        } else {
            redirect('login');
        }
    }

    public function free_wanda_distribution()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'project_category';
            $this->load->model('districts_model');
            $this->load->model('stations_model');
            $this->load->model('free_wanda_distribution_model');
            if ($this->input->post('add_free_dist')) {
                $data = $this->input->post();
                if ($data) {
                    $this->free_wanda_distribution_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/free_wanda_distribution');
            } else if ($this->input->post('update_wanda_distribution')) {
                $data = $this->input->post();
                if ($data) {
                    $distribution_id = $this->input->post('distribution_id');
                    $this->free_wanda_distribution_model->update($distribution_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/free_wanda_distribution');
            } else {
                $data['districts'] = $this->districts_model->get_all();
                $data['stations'] = $this->stations_model->get_all();
                $data['distributions'] = $this->free_wanda_distribution_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'free_wanda_distribution';
                $data['page_name'] = 'free_wanda_distribution';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_free_wanda_distribution()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('free_wanda_distribution_model');
            $distribution_id = $this->input->get('distribution_id');
            $this->free_wanda_distribution_model->delete($distribution_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/free_wanda_distribution');
        } else {
            redirect('login');
        }
    }

    public function budget_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'squad_where_about_listing';
            $this->load->model('budget_model');
            if ($this->input->post('add_new_budget')) {
                $data = $this->input->post();
                if ($data) {
                    $this->budget_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/budget_listing');
                } else {
                    $this->session->set_flashdata('error', 'Data not added');
                    redirect('system/budget_listing');
                }
            } else if ($this->input->post('editaboutusbudget')) {
                $data = $this->input->post();
                $budget_id = $this->input->post('budget_id');
                if ($data) {
                    $this->budget_model->update($budget_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/budget_listing');
                } else {
                    $this->session->set_flashdata('error', 'Data not Updated');
                    redirect('system/budget_listing');
                }
            } else {
                $data['budget_listing'] = $this->budget_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'budget_listing';
                $data['page_name'] = 'budget_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_budget_record()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('budget_model');
            $budget_id = $this->input->get('budget_id');
            $this->budget_model->delete($budget_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/budget_listing');
        } else {
            redirect('login');
        }
    }

    public function autonomous_bodies_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'useful_links_listing';
            $this->load->model('autonomous_bodies_model');
            if ($this->input->post('edit_autonomous_records')) {
                $data = $this->input->post();
                if ($data) {
                    $autonomous_id = $this->input->post('autonomous_id');
                    $this->autonomous_bodies_model->update($autonomous_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/autonomous_bodies_listing');
                } else {
                    $this->session->set_flashdata('error', 'Data not Updated');
                    redirect('system/autonomous_bodies_listing');
                }
            } else if ($this->input->post('add_new_autonomous')) {
                $data = $this->input->post();
                if ($data) {
                    $this->autonomous_bodies_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/autonomous_bodies_listing');
                } else {
                    $this->session->set_flashdata('error', 'Data not added');
                    redirect('system/autonomous_bodies_listing');
                }
            } else {
                $data['autonomous_bodies'] = $this->autonomous_bodies_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'autonomous_bodies';
                $data['page_name'] = 'autonomous_bodies';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_autonomous_bodies()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('autonomous_bodies_model');
            $autonomous_id = $this->input->get('autonomous_id');
            $this->autonomous_bodies_model->delete($autonomous_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/autonomous_bodies_listing');
        } else {
            redirect('login');
        }
    }

    public function laws_and_rules_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'law_and_rules';
            $this->load->model('law_and_rules_model');
            if ($this->input->post('edit_rulelawform')) {
                $data = $this->input->post();
                if ($data) {
                    $law_id = $this->input->post('law_id');
                    $upload_path = 'uploads/Rules&Laws/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    } else {
                        $data['file_link'] = $this->input->post('old_file_link');
                    }
                    $this->law_and_rules_model->update($law_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('system/laws_and_rules_listing');
                } else {
                    $this->session->set_flashdata('error', 'Data not Updated');
                    redirect('system/laws_and_rules_listing');
                }
            } else if ($this->input->post('add_new_lawrule')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Rules&Laws/';
                if ($data) {
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    }
                    $this->law_and_rules_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('system/laws_and_rules_listing');
                } else {
                    $this->session->set_flashdata('error', 'Data not added');
                    redirect('system/laws_and_rules_listing');
                }
            } else {
                $data['laws_and_rules'] = $this->law_and_rules_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'laws_and_rules';
                $data['page_name'] = 'laws_and_rules';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_laws_and_rules()
    {
        if ($this->ion_auth->logged_in()) {

            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('law_and_rules_model');
            $law_rule_id = $this->input->get('law_rule_id');
            //print_r($law_rule_id); exit;
            $this->law_and_rules_model->delete($law_rule_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/laws_and_rules_listing');
        } else {
            redirect('login');
        }
    }

    public function our_secretaries_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'squad_where_about_listing';
            $this->load->model('our_secretaries_model');
            if ($this->input->post('add_new_secretary')) {
                $data = $this->input->post();
                if ($data) {
                    $upload_path = 'uploads/Our_Secretaries/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['image_url']['name']) && $_FILES['image_url']['error'] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['image_url']['tmp_name'], $upload_path . $_FILES['image_url']['name']);
                        $data['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'];

                    }
                    $this->our_secretaries_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/our_secretaries_listing');

            } else if ($this->input->post('edit_Secretaryform')) {
                $data = $this->input->post();
                if ($data) {
                    $secretary_id = $this->input->post('secretary_id');
                    $upload_path = 'uploads/Our_Secretaries/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['image_url']['name']) && $_FILES['image_url']['error'] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['image_url']['tmp_name'], $upload_path . $_FILES['image_url']['name']);
                        $data['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'];
                    } else {
                        $data['image_url'] = $this->input->post('old_image_url');
                    }
                    $this->our_secretaries_model->update($secretary_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/our_secretaries_listing');
            } else {
                $data['secretaries_listing'] = $this->our_secretaries_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'our_secretaries_listing';
                $data['page_name'] = 'our_secretaries_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_secretaries_record()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('our_secretaries_model');
            $secretary_id = $this->input->get('secretary_id');
            $this->our_secretaries_model->delete($secretary_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/our_secretaries_listing');
        } else {
            redirect('login');
        }
    }

    public function about_us_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'squad_where_about_listing';
            $this->load->model('about_us_model');
            if ($this->input->post('add_new_about_us')) {
                $data = $this->input->post();
                if ($data) {
                    $upload_path = 'uploads/AboutUs/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];

                    }
                    $this->about_us_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/about_us_listing');

            } else if ($this->input->post('edit_aboutusform')) {
                $data = $this->input->post();
                if ($data) {
                    $saboutus_id = $this->input->post('aboutus_id');
                    $upload_path = 'uploads/AboutUs/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                    } else {
                        $data['link_url'] = $this->input->post('old_link_url');
                    }
                    $this->about_us_model->update($saboutus_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('system/about_us_listing');
            } else {
                $data['aboutus_listing'] = $this->about_us_model->get_all();
                $data['folder_name'] = 'system';
                $data['file_name'] = 'about_us_listing';
                $data['page_name'] = 'about_us_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_about_us()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('about_us_model');
            $aboutus_id = $this->input->get('aboutus_id');
            $this->about_us_model->delete($aboutus_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('system/about_us_listing');
        } else {
            redirect('login');
        }
    }

    public function upload_editor_image()
    {
        if ($this->ion_auth->logged_in()) {
        
            $upload_path_image = 'uploads/editor_images/';
            if (!is_dir($upload_path_image)) {
                mkdir($upload_path_image, 0777, TRUE);
            }

            if (!empty($_FILES['image_url']['name']) && $_FILES['image_url']['error'] == 0) {
                $type = pathinfo($_FILES['image_url']['name'], PATHINFO_EXTENSION);
                $file_name = pathinfo($_FILES['image_url']['name'], PATHINFO_FILENAME);
                $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                move_uploaded_file($_FILES['image_url']['tmp_name'], $upload_path_image . $_FILES['image_url']['name']);
                $json['link'] = base_url() . $upload_path_image . $_FILES['image_url']['name'];
            } else {
                $json['error'] = 'File could not be Uploaded';
            }
            print_r(json_encode($json));

        } else {
            redirect('login');
        }
    }

    public function upload_editor_file()
    {
        if ($this->ion_auth->logged_in()) {
            $upload_path_image = 'uploads/editor_files/';
            if (!is_dir($upload_path_image)) {
                mkdir($upload_path_image, 0777, TRUE);
            }

            if (!empty($_FILES['file_url']['name']) && $_FILES['file_url']['error'] == 0) {
                $type = pathinfo($_FILES['file_url']['name'], PATHINFO_EXTENSION);
                $file_name = pathinfo($_FILES['file_url']['name'], PATHINFO_FILENAME);
                $_FILES['file_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);;
                move_uploaded_file($_FILES['file_url']['tmp_name'], $upload_path_image . $_FILES['file_url']['name']);
                $json['link'] = base_url() . $upload_path_image . $_FILES['file_url']['name'];
            } else {
                $json['error'] = 'File could not be Uploaded';
            }
            print_r(json_encode($json));

        } else {
            redirect('login');
        }
    }

    public function random_string($length = 5)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

}
