<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index() {
        $this->load->view('login');
    }

    public function check_user() {
        $this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $this->input->post('rememberme'));
        $errors_array = $this->ion_auth->errors_array();
        $messages_array = $this->ion_auth->messages_array();
        // echo "<pre>";print_r($errors_array);die;
        if (!empty($errors_array[0])) {
            $this->session->set_flashdata('error', $errors_array[0]);
            redirect('login');
        } elseif (!empty($messages_array[0])) {
            $this->is_logged_in();
            redirect('dashboard');
        }
    }

    public function is_logged_in() {
        if ($this->ion_auth->logged_in()) {
            $user_type = $this->session->userdata('user_type');
            if ($user_type == '3' || $user_type == '1' || $user_type == '4') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function logout() {

        $this->ion_auth->logout();
        redirect('login');
    }

}
