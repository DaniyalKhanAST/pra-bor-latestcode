<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
        $this->load->library('session');
    }

    public function index()
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('users_model');
            $user_data = $this->ion_auth->user()->row();
            // echo "<pre>";print_r($user_data);die;
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'dashboard';
            $data['page_name'] = 'index';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function banner_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('banners_model');
            if ($this->input->post('add_new_banner')) {
                $data = $this->input->post();
                $upload_path = 'uploads/banner_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['banner_url']['name']) && $_FILES['banner_url']['error'] == 0) {
                    $type = pathinfo($_FILES['banner_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['banner_url']['name'], PATHINFO_FILENAME);
                    $_FILES['banner_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['banner_url']['tmp_name'], $upload_path . $_FILES['banner_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['banner_url']['name'];
                    $this->banners_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('dashboard/banner_listing');
            } else {
                $data['banner'] = $this->banners_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'banner_management';
                $data['page_name'] = 'banner_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function random_string($length = 5)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function edit_banner()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $banner_id = $this->input->get('banner_id');
            $this->load->model('banners_model');
            if ($this->input->post('edit_banner')) {
                $data = $this->input->post();
                $upload_path = 'uploads/banner_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['banner_url']['name']) && $_FILES['banner_url']['error'] == 0) {
                    $type = pathinfo($_FILES['banner_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['banner_url']['name'], PATHINFO_FILENAME);
                    $_FILES['banner_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['banner_url']['tmp_name'], $upload_path . $_FILES['banner_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['banner_url']['name'];
                } else {
                    $data['image_url'] = $this->input->post('old_banner_url');
                }
                $this->banners_model->update($banner_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/banner_listing');
            } else {
                $data['banner'] = $this->banners_model->get_by_id($banner_id);
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'banner_management';
                $data['page_name'] = 'edit_banner';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_banner()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('banners_model');
            $banner_id = $this->input->get('banner_id');
            $this->banners_model->delete($banner_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/banner_listing');
        } else {
            redirect('login');
        }
    }

    public function events_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('event_gallery_model');
            $data['events'] = $this->event_gallery_model->get_all();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'events_listing';
            $data['page_name'] = 'events_listing';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_event()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('event_gallery_model');
            $this->load->model('event_gallery_detail_model');
            $this->load->model('directorates_model');
            $this->load->model('districts_model');
            if ($this->input->post('add_event')) {
                $data = $this->input->post();
                $upload_path = 'uploads/event_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['banner_url']['name']) && $_FILES['banner_url']['error'] == 0) {
                    $type = pathinfo($_FILES['banner_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['banner_url']['name'], PATHINFO_FILENAME);
                    $_FILES['banner_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['banner_url']['tmp_name'], $upload_path . $_FILES['banner_url']['name']);
                    $data_gal['image_url'] = base_url() . $upload_path . $_FILES['banner_url']['name'];
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $data_gal['directorate_id'] = $this->input->post('directorate_id');
                $data_gal['district_id'] = $this->input->post('district_id');
                $gallery_id = $this->event_gallery_model->create($data_gal);
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['gallery_id'] = $gallery_id;
                        $data_gl_detail['imageCaption'] = $_POST['imageCaption'][$i];
                        $this->event_gallery_detail_model->create($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/events_listing');
            } else {
                $data['directorates'] = $this->directorates_model->get_all();
                $data['districts'] = $this->districts_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'events_listing';
                $data['page_name'] = 'add_event';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_gallery_images($event_id = false)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('event_gallery_model');
            $this->load->model('event_gallery_detail_model');
            $banner_id = $this->input->post('p_banner_id');
            $this->event_gallery_detail_model->delete($banner_id);
            $p_banner = $this->event_gallery_detail_model->get_by_event_id($event_id);
            dd($banner_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function edit_event()
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('event_gallery_model');
            $this->load->model('event_gallery_detail_model');
            $this->load->model('directorates_model');
            $this->load->model('districts_model');
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $event_id = $this->input->get('event_id');
            $search = 'banner_listing';
            if ($this->input->post('edit_event')) {
                  
               
                $data = $this->input->post();
                $upload_path = 'uploads/event_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                for ($i = 0; $i < sizeof($_POST['image_url_old']); $i++){
                    $_FILES['image_url']['name'][$i] = $_POST['image_url_old'][$i];
                    $_FILES['image_url']['type'][$i] = "";
                    $_FILES['image_url']['tmp_name'][$i] = "";
                    $_FILES['image_url']['error'][$i] = 0;
                    $_FILES['image_url']['size'][$i] = 659339;
                    
                }
                // echo "<pre>" ; print_r($_POST);
                //     print_r($_FILES['image_url']);die;
                if (!empty($_FILES['banner_url']['name']) && $_FILES['banner_url']['error'] == 0) {
                    $type = pathinfo($_FILES['banner_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['banner_url']['name'], PATHINFO_FILENAME);
                    $_FILES['banner_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['banner_url']['tmp_name'], $upload_path . $_FILES['banner_url']['name']);
                    $data_gal['image_url'] = base_url() . $upload_path . $_FILES['banner_url']['name'];
                } else {
                    $data_gal['image_url'] = $this->input->post('old_banner_url');
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $data_gal['directorate_id'] = $this->input->post('directorate_id');
                $data_gal['district_id'] = $this->input->post('district_id');
                $this->event_gallery_model->update($event_id, $data_gal);
                $old_files_array = $this->event_gallery_detail_model->get_by_event_id($event_id);
                foreach($old_files_array as $array)
                {
                    $ids[] = $array->id;
                }
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        if(in_array($_FILES['image_url']['name'][$i],$_POST['image_url_old']))
                        {
                            $data_gl_detail['image_url'] = $_FILES['image_url']['name'][$i];
                        }else{
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        }
                        
                        $data_gl_detail['gallery_id'] = $event_id;
                        $data_gl_detail['imageCaption'] = $_POST['edit_des'][$i];
                        $this->event_gallery_detail_model->create($data_gl_detail);
                        
                    }
                }
                $this->event_gallery_detail_model->delete_event_images_by_ids($ids);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/events_listing');
            } else {
                $data['events'] = $this->event_gallery_model->get_by_id($event_id);
                $data['event_gallery'] = $this->event_gallery_detail_model->get_by_event_id($event_id);
                $data['directorates'] = $this->directorates_model->get_all();
                $data['districts'] = $this->districts_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'events_listing';
                $data['page_name'] = 'edit_event';
                $this->load->view('index', $data);
            }
           
        } else {
            redirect('login');
        }
    }

    public function delete_event()
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('event_gallery_model');
            $this->load->model('event_gallery_detail_model');
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $event_id = $this->input->get('event_id');
            $this->event_gallery_detail_model->delete_event_images($event_id);
            $this->event_gallery_model->delete($event_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/events_listing');
        } else {
            redirect('login');
        }
    }

    public function country_medicine_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            $data['medicines'] = $this->advertisements_model->get_all_country_medicine();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'country_medicine';
            $data['page_name'] = 'country_medicine';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_country_medicine()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            if ($this->input->post('add_medicine')) {
                $this->load->model('advertisements_model');
                $upload_path = 'uploads/medicine_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                    $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                    $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                    $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                } else {
                    $data_gal['file_link'] = null;
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $c_med_id = $this->advertisements_model->add_country_medicine($data_gal);
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['medicine_id'] = $c_med_id;
                        $this->advertisements_model->add_country_medicine_image($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Country Medicine Added');
                redirect('dashboard/country_medicine_listing');
            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'country_medicine';
                $data['page_name'] = 'add_country_medicine';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_country_medicine($med_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($med_id) && !empty($med_id)) {
                $this->load->model('advertisements_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_medicine')) {
                    $upload_path = 'uploads/event_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    }
                    $data_gal['title'] = $this->input->post('title');
                    $data_gal['description'] = $this->input->post('description');
                    $this->advertisements_model->update_country_medicine($med_id, $data_gal);
                    for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                        if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                            $data_gl_detail['medicine_id'] = $med_id;
                            $this->advertisements_model->add_country_medicine_image($data_gl_detail);
                        }
                    }
                    $this->session->set_flashdata('success', 'Medicine Updated');
                    redirect('dashboard/country_medicine_listing');
                } else {
                    $data['medicine'] = $this->advertisements_model->get_medicine_by_id($med_id);
                    $data['medicine_images'] = $this->advertisements_model->get_medicine_images_by_id($med_id);
                    $data['folder_name'] = 'dashboard';
                    $data['file_name'] = 'country_medicine';
                    $data['page_name'] = 'edit_medicine';
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Medicine Selected");
                redirect('dashboard/country_medicine_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_medicine_images($med_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('advertisements_model');
            $image_id = $this->input->post('p_banner_id');
            $this->advertisements_model->delete_medicine_image($image_id);
            $p_banner = $this->advertisements_model->get_medicine_images_by_id($med_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_country_medicine($med_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($med_id) && !empty($med_id)) {
                $this->load->model('advertisements_model');
                $this->advertisements_model->delete_all_medicine_images($med_id);
                $this->advertisements_model->delete_medicine($med_id);
                $this->session->set_flashdata('success', 'Medicine Deleted');
                redirect('dashboard/country_medicine_listing');
            } else {
                $this->session->set_flashdata('error', "No Medicine Id Found");
                redirect('dashboard/country_medicine_listing');

            }
        } else {
            redirect('login');
        }
    }

    public function ostrich_article_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            $data['articles'] = $this->advertisements_model->get_all_ostrich_article();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'ostrich_article';
            $data['page_name'] = 'ostrich_article_listing';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_ostrich_article()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            if ($this->input->post('add_article')) {
                $this->load->model('advertisements_model');
                $upload_path = 'uploads/ostrich_article_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                    $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                    $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                    $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                } else {
                    $data_gal['file_link'] = null;
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $c_art_id = $this->advertisements_model->add_ostrich_article($data_gal);
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['article_id'] = $c_art_id;
                        $this->advertisements_model->add_ostrich_article_image($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Ostrich Article Added');
                redirect('dashboard/ostrich_article_listing');
            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'ostrich_article';
                $data['page_name'] = 'add_ostrich_article';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_ostrich_article($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_article')) {
                    $upload_path = 'uploads/ostrich_article_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    }
                    $data_gal['title'] = $this->input->post('title');
                    $data_gal['description'] = $this->input->post('description');
                    $this->advertisements_model->update_ostrich_article($art_id, $data_gal);
                    for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                        if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                            $data_gl_detail['article_id'] = $art_id;
                            $this->advertisements_model->add_ostrich_article_image($data_gl_detail);
                        }
                    }
                    $this->session->set_flashdata('success', 'Article Updated');
                    redirect('dashboard/ostrich_article_listing');
                } else {
                    $data['article'] = $this->advertisements_model->get_ostrich_article_by_id($art_id);
                    $data['article_images'] = $this->advertisements_model->get_ostrich_article_images_by_id($art_id);
                    $data['folder_name'] = 'dashboard';
                    $data['file_name'] = 'ostrich_article';
                    $data['page_name'] = 'edit_ostrich_article';
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Article Selected");
                redirect('dashboard/ostrich_article_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_ostrich_article_images($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('advertisements_model');
            $image_id = $this->input->post('p_banner_id');
            $this->advertisements_model->delete_ostrich_article_image($image_id);
            $p_banner = $this->advertisements_model->get_ostrich_article_images_by_id($art_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_ostrich_article($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $this->advertisements_model->delete_all_ostrich_article_images($art_id);
                $this->advertisements_model->delete_ostrich_article($art_id);
                $this->session->set_flashdata('success', 'Article Deleted');
                redirect('dashboard/ostrich_article_listing');
            } else {
                $this->session->set_flashdata('error', "No Article Id Found");
                redirect('dashboard/ostrich_article_listing');

            }
        } else {
            redirect('login');
        }
    }

    public function auction_tender_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('directorates_model');
            $this->load->model('districts_model');
            $this->load->model('auction_type_model');
            $this->load->model('auctions_model');
            if ($this->input->post('add_new_auc')) {
                $data = $this->input->post();
                // dd($_FILES);
                $upload_path = 'uploads/Auction_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                }
                if (!empty($_FILES['bid_doc']['name']) && $_FILES['bid_doc']['error'] == 0) {
                    $type = pathinfo($_FILES['bid_doc']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['bid_doc']['name'], PATHINFO_FILENAME);
                    $_FILES['bid_doc']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['bid_doc']['tmp_name'], $upload_path . $_FILES['bid_doc']['name']);
                    $data['bid_doc'] = base_url() . $upload_path . $_FILES['bid_doc']['name'];
                }
                $this->auctions_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('dashboard/auction_tender_listing');
            } else if ($this->input->post('update_auc')) {
                $data = $this->input->post();
                $auc_id = $this->input->post('auc_id');
                $upload_path = 'uploads/Auction_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['pdf_link'] = $this->input->post('old_pdf_link');
                }
                if (!empty($_FILES['bid_doc']['name']) && $_FILES['bid_doc']['error'] == 0) {
                    $type = pathinfo($_FILES['bid_doc']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['bid_doc']['name'], PATHINFO_FILENAME);
                    $_FILES['bid_doc']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['bid_doc']['tmp_name'], $upload_path . $_FILES['bid_doc']['name']);
                    $data['bid_doc'] = base_url() . $upload_path . $_FILES['bid_doc']['name'];
                }
                $this->auctions_model->update($auc_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/auction_tender_listing');
            } else {
                $data['directorates'] = $this->directorates_model->get_all();
                $data['auction'] = $this->auctions_model->get_all();
                $data['auction_type'] = $this->auction_type_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'tender_documents';
                $data['page_name'] = 'auc_ten_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function procurement_plan_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Procurement_Plans_model');
            if ($this->input->post('add_new_pp')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Procurement_Plans_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                }
                // dd($data);
                $this->Procurement_Plans_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('dashboard/procurement_plan_listing');
            } else if ($this->input->post('update_pp')) {
                $data = $this->input->post();
                $pp_id = $this->input->post('pp_id');
                $upload_path = 'uploads/Procurement_Plans_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['pdf_link'] = $this->input->post('old_pdf_link');
                }
                $this->Procurement_Plans_model->update($pp_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/procurement_plan_listing');
            } else {
                $data['procurement_plans'] = $this->Procurement_Plans_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'procurement_plans';
                $data['page_name'] = 'procurement_plan_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }


    public function company_policies_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            // dd($_FILES);
            $this->load->model('Company_policies_model');
            if ($this->input->post('add_new_company_policy')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Company_Policies_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                }
                // dd($data);
                $this->Company_policies_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('dashboard/company_policies_listing');
            } else if ($this->input->post('update_cp')) {
                $data = $this->input->post();
                $cp_id = $this->input->post('cp_id');
                $upload_path = 'uploads/Company_Policies_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['pdf_link'] = $this->input->post('old_pdf_link');
                }
                $this->Company_policies_model->update($cp_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/company_policies_listing');
            } else {
                $data['company_policies'] = $this->Company_policies_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'company_policies';
                $data['page_name'] = 'company_policies_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function manuals_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            // dd($_FILES);
            $this->load->model('Manuals_model');
            if ($this->input->post('add_new_manual')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Manuals_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                }
                // dd($data);
                $this->Manuals_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('dashboard/manuals_listing');
            } else if ($this->input->post('update_manual')) {
                $data = $this->input->post();
                $manual_id = $this->input->post('manual_id');
                $upload_path = 'uploads/Manuals_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['pdf_link'] = $this->input->post('old_pdf_link');
                }
                $this->Manuals_model->update($manual_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/manuals_listing');
            } else {
                $data['manuals'] = $this->Manuals_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'manuals';
                $data['page_name'] = 'manuals_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }
    public function audit_reports()
    {
        // dd($_FILES);
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Audit_Report_model');
            if ($this->input->post('add_new_audit_report')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Audit_Report_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                }
                $this->Audit_Report_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('dashboard/audit_reports');
            } else if ($this->input->post('update_audit')) {
                $data = $this->input->post();
                $audit_report_id = $this->input->post('audit_report_id');
                $upload_path = 'uploads/Audit_Report_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['pdf_link'] = $this->input->post('old_pdf_link');
                }
                $this->Audit_Report_model->update($audit_report_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/audit_reports');
            } else {
                $data['audit_reports'] = $this->Audit_Report_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'audit_reports';
                $data['page_name'] = 'audit_report_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function job_postings()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            if ($this->input->post('add_new_job')) {
                $data = $this->input->post();
                $t_data['title'] = $data['title'];
                $t_data['adv_date'] = $data['adv_date'];
                $t_data['close_date'] = $data['close_date'];
                $upload_path = 'uploads/job_postings/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['attachment_link']['name']) && $_FILES['attachment_link']['error'] == 0) {
                    $type = pathinfo($_FILES['attachment_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['attachment_link']['name'], PATHINFO_FILENAME);
                    $_FILES['attachment_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['attachment_link']['tmp_name'], $upload_path . $_FILES['attachment_link']['name']);
                    $t_data['attachment_link'] = base_url() . $upload_path . $_FILES['attachment_link']['name'];
                }

                $this->advertisements_model->create_job_posting($t_data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('dashboard/job_postings');
            } else if ($this->input->post('update_job')) {
                $data = $this->input->post();
                $job_id = $this->input->post('job_id');

                $t_data['title'] = $data['title'];
                $t_data['adv_date'] = $data['adv_date'];
                $t_data['close_date'] = $data['close_date'];

                $upload_path = 'uploads/job_postings/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['attachment_link']['name']) && $_FILES['attachment_link']['error'] == 0) {
                    $type = pathinfo($_FILES['attachment_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['attachment_link']['name'], PATHINFO_FILENAME);
                    $_FILES['attachment_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['attachment_link']['tmp_name'], $upload_path . $_FILES['attachment_link']['name']);
                    $t_data['attachment_link'] = base_url() . $upload_path . $_FILES['attachment_link']['name'];
                } else {
                    $t_data['attachment_link'] = $data['old_attachment_link'];
                }

                $this->advertisements_model->update_job_posting($job_id, $t_data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/job_postings');
            } else {
                $data['jobs'] = $this->advertisements_model->get_all_job_postings();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'job_postings';
                $data['page_name'] = 'job_postings';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_job_posting($job_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('advertisements_model');
            if (isset($job_id) && !empty($job_id)) {
                $this->advertisements_model->delete_job_posting($job_id);
                $this->session->set_flashdata('success', 'Job Deleted');
            } else {
                $this->session->set_flashdata('error', "Need Job Id");
            }
            redirect('dashboard/job_postings');
        } else {
            redirect('login');
        }
    }

    public function delete_auction()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('auctions_model');
            $auc_id = $this->input->get('auc_id');
            $this->auctions_model->delete($auc_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/auction_tender_listing');
        } else {
            redirect('login');
        }
    }

    public function delete_company_policy()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Company_policies_model');
            $cp_id = $this->input->get('cp_id');
            $this->Company_policies_model->delete($cp_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/company_policies_listing');
        } else {
            redirect('login');
        }
    }

    public function delete_manual()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Manuals_model');
            $manual_id = $this->input->get('manual_id');
            $this->Manuals_model->delete($manual_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/manuals_listing');
        } else {
            redirect('login');
        }
    }


    public function delete_procurement_plan()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Procurement_Plans_model');
            $auc_id = $this->input->get('pp_id');
            $this->Procurement_Plans_model->delete($auc_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/procurement_plan_listing');
        } else {
            redirect('login');
        }
    }

    public function delete_audit_report()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Audit_Report_model');
            $report_id = $this->input->get('report_id');
            $this->Audit_Report_model->delete($report_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/audit_reports');
        } else {
            redirect('login');
        }
    }


    public function video_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'video_listing';
            $this->load->model('videos_model');
            $data['video_fetch'] = $this->videos_model->get_all();
            if ($this->input->post('add_new_video')) {
                $data = $this->input->post();
                $upload_path = 'uploads/videos_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['image_url']['name']) && $_FILES['image_url']['error'] == 0) {
                    $type = pathinfo($_FILES['image_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['image_url']['name'], PATHINFO_FILENAME);
                    $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['image_url']['tmp_name'], $upload_path . $_FILES['image_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'];
                    $this->videos_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('dashboard/video_listing');

            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'video_management';
                $data['page_name'] = 'video_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_video()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'video_listing';
            $this->load->model('videos_model');
            $video_id = $this->input->post('id');
            if ($this->input->post('editvideo')) {
                $data = $this->input->post();
                $upload_path = 'uploads/videos_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['image_url']['name']) && $_FILES['image_url']['error'] == 0) {
                    $type = pathinfo($_FILES['image_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['image_url']['name'], PATHINFO_FILENAME);
                    $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['image_url']['tmp_name'], $upload_path . $_FILES['image_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'];
                } else {
                    $data['image_url'] = $this->input->post('old_image_url');
                }

                $this->videos_model->update($video_id, $data);
                $this->session->set_flashdata('success', 'Data updated');
                redirect('dashboard/video_listing');
            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'video_management';
                $data['page_name'] = 'video_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }

    }

    public function delete_video()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('videos_model');
            $video_id = $this->uri->segment(3);
            $this->videos_model->delete($video_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/video_listing');
        } else {
            redirect('login');
        }
    }

    public function project_category()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'project_category';
            $this->load->model('project_category_model');
            if ($this->input->post('add_new_category')) {
                $data = $this->input->post();
                if ($data) {
                    $this->project_category_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/project_category');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/project_category');
                }
            } else if ($this->input->post('editpcat')) {
                $updata = $this->input->post();
                $p_cat_id = $this->input->post('pcat_id');
                $this->project_category_model->update($p_cat_id, $updata);
                $this->session->set_flashdata('success', 'Data updated');
                redirect('dashboard/project_category');
            } else {
                $data['project_cats'] = $this->project_category_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'project_category';
                $data['page_name'] = 'project_category';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_pro_cat()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('project_category_model');
            $cat_id = $this->input->get('cat_id');
            $this->project_category_model->delete($cat_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/project_category');
        } else {
            redirect('login');
        }
    }

    public function project_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'project_category';
            $this->load->model('project_category_model');
            $this->load->model('projects_model');
            if ($this->input->post('add_project')) {
                $data = $this->input->post();
                if ($data) {
                    $project_type = $this->input->post('project_cat_id');
                    if ($project_type == 1) {
                        $data_new['project_cat_id'] = $this->input->post('project_cat_id');
                        $data_new['GS_no'] = $this->input->post('GS_no');
                        $data_new['name_of_scheme'] = $this->input->post('name_of_scheme');
                        $data_new['status'] = $this->input->post('status');
                        $data_new['cost_in_million'] = $this->input->post('cost_in_million');
                        $data_new['from_year'] = $this->input->post('from_year');
                        $data_new['to_year'] = $this->input->post('to_year');
                        $data_new['is_completed'] = '0';
                        $data_new['link_type'] = $this->input->post('link_type');
                        if ($data_new['link_type'] == 1) {

                            $upload_path = 'uploads/project_attachments/';
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, TRUE);
                            }
                            if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                                $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                                $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                                $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                                move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                                $data_new['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                            }
                        } else {
                            $data_new['link_url'] = $this->input->post('link_url');
                        }
                        $this->projects_model->create_new($data_new);
                    } elseif ($project_type == 2) {
                        $data_on['project_cat_id'] = $this->input->post('project_cat_id');
                        $data_on['GS_no'] = $this->input->post('GS_no');
                        $data_on['name_of_scheme'] = $this->input->post('name_of_scheme');
                        $data_on['status'] = $this->input->post('status');
                        $data_on['cost_in_million'] = $this->input->post('cost_in_million');
                        $data_on['from_year'] = $this->input->post('from_year');
                        $data_on['to_year'] = $this->input->post('to_year');
                        $data_on['is_completed'] = '0';
                        $data_on['link_type'] = $this->input->post('link_type');
                        if ($data_on['link_type'] == 1) {

                            $upload_path = 'uploads/project_attachments/';
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, TRUE);
                            }
                            if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                                $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                                $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                                $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                                move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                                $data_on['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                            }
                        } else {
                            $data_on['link_url'] = $this->input->post('link_url');
                        }
                        $this->projects_model->create_new($data_on);
                    } else {
                        $data_comp['project_cat_id'] = $this->input->post('project_cat_id');
                        $data_comp['GS_no'] = $this->input->post('GS_no');
                        $data_comp['name_of_scheme'] = $this->input->post('name_of_scheme');
                        $data_comp['cost_in_million'] = $this->input->post('cost_in_million');
                        $data_comp['from_year'] = $this->input->post('from_year');
                        $data_comp['to_year'] = $this->input->post('to_year');
                        $data_comp['duration'] = $this->input->post('duration');
                        $data_comp['is_completed'] = '1';
                        $data_comp['link_type'] = $this->input->post('link_type');
                        if ($data_comp['link_type'] == 1) {

                            $upload_path = 'uploads/project_attachments/';
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, TRUE);
                            }
                            if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                                $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                                $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                                $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                                move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                                $data_comp['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                            }
                        } else {
                            $data_comp['link_url'] = $this->input->post('link_url');
                        }

                        $this->projects_model->create_comp($data_comp);
                    }
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/project_management');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/project_management');
                }
            } else if ($this->input->post('edit_project')) {
                $data = $this->input->post();
                if ($data) {
                    $pro_id = $this->input->post('project_id');
                    $project_type = $this->input->post('project_cat_id');
                    if ($project_type == 1) {
                        $data_new['project_cat_id'] = $this->input->post('project_cat_id');
                        $data_new['GS_no'] = $this->input->post('GS_no');
                        $data_new['name_of_scheme'] = $this->input->post('name_of_scheme');
                        $data_new['status'] = $this->input->post('status');
                        $data_new['cost_in_million'] = $this->input->post('cost_in_million');
                        $data_new['from_year'] = $this->input->post('from_year');
                        $data_new['to_year'] = $this->input->post('to_year');
                        $data_new['is_completed'] = '0';
                        $data_new['link_type'] = $this->input->post('link_type');
                        if ($data_new['link_type'] == 1) {

                            $upload_path = 'uploads/project_attachments/';
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, TRUE);
                            }
                            if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                                $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                                $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                                $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                                move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                                $data_new['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                            }
                        } else {
                            $data_new['link_url'] = $this->input->post('link_url');
                        }
                        $this->projects_model->update_new($pro_id, $data_new);
                    } elseif ($project_type == 2) {
                        $data_on['project_cat_id'] = $this->input->post('project_cat_id');
                        $data_on['GS_no'] = $this->input->post('GS_no');
                        $data_on['name_of_scheme'] = $this->input->post('name_of_scheme');
                        $data_on['status'] = $this->input->post('status');
                        $data_on['cost_in_million'] = $this->input->post('cost_in_million');
                        $data_on['from_year'] = $this->input->post('from_year');
                        $data_on['to_year'] = $this->input->post('to_year');
                        $data_on['is_completed'] = '0';
                        $data_on['link_type'] = $this->input->post('link_type');
                        if ($data_on['link_type'] == 1) {

                            $upload_path = 'uploads/project_attachments/';
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, TRUE);
                            }
                            if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                                $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                                $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                                $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                                move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                                $data_on['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                            }
                        } else {
                            $data_on['link_url'] = $this->input->post('link_url');
                        }
                        $this->projects_model->update_new($pro_id, $data_on);
                    } else {
                        $data_comp['project_cat_id'] = $this->input->post('project_cat_id');
                        $data_comp['GS_no'] = $this->input->post('GS_no');
                        $data_comp['name_of_scheme'] = $this->input->post('name_of_scheme');
                        $data_comp['cost_in_million'] = $this->input->post('cost_in_million');
                        $data_comp['from_year'] = $this->input->post('from_year');
                        $data_comp['to_year'] = $this->input->post('to_year');
                        $data_comp['duration'] = $this->input->post('duration');
                        $data_comp['is_completed'] = '1';
                        $data_comp['link_type'] = $this->input->post('link_type');
                        if ($data_comp['link_type'] == 1) {
                            $upload_path = 'uploads/project_attachments/';
                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, TRUE);
                            }
                            if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                                $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                                $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                                $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                                move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                                $data_comp['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                            }
                        } else {
                            $data_comp['link_url'] = $this->input->post('link_url');
                        }
                        $this->projects_model->update_comp($pro_id, $data_comp);
                    }
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('dashboard/project_management');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/project_management');
                }
            } else {
                $data['project_cats'] = $this->project_category_model->get_all();
                $data['new_project'] = $this->projects_model->get_all_new();
                $data['ongoing_project'] = $this->projects_model->get_all_ongoing();
                $data['completed_project'] = $this->projects_model->get_all_completed();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'projects_management';
                $data['page_name'] = 'projects_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_project()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('projects_model');
            $project_id = $this->input->get('project_id');
            $this->projects_model->delete($project_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/project_management');
        } else {
            redirect('login');
        }
    }

    public function research_thesis_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'research_thesis';
            $this->load->model('research_thesis_model');
            $data['research'] = $this->research_thesis_model->get_all();
            if ($this->input->post('editresearch')) {
                $data = $_POST;
                $id = $this->input->post('id');
                $this->research_thesis_model->update($id, $data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('dashboard/research_thesis_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('dashboard/research_thesis_listing');
                }
            } else if ($this->input->post('add_new_research')) {
                $data = $_POST;
                $this->research_thesis_model->create($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/research_thesis_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('dashboard/research_thesis_listing');
                }
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'research_management';
            $data['page_name'] = 'research_thesis';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }

    }

    public function delete_research()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('research_thesis_model');
            $research_id = $this->uri->segment(3);
            $this->research_thesis_model->delete($research_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/research_thesis_listing');
        } else {
            redirect('login');
        }
    }

    public function species_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'species_table';
            $this->load->model('species_table_model');
            $data['species'] = $this->species_table_model->get_all();
            if ($this->input->post('editspecies')) {
                $data = $_POST;
                $id = $this->input->post('id');
                $this->species_table_model->update($id, $data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('dashboard/species_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('dashboard/species_listing');
                }
            } else if ($this->input->post('add_new_species')) {
                $data = $_POST;
                $this->species_table_model->create($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/species_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('dashboard/species_listing');
                }
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'species_management';
            $data['page_name'] = 'species_table';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function delete_species()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('species_table_model');
            $species_id = $this->uri->segment(3);
            $this->species_table_model->delete($species_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/species_listing');
        } else {
            redirect('login');
        }
    }

    public function report_category_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'report_category';
            $this->load->model('report_category_model');
            $data['report_category'] = $this->report_category_model->get_all();
            if ($this->input->post('edit_reportcategory')) {
                $data = $_POST;
                $id = $this->input->post('id');
                $this->report_category_model->update($id, $data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('dashboard/report_category_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('dashboard/report_category_listing');
                }
            } else if ($this->input->post('add_new_reportcategory')) {
                $data = $_POST;
                $this->report_category_model->create($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/report_category_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('dashboard/report_category_listing');
                }
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'report_category_management';
            $data['page_name'] = 'report_category';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function delete_report_category()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('report_category_model');
            $reportcategory_id = $this->uri->segment(3);
            $this->report_category_model->delete($reportcategory_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/report_category_listing');
        } else {
            redirect('login');
        }
    }

    public function reports_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'project_category';
            $this->load->model('report_category_model');
            $this->load->model('reports_model');
            if ($this->input->post('add_new_report')) {
                $data = $this->input->post();
                $upload_path = 'uploads/Reports_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                    $this->reports_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('dashboard/reports_management');
            } else if ($this->input->post('update_report')) {
                $data = $this->input->post();
                $report_id = $this->input->post('report_id');
                $upload_path = 'uploads/Reports_Documents/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['pdf_link'] = $this->input->post('old_pdf_link');
                }
                $this->reports_model->update($report_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/reports_management');
            } else {
                $data['reports_cats'] = $this->report_category_model->get_all();
                $data['reports'] = $this->reports_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'evaluation_reports';
                $data['page_name'] = 'reports_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_report()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('reports_model');
            $report_id = $this->input->get('report_id');
            $this->reports_model->delete($report_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/reports_management');
        } else {
            redirect('login');
        }
    }

    public function poultry_items_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'poultry_items';
            $this->load->model('poultry_items_model');
            $data['poultry'] = $this->poultry_items_model->get_all();
            if ($this->input->post('editpoultry')) {
                $data = $_POST;
                $id = $this->input->post('id');
                $this->poultry_items_model->update($id, $data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('dashboard/poultry_items_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('dashboard/poultry_items_listing');
                }
            } else if ($this->input->post('add_new_poultry')) {
                $data = $_POST;
                $this->poultry_items_model->create($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/poultry_items_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('dashboard/poultry_items_listing');
                }
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'poultry_management';
            $data['page_name'] = 'poultry_items';
            $this->load->view('index', $data);

        } else {
            redirect('login');
        }
    }

    public function delete_poultry()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('poultry_items_model');
            $poultry_id = $this->uri->segment(3);
            $this->poultry_items_model->delete($poultry_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/poultry_items_listing');
        } else {
            redirect('login');
        }
    }

    public function poultry_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('poultry_products_model');
            $this->load->model('poultry_items_model');
            if ($this->input->post('add_new_product')) {
                $data = $this->input->post();
                if ($data) {
                    $this->poultry_products_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/poultry_management');
            } else if ($this->input->post('update_p_product')) {
                $data = $this->input->post();
                $product_id = $this->input->post('product_id');
                if ($product_id) {
                    $this->poultry_products_model->update($product_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('success', 'Empty Submission');
                }

                redirect('dashboard/poultry_management');
            } else {
                $data['p_items'] = $this->poultry_items_model->get_all();
                $data['products'] = $this->poultry_products_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'poultry_product_listing';
                $data['page_name'] = 'poultry_product_listing';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }

    }

    public function delete_poultry_product()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('poultry_products_model');
            $product_id = $this->input->get('p_product_id');
            $this->poultry_products_model->delete($product_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/poultry_management');
        } else {
            redirect('login');
        }
    }

    public function population_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('population_model');
            $this->load->model('species_table_model');
            if ($this->input->post('add_new_population')) {
                $data = $this->input->post();
                if ($data) {
                    $this->population_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/population_management');
            } else if ($this->input->post('update_p_product')) {
                $data = $this->input->post();
                $population_id = $this->input->post('population_id');
                if ($population_id) {
                    $this->population_model->update($population_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('success', 'Empty Submission');
                }

                redirect('dashboard/population_management');
            } else {
                $data['species'] = $this->species_table_model->get_all();
                $data['populations'] = $this->population_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'population_management';
                $data['page_name'] = 'population_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_population_product()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('population_model');
            $population_id = $this->input->get('population_id');
            $this->population_model->delete($population_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/population_management');
        } else {
            redirect('login');
        }
    }

    public function stock_pattern_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('stock_pattern_table_model');
            if ($this->input->post('add_new_stock')) {
                $data = $this->input->post();
                if ($data) {
                    $this->stock_pattern_table_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/stock_pattern_management');
            } else if ($this->input->post('update_stock_p')) {
                $data = $this->input->post();
                $stock_id = $this->input->post('stock_id');
                if ($stock_id) {
                    $this->stock_pattern_table_model->update($stock_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('success', 'Empty Submission');
                }

                redirect('dashboard/stock_pattern_management');
            } else {
                $data['stocks'] = $this->stock_pattern_table_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'stockPattern_management';
                $data['page_name'] = 'stockPattern_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_stockpattern()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('stock_pattern_table_model');
            $stock_id = $this->input->get('stock_id');
            $this->stock_pattern_table_model->delete($stock_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/stock_pattern_management');
        } else {
            redirect('login');
        }
    }

    public function export_major_commodities()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'banner_listing';
            $this->load->model('export_major_model');
            $this->load->model('poultry_items_model');
            $this->load->model('species_table_model');
            if ($this->input->post('add_new_export')) {
                $data = $this->input->post();
                if ($data) {
                    $this->export_major_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/export_major_commodities');
            } else if ($this->input->post('update_new_export')) {
                $data = $this->input->post();
                $export_id = $this->input->post('export_id');
                if ($export_id) {
                    $this->export_major_model->update($export_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('success', 'Empty Submission');
                }
                redirect('dashboard/export_major_commodities');
            } else {
                $data['p_items'] = $this->poultry_items_model->get_all();
                $data['species'] = $this->species_table_model->get_all();
                $data['export_major'] = $this->export_major_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'export_major';
                $data['page_name'] = 'export_major';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_major_export()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('export_major_model');
            $export_id = $this->input->get('export_id');
            $this->export_major_model->delete($export_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/export_major_commodities');
        } else {
            redirect('login');
        }
    }

//    advertisement //

    public function advertisements_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $search = 'advertisements_listing';

            $this->load->model('advertisements_model');
            $data['advertisements'] = $this->advertisements_model->get_all();
            if ($this->input->post('add_new_advertisements')) {
                $data = $this->input->post();
                $upload_path = 'uploads/advertisements_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['advertisement_image_url']['name']) && $_FILES['advertisement_image_url']['error'] == 0) {
                    $type = pathinfo($_FILES['advertisement_image_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['advertisement_image_url']['name'], PATHINFO_FILENAME);
                    $_FILES['advertisement_image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['advertisement_image_url']['tmp_name'], $upload_path . $_FILES['advertisement_image_url']['name']);
                    $data['advertisement_image_url'] = base_url() . $upload_path . $_FILES['advertisement_image_url']['name'];
                }
                $this->advertisements_model->create($data);
                redirect('dashboard/advertisements_listing');
            } else if ($this->input->post('edit_advertisements')) {
                $data = $this->input->post();
                $upload_path = 'uploads/advertisements_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['advertisement_image_url']['name']) && $_FILES['advertisement_image_url']['error'] == 0) {
                    $type = pathinfo($_FILES['advertisement_image_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['advertisement_image_url']['name'], PATHINFO_FILENAME);
                    $_FILES['advertisement_image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['advertisement_image_url']['tmp_name'], $upload_path . $_FILES['advertisement_image_url']['name']);
                    $data['advertisement_image_url'] = base_url() . $upload_path . $_FILES['advertisement_image_url']['name'];
                } else {
                    $data['advertisement_image_url'] = $this->input->post('old_advertisement_image_url');
                }
                $advertisements_id = $this->input->post('id');
                $this->advertisements_model->update($advertisements_id, $data);
                redirect('dashboard/advertisements_listing');
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'advertisements_management';
            $data['page_name'] = 'advertisements_management';
            $this->load->view('index', $data);

        } else {
            redirect('login');
        }
    }

    public function delete_advertisements()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            $advertisements_id = $this->uri->segment(3);
            $this->advertisements_model->delete($advertisements_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/advertisements_listing');
        } else {
            redirect('login');
        }
    }

//   virtual_governance
    public function virtual_governance_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $search = 'virtual_governance_listing';

            $this->load->model('virtual_governance_model');
            $data['vitual_gov'] = $this->virtual_governance_model->get_all();
            if ($this->input->post('add_new_virtual_governance')) {
                $data = $this->input->post();
                $upload_path = 'uploads/virtual_governance_document/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['virtual_url']['name']) && $_FILES['virtual_url']['error'] == 0) {
                    $type = pathinfo($_FILES['virtual_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['virtual_url']['name'], PATHINFO_FILENAME);
                    $_FILES['virtual_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['virtual_url']['tmp_name'], $upload_path . $_FILES['virtual_url']['name']);
                    $data['virtual_url'] = base_url() . $upload_path . $_FILES['virtual_url']['name'];
                }
                $this->virtual_governance_model->create($data);
                redirect('dashboard/virtual_governance_listing');
            } else if ($this->input->post('edit_virtual_governance')) {
                $data = $this->input->post();
                $upload_path = 'uploads/virtual_governance_document/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['virtual_url']['name']) && $_FILES['virtual_url']['error'] == 0) {
                    $type = pathinfo($_FILES['virtual_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['virtual_url']['name'], PATHINFO_FILENAME);
                    $_FILES['virtual_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['virtual_url']['tmp_name'], $upload_path . $_FILES['virtual_url']['name']);
                    $data['virtual_url'] = base_url() . $upload_path . $_FILES['virtual_url']['name'];
                } else {
                    $data['virtual_url'] = $this->input->post('previous_virtual_url');
                }
                $virtual_id = $this->input->post('id');
                $this->virtual_governance_model->update($virtual_id, $data);
                redirect('dashboard/virtual_governance_listing');
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'virtual_governance_management';
            $data['page_name'] = 'virtual_governance';
            $this->load->view('index', $data);

        } else {
            redirect('login');
        }
    }

    public function main_slider_management()
    {
    
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('main_banners_model');
            $array = ['is_main_slider'=>'1'];
            $data['slider_images'] = $this->main_banners_model->get_all_slider_imgs($array);
            if ($this->input->post('add_new_slider_img')) {
                $data = $this->input->post();
                if (isset($data['title']) && !empty($data['title'])) {
                    $t_data['title'] = trim($data['title']);
                    $t_data['priority'] = trim($data['priority']);
                    $t_data['is_main_slider'] = 1;

                    $has_banner = $this->main_banners_model->get_slider_imgs_by_title($t_data['title']);
                    if (!isset($has_banner) && empty($has_banner)) {
                        $t_data['title'] = trim($data['title']);
                        $upload_path = '../images/main_slider_banners/';
                        if (!is_dir($upload_path)) {
                            mkdir($upload_path, 0777, TRUE);
                        }
                        if (!empty($_FILES['banner_url']['name']) && $_FILES['banner_url']['error'] == 0) {
                            $type = pathinfo($_FILES['banner_url']['name'], PATHINFO_EXTENSION);
                            //$file_name = pathinfo($_FILES['banner_url']['name'], PATHINFO_FILENAME);
                            $_FILES['banner_url']['name'] = str_replace(' ','',$t_data['title']). "." . strtolower($type);
                            move_uploaded_file($_FILES['banner_url']['tmp_name'], $upload_path . $_FILES['banner_url']['name']);
                            $t_data['banner_url'] = $upload_path . $_FILES['banner_url']['name'];
                        }
                        $this->session->set_flashdata('success', 'Slider Image Added');
                        $this->main_banners_model->add_slider_img($t_data);
                    } else {
                        $this->session->set_flashdata('error', "An Image for this Title already Exists Please Choose a different title");
                    }
                } else {
                    $this->session->set_flashdata('error', "Title is Required");
                }
                redirect('dashboard/main_slider_management');
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'main_slider_management';
            $data['page_name'] = 'main_slider';
            $this->load->view('index', $data);

        } else {
            redirect('login');
        }
    }

    public function add_employee()
    {
        $data['folder_name'] = 'dashboard';
        $data['file_name'] = 'add_employee';
        $data['page_name'] = 'add_employee';
        $this->load->view('index', $data);
    }

    public function edit_employee($id)
    {
        $this->load->model('Employee_verification_model');
        $data['employee'] = $this->Employee_verification_model->get_by_id($id);
        $data['folder_name'] = 'dashboard';
        $data['file_name'] = 'edit_employee';
        $data['page_name'] = 'edit_employee';
        $this->load->view('index', $data);
    }
    public function employee_listing()
    {
        $this->load->library('form_validation');
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Employee_verification_model');
            $data['employees'] = $this->Employee_verification_model->get_all();
            if ($this->input->post('add_new_employee')) {

                
                $this->form_validation->set_rules('name','Name','required');
                $this->form_validation->set_rules('cnic','CNIC','required|is_unique[employee_verification.cnic]');
                $this->form_validation->set_rules('mobile','Mobile','required');
                $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[employee_verification.email]');
                if ($this->form_validation->run()== FALSE) {
                    $errors = $this->form_validation->error_array();
                    $this->session->set_flashdata('error',$errors);
                    redirect('dashboard/add_employee');
                    // redirect($_SERVER['HTTP_REFERER']);
                }else {
                    $data = $this->input->post();
                    if($this->Employee_verification_model->add_employee($data))
                    {
                        $this->session->set_flashdata('success','Employee Added successfully.');
                        redirect('dashboard/employee_listing');
                    }else{
                        $this->session->set_flashdata('fail','Employee not added.Please try again.');
                        redirect('dashboard/add_employee');
                    }
                }
                    
            }else if($this->input->post('edit_employee')){
                $this->form_validation->set_rules('name','Name','required');
                
                $this->form_validation->set_rules('mobile','Mobile','required');
                $this->form_validation->set_rules('is_active','Status','required|exact_length[1]');
                if ($this->form_validation->run()== FALSE) {
                    $errors = $this->form_validation->error_array();
                    $this->session->set_flashdata('error',$errors);
                    redirect('dashboard/edit_employee');
                    // redirect($_SERVER['HTTP_REFERER']);
                }else {
                    $data = $this->input->post();
                    $id = $this->input->post('employee_id');
                    if($this->Employee_verification_model->update($id,$data))
                    {
                        $this->session->set_flashdata('success','Employee updated successfully.');
                        redirect('dashboard/employee_listing');
                    }else{
                        $this->session->set_flashdata('fail','Employee not update.Please try again.');
                        redirect('dashboard/edit_employee/'.$id);
                    }
                }
            }else{
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'employee_management';
                $data['page_name'] = 'employees';
                $this->load->view('index', $data);
            }

        } else {
            redirect('login');
        }
    }

    public function slider_management()
    {
        
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('main_banners_model');
            $array = ['is_main_slider'=>'0'];
            $data['slider_images'] = $this->main_banners_model->get_all_slider_imgs($array);
            if ($this->input->post('add_new_slider_img')) {
                $data = $this->input->post();
                
                // dd($data);
                if (isset($data['title']) && !empty($data['title'])) {
                    $t_data['title'] = trim($data['title']);
                    $t_data['priority'] = trim($data['priority']);
                    $t_data['slider_number'] = trim($data['slider_number']);
                    $has_banner = $this->main_banners_model->get_slider_imgs_by_title($t_data['title']);
                   
                    if (!isset($has_banner) && empty($has_banner)) {
                        $t_data['title'] = trim($data['title']);
                        $upload_path = '../images/main_slider_banners/';
                        if (!is_dir($upload_path)) {
                            mkdir($upload_path, 0777, TRUE);
                        }

                        if (!empty($_FILES['banner_url']['name']) && $_FILES['banner_url']['error'] == 0) {
                            $type = pathinfo($_FILES['banner_url']['name'], PATHINFO_EXTENSION);
                            //$file_name = pathinfo($_FILES['banner_url']['name'], PATHINFO_FILENAME);
                            $_FILES['banner_url']['name'] = str_replace(' ','',$t_data['title']). "." . strtolower($type);
                            move_uploaded_file($_FILES['banner_url']['tmp_name'], $upload_path . $_FILES['banner_url']['name']);
                            $t_data['banner_url'] = $upload_path . $_FILES['banner_url']['name'];
                        }
                        $this->session->set_flashdata('success', 'Slider Image Added');
                        $this->main_banners_model->add_slider_img($t_data);
                    } else {
                        $this->session->set_flashdata('error', "An Image for this Title already Exists Please Choose a different title");
                    }
                } else {
                    $this->session->set_flashdata('error', "Title is Required");
                }
                redirect('dashboard/slider_management');
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'slider_management';
            $data['page_name'] = 'slider_management';
            $this->load->view('index', $data);

        } else {
            redirect('login');
        }
    }

    public function main_slider_priority()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('main_banners_model');
            if ($this->input->post('set_slider_priority')) {
                $data = $this->input->post();
                $redirect = $data['priority_redirect'];
                if (isset($data['priority']) && !empty($data['priority']) && isset($data['main_slider_id']) && $data['main_slider_id'] != "") {
                    $t_data['priority'] = trim($data['priority']);

                    $this->main_banners_model->set_slider_priority($data['main_slider_id'], $t_data);
                    $this->session->set_flashdata('success', 'Slider Priority has been updated');
                } else {
                    $this->session->set_flashdata('error', "No Record Found");
                }

                redirect('dashboard/'.$redirect);
            }

            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'main_slider';
            $data['page_name'] = 'main_slider';
            $this->load->view('index', $data);


        } else {
            redirect('login');
        }
    }

    public function delete_virtual_governance()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('virtual_governance_model');
            $virtual_id = $this->uri->segment(3);
            $this->virtual_governance_model->delete($virtual_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/virtual_governance_listing');
        } else {
            redirect('login');
        }
    }

    public function delete_slider_banner($img_id = null,$name='main')
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($img_id) && !empty($img_id)) {
                $this->load->model('main_banners_model');
                $this->main_banners_model->delete_slider_img($img_id);
                $this->session->set_flashdata('success', 'Slider Image Deleted');
                if($name=='slider')
                {
                    redirect('dashboard/slider_management');
                }
                redirect('dashboard/main_slider_management');
            } else {
                $this->session->set_flashdata('error', "No Banner Id Found");
                if($name=='slider')
                {
                    redirect('dashboard/slider_management');
                }
                redirect('dashboard/main_slider_management');
            }
        } else {
            redirect('login');
        }
    }

    public function marital_status_management()
    {
        $this->load->model('Reference_management_model');
        if ($this->input->post('add_new_marital_status')) {

            $data = $this->input->post();
            if ($data) {

                $this->Reference_management_model->add($data,'marital_status');
                $this->session->set_flashdata('success', 'Data added');
            } else {
                $this->session->set_flashdata('error', 'Empty Submission');
            }
            redirect('dashboard/marital_status_management');

        }elseif($this->input->post('edit_marital_status')){
            $data = $this->input->post();
            $id = $this->input->post('marital_status_id');
            if(!empty($this->input->post('marital_status_id'))){
               $result =  $this->Reference_management_model->get_by_id($id,'marital_status');
                if($result){
                    $this->Reference_management_model->update($id,$data,'marital_status');
                    $this->session->set_flashdata('success', 'Data updated.');
                    redirect('dashboard/marital_status_management');
                }else{
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/marital_status_management');
                }
            }else{
                $this->session->set_flashdata('error', 'Empty Submission');
                redirect('dashboard/marital_status_management');
            }
        }else{
            $data['marital_status'] = $this->Reference_management_model->get_all('marital_status');
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'marital_status_listing';
            $data['page_name'] = 'marital_status_listing';
            $this->load->view('index', $data);
        }
    }

    public function delete_employee($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Employee_verification_model');
                $this->Employee_verification_model->delete($id);
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/employee_listing');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/employee_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_marital_status($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Reference_management_model');
                $this->Reference_management_model->delete($id,'marital_status');
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/marital_status_management');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/marital_status_management');
            }
        } else {
            redirect('login');
        }
    }

    public function programs_management()
    {
        $this->load->model('Reference_management_model');
        if ($this->input->post('add_new_program')) {

            $data = $this->input->post();
            if ($data) {

                $this->Reference_management_model->add($data,'programs');
                $this->session->set_flashdata('success', 'Data added');
            } else {
                $this->session->set_flashdata('error', 'Empty Submission');
            }
            redirect('dashboard/programs_management');

        }elseif($this->input->post('edit_program')){
            $data = $this->input->post();
            $id = $this->input->post('program_id');
            if(!empty($this->input->post('program_id'))){
               $result =  $this->Reference_management_model->get_by_id($id,'programs');
                if($result){
                    $this->Reference_management_model->update($id,$data,'programs');
                    $this->session->set_flashdata('success', 'Data updated.');
                    redirect('dashboard/programs_management');
                }else{
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/programs_management');
                }
            }else{
                $this->session->set_flashdata('error', 'Empty Submission');
                redirect('dashboard/programs_management');
            }
        }else{
            $data['programs'] = $this->Reference_management_model->get_all('programs');
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'programs_listing';
            $data['page_name'] = 'programs_listing';
            $this->load->view('index', $data);
        }
    }

    public function delete_program($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Reference_management_model');
                $this->Reference_management_model->delete($id,'programs');
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/programs_management');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/programs_management');
            }
        } else {
            redirect('login');
        }
    }

    public function offices_management()
    {
        $this->load->model('Reference_management_model');
        if ($this->input->post('add_new_office')) {

            $data = $this->input->post();
            if ($data) {

                $this->Reference_management_model->add($data,'offices');
                $this->session->set_flashdata('success', 'Data added');
            } else {
                $this->session->set_flashdata('error', 'Empty Submission');
            }
            redirect('dashboard/offices_management');

        }elseif($this->input->post('edit_office')){
            $data = $this->input->post();
            $id = $this->input->post('office_id');
            if(!empty($this->input->post('office_id'))){
               $result =  $this->Reference_management_model->get_by_id($id,'offices');
                if($result){
                    $this->Reference_management_model->update($id,$data,'offices');
                    $this->session->set_flashdata('success', 'Data updated.');
                    redirect('dashboard/offices_management');
                }else{
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/offices_management');
                }
            }else{
                $this->session->set_flashdata('error', 'Empty Submission');
                redirect('dashboard/offices_management');
            }
        }else{
            $data['offices'] = $this->Reference_management_model->get_all('offices');
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'offices_listing';
            $data['page_name'] = 'offices_listing';
            $this->load->view('index', $data);
        }
    }

    public function delete_office($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Reference_management_model');
                $this->Reference_management_model->delete($id,'offices');
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/offices_management');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/offices_management');
            }
        } else {
            redirect('login');
        }
    }

    public function qualifications_management()
    {
        $this->load->model('Reference_management_model');
        if ($this->input->post('add_new_qualification')) {

            $data = $this->input->post();
            if ($data) {

                $this->Reference_management_model->add($data,'qualifications');
                $this->session->set_flashdata('success', 'Data added');
            } else {
                $this->session->set_flashdata('error', 'Empty Submission');
            }
            redirect('dashboard/qualifications_management');

        }elseif($this->input->post('edit_qualification')){
            $data = $this->input->post();
            $id = $this->input->post('qualification_id');
            if(!empty($this->input->post('qualification_id'))){
               $result =  $this->Reference_management_model->get_by_id($id,'qualifications');
                if($result){
                    $this->Reference_management_model->update($id,$data,'qualifications');
                    $this->session->set_flashdata('success', 'Data updated.');
                    redirect('dashboard/qualifications_management');
                }else{
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/qualifications_management');
                }
            }else{
                $this->session->set_flashdata('error', 'Empty Submission');
                redirect('dashboard/qualifications_management');
            }
        }else{
            $data['qualifications'] = $this->Reference_management_model->get_all('qualifications');
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'qualifications_listing';
            $data['page_name'] = 'qualifications_listing';
            $this->load->view('index', $data);
        }
    }

    public function delete_qualification($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Reference_management_model');
                $this->Reference_management_model->delete($id,'qualifications');
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/qualifications_management');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/qualifications_management');
            }
        } else {
            redirect('login');
        }
    }


    public function relations_management()
    {
        $this->load->model('Reference_management_model');
        if ($this->input->post('add_new_relation')) {

            $data = $this->input->post();
            if ($data) {

                $this->Reference_management_model->add($data,'relations');
                $this->session->set_flashdata('success', 'Data added');
            } else {
                $this->session->set_flashdata('error', 'Empty Submission');
            }
            redirect('dashboard/relations_management');

        }elseif($this->input->post('edit_relation')){
            $data = $this->input->post();
            $id = $this->input->post('relation_id');
            if(!empty($this->input->post('relation_id'))){
               $result =  $this->Reference_management_model->get_by_id($id,'relations');
                if($result){
                    $this->Reference_management_model->update($id,$data,'relations');
                    $this->session->set_flashdata('success', 'Data updated.');
                    redirect('dashboard/relations_management');
                }else{
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/relations_management');
                }
            }else{
                $this->session->set_flashdata('error', 'Empty Submission');
                redirect('dashboard/relations_management');
            }
        }else{
            $data['relations'] = $this->Reference_management_model->get_all('relations');
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'relations_listing';
            $data['page_name'] = 'relations_listing';
            $this->load->view('index', $data);
        }
    }

    public function delete_relation($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Reference_management_model');
                $this->Reference_management_model->delete($id,'relations');
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/relations_management');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/relations_management');
            }
        } else {
            redirect('login');
        }
    }


    public function import_employee()
    {
        $this->load->library(array('excel','form_validation'));
        if($this->input->post('upload_file'))
        {
            $this->load->library('excel');
		    $spreadsheet = new PHPExcel();
            $path = $_FILES["add_employee"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $name = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $cnic = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $email = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $mobile = $worksheet->getCellByColumnAndRow(3, $row)->getValue();

                    	
                    $data = array();
                    $data = array(
                        'name' => $name,
                        'cnic' => $cnic,
                        'email' => $email,
                        'mobile' => $mobile
                    );
                    $this->load->model('Employee_verification_model');
                    $this->Employee_verification_model->insert($data);
                }
               
                $this->session->flashdata('success','Data Imported successfully');
                redirect('dashboard/employee_listing');
            }
           
        }else{
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'add_employee';
            $data['page_name'] = 'import_employee';
            $this->load->view('index', $data);
        }
    }

    public function designations_management()
    {
        $this->load->model('Reference_management_model');
        if ($this->input->post('add_new_designation')) {

            $data = $this->input->post();
            if ($data) {

                $this->Reference_management_model->add($data,'designations');
                $this->session->set_flashdata('success', 'Data added');
            } else {
                $this->session->set_flashdata('error', 'Empty Submission');
            }
            redirect('dashboard/designations_management');

        }elseif($this->input->post('edit_designation')){
            $data = $this->input->post();
            $id = $this->input->post('designation_id');
            if(!empty($this->input->post('designation_id'))){
               $result =  $this->Reference_management_model->get_by_id($id,'designations');
                if($result){
                    $this->Reference_management_model->update($id,$data,'designations');
                    $this->session->set_flashdata('success', 'Data updated.');
                    redirect('dashboard/designations_management');
                }else{
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/designations_management');
                }
            }else{
                $this->session->set_flashdata('error', 'Empty Submission');
                redirect('dashboard/designations_management');
            }
        }else{
            $data['designations'] = $this->Reference_management_model->get_all('designations');
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'designations_listing';
            $data['page_name'] = 'designations_listing';
            $this->load->view('index', $data);
        }
    }

    public function directorates_management()
    {
        $this->load->model('Reference_management_model');
        if ($this->input->post('add_new_directorate')) {

            $data = $this->input->post();
            if ($data) {

                $this->Reference_management_model->add($data,'directorates');
                $this->session->set_flashdata('success', 'Data added');
            } else {
                $this->session->set_flashdata('error', 'Empty Submission');
            }
            redirect('dashboard/directorates_management');

        }elseif($this->input->post('edit_directorate')){
            $data = $this->input->post();
            $id = $this->input->post('directorate_id');
            if(!empty($this->input->post('directorate_id'))){
               $result =  $this->Reference_management_model->get_by_id($id,'directorates');
                if($result){
                    $this->Reference_management_model->update($id,$data,'directorates');
                    $this->session->set_flashdata('success', 'Data updated.');
                    redirect('dashboard/directorates_management');
                }else{
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/directorates_management');
                }
            }else{
                $this->session->set_flashdata('error', 'Empty Submission');
                redirect('dashboard/directorates_management');
            }
        }else{
            $data['directorates'] = $this->Reference_management_model->get_all('directorates');
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'directorates_listing';
            $data['page_name'] = 'directorates_listing';
            $this->load->view('index', $data);
        }
    }

    public function delete_designation($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Reference_management_model');
                $this->Reference_management_model->delete($id,'designations');
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/designations_management');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/designations_management');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_directorate($id)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($id) && !empty($id)) {
                $this->load->model('Reference_management_model');
                $this->Reference_management_model->delete($id,'directorates');
                $this->session->set_flashdata('success', 'Record Deleted');
                redirect('dashboard/directorates_management');
            } else {
                $this->session->set_flashdata('error', "No Record Found");
                redirect('dashboard/directorates_management');
            }
        } else {
            redirect('login');
        }
    }

    public function provisional_budget()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'squad_where_about_listing';
            $this->load->model('provisional_budget_model');
            if ($this->input->post('add_new_about_us')) {

                $data = $this->input->post();

                if ($data) {
                    $upload_path = 'uploads/AboutUs/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];

                    }
                    $this->provisional_budget_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/provisional_budget');

            } else if ($this->input->post('edit_aboutusform')) {
                $data = $this->input->post();
                if ($data) {
                    $saboutus_id = $this->input->post('aboutus_id');
                    $upload_path = 'uploads/AboutUs/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                    } else {
                        $data['link_url'] = $this->input->post('old_link_url');
                    }
                    $this->provisional_budget_model->update($saboutus_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/provisional_budget');
            } else {
                $data['aboutus_listing'] = $this->provisional_budget_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'provisional_budget';
                $data['page_name'] = 'provisional_budget';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function poverty_alleviation()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('poverty_alleviation_model');
            if ($this->input->post('add_new_poverty')) {
                $data = $this->input->post();
                if ($data) {
                    $upload_path = 'uploads/Poverty/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];

                    }
                    $this->poverty_alleviation_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/poverty_alleviation');

            } else if ($this->input->post('edit_povertyform')) {
                $data = $this->input->post();
                if ($data) {
                    $poverty_id = $this->input->post('poverty_id');
                    $upload_path = 'uploads/Poverty/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                    } else {
                        $data['link_url'] = $this->input->post('old_link_url');
                    }
                    $this->poverty_alleviation_model->update($poverty_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                }
                redirect('dashboard/poverty_alleviation');
            } else {
                $data['aboutus_listing'] = $this->poverty_alleviation_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'poverty_alleviation';
                $data['page_name'] = 'poverty_alleviation';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }


    public function delete_provisional_budget()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('provisional_budget_model');
            $aboutus_id = $this->input->get('aboutus_id');
            $this->provisional_budget_model->delete($aboutus_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/provisional_budget');
        } else {
            redirect('login');
        }
    }

    public function delete_poverty_alleviation()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('poverty_alleviation_model');
            $poverty_id = $this->input->get('poverty_id');
            $this->poverty_alleviation_model->delete($poverty_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/poverty_alleviation');
        } else {
            redirect('login');
        }
    }

    public function squad_where_about_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'squad_where_about_listing';
            $this->load->model('squad_where_about_model');
            $data['squad_where_about'] = $this->squad_where_about_model->get_all();
            if ($this->input->post('add_new_squad_where_about')) {
                $data = $this->input->post();
                if ($data) {
                    $this->squad_where_about_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/squad_where_about_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not added');
                    redirect('dashboard/squad_where_about_listing');
                }
            } else if ($this->input->post('edit_squad_where_about')) {
                $data = $this->input->post();
                $squad_where_about_id = $this->input->post('id');
                if ($data) {
                    $this->squad_where_about_model->update($squad_where_about_id, $data);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('dashboard/squad_where_about_listing');
                } else {
                    $this->session->set_flashdata('fail', 'Data not Updated');
                    redirect('dashboard/squad_where_about_listing');
                }
            }
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'squad_where_about_management';
            $data['page_name'] = 'squad_where_about';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function delete_squad_where_about()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('squad_where_about_model');
            $squad_where_about_id = $this->uri->segment(3);
            $this->squad_where_about_model->delete($squad_where_about_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/squad_where_about_listing');
        } else {
            redirect('login');
        }
    }

    public function staff_corners()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'project_category';
            $this->load->model('staff_corner_model');
            $this->load->model('projects_model');
            $this->load->model('directorates_model');
            if ($this->input->post('add_staff')) {
                $data = $this->input->post();
                if ($data) {
                    $data_new['staff_type_id'] = $this->input->post('staff_cat_id');
                    $data_new['name_of_officer'] = $this->input->post('name_of_officer');
                    $data_new['transfer_from'] = $this->input->post('transfer_from');
                    $data_new['designation_from'] = $this->input->post('designation_from');
                    $data_new['transfer_to'] = $this->input->post('transfer_to');
                    $data_new['designation_to'] = $this->input->post('designation_to');
                    $data_new['date'] = $this->input->post('date');
                    $data_new['order_no'] = $this->input->post('order_no');
                    $data_new['scale'] = $this->input->post('scale');
                    $data_new['instructions_texts'] = $this->input->post('instructions_texts');
                    $data_new['notification_no'] = $this->input->post('notification_no');
                    $data_new['directorate_id'] = $this->input->post('directorate_id');
                    $upload_path = 'uploads/staff_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['status_link']['name']) && $_FILES['status_link']['error'] == 0) {
                        $type = pathinfo($_FILES['status_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['status_link']['name'], PATHINFO_FILENAME);
                        $_FILES['status_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['status_link']['tmp_name'], $upload_path . $_FILES['status_link']['name']);
                        $data_new['status_link'] = base_url() . $upload_path . $_FILES['status_link']['name'];
                    }

                    $this->staff_corner_model->create_new($data_new);

                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/staff_corners');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/staff_corners');
                }
            } else if ($this->input->post('edit_staff')) {
                $data = $this->input->post();
                // echo "<pre>"; print_r($_FILES); exit;

                if ($data) {
                    $pro_id = $this->input->post('staff_id');
                    $data_comp['staff_type_id'] = $this->input->post('staff_cat_id');
                    $data_comp['name_of_officer'] = $this->input->post('name_of_officer');
                    $data_comp['transfer_from'] = $this->input->post('transfer_from');
                    $data_comp['designation_from'] = $this->input->post('designation_from');
                    $data_comp['transfer_to'] = $this->input->post('transfer_to');
                    $data_comp['designation_to'] = $this->input->post('designation_to');
                    $data_comp['date'] = $this->input->post('date');
                    $data_comp['order_no'] = $this->input->post('order_no');
                    $data_comp['scale'] = $this->input->post('scale');
                    $data_comp['instructions_texts'] = $this->input->post('instructions_texts');
                    $data_comp['notification_no'] = $this->input->post('notification_no');
                    $data_comp['directorate_id'] = $this->input->post('directorate_id');
                    $upload_path = 'uploads/staff_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['status_link']['name']) && $_FILES['status_link']['error'] == 0) {
                        $type = pathinfo($_FILES['status_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['status_link']['name'], PATHINFO_FILENAME);
                        $_FILES['status_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['status_link']['tmp_name'], $upload_path . $_FILES['status_link']['name']);
                        $data_comp['status_link'] = base_url() . $upload_path . $_FILES['status_link']['name'];
                    } else {
                        $data_comp['status_link'] = $this->input->post('old_status_links');
                    }

                    $this->staff_corner_model->update_new($pro_id, $data_comp);
                    $this->session->set_flashdata('success', 'Data Updated');
                    redirect('dashboard/staff_corners');
                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/staff_corners');
                }
            } else {
                $data['transfer_posting'] = $this->staff_corner_model->get_all_transfer_posting();
                $data['all_suspension'] = $this->staff_corner_model->get_all_suspension();
                $data['all_notification'] = $this->staff_corner_model->get_all_notification();
                $data['promotions'] = $this->staff_corner_model->get_all_promotions();
                $data['all_appointments'] = $this->staff_corner_model->get_all_appointments();
                $data['directorates'] = $this->directorates_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'staff_corners';
                $data['page_name'] = 'staff_corners';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function sector_camps()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'sector_camps';
            $this->load->model('sector_camps_model');
            $this->load->model('districts_model');
            if ($this->input->post('add_sector_camps')) {
                $data = $this->input->post();
                if ($data) {
                    $data_new['district_id'] = $this->input->post('district_id');
                    $data_new['flood_sector'] = $this->input->post('flood_sector');
                    $data_new['flood_sub_sector'] = $this->input->post('flood_sub_sector');
                    $data_new['flood_relief_camp'] = $this->input->post('flood_relief_camp');
                    $data_new['veterinary_officers'] = $this->input->post('veterinary_officers');
                    $data_new['veterinary_assistants'] = $this->input->post('veterinary_assistants');
                    $data_new['mobile_veterinary_dispenceries'] = $this->input->post('mobile_veterinary_dispenceries');


                    $this->sector_camps_model->create($data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/sector_camps');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/sector_camps');
                }
            } else if ($this->input->post('edit_sector_camps')) {
                $data = $this->input->post();
                if ($data) {
                    $sector_id = $this->input->post('sector_id');
                    $data_new['district_id'] = $this->input->post('district_id');
                    $data_new['flood_sector'] = $this->input->post('flood_sector');
                    $data_new['flood_sub_sector'] = $this->input->post('flood_sub_sector');
                    $data_new['flood_relief_camp'] = $this->input->post('flood_relief_camp');
                    $data_new['veterinary_officers'] = $this->input->post('veterinary_officers');
                    $data_new['veterinary_assistants'] = $this->input->post('veterinary_assistants');
                    $data_new['mobile_veterinary_dispenceries'] = $this->input->post('mobile_veterinary_dispenceries');


                    $this->sector_camps_model->update($sector_id, $data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/sector_camps');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/sector_camps');
                }
            } else {
                $data['sector_camps'] = $this->sector_camps_model->get_all();
                $data['districts'] = $this->districts_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'sector_camps';
                $data['page_name'] = 'sector_camps';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function press_release()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'sector_camps';
            $this->load->model('press_release_model');
            if ($this->input->post('add_press')) {
                $data = $this->input->post();
                if ($data) {
                    $data_new['name'] = $this->input->post('press_name');
                    $data_new['description'] = $this->input->post('press_description');

                    $upload_path = 'uploads/press_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['press_image']['name']) && $_FILES['press_image']['error'] == 0) {
                        $type = pathinfo($_FILES['press_image']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['press_image']['name'], PATHINFO_FILENAME);
                        $_FILES['press_image']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['press_image']['tmp_name'], $upload_path . $_FILES['press_image']['name']);
                        $data_new['image_link'] = base_url() . $upload_path . $_FILES['press_image']['name'];
                    }
                    $this->press_release_model->create($data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/press_release');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/press_release');
                }
            } else if ($this->input->post('editpress')) {
                $data = $this->input->post();
                $press_id = $this->input->post("press_id");
                if ($data) {
                    $data_new['name'] = $this->input->post('press_name');
                    $data_new['description'] = $this->input->post('press_description');

                    $upload_path = 'uploads/press_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['press_image']['name']) && $_FILES['press_image']['error'] == 0) {
                        $type = pathinfo($_FILES['press_image']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['press_image']['name'], PATHINFO_FILENAME);
                        $_FILES['press_image']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['press_image']['tmp_name'], $upload_path . $_FILES['press_image']['name']);
                        $data_new['image_link'] = base_url() . $upload_path . $_FILES['press_image']['name'];
                    } else {
                        $data_new['image_link'] = $this->input->post("image_link");
                    }

                    $this->press_release_model->update($press_id, $data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/press_release');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/press_release');
                }
            } else {
                $data['press_release'] = $this->press_release_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'press_release';
                $data['page_name'] = 'press_release';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');

        }
    }

    public function download_careers()
    {
        if ($this->ion_auth->logged_in()) {

            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'sector_camps';
            $this->load->model('download_careers_model');
            if ($this->input->post('add_careers')) {
                $data = $this->input->post();
                if ($data) {
                    $data_new['name'] = $this->input->post('careers_name');
                    $data_new['description'] = $this->input->post('careers_description');

                    $upload_path = 'uploads/careers_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['careers_image']['name']) && $_FILES['careers_image']['error'] == 0) {
                        $type = pathinfo($_FILES['careers_image']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['careers_image']['name'], PATHINFO_FILENAME);
                        $_FILES['careers_image']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['careers_image']['tmp_name'], $upload_path . $_FILES['careers_image']['name']);
                        $data_new['image_link'] = base_url() . $upload_path . $_FILES['careers_image']['name'];
                    }
                    $this->download_careers_model->create($data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/download_careers');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/download_careers');
                }
            } else if ($this->input->post('editcareers')) {
                $data = $this->input->post();
                $press_id = $this->input->post("careers_id");
                if ($data) {
                    $data_new['name'] = $this->input->post('careers_name');
                    $data_new['description'] = $this->input->post('careers_description');

                    $upload_path = 'uploads/careers_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['careers_image']['name']) && $_FILES['careers_image']['error'] == 0) {
                        $type = pathinfo($_FILES['careers_image']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['careers_image']['name'], PATHINFO_FILENAME);
                        $_FILES['careers_image']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['careers_image']['tmp_name'], $upload_path . $_FILES['careers_image']['name']);
                        $data_new['image_link'] = base_url() . $upload_path . $_FILES['careers_image']['name'];
                    } else {
                        $data_new['image_link'] = $this->input->post("image_link");
                    }

                    $this->download_careers_model->update($press_id, $data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/download_careers');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/download_careers');
                }
            } else {
                $data['download_careers'] = $this->download_careers_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'download_careers';
                $data['page_name'] = 'download_careers';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');

        }
    }

    public function download_career_jobs()
    {
    
        if ($this->ion_auth->logged_in()) {

            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'sector_camps';
            $this->load->model('download_career_jobs_model');
            if ($this->input->post('add_careers')) {
                $data = $this->input->post();
                // dd($_FILES);
                if ($data) {
                    $data_new['name'] = $this->input->post('job_name');

                    $upload_path = 'uploads/careers_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['careers_image']['name']) && $_FILES['careers_image']['error'] == 0) {
                        $type = pathinfo($_FILES['careers_image']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['careers_image']['name'], PATHINFO_FILENAME);
                        $_FILES['careers_image']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['careers_image']['tmp_name'], $upload_path . $_FILES['careers_image']['name']);
                        $data_new['file_link'] = base_url() . $upload_path . $_FILES['careers_image']['name'];
                    }
                    $this->download_career_jobs_model->create($data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/download_career_jobs');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/download_career_jobs');
                }
            } else if ($this->input->post('editcareers')) {
                // dd($this->input->post());
                $data = $this->input->post();
                $press_id = $this->input->post("careers_id");
                if ($data) {
                    $data_new['name'] = $this->input->post('job_name');

                    $upload_path = 'uploads/careers_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['careers_image']['name']) && $_FILES['careers_image']['error'] == 0) {
                        $type = pathinfo($_FILES['careers_image']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['careers_image']['name'], PATHINFO_FILENAME);
                        $_FILES['careers_image']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['careers_image']['tmp_name'], $upload_path . $_FILES['careers_image']['name']);
                        $data_new['image_link'] = base_url() . $upload_path . $_FILES['careers_image']['name'];
                    } else {
                        $data_new['image_link'] = $this->input->post("image_link");
                    }

                    $this->download_career_jobs_model->update($press_id, $data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/download_career_jobs');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/download_career_jobs');
                }
            } else {
                
                $data['download_careers'] = $this->download_career_jobs_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'download_career_jobs';
                $data['page_name'] = 'download_career_jobs';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');

        }
    }

    public function public_info()
    {
        if ($this->ion_auth->logged_in()) {

            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'sector_camps';
            $this->load->model('public_info_officers_model');
            if ($this->input->post('add_public')) {
                $data = $this->input->post();
                //dd($data);  
                if ($data) {
                    
                    $data_new['category'] = $this->input->post('category');
                    $data_new['name_of_office_facility'] = $this->input->post('name_of_office_facility');
                    $data_new['designation'] = $this->input->post('designation');
                    
                    $data_new['district'] = $this->input->post('district');
                    $data_new['mobile_no'] = $this->input->post('mobile_no');
                    $data_new['email'] = $this->input->post('email');
                    $data_new['phone_no'] = $this->input->post('phone_no');
                    $data_new['latitude'] = $this->input->post('latitude');
                    $data_new['longitude'] = $this->input->post('longitude');
                    //print_r($data_new);exit();
                    $this->public_info_officers_model->create($data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/public_info');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/public_info');
                }
            } else if ($this->input->post('edit_public')) {
                $data = $this->input->post();
                $info_id = $this->input->post("info_id");
                if ($data) {
                    $data_new['category'] = $this->input->post('category');
                    $data_new['name_of_office_facility'] = $this->input->post('name_of_office_facility');
                    $data_new['designation'] = $this->input->post('designation');
                    $data_new['district'] = $this->input->post('district');
                    $data_new['email'] = $this->input->post('email');
                    $data_new['mobile_no'] = $this->input->post('mobile_no');
                    $data_new['phone_no'] = $this->input->post('phone_no');
                    $data_new['latitude'] = $this->input->post('latitude');
                    $data_new['longitude'] = $this->input->post('longitude');

                    $this->public_info_officers_model->update($info_id, $data_new);
                    $this->session->set_flashdata('success', 'Data added');
                    redirect('dashboard/public_info');

                } else {
                    $this->session->set_flashdata('error', 'Empty Submission');
                    redirect('dashboard/public_info');
                }
            } else {
                $data['public_info'] = $this->public_info_officers_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'public_info_officers';
                $data['page_name'] = 'public_info_officers';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');

        }
    }

    public function delete_staff()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('staff_corner_model');
            $staff_id = $this->input->get('staff_id');
            $this->staff_corner_model->delete($staff_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/staff_corners');
        } else {
            redirect('login');
        }
    }

    public function delete_info()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('public_info_officers_model');
            $info_id = $this->input->get('info_id');
            $this->public_info_officers_model->delete($info_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/public_info');
        } else {
            redirect('login');
        }
    }

    public function delete_press()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('press_release_model');
            $press_id = $this->input->get('press_id');
            $this->press_release_model->delete($press_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/press_release');
        } else {
            redirect('login');
        }
    }

    public function delete_careers()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('download_careers_model');
            $press_id = $this->input->get('careers_id');
            $this->download_careers_model->delete($press_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/download_careers');
        } else {
            redirect('login');
        }
    }

    public function delete_career_jobs()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('download_career_jobs_model');
            $press_id = $this->input->get('careers_id');
            $this->download_career_jobs_model->delete($press_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/download_career_jobs');
        } else {
            redirect('login');
        }
    }

    public function organogram_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'organogram_listing';
            $this->load->model('organogram_model');
            if ($this->input->post('add_new_organogram')) {
                $data = $this->input->post();
                $upload_path = 'uploads/organogram_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['organogram_url']['name']) && $_FILES['organogram_url']['error'] == 0) {
                    $type = pathinfo($_FILES['organogram_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['organogram_url']['name'], PATHINFO_FILENAME);
                    $_FILES['organogram_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['organogram_url']['tmp_name'], $upload_path . $_FILES['organogram_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['organogram_url']['name'];
                    $this->organogram_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('dashboard/organogram_listing');
            } else {
                $data['organogram'] = $this->organogram_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'organogram_management';
                $data['page_name'] = 'organogram';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_organogram()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'organogram_listing';
            $organogram_id = $this->input->get('organogram_id');
            $this->load->model('organogram_model');
            if ($this->input->post('edit_organogram')) {
                $data = $this->input->post();
                $upload_path = 'uploads/organogram_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['organogram_url']['name']) && $_FILES['organogram_url']['error'] == 0) {
                    $type = pathinfo($_FILES['organogram_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['organogram_url']['name'], PATHINFO_FILENAME);
                    $_FILES['organogram_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['organogram_url']['tmp_name'], $upload_path . $_FILES['organogram_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['organogram_url']['name'];
                } else {
                    $data['image_url'] = $this->input->post('old_organogram_url');
                }
                $this->organogram_model->update($organogram_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/organogram_listing');
            } else {
                $data['organogram'] = $this->organogram_model->get_by_id($organogram_id);
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'organogram_management';
                $data['page_name'] = 'edit_organogram';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_organogram()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('organogram_model');
            $organogram_id = $this->input->get('organogram_id');
            $this->organogram_model->delete($organogram_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/organogram_listing');
        } else {
            redirect('login');
        }
    }

    public function section_statistics_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'section_statistics_listing';
            $this->load->model('section_statistics_model');
            if ($this->input->post('add_new_section_statistics')) {
                $data = $this->input->post();
                $upload_path = 'uploads/section_statistics_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['section_statistics_url']['name']) && $_FILES['section_statistics_url']['error'] == 0) {
                    $type = pathinfo($_FILES['section_statistics_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['section_statistics_url']['name'], PATHINFO_FILENAME);
                    $_FILES['section_statistics_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['section_statistics_url']['tmp_name'], $upload_path . $_FILES['section_statistics_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['section_statistics_url']['name'];
                    $this->section_statistics_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('dashboard/section_statistics_listing');
            } else {
                $data['section_statistics'] = $this->section_statistics_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'section_statistics_management';
                $data['page_name'] = 'section_statistics';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_section_statistics()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'section_statistics_listing';
            $section_statistics_id = $this->input->get('section_statistics_id');
            $this->load->model('section_statistics_model');
            if ($this->input->post('edit_section_statistics')) {
                $data = $this->input->post();
                $upload_path = 'uploads/section_statistics_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['section_statistics_url']['name']) && $_FILES['section_statistics_url']['error'] == 0) {
                    $type = pathinfo($_FILES['section_statistics_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['section_statistics_url']['name'], PATHINFO_FILENAME);
                    $_FILES['section_statistics_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);

                    move_uploaded_file($_FILES['section_statistics_url']['tmp_name'], $upload_path . $_FILES['section_statistics_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['section_statistics_url']['name'];
                } else {
                    $data['image_url'] = $this->input->post('old_section_statistics_url');
                }
                $this->section_statistics_model->update($section_statistics_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/section_statistics_listing');
            } else {
                $data['section_statistics'] = $this->section_statistics_model->get_by_id($section_statistics_id);
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'section_statistics_management';
                $data['page_name'] = 'edit_section_statistics';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_section_statistics()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('section_statistics_model');
            $section_statistics_id = $this->input->get('section_statistics_id');
            $this->section_statistics_model->delete($section_statistics_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/section_statistics_listing');
        } else {
            redirect('login');
        }
    }

    public function development_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'development_listing';
            $this->load->model('development_model');
            if ($this->input->post('add_new_development')) {
                $data = $this->input->post();
                $upload_path = 'uploads/development_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['development_url']['name']) && $_FILES['development_url']['error'] == 0) {
                    $type = pathinfo($_FILES['development_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['development_url']['name'], PATHINFO_FILENAME);
                    $_FILES['development_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['development_url']['tmp_name'], $upload_path . $_FILES['development_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['development_url']['name'];
                    $this->development_model->create($data);
                    $this->session->set_flashdata('success', 'Data added');
                }
                redirect('dashboard/development_listing');
            } else {
                $data['development'] = $this->development_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'development_management';
                $data['page_name'] = 'development';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_development()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $search = 'development_listing';
            $development_id = $this->input->get('development_id');
            $this->load->model('development_model');
            if ($this->input->post('edit_development')) {
                $data = $this->input->post();
                $upload_path = 'uploads/development_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['development_url']['name']) && $_FILES['development_url']['error'] == 0) {
                    $type = pathinfo($_FILES['development_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['development_url']['name'], PATHINFO_FILENAME);
                    $_FILES['development_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);

                    move_uploaded_file($_FILES['development_url']['tmp_name'], $upload_path . $_FILES['development_url']['name']);
                    $data['image_url'] = base_url() . $upload_path . $_FILES['development_url']['name'];
                } else {
                    $data['image_url'] = $this->input->post('old_development_url');
                }
                $this->development_model->update($development_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/development_listing');
            } else {
                $data['development'] = $this->development_model->get_by_id($development_id);
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'development_management';
                $data['page_name'] = 'edit_development';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_development()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('development_model');
            $development_id = $this->input->get('development_id');
            $this->development_model->delete($development_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/development_listing');
        } else {
            redirect('login');
        }
    }

    public function delete_sector()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('sector_camps_model');
            $sector_id = $this->input->get('sector_id');
            $this->sector_camps_model->delete($sector_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/sector_camps');
        } else {
            redirect('login');
        }
    }

    //////////////duck function in controller starts//////////
    public function duck_article_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            $data['articles'] = $this->advertisements_model->get_all_duck_article();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'duck_article';
            $data['page_name'] = 'duck_article_listing';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_duck_article()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            if ($this->input->post('add_article')) {
                $this->load->model('advertisements_model');
                $upload_path = 'uploads/duck_article_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                    $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                    $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                    $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                } else {
                    $data_gal['file_link'] = null;
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $c_art_id = $this->advertisements_model->add_duck_article($data_gal);
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['article_id'] = $c_art_id;
                        $this->advertisements_model->add_duck_article_image($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Duck Article Added');
                redirect('dashboard/duck_article_listing');
            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'duck_article';
                $data['page_name'] = 'add_duck_article';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_duck_article($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_article')) {
                    $upload_path = 'uploads/duck_article_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    }
                    $data_gal['title'] = $this->input->post('title');
                    $data_gal['description'] = $this->input->post('description');
                    $this->advertisements_model->update_duck_article($art_id, $data_gal);
                    for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                        if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                            $data_gl_detail['article_id'] = $art_id;
                            $this->advertisements_model->add_duck_article_image($data_gl_detail);
                        }
                    }
                    $this->session->set_flashdata('success', 'Article Updated');
                    redirect('dashboard/duck_article_listing');
                } else {
                    $data['article'] = $this->advertisements_model->get_duck_article_by_id($art_id);
                    $data['article_images'] = $this->advertisements_model->get_duck_article_images_by_id($art_id);
                    $data['folder_name'] = 'dashboard';
                    $data['file_name'] = 'duck_article';
                    $data['page_name'] = 'edit_duck_article';
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Article Selected");
                redirect('dashboard/duck_article_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_duck_article_images($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('advertisements_model');
            $image_id = $this->input->post('p_banner_id');
            $this->advertisements_model->delete_duck_article_image($image_id);
            $p_banner = $this->advertisements_model->get_duck_article_images_by_id($art_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_duck_article($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $this->advertisements_model->delete_all_duck_article_images($art_id);
                $this->advertisements_model->delete_duck_article($art_id);
                $this->session->set_flashdata('success', 'Article Deleted');
                redirect('dashboard/duck_article_listing');
            } else {
                $this->session->set_flashdata('error', "No Article Id Found");
                redirect('dashboard/duck_article_listing');

            }
        } else {
            redirect('login');
        }
    }

    //////////////duck function in controller ends////////////


    //////////////cock function in controller starts//////////
    public function cock_article_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            $data['articles'] = $this->advertisements_model->get_all_cock_article();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'cock_article';
            $data['page_name'] = 'cock_article_listing';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_cock_article()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            if ($this->input->post('add_article')) {
                $this->load->model('advertisements_model');
                $upload_path = 'uploads/cock_article_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                    $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                    $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                    $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                } else {
                    $data_gal['file_link'] = null;
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $c_art_id = $this->advertisements_model->add_cock_article($data_gal);
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['article_id'] = $c_art_id;
                        $this->advertisements_model->add_cock_article_image($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Rural Poultry Article Added');
                redirect('dashboard/cock_article_listing');
            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'cock_article';
                $data['page_name'] = 'add_cock_article';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_cock_article($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_article')) {
                    $upload_path = 'uploads/cock_article_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    }
                    $data_gal['title'] = $this->input->post('title');
                    $data_gal['description'] = $this->input->post('description');
                    $this->advertisements_model->update_cock_article($art_id, $data_gal);
                    for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                        if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                            $data_gl_detail['article_id'] = $art_id;
                            $this->advertisements_model->add_cock_article_image($data_gl_detail);
                        }
                    }
                    $this->session->set_flashdata('success', 'Article Updated');
                    redirect('dashboard/cock_article_listing');
                } else {
                    $data['article'] = $this->advertisements_model->get_cock_article_by_id($art_id);
                    $data['article_images'] = $this->advertisements_model->get_cock_article_images_by_id($art_id);
                    $data['folder_name'] = 'dashboard';
                    $data['file_name'] = 'cock_article';
                    $data['page_name'] = 'edit_cock_article';
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Article Selected");
                redirect('dashboard/cock_article_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_cock_article_images($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('advertisements_model');
            $image_id = $this->input->post('p_banner_id');
            $this->advertisements_model->delete_cock_article_image($image_id);
            $p_banner = $this->advertisements_model->get_cock_article_images_by_id($art_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_cock_article($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $this->advertisements_model->delete_all_cock_article_images($art_id);
                $this->advertisements_model->delete_cock_article($art_id);
                $this->session->set_flashdata('success', 'Article Deleted');
                redirect('dashboard/cock_article_listing');
            } else {
                $this->session->set_flashdata('error', "No Article Id Found");
                redirect('dashboard/cock_article_listing');

            }
        } else {
            redirect('login');
        }
    }

    //////////////cock function in controller ends////////////

    //////////////training function in controller starts//////////
    public function training_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            $data['articles'] = $this->advertisements_model->get_all_training();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'training';
            $data['page_name'] = 'training_listing';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_training()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            if ($this->input->post('add_article')) {
                $this->load->model('advertisements_model');
                $upload_path = 'uploads/training_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                    $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                    $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                    $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                } else {
                    $data_gal['file_link'] = null;
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $c_art_id = $this->advertisements_model->add_training($data_gal);
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['article_id'] = $c_art_id;
                        $this->advertisements_model->add_training_image($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Training Added');
                redirect('dashboard/training_listing');
            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'training';
                $data['page_name'] = 'add_training';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_training($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_article')) {
                    $upload_path = 'uploads/training_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    }
                    $data_gal['title'] = $this->input->post('title');
                    $data_gal['description'] = $this->input->post('description');
                    $this->advertisements_model->update_training($art_id, $data_gal);
                    for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                        if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                            $data_gl_detail['article_id'] = $art_id;
                            $this->advertisements_model->add_training_image($data_gl_detail);
                        }
                    }
                    $this->session->set_flashdata('success', 'Article Updated');
                    redirect('dashboard/training_listing');
                } else {
                    $data['article'] = $this->advertisements_model->get_training_by_id($art_id);
                    $data['article_images'] = $this->advertisements_model->get_training_images_by_id($art_id);
                    $data['folder_name'] = 'dashboard';
                    $data['file_name'] = 'training';
                    $data['page_name'] = 'edit_training';
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Article Selected");
                redirect('dashboard/training_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_training_images($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('advertisements_model');
            $image_id = $this->input->post('p_banner_id');
            $this->advertisements_model->delete_training_image($image_id);
            $p_banner = $this->advertisements_model->get_training_images_by_id($art_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_training($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $this->advertisements_model->delete_all_training_images($art_id);
                $this->advertisements_model->delete_training($art_id);
                $this->session->set_flashdata('success', 'Article Deleted');
                redirect('dashboard/training_listing');
            } else {
                $this->session->set_flashdata('error', "No Article Id Found");
                redirect('dashboard/training_listing');

            }
        } else {
            redirect('login');
        }
    }

    //////////////training function in controller ends////////////

    //////////////poultry_farm function in controller starts//////////
    public function poultry_farm_listing()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('advertisements_model');
            $data['articles'] = $this->advertisements_model->get_all_poultry_farm();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'poultry_farm';
            $data['page_name'] = 'poultry_farm_listing';
            $this->load->view('index', $data);
        } else {
            redirect('login');
        }
    }

    public function add_poultry_farm()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            if ($this->input->post('add_article')) {
                $this->load->model('advertisements_model');
                $upload_path = 'uploads/poultry_farm_images/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                    $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                    $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                    $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                } else {
                    $data_gal['file_link'] = null;
                }
                $data_gal['title'] = $this->input->post('title');
                $data_gal['description'] = $this->input->post('description');
                $c_art_id = $this->advertisements_model->add_poultry_farm($data_gal);
                for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                    if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                        $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                        $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                        $data_gl_detail['article_id'] = $c_art_id;
                        $this->advertisements_model->add_poultry_farm_image($data_gl_detail);
                    }
                }
                $this->session->set_flashdata('success', 'Poultry Farm Added');
                redirect('dashboard/poultry_farm_listing');
            } else {
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'poultry_farm';
                $data['page_name'] = 'add_poultry_farm';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function edit_poultry_farm($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $user_data = $this->ion_auth->user()->row();
                $data['user_id'] = $user_data->id;
                $data['name'] = $user_data->username;
                if ($this->input->post('edit_article')) {
                    $upload_path = 'uploads/poultry_farm_images/';
                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!empty($_FILES['file_link']['name']) && $_FILES['file_link']['error'] == 0) {
                        $type = pathinfo($_FILES['file_link']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['file_link']['name'], PATHINFO_FILENAME);
                        $_FILES['file_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['file_link']['tmp_name'], $upload_path . $_FILES['file_link']['name']);
                        $data_gal['file_link'] = base_url() . $upload_path . $_FILES['file_link']['name'];
                    }
                    $data_gal['title'] = $this->input->post('title');
                    $data_gal['description'] = $this->input->post('description');
                    $this->advertisements_model->update_poultry_farm($art_id, $data_gal);
                    for ($i = 0; $i < sizeof($_FILES['image_url']['name']); $i++) {
                        if (!empty($_FILES['image_url']['name'][$i]) && $_FILES['image_url']['error'][$i] == 0) {
                            $type = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_EXTENSION);
                            $file_name = pathinfo($_FILES['image_url']['name'][$i], PATHINFO_FILENAME);
                            $_FILES['image_url']['name'][$i] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                            move_uploaded_file($_FILES['image_url']['tmp_name'][$i], $upload_path . $_FILES['image_url']['name'][$i]);
                            $data_gl_detail['image_url'] = base_url() . $upload_path . $_FILES['image_url']['name'][$i];
                            $data_gl_detail['article_id'] = $art_id;
                            $this->advertisements_model->add_poultry_farm_image($data_gl_detail);
                        }
                    }
                    $this->session->set_flashdata('success', 'Article Updated');
                    redirect('dashboard/poultry_farm_listing');
                } else {
                    $data['article'] = $this->advertisements_model->get_poultry_farm_by_id($art_id);
                    $data['article_images'] = $this->advertisements_model->get_poultry_farm_images_by_id($art_id);
                    $data['folder_name'] = 'dashboard';
                    $data['file_name'] = 'poultry_farm';
                    $data['page_name'] = 'edit_poultry_farm';
                    $this->load->view('index', $data);
                }
            } else {
                $this->session->set_flashdata('error', "No Article Selected");
                redirect('dashboard/poultry_farm_listing');
            }
        } else {
            redirect('login');
        }
    }

    public function delete_poultry_farm_images($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            $this->load->model('advertisements_model');
            $image_id = $this->input->post('p_banner_id');
            $this->advertisements_model->delete_poultry_farm_image($image_id);
            $p_banner = $this->advertisements_model->get_poultry_farm_images_by_id($art_id);
            $html = '';
            if (!empty($p_banner)) {
                foreach ($p_banner as $banner) {
                    $html .= '<div class="col-sm-4" style="margin-top: 10px;" >
                  <div class="ImageEdit">
                    <img src="' . $banner->image_url . '" style="height: 120px; width: 165px; ">
                     <span class="close2 delete_p_image" style="right:18px;position:absolute;cursor:pointer; background-color: black;color: white;border-radius: 50px;padding-right: 5px;padding-left: 5px;" id="' . $banner->id . '">x</span>
                        </div>
                      </div>';
                }
                print_r(json_encode($html));
            }
        } else {
            redirect('login');
        }
    }

    public function delete_poultry_farm($art_id = null)
    {
        if ($this->ion_auth->logged_in()) {
            if (isset($art_id) && !empty($art_id)) {
                $this->load->model('advertisements_model');
                $this->advertisements_model->delete_all_poultry_farm_images($art_id);
                $this->advertisements_model->delete_poultry_farm($art_id);
                $this->session->set_flashdata('success', 'Article Deleted');
                redirect('dashboard/poultry_farm_listing');
            } else {
                $this->session->set_flashdata('error', "No Article Id Found");
                redirect('dashboard/poultry_farm_listing');

            }
        } else {
            redirect('login');
        }
    }

    //////////////poultry_farm function in controller ends////////////


    public function multimedia_management()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('multimedia_model');
            if ($this->input->post('add_new_multimedia')) {
                $data = $this->input->post();
                $upload_path = 'uploads/multimedia/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if ($data['type'] != 2) {
                    if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                        $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                        $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                        $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                        move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                        $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                    }
                }

                $this->multimedia_model->create($data);
                $this->session->set_flashdata('success', 'Data added');

                redirect('dashboard/multimedia_management');

            } else if ($this->input->post('update_multimedia')) {
                $data = $this->input->post();
                $multimedia_id = $this->input->post('multimedia_id');
                $upload_path = 'uploads/multimedia/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['link_url']['name']) && $_FILES['link_url']['error'] == 0) {
                    $type = pathinfo($_FILES['link_url']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['link_url']['name'], PATHINFO_FILENAME);
                    $_FILES['link_url']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['link_url']['tmp_name'], $upload_path . $_FILES['link_url']['name']);
                    $data['link_url'] = base_url() . $upload_path . $_FILES['link_url']['name'];
                } else {
                    $data['link_url'] = $this->input->post('old_link_url');
                }
                $this->multimedia_model->update($multimedia_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/multimedia_management');
            } else {
                //$data['material_cats'] = $this->multimedia_category_model->get_all();
                $data['items'] = $this->multimedia_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'multimedia_management';
                $data['page_name'] = 'multimedia_management';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_multimedia()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;

            $this->load->model('multimedia_model');
            $multimedia_id = $this->input->get('multimedia_id');
            $this->multimedia_model->delete($multimedia_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/multimedia_management');
        } else {
            redirect('login');
        }
    }

    public function feedback()
    {
        
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $this->load->model('Feedback_model');
            $data['suggestions'] = $this->Feedback_model->get_feedback();
            $data['folder_name'] = 'dashboard';
            $data['file_name'] = 'feedback';
            $data['page_name'] = 'feedback';
             //echo "<pre>";print_r($data);die;

            $this->load->view('index',$data);
        }else {
            redirect('login');
        }
    }

    public function preliminary_list_candidates()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Preliminary_list_candidates_model');
            if ($this->input->post('add_new_ml')) {
                $data = $this->input->post();
                // dd($data);
                $upload_path = 'uploads/Preliminary_List_Candidates_Document/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                }
                //  dd($data);
                $this->Preliminary_list_candidates_model->create($data);
                $this->session->set_flashdata('success', 'Data added');
                redirect('dashboard/preliminary_list_candidates');
            } else if ($this->input->post('update_ml')) {
                $data = $this->input->post();
                $pp_id = $this->input->post('ml_id');
                $upload_path = 'uploads/Preliminary_List_Candidates_Document/';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
                if (!empty($_FILES['pdf_link']['name']) && $_FILES['pdf_link']['error'] == 0) {
                    $type = pathinfo($_FILES['pdf_link']['name'], PATHINFO_EXTENSION);
                    $file_name = pathinfo($_FILES['pdf_link']['name'], PATHINFO_FILENAME);
                    $_FILES['pdf_link']['name'] = substr(url_title($file_name,'underscore',TRUE), 0, 900) . '_' . $this->random_string() . "." . strtolower($type);
                    move_uploaded_file($_FILES['pdf_link']['tmp_name'], $upload_path . $_FILES['pdf_link']['name']);
                    $data['pdf_link'] = base_url() . $upload_path . $_FILES['pdf_link']['name'];
                } else {
                    $data['pdf_link'] = $this->input->post('old_pdf_link');
                }
                $this->Preliminary_list_candidates_model->update($pp_id, $data);
                $this->session->set_flashdata('success', 'Data Updated');
                redirect('dashboard/preliminary_list_candidates');
            } else {
                $data['preliminary_list_candidates'] = $this->Preliminary_list_candidates_model->get_all();
                $data['folder_name'] = 'dashboard';
                $data['file_name'] = 'preliminary_list_candidates';
                $data['page_name'] = 'preliminary_list_candidates';
                $this->load->view('index', $data);
            }
        } else {
            redirect('login');
        }
    }

    public function delete_preliminary_list_candidates()
    {
        if ($this->ion_auth->logged_in()) {
            $user_data = $this->ion_auth->user()->row();
            $data['user_id'] = $user_data->id;
            $data['name'] = $user_data->username;
            $this->load->model('Preliminary_list_candidates_model');
            $auc_id = $this->input->get('ml_id');
            $this->Preliminary_list_candidates_model->delete($auc_id);
            $this->session->set_flashdata('success', 'Data Deleted');
            redirect('dashboard/preliminary_list_candidates');
        } else {
            redirect('login');
        }
    }
}