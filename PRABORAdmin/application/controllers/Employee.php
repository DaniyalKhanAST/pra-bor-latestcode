<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('Employee_model');
        $data['employees'] = $this->Employee_model->get_all();
        $data['folder_name'] = 'dashboard';
        $data['file_name'] = 'employee_listing';
        $data['page_name'] = 'employee_listing';
        $this->load->view('index', $data);
    }

    public function createExcel()
	{
		$fileName = 'employees_sheet';
		$this->load->model('Employee_model');
		$data = $this->Employee_model->get_all();
        $this->load->library('excel');
		$spreadsheet = new PHPExcel();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->SetCellValue('A1', 'UserID');
		$sheet->SetCellValue('B1', 'Name');
		$sheet->SetCellValue('C1', 'FatherName');
		$sheet->SetCellValue('D1', 'CNIC');
		$sheet->SetCellValue('E1', 'Gender');
		$sheet->SetCellValue('F1', 'Date of Birth');
		$sheet->SetCellValue('G1', 'Mobile');
		$sheet->SetCellValue('H1', 'Email');
		$sheet->SetCellValue('I1', 'Domicile');
		$sheet->SetCellValue('J1', 'Marital Status');
		$sheet->SetCellValue('K1', 'Nationality');
		$sheet->SetCellValue('L1', 'Blood Group');
		$sheet->SetCellValue('M1', 'Religion');
		$sheet->SetCellValue('N1', 'Whatsapp No');
		$sheet->SetCellValue('O1', 'Residential Phone No');
		$sheet->SetCellValue('P1', 'Permanent Address');
		$sheet->SetCellValue('Q1', 'Present Address');
		$sheet->SetCellValue('R1', 'High Qualification');
		$sheet->SetCellValue('S1', 'Year of passing');
		$sheet->SetCellValue('T1', 'Grade');
		$sheet->SetCellValue('U1', 'Institute');
		$sheet->SetCellValue('V1', 'Date of entry in service');
		$sheet->SetCellValue('W1', 'Pay Scales');
		$sheet->SetCellValue('X1', 'Name of office joined');
		$sheet->SetCellValue('Y1', 'Place of first posting');
		$sheet->SetCellValue('Z1', 'Current Designation');
		$sheet->SetCellValue('AA1', 'Designation at service entry');
		$sheet->SetCellValue('AB1', 'Current Pay Scale');
		$sheet->SetCellValue('AC1', 'Name of current office');
		$sheet->SetCellValue('AD1', 'Place of current posting');
		$sheet->SetCellValue('AE1', 'Date of last promotion');
		$sheet->SetCellValue('AF1', 'Emergency Contact Name');
		$sheet->SetCellValue('AG1', 'Relationship');
		$sheet->SetCellValue('AH1', 'Emergency Mobile No');
		$sheet->SetCellValue('AI1', 'Postal Address');
		$sheet->SetCellValue('AJ1', 'Training Program 1');
		$sheet->SetCellValue('AK1', 'Training Program 2');
		$sheet->SetCellValue('AL1', 'Training Program 3');
		$sheet->SetCellValue('AM1', 'Training Program 4');
		$sheet->SetCellValue('AN1', 'Training Program 5');
		$sheet->SetCellValue('AO1', 'Name of program');
		$rows = 2;
		foreach ($data as $k=> $val){
			$sheet->SetCellValue('A' . $rows, $val->id);
			$sheet->SetCellValue('B' . $rows, $val->name);
			$sheet->SetCellValue('C' . $rows, $val->father_name);
			$sheet->SetCellValue('D' . $rows, $val->cnic);
			$sheet->SetCellValue('E' . $rows, $val->gender);
			$sheet->SetCellValue('F' . $rows, $val->date_of_birth);
			$sheet->SetCellValue('G' . $rows, $val->mobile);
			$sheet->SetCellValue('H' . $rows, $val->email);
			$sheet->SetCellValue('I' . $rows, $val->domicile);
			$sheet->SetCellValue('J' . $rows, $val->marital_status);
			$sheet->SetCellValue('K' . $rows, $val->nationality);
			$sheet->SetCellValue('L' . $rows, $val->blood_group);
			$sheet->SetCellValue('M' . $rows, $val->religion);
			$sheet->SetCellValue('N' . $rows, $val->whatsapp_no);
			$sheet->SetCellValue('O' . $rows, $val->residential_phone_no);
			$sheet->SetCellValue('P' . $rows, $val->permanent_address);
			$sheet->SetCellValue('Q' . $rows, $val->present_address);
			$sheet->SetCellValue('R' . $rows, $val->highest_qualification);
			$sheet->SetCellValue('S' . $rows, $val->year_of_passing);
			$sheet->SetCellValue('T' . $rows, $val->grade);
			$sheet->SetCellValue('U' . $rows, $val->institute);
			$sheet->SetCellValue('V' . $rows, $val->date_of_entry_in_service);
			$sheet->SetCellValue('W' . $rows, $val->pay_scales);
			$sheet->SetCellValue('X' . $rows, $val->name_of_office_joined);
			$sheet->SetCellValue('Y' . $rows, $val->place_of_first_posting);
			$sheet->SetCellValue('Z' . $rows, $val->current_designation);
			$sheet->SetCellValue('AA' . $rows, $val->designation_at_service_entry);
			$sheet->SetCellValue('AB' . $rows, $val->current_pay_scale);
			$sheet->SetCellValue('AC' . $rows, $val->name_of_current_office);
			$sheet->SetCellValue('AD' . $rows, $val->place_of_current_posting);
			$sheet->SetCellValue('AE' . $rows, $val->date_of_last_promotion);
			$sheet->SetCellValue('AF' . $rows, $val->emergency_contact_name);
			$sheet->SetCellValue('AG' . $rows, $val->relationship);
			$sheet->SetCellValue('AH' . $rows, $val->emergency_mobile_no);
			$sheet->SetCellValue('AI' . $rows, $val->postal_address);
			$sheet->SetCellValue('AJ' . $rows, $val->training_program_);
			$sheet->SetCellValue('AK' . $rows, $val->training_program_2);
			$sheet->SetCellValue('AL' . $rows, $val->training_program_3);
			$sheet->SetCellValue('AM' . $rows, $val->training_program_4);
			$sheet->SetCellValue('AN' . $rows, $val->training_program_5);
			$sheet->SetCellValue('AO' . $rows, $val->name_of_program);
			$rows++;
		}

        $objWriter = new PHPExcel_Writer_Excel2007($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $fileName .'.xlsx"');
		header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
	}

    }
