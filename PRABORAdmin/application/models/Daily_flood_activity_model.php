<?php
class Daily_flood_activity_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'activity_date' => $item['activity_date'],
			'districtwise_report_link' => $item['districtwise_report_link'],
			'consolidate_report_link' => $item['consolidate_report_link']
			 ); 

		$this->db->insert('daily_flood_activity', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('daily_flood_activity');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('daily_flood_activity');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'activity_date' => $item['activity_date'],
			'districtwise_report_link' => $item['districtwise_report_link'],
			'consolidate_report_link' => $item['consolidate_report_link']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('daily_flood_activity', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('daily_flood_activity');
	}
}