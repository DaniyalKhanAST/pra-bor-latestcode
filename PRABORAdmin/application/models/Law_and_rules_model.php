<?php
class Law_and_rules_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'title' => $item['title'],
			'description' => $item['description'],
			'file_link' => $item['file_link'],
			'type' => $item['type']
			 ); 

		$this->db->insert('law_and_rules', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('law_and_rules');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('law_and_rules');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'title' => $item['title'],
			'description' => $item['description'],
			'file_link' => $item['file_link'],
			'type' => $item['type']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('law_and_rules', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('law_and_rules');
	}
}