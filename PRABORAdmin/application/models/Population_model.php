<?php
class Population_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'species_id' => $item['species_id'],
			'pak_qty' => $item['pak_qty'],
			'punjab_qty' => $item['punjab_qty'],
			'share' => $item['share'],
			'source' => $item['source']
			 ); 

		$this->db->insert('population', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('population');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('population.*,species_table.name as species_name');
		$this->db->from('population');
		$this->db->join("species_table", "population.species_id = species_table.id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'species_id' => $item['species_id'],
			'pak_qty' => $item['pak_qty'],
			'punjab_qty' => $item['punjab_qty'],
			'share' => $item['share'],
			'source' => $item['source']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('population', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('population');
	}
}