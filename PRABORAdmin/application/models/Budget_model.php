<?php
class Budget_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'from_year' => $item['from_year'],
			'to_year' => $item['to_year'],
			'development_price' => $item['development_price'],
			'non_development_price' => $item['non_development_price']
			 ); 

		$this->db->insert('budget', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('budget');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('budget');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'from_year' => $item['from_year'],
			'to_year' => $item['to_year'],
			'development_price' => $item['development_price'],
			'non_development_price' => $item['non_development_price']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('budget', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('budget');
	}
}