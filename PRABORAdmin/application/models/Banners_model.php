<?php
class Banners_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'title' => $item['name'],
			'image_url' => $item['image_url'],
			'description' => $item['description'],
			'isactive' => $item['status']
			 ); 

		$this->db->insert('banners', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('banners');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('banners');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'title' => $item['title'],
			'image_url' => $item['image_url'],
			'description' => $item['description'],
			'isactive' => $item['isactive']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('banners', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('banners');
	}
}