<?php
class download_career_jobs_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($item)
    {
       
        $data = array(
            'name' => $item['name'],
            'file_link' => $item['file_link']
        );
//  dd($item);  
        $this->db->insert('download_career_jobs', $data);
    }

    function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('download_career_jobs');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->row();
        }
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('download_career_jobs');
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->result();
        }
    }

    function update($id, $item)
    {
        // dd($item);
        $data = array(
            'name' => $item['name'],
            'file_link' => $item['image_link']
        );

        $this->db->where('id', $id);
        $this->db->update('download_career_jobs', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('download_career_jobs');
    }
}