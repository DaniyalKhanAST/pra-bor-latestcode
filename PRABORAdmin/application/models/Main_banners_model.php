<?php

class Main_banners_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($item)
    {
        $data = array(
            'banner_url' => $item['banner_url'],
            'link_url' => $item['link_url'],
            'status' => '1'
        );

        $this->db->insert('main_banners', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_slider_img($item)
    {
        $data = array(
            'title' => $item['title'],
            'priority' => $item['priority'],
            'banner_url' => $item['banner_url'],
            'is_main_slider' => isset($item['is_main_slider'])? $item['is_main_slider'] : 0,
            'slider_number' => isset($item['slider_number'])? $item['slider_number'] : 0
        );

        $this->db->insert('main_slider', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function get_all_slider_imgs($array)
    {
        $this->db->select("*");
        $this->db->from('main_slider');
        $this->db->order_by('priority','ASC');
        $this->db->where('is_active', 1);
        if($array)
        {
            $this->db->where($array);
        }
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_slider_imgs_by_title($title)
    {
        $this->db->select("title");
        $this->db->from('main_slider');
        $this->db->where('title', $title);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_slider_img($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'banner_url' => $item['banner_url']
        );

        $this->db->where('id', $id);
        $this->db->update('main_slider', $data);
    }
    function set_slider_priority($id,$item){
        $data = array(
            'priority' => $item['priority']
        );
        $this->db->where('id', $id);
        $this->db->update('main_slider', $data);
    }

    function delete_slider_img($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('main_slider');
    }

    function get_by_id($id)
    {
        $this->db->select("main_banners.*, GROUP_CONCAT(ID SEPARATOR ' ,') AS zones", FALSE);
        $this->db->from('main_banners');
        $this->db->join("banners", "main_banners.banner_id = banners.banner_id", "Left");
        $this->db->join("banner_zones", "banners.zone_id = banner_zones.id", "Left");
        $this->db->group_by('main_banners.banner_id');
        $this->db->where('main_banners.banner_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_all()
    {
        $this->db->select("main_banners.*, GROUP_CONCAT(NAME SEPARATOR ' ,') AS zones", FALSE);
        $this->db->from('main_banners');
        $this->db->join("banners", "main_banners.banner_id = banners.banner_id", "Left");
        $this->db->join("banner_zones", "banners.zone_id = banner_zones.id", "Left");
        $this->db->group_by('main_banners.banner_id');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update($id, $item)
    {
        $data = array(
            'banner_url' => $item['banner_url'],
            'link_url' => $item['link_url'],
            'status' => '1'
        );

        $this->db->where('banner_id', $id);
        $this->db->update('main_banners', $data);
    }

    function delete($id)
    {
        $this->db->where('banner_id', $id);
        $this->db->delete('main_banners');
    }

}
