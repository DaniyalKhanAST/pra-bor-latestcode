<?php

class Employee_verification_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_all()
	{
		$this->db->select('*');
		$this->db->from('employee_verification');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

  public function add_employee($data)
  {
    unset($data['add_new_employee']);
    $this->db->insert('employee_verification', $data);
    $id = $this->db->insert_id();
    return $id;
  }


  public function insert($data)
{
	$insert_query = $this->db->insert_string('employee_verification', $data);
	$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
	
    $this->db->query($insert_query);
}

  public function get_by_id($id = 0)
  {
    $this->db->select('*');
		$this->db->from('employee_verification');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
  }

  function update($id, $item)
	{
		$data = array(
			// 'cnic' => $item['cnic'],
			// 'email' => $item['email'],
			'mobile' => $item['mobile'],
			'name' => $item['name'],
			'is_active' => $item['is_active'],
			 ); 

		$this->db->where('id', $id);
		$this->db->update('employee_verification', $data);
    return $this->db->affected_rows();
	}

  function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('employee_verification');
	}

}