<?php
class Vaccines_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'image_link' => $item['vaccines_url'],
			'type' => $item['vaccines_type']
			 ); 

		$this->db->insert('vaccines', $data);
	}

	function get_by_id($id)
	{
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('vaccines');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'image_link' => $item['vaccines_url'],
			'type' => $item['vaccines_type']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('vaccines', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('vaccines');
	}
}