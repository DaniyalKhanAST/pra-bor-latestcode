<?php
class Reports_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['name'],
			'report_cat_id' => $item['report_cat_id'],
			'detail' => $item['detail'],
			'pdf_link' => $item['pdf_link']
			 ); 

		$this->db->insert('reports', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('reports');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('reports');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['name'],
			'report_cat_id' => $item['report_cat_id'],
			'detail' => $item['detail'],
			'pdf_link' => $item['pdf_link']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('reports', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('reports');
	}
}