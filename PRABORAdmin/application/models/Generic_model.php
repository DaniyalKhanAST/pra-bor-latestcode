<?php

class Generic_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /////////////generic starts//////////////////////////

    function get_all_meta_page()
    {
        $this->db->select('*');
        $this->db->from('meta_page_table');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_any_meta_page()
    {
        $this->db->select('*');
        $this->db->from('meta_page_table');
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_static_page_by_slug($slug)
    {
        $this->db->select('*');
        $this->db->from('static_pages');
        $this->db->where('page_slug', $slug);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function add_item($item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
            'meta_page_id' => $item['meta_page_id']
        );
        $this->db->insert('page_info_table', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_item_image($item)
    {
        $data = array(
            'page_id' => $item['page_id'],
            'image_url' => $item['image_url']
        );
        $this->db->insert('page_images_table', $data);

    }

    function add_item_file($item)
    {
        $data = array(
            'page_id' => $item['page_id'],
            'file_link' => $item['file_link']
        );
        $this->db->insert('page_files_table', $data);

    }

    function get_all_items($meta_page_id)
    {
        $this->db->select('*');
        $this->db->from('page_info_table');
        $this->db->where('meta_page_id', $meta_page_id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_item_files($meta_page_id)
    {
        $this->db->select('*');
        $this->db->from('page_files_table');
        $this->db->where('page_id', $meta_page_id);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_item_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('page_info_table');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_item_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('page_images_table');
        $this->db->where('page_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_item_files_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('page_files_table');
        $this->db->where('page_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_item($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
        );

        $this->db->where('id', $id);
        $this->db->update('page_info_table', $data);
    }

    function update_static_page($slug, $page_detail)
    {
        $this->db->set('page_detail', $page_detail);
        $this->db->where('page_slug', $slug);
        $this->db->update('static_pages');
    }

    function delete_item_image($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('page_images_table');
    }

    function delete_item_file($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('page_files_table');
    }


    function delete_item($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('page_info_table');
    }

    function delete_all_item_files($id)
    {
        $this->db->where('page_id', $id);
        $this->db->delete('page_files_table');
    }

    function delete_all_item_images($id)
    {
        $this->db->where('page_id', $id);
        $this->db->delete('page_images_table');
    }

    function get_meta_page_by_item_id($item_id)
    {
        $this->db->select('*');
        $this->db->from('page_info_table');
        $this->db->join('meta_page_table', 'page_info_table.meta_page_id=meta_page_table.id', 'INNER');
        $this->db->where('page_info_table.id', $item_id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }

    }

    function get_meta_page_by_id($meta_page_id)
    {
        $this->db->select('*');
        $this->db->from('meta_page_table');
        $this->db->where('id', $meta_page_id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }

    }

    function get_meta_page_by_slug($meta_page_slug)
    {
        $this->db->select('*');
        $this->db->from('meta_page_table');
        $this->db->where('slug', $meta_page_slug);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }

    }
    /////////////generic ends///////////////////////////
}