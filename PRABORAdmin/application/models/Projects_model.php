<?php
class Projects_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create_comp($item)
	{
		$data = array(
			'project_cat_id' => $item['project_cat_id'],
			'name_of_scheme' => $item['name_of_scheme'],
			'cost_in_million' => $item['cost_in_million'],
			'from_year' => $item['from_year'],
			'to_year' => $item['to_year'],
			'duration' => $item['duration'],
			'is_completed' => $item['is_completed'],
			'link_type'=>$item['link_type'],
			'link_url'=>$item['link_url']
			 );

		$this->db->insert('projects', $data);
	}

	function create_new($item)
	{
		$data = array(
				'project_cat_id' => $item['project_cat_id'],
				'GS_no' => $item['GS_no'],
				'name_of_scheme' => $item['name_of_scheme'],
				'status' => $item['status'],
				'cost_in_million' => $item['cost_in_million'],
				'from_year' => $item['from_year'],
				'to_year' => $item['to_year'],
				'is_completed' => $item['is_completed'],
				'link_type'=>$item['link_type'],
				'link_url'=>$item['link_url']
		);

		$this->db->insert('projects', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('projects');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('projects');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all_new()
	{
		$this->db->select('*');
		$this->db->from('projects');
		$this->db->where('project_cat_id', 1);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all_ongoing()
	{
		$this->db->select('*');
		$this->db->from('projects');
		$this->db->where('project_cat_id', 2);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all_completed()
	{
		$this->db->select('*');
		$this->db->from('projects');
		$this->db->where('project_cat_id', 3);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update_new($id, $item)
	{
		$data = array(
				'project_cat_id' => $item['project_cat_id'],
				'GS_no' => $item['GS_no'],
				'name_of_scheme' => $item['name_of_scheme'],
				'status' => $item['status'],
				'cost_in_million' => $item['cost_in_million'],
				'from_year' => $item['from_year'],
				'to_year' => $item['to_year'],
				'duration' => NULL,
				'is_completed' => $item['is_completed'],
				'link_type' => $item['link_type']
			 );

		if (isset($item['link_url'])) {
			$this->db->set('link_url', $item['link_url']);
		}
		$this->db->where('id', $id);
		$this->db->update('projects', $data);
	}

	function update_comp($id, $item)
	{
		$data = array(
				'project_cat_id' => $item['project_cat_id'],
				'name_of_scheme' => $item['name_of_scheme'],
				'cost_in_million' => $item['cost_in_million'],
				'from_year' => $item['from_year'],
				'to_year' => $item['to_year'],
				'GS_no' => NULL,
				'status' => NULL,
				'duration' => $item['duration'],
				'is_completed' => $item['is_completed'],
				'link_type' => $item['link_type']
		);
		if (isset($item['link_url'])) {
			$this->db->set('link_url', $item['link_url']);
		}
		$this->db->where('id', $id);
		$this->db->update('projects', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('projects');
	}
}