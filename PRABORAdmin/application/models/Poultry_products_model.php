<?php
class Poultry_products_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'poultry_item_id' => $item['poultry_item_id'],
			'pak_qty' => $item['pak_qty'],
			'punjab_qty' => $item['punjab_qty'],
			'share' => $item['share'],
			'source' => $item['source']
			 ); 

		$this->db->insert('poultry_products', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('poultry_products');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('poultry_products.*,poultry_items.name as item_name');
		$this->db->from('poultry_products');
		$this->db->join("poultry_items", "poultry_products.poultry_item_id = poultry_items.id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'poultry_item_id' => $item['poultry_item_id'],
			'pak_qty' => $item['pak_qty'],
			'punjab_qty' => $item['punjab_qty'],
			'share' => $item['share'],
			'source' => $item['source']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('poultry_products', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('poultry_products');
	}
}