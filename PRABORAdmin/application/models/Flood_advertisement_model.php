<?php
class Flood_advertisement_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'image_url' => $item['image_url']
			 ); 

		$this->db->insert('flood_advertisement', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('flood_advertisement');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('flood_advertisement');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'image_url' => $item['image_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('flood_advertisement', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('flood_advertisement');
	}
}