<?php

class Advertisements_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($item)
    {
        $data = array(
            'name' => $item['advertisement_name'],
            'image_url' => $item['advertisement_image_url']
        );

        $this->db->insert('advertisements', $data);
    }

    function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('advertisements');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('advertisements');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function create_job_posting($item)
    {
        $data = array(
            'title' => $item['title'],
            'adv_date' => $item['adv_date'],
            'close_date' => $item['close_date'],
            'attachment_link' => $item['attachment_link']
        );

        $this->db->insert('job_postings', $data);
    }

    function get_all_job_postings()
    {
        $this->db->select("job_postings.*,IF(job_postings.close_date < NOW() - INTERVAL 1 DAY,'close','open') AS job_status");
        $this->db->from('job_postings');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_job_posting($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'adv_date' => $item['adv_date'],
            'close_date' => $item['close_date'],
            'attachment_link' => $item['attachment_link']
        );

        $this->db->where('id', $id);
        $this->db->update('job_postings', $data);
    }
    function delete_job_posting($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('job_postings');
    }
    
function add_country_medicine($item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
            'file_link' => $item['file_link']
        );
        $this->db->insert('country_medicine', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_country_medicine_image($item)
    {
        $data = array(
            'medicine_id' => $item['medicine_id'],
            'image_url' => $item['image_url']
        );
        $this->db->insert('country_medicine_images', $data);

    }

    function get_all_country_medicine()
    {
        $this->db->select('*');
        $this->db->from('country_medicine');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_medicine_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('country_medicine');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_medicine_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('country_medicine_images');
        $this->db->where('medicine_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_country_medicine($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description']
        );
        if (isset($item['file_link'])) {
            $this->db->set('file_link', $item['file_link']);
        }
        $this->db->where('id', $id);
        $this->db->update('country_medicine', $data);
    }

    function delete_medicine_image($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('country_medicine_images');
    }

    function delete_medicine($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('country_medicine');
    }

    function delete_all_medicine_images($id)
    {
        $this->db->where('medicine_id', $id);
        $this->db->delete('country_medicine_images');
    }
    
    function add_ostrich_article($item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
            'file_link' => $item['file_link']
        );
        $this->db->insert('ostrich_articles', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_ostrich_article_image($item)
    {
        $data = array(
            'article_id' => $item['article_id'],
            'image_url' => $item['image_url']
        );
        $this->db->insert('ostrich_article_images', $data);

    }

    function get_all_ostrich_article()
    {
        $this->db->select('*');
        $this->db->from('ostrich_articles');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_ostrich_article_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('ostrich_articles');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_ostrich_article_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('ostrich_article_images');
        $this->db->where('article_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_ostrich_article($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description']
        );
        if (isset($item['file_link'])) {
            $this->db->set('file_link', $item['file_link']);
        }
        $this->db->where('id', $id);
        $this->db->update('ostrich_articles', $data);
    }

    function delete_ostrich_article_image($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('ostrich_article_images');
    }

    function delete_ostrich_article($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('ostrich_articles');
    }

    function delete_all_ostrich_article_images($id)
    {
        $this->db->where('article_id', $id);
        $this->db->delete('ostrich_article_images');
    }
    
    function update($id, $item)
    {
        $data = array(
            'name' => $item['advertisement_name'],
            'image_url' => $item['advertisement_image_url']
        );

        $this->db->where('id', $id);
        $this->db->update('advertisements', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('advertisements');
    }

    /////////////duck model code starts/////////////////
    function add_duck_article($item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
            'file_link' => $item['file_link']
        );
        $this->db->insert('duck_articles', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_duck_article_image($item)
    {
        $data = array(
            'article_id' => $item['article_id'],
            'image_url' => $item['image_url']
        );
        $this->db->insert('duck_article_images', $data);

    }

    function get_all_duck_article()
    {
        $this->db->select('*');
        $this->db->from('duck_articles');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_duck_article_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('duck_articles');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_duck_article_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('duck_article_images');
        $this->db->where('article_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_duck_article($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description']
        );
        if (isset($item['file_link'])) {
            $this->db->set('file_link', $item['file_link']);
        }
        $this->db->where('id', $id);
        $this->db->update('duck_articles', $data);
    }

    function delete_duck_article_image($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('duck_article_images');
    }

    function delete_duck_article($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('duck_articles');
    }

    function delete_all_duck_article_images($id)
    {
        $this->db->where('article_id', $id);
        $this->db->delete('duck_article_images');
    }
    /////////////duck model code ends/////////////////


    /////////////cock model code starts/////////////////
    function add_cock_article($item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
            'file_link' => $item['file_link']
        );
        $this->db->insert('cock_articles', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_cock_article_image($item)
    {
        $data = array(
            'article_id' => $item['article_id'],
            'image_url' => $item['image_url']
        );
        $this->db->insert('cock_article_images', $data);

    }

    function get_all_cock_article()
    {
        $this->db->select('*');
        $this->db->from('cock_articles');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_cock_article_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('cock_articles');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_cock_article_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('cock_article_images');
        $this->db->where('article_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_cock_article($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description']
        );
        if (isset($item['file_link'])) {
            $this->db->set('file_link', $item['file_link']);
        }
        $this->db->where('id', $id);
        $this->db->update('cock_articles', $data);
    }

    function delete_cock_article_image($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('cock_article_images');
    }

    function delete_cock_article($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('cock_articles');
    }

    function delete_all_cock_article_images($id)
    {
        $this->db->where('article_id', $id);
        $this->db->delete('cock_article_images');
    }
    /////////////cock model code ends/////////////////


    /////////////Training model code starts/////////////////
    function add_training($item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
            'file_link' => $item['file_link']
        );
        $this->db->insert('training', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_training_image($item)
    {
        $data = array(
            'article_id' => $item['article_id'],
            'image_url' => $item['image_url']
        );
        $this->db->insert('training_images', $data);

    }

    function get_all_training()
    {
        $this->db->select('*');
        $this->db->from('training');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_training_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('training');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_training_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('training_images');
        $this->db->where('article_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_training($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description']
        );
        if (isset($item['file_link'])) {
            $this->db->set('file_link', $item['file_link']);
        }
        $this->db->where('id', $id);
        $this->db->update('training', $data);
    }

    function delete_training_image($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('training_images');
    }

    function delete_training($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('training');
    }

    function delete_all_training_images($id)
    {
        $this->db->where('article_id', $id);
        $this->db->delete('training_images');
    }
    /////////////training model code ends/////////////////


    /////////////poultry_farm model code starts/////////////////
    function add_poultry_farm($item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description'],
            'file_link' => $item['file_link']
        );
        $this->db->insert('poultry_farm', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_poultry_farm_image($item)
    {
        $data = array(
            'article_id' => $item['article_id'],
            'image_url' => $item['image_url']
        );
        $this->db->insert('poultry_farm_images', $data);

    }

    function get_all_poultry_farm()
    {
        $this->db->select('*');
        $this->db->from('poultry_farm');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_poultry_farm_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('poultry_farm');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_poultry_farm_images_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('poultry_farm_images');
        $this->db->where('article_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update_poultry_farm($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'description' => $item['description']
        );
        if (isset($item['file_link'])) {
            $this->db->set('file_link', $item['file_link']);
        }
        $this->db->where('id', $id);
        $this->db->update('poultry_farm', $data);
    }

    function delete_poultry_farm_image($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('poultry_farm_images');
    }

    function delete_poultry_farm($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('poultry_farm');
    }

    function delete_all_poultry_farm_images($id)
    {
        $this->db->where('article_id', $id);
        $this->db->delete('poultry_farm_images');
    }
    /////////////poultry_farm model code ends/////////////////
}