<?php
class Export_major_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'species_id' => $item['species_id'],
			'poultry_item_id' => $item['poultry_item_id'],
			'unity' => $item['unity'],
			'qty' => $item['qty'],
			'value' => $item['value'],
			'source' => $item['source']
			 ); 

		$this->db->insert('export_major', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('export_major');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('export_major.*,species_table.name as species_name,poultry_items.name as poultry_item_name');
		$this->db->from('export_major');
		$this->db->join("species_table", "export_major.species_id = species_table.id", "LEFT");
		$this->db->join("poultry_items", "export_major.poultry_item_id = poultry_items.id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'species_id' => $item['species_id'],
			'poultry_item_id' => $item['poultry_item_id'],
			'unity' => $item['unity'],
			'qty' => $item['qty'],
			'value' => $item['value'],
			'source' => $item['source']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('export_major', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('export_major');
	}
}