<?php
class Downloads_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['download_name'],
			'category' => $item['category'],
			'link_url' => $item['download_link_url'],
			'description' => $item['download_description']
			 ); 

		$this->db->insert('downloads', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('downloads');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('downloads');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['download_name'],
			'category' => $item['category'],
			'link_url' => $item['download_link_url'],
			'description' => $item['download_description']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('downloads', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('downloads');
	}
}