<?php
class Event_gallery_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'title' => $item['title'],
			'image_url' => $item['image_url'],
			'description' => $item['description'],
			'directorate_id' => $item['directorate_id'],
			'district_id' => $item['district_id']
			 );
		$this->db->set('created_date', 'NOW()', FALSE);
		$this->db->insert('event_gallery', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('event_gallery');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('event_gallery.*,districts.name as district_name,directorates.name as directorate_name');
		$this->db->from('event_gallery');
		$this->db->join("districts", "event_gallery.district_id = districts.id", "LEFT");
		$this->db->join("directorates", "event_gallery.directorate_id = directorates.id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'title' => $item['title'],
			'image_url' => $item['image_url'],
			'description' => $item['description'],
			'directorate_id' => $item['directorate_id'],
			'district_id' => $item['district_id']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('event_gallery', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('event_gallery');
	}
}