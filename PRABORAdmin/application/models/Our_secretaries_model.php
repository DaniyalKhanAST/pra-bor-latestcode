<?php
class Our_secretaries_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['name'],
			'from_date' => $item['from_date'],
			'to_date' => $item['to_date'],
			'image_url' => $item['image_url'],
			'designation' => $item['designation'],
			'qualification' => $item['qualification'],
			'type' => $item['type']
			 ); 

		$this->db->insert('our_secretaries', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('our_secretaries');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('our_secretaries');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['name'],
			'from_date' => $item['from_date'],
			'to_date' => $item['to_date'],
			'image_url' => $item['image_url'],
			'designation' => $item['designation'],
			'qualification' => $item['qualification'],
			'type' => $item['type']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('our_secretaries', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('our_secretaries');
	}
}