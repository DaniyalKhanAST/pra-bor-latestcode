<?php
class Report_category_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['reportcategory_name']
			 ); 

		$this->db->insert('report_category', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('report_category');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('report_category');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['reportcategory_name']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('report_category', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('report_category');
	}
}