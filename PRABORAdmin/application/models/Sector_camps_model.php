<?php
class Sector_camps_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'district_id' => $item['district_id'],
			'flood_sector' => $item['flood_sector'],
			'flood_sub_sector' => $item['flood_sub_sector'],
			'flood_relief_camp' => $item['flood_relief_camp'],
			'veterinary_officers' => $item['veterinary_officers'],
			'veterinary_assistants' => $item['veterinary_assistants'],
			'mobile_veterinary_dispenceries' => $item['mobile_veterinary_dispenceries']
			 ); 

		$this->db->insert('sector_camps', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('sector_camps');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('sector_camps.*,districts.name as district_name');
		$this->db->from('sector_camps');
		$this->db->join("districts","sector_camps.district_id = districts.id","LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'district_id' => $item['district_id'],
			'flood_sector' => $item['flood_sector'],
			'flood_sub_sector' => $item['flood_sub_sector'],
			'flood_relief_camp' => $item['flood_relief_camp'],
			'veterinary_officers' => $item['veterinary_officers'],
			'veterinary_assistants' => $item['veterinary_assistants'],
			'mobile_veterinary_dispenceries' => $item['mobile_veterinary_dispenceries']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('sector_camps', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('sector_camps');
	}
}