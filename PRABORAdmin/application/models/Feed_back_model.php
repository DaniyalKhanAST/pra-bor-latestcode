<?php
class Feed_back_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'feeback_type_name' => $item['feeback_type_name'],
			'name' => $item['name'],
			'email' => $item['email'],
			'address' => $item['address'],
			'cnic' => $item['cnic'],
			'subject' => $item['subject'],
			'contact_no' => $item['contact_no'],
			'message' => $item['message'],
			'tracking_no' => $item['tracking_no']
			 ); 

		$this->db->insert('feed_back', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('feed_back');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('feed_back');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'feeback_type_name' => $item['feeback_type_name'],
			'name' => $item['name'],
			'email' => $item['email'],
			'address' => $item['address'],
			'cnic' => $item['cnic'],
			'subject' => $item['subject'],
			'contact_no' => $item['contact_no'],
			'message' => $item['message'],
			'tracking_no' => $item['tracking_no']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('feed_back', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('feed_back');
	}
}