<?php
class Videos_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['name'],
			'video_link' => $item['video_link'],
			'description' => $item['description'],
			'image_url' => $item['image_url']
			 );

		$this->db->insert('videos', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('videos');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('videos');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(

			'name' => $item['name'],
			'video_link' => $item['video_link'],
			'description' => $item['description'],
				'image_url' => $item['image_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('videos', $data);
		return true;
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('videos');
	}
}