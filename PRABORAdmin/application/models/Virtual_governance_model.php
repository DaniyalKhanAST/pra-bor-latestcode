<?php
class Virtual_governance_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'text' => $item['virtual_gov_text'],
			'link_url' => $item['virtual_url']
			 ); 

		$this->db->insert('virtual_governance', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('virtual_governance');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('virtual_governance');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'text' => $item['virtual_gov_text'],
			'link_url' => $item['virtual_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('virtual_governance', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('virtual_governance');
	}
}