<?php
class Auctions_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		// dd($item);
		$data = array(
			'directorate_id' => $item['directorate_id'],
			'name' => $item['name'],
			'closing_date' => $item['closing_date'],
			'status' => $item['status'],
			'pdf_link' => $item['pdf_link'],
			'bid_doc' => isset($item['bid_doc'])?$item['bid_doc']:'',
			'type' => $item['type']
			 ); 

		$this->db->insert('auctions', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('auctions');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('auctions.*,directorates.name as directorate_name,auction_type.name as type_name');
		$this->db->from('auctions');
		$this->db->join("directorates", "auctions.directorate_id = directorates.id", "LEFT");
		$this->db->join("auction_type", "auctions.type = auction_type.id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'directorate_id' => $item['directorate_id'],
			'name' => $item['name'],
			'closing_date' => $item['closing_date'],
			'status' => $item['status'],
			'pdf_link' => $item['pdf_link'],
			'bid_doc' => $item['bid_doc'],
			'type' => $item['type']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('auctions', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('auctions');
	}
}