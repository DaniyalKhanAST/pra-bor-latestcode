<?php
class Flood_officers_contacts_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'district_id' => $item['district_id'],
			'name_of_DLO' => $item['name_of_DLO'],
			'contact_no' => $item['contact_no']
			 ); 

		$this->db->insert('flood_officers_contacts', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('flood_officers_contacts');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('flood_officers_contacts.*,districts.name as district_name');
		$this->db->from('flood_officers_contacts');
		$this->db->join("districts", "flood_officers_contacts.district_id = districts.id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'district_id' => $item['district_id'],
			'name_of_DLO' => $item['name_of_DLO'],
			'contact_no' => $item['contact_no']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('flood_officers_contacts', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('flood_officers_contacts');
	}
}