<?php

class Feedback_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_feedback()
	{
		$query = $this->db->get('suggestions');
        return $query->result();
	}
    
}