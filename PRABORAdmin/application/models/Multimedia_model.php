<?php
class Multimedia_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($item)
    {
        $data = array(
            'type' => $item['type'],
            'title' => $item['title'],
            'link_url' => $item['link_url']
        );

        $this->db->insert('multimedia', $data);
    }

    function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('multimedia');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->row();
        }
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('multimedia');
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->result();
        }
    }

    function update($id, $item)
    {
        $data = array(
            'type' => $item['type'],
            'title' => $item['title'],
            'link_url' => $item['link_url']
        );

        $this->db->where('id', $id);
        $this->db->update('multimedia', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('multimedia');
    }
}