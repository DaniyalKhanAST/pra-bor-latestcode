<?php
class Reference_management_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function add($item,$table)
	{
		$data = array(
			'name' => $item['name'],
			 );

		$this->db->insert($table, $data);
	}

	function get_by_id($id,$table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all($table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item,$table)
	{
		$data = array(
			'name' => $item['name']
			 ); 

		$this->db->where('id', $id);
		$this->db->update($table, $data);
	}

	function delete($id,$table)
	{
		$this->db->where('id', $id);
		$this->db->delete($table);
	}
}