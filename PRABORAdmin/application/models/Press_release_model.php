<?php
class Press_release_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['name'],
			'description' => $item['description'],
			'image_link' => $item['image_link']
			 ); 

		$this->db->insert('press_release', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('press_release');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('press_release');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['name'],
			'description' => $item['description'],
			'image_link' => $item['image_link']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('press_release', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('press_release');
	}
}