<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class infodesk extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
    }

    public function index()
    {
        $this->load->model('staff_corner_model');
        $data['transfer_posting'] = $this->staff_corner_model->get_all_transfer_posting();
        $data['all_suspension'] = $this->staff_corner_model->get_all_suspension();
        $data['all_notification'] = $this->staff_corner_model->get_all_notification();
        $data['promotions'] = $this->staff_corner_model->get_all_promotions();
        $data['all_appointments'] = $this->staff_corner_model->get_all_appointments();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'infodesk';
        $data['page_name'] = 'staff_corners';
        $data['page_title'] = 'Info Desk';
        $this->load->view('index', $data);
    }

    public function booklets()
    {
        $this->load->model('supportive_material_model');
        $data['booklets'] = $this->supportive_material_model->get_booklets();
        $data['supplements'] = $this->supportive_material_model->get_supplements();
        $data['posters'] = $this->supportive_material_model->get_posters();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'infodesk';
        $data['page_name'] = 'booklets';
        $data['page_title'] = 'Booklets';
        $this->load->view('index', $data);
    }

    public function publications()
    {
        $this->load->model('publications_model');
        $data['publications'] = $this->publications_model->get_all();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'infodesk';
        $data['page_name'] = 'publications';
        $data['page_title'] = 'Publications';
        $this->load->view('index', $data);
    }
    
    function agreements()
    {
        $this->load->model('publications_model');
        $data['agreements'] = $this->publications_model->get_all_agreements();
        $data['folder_name'] = 'infodesk';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'agreements';
        $data['page_title'] = 'Agreements';
        $this->load->view('index', $data);
    }
    
    public function adp()
    {
        $this->load->model('projects_model');
        $data['new_project'] = $this->projects_model->get_all_new();
        $data['ongoing_project'] = $this->projects_model->get_all_ongoing();
        $data['completed_project'] = $this->projects_model->get_all_completed();
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'projects';
        $data['page_name'] = 'adps';
        $data['page_title'] = 'Annual Development Programs';
        $this->load->view('index', $data);
    }
    public function lamp()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'infodesk';
        $data['page_name'] = 'lamp';
        $data['page_title'] = 'LAMP';
        $this->load->view('index', $data);
    }
    public function ppra()
    {
        $data['header_name'] = 'header_main';
        $data['folder_name'] = 'infodesk';
        $data['page_name'] = 'ppra';
        $data['page_title'] = 'Punjab Procurement Rules';
        $this->load->view('index', $data);
    }

    function important_links()
    {
        $this->load->model('useful_links_model');
        $data['academics'] = $this->useful_links_model->get_academics();
        $data['industrial'] = $this->useful_links_model->get_industrial();
        $data['national'] = $this->useful_links_model->get_national();
        $data['international'] = $this->useful_links_model->get_international();
$data['miscellaneous_articles'] = $this->useful_links_model->get_miscellaneous_articles();
        $data['folder_name'] = 'infodesk';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'important_links';
        $data['page_title'] = 'Important Links';
        $this->load->view('index', $data);
    }

    function press_release()
    {
        $this->load->model('press_release_model');
        $data['press_releases'] = $this->press_release_model->get_all();
        $data['folder_name'] = 'infodesk';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'press_release';
        $data['page_title'] = 'Press Releases';
        $this->load->view('index', $data);
    }

    function download_careers()
    {
        $this->load->model('press_release_model');
        $data['download_careers'] = $this->press_release_model->get_all_download_careers();
        $data['folder_name'] = 'infodesk';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'download_careers';
        $data['page_title'] = 'Downloads';
        $this->load->view('index', $data);
    }

    function tenders()
    {
        $this->load->model('auctions_model');
        $data['auction'] = $this->auctions_model->get_all();
        $data['folder_name'] = 'infodesk';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'tenders';
        $data['page_title'] = 'Tenders';
        $this->load->view('index', $data);
    }

    function job_postings()
    {
        $this->load->model('Advertisements_model');
        $data['jobs'] = $this->Advertisements_model->get_all_job_postings();
        $data['folder_name'] = 'infodesk';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'job_postings';
        $data['page_title'] = 'Job Postings';
        $this->load->view('index', $data);
    }

    function country_medicine()
    {
        $this->load->model('Advertisements_model');
        $data['medicines'] = $this->Advertisements_model->get_all_medicines();
        $data['folder_name'] = 'infodesk';
        $data['header_name'] = 'header_main';
        $data['page_name'] = 'country_medicine';
        $data['page_title'] = 'Country Medicine';
        $this->load->view('index', $data);
    }

    function country_medicine_detail($med_id = null)
    {
        if (isset($med_id) && !empty($med_id)) {
            $this->load->model('Advertisements_model');
            $data['medicine'] = $this->Advertisements_model->get_medicine_by_id($med_id);
            if (isset($data['medicine']) && !empty($data['medicine'])) {
                $data['medicine_images'] = $this->Advertisements_model->get_medicine_images_by_id($med_id);
                $data['folder_name'] = 'infodesk';
                $data['header_name'] = 'header_main';
                $data['page_name'] = 'country_medicine_detail';
                $data['page_title'] = $data['medicine']->title;
                $this->load->view('index', $data);
            } else {
                $this->session->set_flashdata('error', "No Medicine Found");
                redirect('infodesk/country_medicine');
            }
        } else {
            $this->session->set_flashdata('error', "No Medicine Selected");
            redirect('infodesk/country_medicine');

        }
    }

}