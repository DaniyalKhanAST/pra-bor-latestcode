<?php

class Footer_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_footer_page_by_slug($slug)
    {
        $this->db->select('*');
        $this->db->from('footer');
        $this->db->where('page_slug', $slug);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function update_footer_page($page_url, $page_tooltip, $page_detail, $slug)
    {
        $data = array(
            'page_url' => $page_url,
            'page_tooltip' => $page_tooltip,
            'page_detail' => $page_detail
        );
        //$this->db->set('page_detail', $page_detail);
        $this->db->where('page_slug', $slug);
        $this->db->update('footer', $data);
    }

    
}