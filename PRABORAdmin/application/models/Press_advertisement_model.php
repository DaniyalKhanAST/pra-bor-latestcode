<?php
class Press_advertisement_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'title' => $item['title'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->insert('press_advertisement', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('press_advertisement');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('press_advertisement');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'title' => $item['title'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('press_advertisement', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('press_advertisement');
	}
}