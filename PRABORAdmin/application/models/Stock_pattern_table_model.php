<?php
class Stock_pattern_table_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'herd_size' => $item['herd_size'],
			'pak_qty' => $item['pak_qty'],
			'punjab_qty' => $item['punjab_qty'],
			'source' => $item['source']
			 ); 

		$this->db->insert('stock_pattern_table', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('stock_pattern_table');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('stock_pattern_table');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'herd_size' => $item['herd_size'],
			'pak_qty' => $item['pak_qty'],
			'punjab_qty' => $item['punjab_qty'],
			'source' => $item['source']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('stock_pattern_table', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('stock_pattern_table');
	}
}