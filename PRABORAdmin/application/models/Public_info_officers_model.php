<?php
class Public_info_officers_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		// dd($item);
		$data = array(
			
			'category' => $item['category'],
			'name_of_office_facility' => $item['name_of_office_facility'],
			'designation' => $item['designation'],
			'district' => $item['district'],
			'email' => $item['email'],
			'mobile_no' => $item['mobile_no'],
			'phone_no' => $item['phone_no'],
			'latitude' => $item['latitude'],
			'longitude' => $item['longitude']
			);

		$this->db->insert('public_info_officers', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('public_info_officers');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('public_info_officers');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		// dd($item);
		$data = array(
			'category' => $item['category'],
			'name_of_office_facility' => $item['name_of_office_facility'],
			'designation' => $item['designation'],
			'email' => $item['email'],
			'district' => $item['district'],
			'mobile_no' => $item['mobile_no'],
			'phone_no' => $item['phone_no'],
			'latitude' => $item['latitude'],
			'longitude' => $item['longitude']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('public_info_officers', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('public_info_officers');
	}
}