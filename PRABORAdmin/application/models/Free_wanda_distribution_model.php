<?php
class Free_wanda_distribution_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'district_id' => $item['district_id'],
			'total_qty_kg' => $item['total_qty_kg'],
			'date' => $item['date'],
			'transport_number' => $item['transport_number'],
			'station_id' => $item['station_id']
			 ); 

		$this->db->insert('free_wanda_distribution', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('free_wanda_distribution');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('free_wanda_distribution.*,districts.name as district_name,stations.name as station_name');
		$this->db->from('free_wanda_distribution');
		$this->db->join("districts", "free_wanda_distribution.district_id = districts.id", "LEFT");
		$this->db->join("stations", "free_wanda_distribution.station_id = stations.id", "LEFT");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'district_id' => $item['district_id'],
			'total_qty_kg' => $item['total_qty_kg'],
			'date' => $item['date'],
			'transport_number' => $item['transport_number'],
			'station_id' => $item['station_id']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('free_wanda_distribution', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('free_wanda_distribution');
	}
}