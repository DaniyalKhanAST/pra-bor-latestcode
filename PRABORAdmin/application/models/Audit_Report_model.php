<?php
class Audit_Report_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		// dd($item);
		$data = array(
			'title' => $item['title'],
			'year' => $item['year'],
			'pdf_link' => $item['pdf_link']
			 ); 

		$this->db->insert('audit_reports', $data);
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('audit_reports');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'title' => $item['title'],
			'pdf_link' => $item['pdf_link'],
			'year' => $item['year']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('audit_reports', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('audit_reports');
	}
}