<?php
class download_careers_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($item)
    {
        $data = array(
            'name' => $item['name'],
            'description' => $item['description'],
            'image_link' => $item['image_link']
        );

        $this->db->insert('download_careers', $data);
    }

    function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('download_careers');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->row();
        }
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('download_careers');
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->result();
        }
    }

    function update($id, $item)
    {
        $data = array(
            'name' => $item['name'],
            'description' => $item['description'],
            'image_link' => $item['image_link']
        );

        $this->db->where('id', $id);
        $this->db->update('download_careers', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('download_careers');
    }
}