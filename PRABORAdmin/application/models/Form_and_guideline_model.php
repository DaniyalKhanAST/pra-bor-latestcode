<?php
class Form_and_guideline_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['formguideline_name'],
			'pdf_link' => $item['formguideline_pdf_link']
			 ); 

		$this->db->insert('form_and_guideline', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('form_and_guideline');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('form_and_guideline');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['formguideline_name'],
			'pdf_link' => $item['formguideline_pdf_link']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('form_and_guideline', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('form_and_guideline');
	}
}