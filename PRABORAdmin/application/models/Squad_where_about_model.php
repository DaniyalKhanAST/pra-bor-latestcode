<?php
class Squad_where_about_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['squad_where_about_name'],
			'designation' => $item['squad_where_about_designation'],
			'date' => $item['squad_where_about_date'],
			'where_about' => $item['squad_where_about'],
			'remarks' => $item['squad_where_about_remarks']
			 ); 

		$this->db->insert('squad_where_about', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('squad_where_about');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('squad_where_about');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['squad_where_about_name'],
			'designation' => $item['squad_where_about_designation'],
			'date' => $item['squad_where_about_date'],
			'where_about' => $item['squad_where_about'],
			'remarks' => $item['squad_where_about_remarks']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('squad_where_about', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('squad_where_about');
	}
}