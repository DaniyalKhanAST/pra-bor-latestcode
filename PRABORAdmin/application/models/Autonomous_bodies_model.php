<?php
class Autonomous_bodies_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['name'],
			'description' => $item['description'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->insert('autonomous_bodies', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('autonomous_bodies');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('autonomous_bodies');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['name'],
			'description' => $item['description'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('autonomous_bodies', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('autonomous_bodies');
	}
}