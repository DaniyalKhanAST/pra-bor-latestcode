<?php
class Section_statistics_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($item)
    {
        $data = array(
            'title' => $item['name'],
            'image_url' => $item['image_url'],
            'description' => $item['description'],
            'isactive' => $item['status']
        );

        $this->db->insert('section_statistics', $data);
    }

    function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('section_statistics');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->row();
        }
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('section_statistics');
        $query = $this->db->get();

        if($query->num_rows()<1){
            return null;
        }
        else{
            return $query->result();
        }
    }

    function update($id, $item)
    {
        $data = array(
            'title' => $item['title'],
            'image_url' => $item['image_url'],
            'description' => $item['description'],
            'isactive' => $item['isactive']
        );

        $this->db->where('id', $id);
        $this->db->update('section_statistics', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('section_statistics');
    }
}