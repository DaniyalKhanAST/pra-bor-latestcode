<?php
class Useful_links_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'category' => $item['useful_category'],
			'name' => $item['useful_link_name'],
			'link_url' => $item['useful_link_url']
			 ); 

		$this->db->insert('useful_links', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('useful_links');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('useful_links');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'category' => $item['useful_category'],
			'name' => $item['useful_link_name'],
			'link_url' => $item['useful_link_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('useful_links', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('useful_links');
	}
}