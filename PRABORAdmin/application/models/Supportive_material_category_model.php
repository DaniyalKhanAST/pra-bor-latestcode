<?php
class Supportive_material_category_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['name']
			 ); 

		$this->db->insert('supportive_material_category', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('supportive_material_category');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('supportive_material_category');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['name']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('supportive_material_category', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('supportive_material_category');
	}
}