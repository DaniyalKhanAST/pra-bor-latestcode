<?php
class Flood_activity_visits_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'date' => $item['date'],
			'name_of_officer' => $item['name_of_officer'],
			'designation' => $item['designation'],
			'place_of_supervision' => $item['place_of_supervision'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->insert('flood_activity_visits', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('flood_activity_visits');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('flood_activity_visits');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
				'date' => $item['visit_date'],
			'name_of_officer' => $item['name_of_officer'],
			'designation' => $item['designation'],
			'place_of_supervision' => $item['place_of_supervision'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('flood_activity_visits', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('flood_activity_visits');
	}
}