<?php
class Procurement_Plans_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		// dd($item);
		$data = array(
			'year'=>$item['year'],
			'description'=>isset($item['description'])?$item['description']:'',
			'pdf_link' => $item['pdf_link']
			 ); 

		$this->db->insert('procurement_plans', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('procurement_plans');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('procurement_plans');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'year' => $item['year'],
			'description'=>isset($item['description'])?$item['description']:'',
			'pdf_link' => $item['pdf_link']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('procurement_plans', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('procurement_plans');
	}
}