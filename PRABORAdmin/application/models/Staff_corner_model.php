<?php
class Staff_corner_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
				'name_of_officer' => $item['name_of_officer'],
				'transfer_from' => $item['transfer_from'],
				'designation_from' => $item['designation_from'],
				'transfer_to' => $item['transfer_to'],
				'designation_to' => $item['designation_to'],
				'date' => $item['date'],
				'status_link' => $item['status_link'],
				'scale' => $item['scale'],
				'suspension_status' => $item['suspension_status'],
				'suspension_document_url' => $item['suspension_document_url'],
				'sus_order_date' => $item['sus_order_date'],
				'notification_date' => $item['notification_date'],
				'instructions_texts' => $item['instructions_texts'],
				'directorate_id' => $item['directorate_id'],
				'staff_type_id' => $item['staff_type_id']
		);

		$this->db->insert('staff_corner', $data);
	}
	function create_new($item)
	{
		$data = array(
				'name_of_officer' => $item['name_of_officer'],
				'transfer_from' => $item['transfer_from'],
				'designation_from' => $item['designation_from'],
				'transfer_to' => $item['transfer_to'],
				'designation_to' => $item['designation_to'],
				'date' => $item['date'],
				'status_link' => $item['status_link'],
				'staff_type_id' => $item['staff_type_id'],
				'scale' => $item['scale'],
				'order_no' => $item['order_no'],
				'instructions_texts' => $item['instructions_texts'],
				'notification_no' => $item['notification_no'],
				'directorate_id' => $item['directorate_id']
		);

		$this->db->insert('staff_corner', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('staff_corner');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
	function get_all_transfer_posting()
	{
		$this->db->select('*');
		$this->db->from('staff_corner');
		$this->db->where('staff_type_id', "1");
		$this->db->order_by("id", "desc");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
	function get_all_suspension()
	{
		$this->db->select('*');
		$this->db->from('staff_corner');
		$this->db->where('staff_type_id', "2");
		$this->db->order_by("id", "desc");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
	function get_all_notification()
	{
		$this->db->select('*');
		$this->db->from('staff_corner');
		$this->db->where('staff_type_id', "3");
		$this->db->order_by("id", "desc");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
	function get_all_promotions()
	{
		$this->db->select('*');
		$this->db->from('staff_corner');
		$this->db->where('staff_type_id', "4");
		$this->db->order_by("id", "desc");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
	function get_all_appointments()
	{
		$this->db->select('staff_corner.*,directorates.name as directorate_name');
		$this->db->from('staff_corner');
		$this->db->join("directorates","staff_corner.directorate_id=directorates.id","left");
		$this->db->where('staff_type_id', "5");
		$this->db->order_by("id", "desc");
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('staff_corner');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
				'name_of_officer' => $item['name_of_officer'],
				'transfer_from' => $item['transfer_from'],
				'designation_from' => $item['designation_from'],
				'transfer_to' => $item['transfer_to'],
				'designation_to' => $item['designation_to'],
				'date' => $item['date'],
				'status_link' => $item['status_link'],
				'scale' => $item['scale'],
				'suspension_status' => $item['suspension_status'],
				'suspension_document_url' => $item['suspension_document_url'],
				'sus_order_date' => $item['sus_order_date'],
				'notification_date' => $item['notification_date'],
				'instructions_texts' => $item['instructions_texts'],
				'directorate_id' => $item['directorate_id'],
				'staff_type_id' => $item['staff_type_id']
		);

		$this->db->where('id', $id);
		$this->db->update('staff_corner', $data);
	}
	function update_new($id,$item)
	{

		$data = array(
				'name_of_officer' => $item['name_of_officer'],
				'transfer_from' => $item['transfer_from'],
				'designation_from' => $item['designation_from'],
				'transfer_to' => $item['transfer_to'],
				'designation_to' => $item['designation_to'],
				'date' => $item['date'],
				'status_link' => $item['status_link'],
				'staff_type_id' => $item['staff_type_id'],
				'scale' => $item['scale'],
				'order_no' => $item['order_no'],
				'instructions_texts' => $item['instructions_texts'],
				'notification_no' => $item['notification_no'],
				'directorate_id' => $item['directorate_id']
		);
		$this->db->where('id', $id);
		$this->db->update('staff_corner', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('staff_corner');
	}
}