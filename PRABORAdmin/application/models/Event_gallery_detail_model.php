<?php
class Event_gallery_detail_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'gallery_id' => $item['gallery_id'],
			'image_url' => $item['image_url'],
			'image_desc' => ($item['imageCaption'])?$item['imageCaption']:NULL
			 ); 

		$this->db->insert('event_gallery_detail', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('event_gallery_detail');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_by_event_id($id)
	{
		$this->db->select('*');
		$this->db->from('event_gallery_detail');
		$this->db->where('gallery_id', $id);
		$query = $this->db->get();
		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('event_gallery_detail');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'gallery_id' => $item['gallery_id'],
			'image_url' => $item['image_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('event_gallery_detail', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('event_gallery_detail');
	}

	function delete_event_images($id)
	{
		$this->db->where('gallery_id', $id);
		$this->db->delete('event_gallery_detail');
	}

	function delete_event_images_by_ids($ids)
	{
		if (!empty($ids)) {
			$this->db->where_in('id', $ids);
			$this->db->delete('event_gallery_detail');
		}
	}
}