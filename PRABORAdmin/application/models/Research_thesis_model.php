<?php
class Research_thesis_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'dissertation_name' => $item['dissertation_name'],
			'dissertation__by' => $item['dissertation__by'],
			'current_place_of_posting' => $item['current_place_of_posting']
			 ); 

		$this->db->insert('research_thesis', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('research_thesis');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('research_thesis');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'dissertation_name' => $item['dissertation_name'],
			'dissertation__by' => $item['dissertation__by'],
			'current_place_of_posting' => $item['current_place_of_posting']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('research_thesis', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('research_thesis');
	}
}