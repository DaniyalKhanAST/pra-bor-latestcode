<?php
class Poultry_items_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'name' => $item['poultry_name'],
			'qty' => $item['poultry_qty']
			 ); 

		$this->db->insert('poultry_items', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('poultry_items');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('poultry_items');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name' => $item['poultry_name'],
			'qty' => $item['poultry_qty']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('poultry_items', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('poultry_items');
	}
}