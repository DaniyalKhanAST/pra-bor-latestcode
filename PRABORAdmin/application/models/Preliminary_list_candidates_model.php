<?php
class Preliminary_list_candidates_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		// dd($item);
		$data = array(
			'district'=>$item['district'],
			'description'=>isset($item['description'])?$item['description']:'',
			'pdf_link' => $item['pdf_link']
			 ); 

		$this->db->insert('preliminary_list_candidates', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('preliminary_list_candidates');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('preliminary_list_candidates');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'district'=>$item['district'],
			'description'=>isset($item['description'])?$item['description']:'',
			'pdf_link' => $item['pdf_link']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('preliminary_list_candidates', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('preliminary_list_candidates');
	}
}