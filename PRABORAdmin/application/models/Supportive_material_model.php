<?php
class Supportive_material_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'sup_material_cat_id' => $item['sup_material_cat_id'],
			'description' => $item['description'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->insert('supportive_material', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('supportive_material');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('supportive_material');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'sup_material_cat_id' => $item['sup_material_cat_id'],
			'description' => $item['description'],
			'link_url' => $item['link_url']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('supportive_material', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('supportive_material');
	}
}