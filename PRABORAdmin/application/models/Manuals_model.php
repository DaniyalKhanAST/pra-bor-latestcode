<?php
class Manuals_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		// dd($item);
		$data = array(
			'name'=>$item['name'],
			'pdf_file_link' => $item['pdf_link']
			 ); 

		$this->db->insert('manuals', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('manuals');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('manuals');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'name'=>$item['name'],
			'pdf_file_link' => $item['pdf_link']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('manuals', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('manuals');
	}
}