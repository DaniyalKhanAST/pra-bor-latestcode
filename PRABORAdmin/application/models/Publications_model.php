<?php
class Publications_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'title' => $item['title'],
			'author_publisher' => $item['author_publisher'],
			'year' => $item['year'],
			'version_no' => $item['version_no']
			 ); 

		$this->db->insert('publications', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('publications');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('publications');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'title' => $item['title'],
			'author_publisher' => $item['author_publisher'],
			'year' => $item['year'],
			'version_no' => $item['version_no']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('publications', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('publications');
	}
}